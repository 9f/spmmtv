# This file provides a function that enables extra compilation and link options
# for a given CMake target.  The purpose of the enabled options is to
# facilitate the development of C++ programs.  The function supports compilers
# Clang and GCC.  The options are only enabled if requested using CMake option
# ‘SPMMTV_ENABLE_DEVELOPMENT_OPTIONS’.

option(SPMMTV_ENABLE_DEVELOPMENT_OPTIONS "Enable extra compile and link options that assist development." OFF)

function(enable_development_options target)
  if(SPMMTV_ENABLE_DEVELOPMENT_OPTIONS)
    if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
      target_compile_options("${target}" PRIVATE
        -Wall
        -Wconversion
        -Wpedantic
        -Wsign-compare
        $<$<CONFIG:Debug>:-Og>
        $<$<CONFIG:RelWithDebInfo>:-fno-omit-frame-pointer>
        $<$<STREQUAL:"${CMAKE_GENERATOR}","Ninja">:-fcolor-diagnostics>
        )

      target_link_options("${target}" PRIVATE
        $<$<CONFIG:Debug>:-Og>
        $<$<CONFIG:RelWithDebInfo>:-fno-omit-frame-pointer>
        $<$<STREQUAL:"${CMAKE_GENERATOR}","Ninja">:-fcolor-diagnostics>
        )

    elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      target_compile_options("${target}" PRIVATE
        -Wall
        -Wconversion
        -Wpedantic
        -Wsign-conversion
        $<$<CONFIG:Debug>:-Og>
        $<$<CONFIG:RelWithDebInfo>:-fno-omit-frame-pointer>
        $<$<STREQUAL:"${CMAKE_GENERATOR}","Ninja">:-fdiagnostics-color=always>
        )

      target_link_options("${target}" PRIVATE
        $<$<CONFIG:Debug>:-Og>
        $<$<CONFIG:RelWithDebInfo>:-fno-omit-frame-pointer>
        $<$<STREQUAL:"${CMAKE_GENERATOR}","Ninja">:-fdiagnostics-color=always>
        )

    endif()
  endif()
endfunction()
