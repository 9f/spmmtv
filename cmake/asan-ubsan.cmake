# This is a CMake toolchain file for enabling Address Sanitiser (ASan) and
# Undefined Behaviour Sanitiser (UBSan) for programming languages C and C++.

set(CMAKE_C_FLAGS_INIT "-fsanitize=address,undefined")
set(CMAKE_CXX_FLAGS_INIT "-fsanitize=address,undefined")
set(CMAKE_EXE_LINKER_FLAGS_INIT "-fsanitize=address,undefined")
