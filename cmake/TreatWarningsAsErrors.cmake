# This file provides a function that enables treating compilation warnings as
# errors for a given CMake target.  The function supports the C and C++
# programming languages when using compilers Clang or GCC.  Compilation
# settings are only modified if requested using CMake option
# ‘SPMMTV_TREAT_WARNINGS_AS_ERRORS’.

option(SPMMTV_TREAT_WARNINGS_AS_ERRORS "Treat warnings as errors." OFF)

function(treat_warnings_as_errors target)
  if(SPMMTV_TREAT_WARNINGS_AS_ERRORS)
    if(CMAKE_C_COMPILER_ID STREQUAL "Clang" OR CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
      target_compile_options("${target}" PRIVATE -Werror)

    elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU" OR CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      target_compile_options("${target}" PRIVATE -Werror)

    endif()
  endif()
endfunction()
