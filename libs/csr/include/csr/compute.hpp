#pragma once

#include <span>                 // std::span

namespace csr {

enum struct Symmetric;
template<typename T, Symmetric Symm> class Matrix;

}  // namespace csr

namespace csr::compute {

// Perform an SpMMᵀV operation with a CSR matrix sequentially.  Parameters
// ‘matrix’ and ‘x1’ are respectively the input matrix and vector.  Parameters
// ‘y1’ and ‘y2’ are the resulting output vectors.
template<typename T, Symmetric Symm>
auto spmmtv_sequential(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2
    ) -> void;

// Perform an SpMMᵀV operation with a CSR matrix sequentially.  Parameter
// ‘matrix’ is the input matrix.  Parameters ‘x1’ and ‘x2’ are input vectors.
// Parameters ‘y1’ and ‘y2’ are the resulting output vectors.
template<typename T, Symmetric Symm>
auto spmmtv_sequential(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T> y1,
        std::span<T> y2
    ) -> void;

}  // namespace csr::compute
