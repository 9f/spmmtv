#pragma once

#include <cstdint>              // std::uint32_t

namespace csr::info {

using Index = std::uint32_t;

}  // namespace csr::info
