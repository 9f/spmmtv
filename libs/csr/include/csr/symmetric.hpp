#pragma once

namespace csr {

// Indicate the symmetry of a CSR matrix.
enum struct Symmetric {
    no,
    yes,
};

}  // namespace csr
