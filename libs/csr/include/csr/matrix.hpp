#pragma once

#include <cstddef>              // std::size_t
#include <filesystem>           // std::filesystem
#include <vector>               // std::vector

#include <csr/info.hpp>         // csr::info

namespace csr {

enum struct Symmetric;

template<typename T, Symmetric Symm>
class Matrix {
public:
    using Index = info::Index;
    using Value = T;

    static constexpr auto symmetric = Symm;

    // The matrix’s CSR row pointer array.
    std::vector<Index> row_pointers{0};

    // The matrix’s CSR column index array.
    std::vector<Index> columns{};

    // The matrix’s CSR element array.
    std::vector<T> elements{};

    // Load a CSR matrix from the HDF5 file specified by ‘file_path’.  Throw
    // ‘csr::RuntimeError’ if loading the matrix fails.
    static auto load_from_hdf5(std::filesystem::path const& file_path) -> Matrix;

    // Load a CSR matrix from the Matrix Market file specified by ‘file_path’.
    // Throw ‘csr::RuntimeError’ if loading the matrix fails.
    static auto load_from_mm(std::filesystem::path const& file_path) -> Matrix;

    Matrix() = default;

    // Instantiate a CSR matrix with row count, column count and element count
    // respectively specified by parameters ‘row_count’, ‘column_count’ and
    // ‘element_count’.  Throw ‘csr::LogicError’ if any of the passed arguments
    // are invalid.
    Matrix(std::size_t row_count, std::size_t column_count, std::size_t element_count);

    // Instantiate a CSR matrix with the row pointer array specified by
    // ‘row_pointers’, column index array specified by ‘columns’, element array
    // specified by ‘elements’ and column count specified by ‘column_count’.
    // Throw ‘csr::LogicError’ if any of the passed arguments are invalid.
    Matrix(std::vector<Index>&& row_pointers,
           std::vector<Index>&& columns,
           std::vector<T>&& elements,
           std::size_t column_count);

    auto row_count() const noexcept -> std::size_t;
    auto column_count() const noexcept -> std::size_t;
    auto element_count() const noexcept -> std::size_t;

    auto is_valid() const -> bool;

private:
    std::size_t column_count_{};
};

}  // namespace csr
