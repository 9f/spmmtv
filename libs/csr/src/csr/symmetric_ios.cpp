#include <csr/symmetric_ios.hpp>

#include <ostream>              // std::ostream

#include <csr/logic_error.hpp>  // csr::LogicError
#include <csr/symmetric.hpp>    // csr::Symmetric

namespace csr {

auto operator<<(std::ostream& os, Symmetric const symmetric) -> std::ostream&
{
    switch (symmetric) {
    case Symmetric::no:
        return os << "no";
    case Symmetric::yes:
        return os << "yes";
    }

    throw LogicError{"Unexpected enumeration value."};
}

}  // namespace csr
