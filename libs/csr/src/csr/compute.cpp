#include <csr/compute.hpp>

#include <algorithm>            // std::copy
#include <array>                // std::array
#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <span>                 // std::span

#include <csr/cstddef.hpp>      // csr::operator""_uz
#include <csr/info.hpp>         // csr::info
#include <csr/matrix.hpp>       // csr::Matrix
#include <csr/symmetric.hpp>    // csr::Symmetric

namespace csr::compute {

namespace {

// Verify that the passed pointer arguments don’t alias.  The function returns
// ‘true’ if no pair of arguments alias, or ‘false’ otherwise.  Arguments are
// treated as ‘void’ pointers.
template<typename... Args>
auto do_not_alias(Args... args) noexcept -> bool;

// Perform SpMMᵀV with a CSR matrix.
template<typename T>
auto spmmtv_csr(
        info::Index const* row_pointers,
        info::Index const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T* y1,
        T* y2
    ) -> void;

// Perform SpMMᵀV with a CSR matrix.
template<typename T>
auto spmmtv_csr(
        info::Index const* row_pointers,
        info::Index const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T const* x2,
        T* y1,
        T* y2
    ) -> void;

// Perform SpMMᵀV with a symmetric CSR matrix.
template<typename T>
auto spmmtv_csr_symmetric(
        info::Index const* row_pointers,
        info::Index const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T* y1,
        T* y2
    ) -> void;

// Perform SpMMᵀV with a symmetric CSR matrix.
template<typename T>
auto spmmtv_csr_symmetric(
        info::Index const* row_pointers,
        info::Index const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T const* x2,
        T* y1,
        T* y2
    ) -> void;

template<typename T>
auto spmmtv_sequential_impl(
        Matrix<T, Symmetric::no> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2
    ) -> void;

template<typename T>
auto spmmtv_sequential_impl(
        Matrix<T, Symmetric::yes> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2
    ) -> void;

template<typename T>
auto spmmtv_sequential_impl(
        Matrix<T, Symmetric::no> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T> y1,
        std::span<T> y2
    ) -> void;

template<typename T>
auto spmmtv_sequential_impl(
        Matrix<T, Symmetric::yes> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T> y1,
        std::span<T> y2
    ) -> void;

}  // namespace

template<typename T, Symmetric Symm>
auto spmmtv_sequential(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2
    ) -> void
{
    spmmtv_sequential_impl(matrix, x1, y1, y2);
}

template auto spmmtv_sequential(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2
    ) -> void;

template auto spmmtv_sequential(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2
    ) -> void;

template auto spmmtv_sequential(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2
    ) -> void;

template auto spmmtv_sequential(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_sequential(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T> const y1,
        std::span<T> const y2
    ) -> void
{
    spmmtv_sequential_impl(matrix, x1, x2, y1, y2);
}

template auto spmmtv_sequential(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2
    ) -> void;

template auto spmmtv_sequential(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2
    ) -> void;

template auto spmmtv_sequential(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2
    ) -> void;

template auto spmmtv_sequential(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2
    ) -> void;

namespace {

template<typename... Args>
auto do_not_alias(Args... args) noexcept -> bool
{
    auto const ps = std::array<void const*, sizeof...(args)>{args...};
    for (auto i = 0_uz; i < ps.size() - 1; ++i) {
        for (auto j = i + 1; j < ps.size(); ++j) {
            if (ps[i] == ps[j]) {
                return false;
            }
        }
    }
    return true;
}

template<typename T>
auto spmmtv_csr(
        info::Index const* __restrict const row_pointers,
        info::Index const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        T* __restrict const y2
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, y1, y2));

    for (auto row = 0_uz; row < row_count; ++row) {
        auto const row_begin = row_pointers[row];
        auto const row_end = row_pointers[row + 1];
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto column = columns[i];
        auto y1_element = elements[i] * x1[column];
        y2[column] += elements[i] * x1[row];

        for (++i; i < row_end; ++i) {
            column = columns[i];
            y1_element += elements[i] * x1[column];
            y2[column] += elements[i] * x1[row];
        }

        y1[row] = y1_element;
    }
}

template<typename T>
auto spmmtv_csr(
        info::Index const* __restrict const row_pointers,
        info::Index const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T const* __restrict const x2,
        T* __restrict const y1,
        T* __restrict const y2
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, x2, y1, y2));

    for (auto row = 0_uz; row < row_count; ++row) {
        auto const row_begin = row_pointers[row];
        auto const row_end = row_pointers[row + 1];
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto column = columns[i];
        auto y1_element = elements[i] * x1[column];
        y2[column] += elements[i] * x2[row];

        for (++i; i < row_end; ++i) {
            column = columns[i];
            y1_element += elements[i] * x1[column];
            y2[column] += elements[i] * x2[row];
        }

        y1[row] = y1_element;
    }
}

template<typename T>
auto spmmtv_csr_symmetric(
        info::Index const* __restrict const row_pointers,
        info::Index const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        T* __restrict const y2
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, y1, y2));

    for (auto row = 0_uz; row < row_count; ++row) {
        auto const row_begin = row_pointers[row];
        auto const row_end = row_pointers[row + 1];
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto column = columns[i];
        auto y1_element = elements[i] * x1[column];
        if (row != column) {
            y1[column] += elements[i] * x1[row];
        }

        for (++i; i < row_end; ++i) {
            column = columns[i];
            y1_element += elements[i] * x1[column];
            y1[column] += elements[i] * x1[row];
        }

        y1[row] += y1_element;
    }

    std::copy(y1, y1 + row_count, y2);
}

template<typename T>
auto spmmtv_csr_symmetric(
        info::Index const* __restrict const row_pointers,
        info::Index const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T const* __restrict const x2,
        T* __restrict const y1,
        T* __restrict const y2
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, x2, y1, y2));

    for (auto row = 0_uz; row < row_count; ++row) {
        auto const row_begin = row_pointers[row];
        auto const row_end = row_pointers[row + 1];
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto column = columns[i];
        auto y1_element = elements[i] * x1[column];
        auto y2_element = elements[i] * x2[column];
        if (row != column) {
            y1[column] += elements[i] * x1[row];
            y2[column] += elements[i] * x2[row];
        }

        for (++i; i < row_end; ++i) {
            column = columns[i];
            y1_element += elements[i] * x1[column];
            y2_element += elements[i] * x2[column];
            y1[column] += elements[i] * x1[row];
            y2[column] += elements[i] * x2[row];
        }

        y1[row] += y1_element;
        y2[row] += y2_element;
    }
}

template<typename T>
auto spmmtv_sequential_impl(
        Matrix<T, Symmetric::no> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2
    ) -> void
{
    assert(matrix.column_count() == matrix.row_count());
    assert(matrix.column_count() == x1.size());
    assert(matrix.column_count() == y1.size());
    assert(matrix.column_count() == y2.size());

    spmmtv_csr(
            matrix.row_pointers.data(),
            matrix.columns.data(),
            matrix.elements.data(),
            matrix.row_count(),
            x1.data(),
            y1.data(),
            y2.data()
        );
}

template<typename T>
auto spmmtv_sequential_impl(
        Matrix<T, Symmetric::yes> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2
    ) -> void
{
    assert(matrix.column_count() == matrix.row_count());
    assert(matrix.column_count() == x1.size());
    assert(matrix.column_count() == y1.size());
    assert(matrix.column_count() == y2.size());

    spmmtv_csr_symmetric(
            matrix.row_pointers.data(),
            matrix.columns.data(),
            matrix.elements.data(),
            matrix.row_count(),
            x1.data(),
            y1.data(),
            y2.data()
        );
}

template<typename T>
auto spmmtv_sequential_impl(
        Matrix<T, Symmetric::no> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T> const y1,
        std::span<T> const y2
    ) -> void
{
    assert(matrix.column_count() == x1.size());
    assert(matrix.row_count() == y1.size());
    assert(matrix.row_count() == x2.size());
    assert(matrix.column_count() == y2.size());

    spmmtv_csr(
            matrix.row_pointers.data(),
            matrix.columns.data(),
            matrix.elements.data(),
            matrix.row_count(),
            x1.data(),
            x2.data(),
            y1.data(),
            y2.data()
        );
}

template<typename T>
auto spmmtv_sequential_impl(
        Matrix<T, Symmetric::yes> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T> const y1,
        std::span<T> const y2
    ) -> void
{
    assert(matrix.column_count() == matrix.row_count());
    assert(matrix.column_count() == x1.size());
    assert(matrix.column_count() == x2.size());
    assert(matrix.column_count() == y1.size());
    assert(matrix.column_count() == y2.size());

    spmmtv_csr_symmetric(
            matrix.row_pointers.data(),
            matrix.columns.data(),
            matrix.elements.data(),
            matrix.row_count(),
            x1.data(),
            x2.data(),
            y1.data(),
            y2.data()
        );
}

}  // namespace

}  // namespace csr::compute
