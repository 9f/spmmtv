#include <csr/matrix_ios.hpp>

#include <ostream>              // std::ostream
#include <string_view>          // std::string_view

#include <csr/matrix.hpp>       // csr::Matrix
#include <csr/symmetric.hpp>    // csr::Symmetric
#include <csr/symmetric_ios.hpp>  // csr::operator<<

namespace csr {

namespace {

// Return a string representation of template argument ‘T’.  The supported
// template arguments are ‘float’ and ‘double’.
template<typename T>
constexpr auto get_type_name() noexcept -> std::string_view;

template<> constexpr auto get_type_name<float>() noexcept -> std::string_view;
template<> constexpr auto get_type_name<double>() noexcept -> std::string_view;

}  // namespace

template<typename T, Symmetric Symm>
auto operator<<(std::ostream& os, Matrix<T, Symm> const& matrix) -> std::ostream&
{
    os << "element type: " << get_type_name<T>() << '\n'
       << "element count: " << matrix.element_count() << '\n'
       << "dimensions: " << matrix.row_count() << " × " << matrix.column_count() << '\n'
       << "symmetric: " << Symm << '\n';
    return os;
}

template auto operator<<(std::ostream& os, Matrix<float, Symmetric::no> const& matrix) -> std::ostream&;
template auto operator<<(std::ostream& os, Matrix<float, Symmetric::yes> const& matrix) -> std::ostream&;
template auto operator<<(std::ostream& os, Matrix<double, Symmetric::no> const& matrix) -> std::ostream&;
template auto operator<<(std::ostream& os, Matrix<double, Symmetric::yes> const& matrix) -> std::ostream&;

namespace {

template<>
constexpr auto get_type_name<float>() noexcept -> std::string_view
{
    return "float";
}

template<>
constexpr auto get_type_name<double>() noexcept -> std::string_view
{
    return "double";
}

}  // namespace

}  // namespace csr
