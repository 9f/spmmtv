#pragma once

#include <cstddef>              // std::size_t
#include <span>                 // std::span
#include <vector>               // std::vector

#include <hsf/exec_mode.hpp>    // hsf::ExecMode
#include <hsf/priv/ndebug.hpp>  // hsf::ndebug
#include <hsf/priv/omp.hpp>     // hsf::omp
#include <hsf/priv/per_thread_vectors.hpp>  // hsf::PerThreadVectors
#include <hsf/priv/region_indices_dll.hpp>  // hsf::RegionIndicesDll hsf::RegionIndicesDllIt
#include <hsf/priv/region_indices_sll.hpp>  // hsf::RegionIndicesSll
#include <hsf/priv/regions_data.hpp>  // hsf::RegionsData
#include <hsf/tags.hpp>         // hsf::tags

namespace hsf {

enum struct Symmetric;
template<typename T, Symmetric Symm> class Matrix;

// Execution data for a given SpMMᵀV execution mode of a hierarchical matrix.
template<ExecMode EM, typename T, Symmetric Symm> class ExecData;

template<typename T, Symmetric Symm>
class ExecData<ExecMode::atomic_exclusive, T, Symm> {
public:
    ExecData() = default;

    // Instantiate an ‘ExecData’ object for a given thread count and
    // hierarchical matrix.
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::OneVector);

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept -> std::size_t;

    // Return the specified thread’s region indices.  Specifying an
    // out-of-bounds thread ID is undefined behaviour.
    auto region_indices(int thread_id) -> std::span<std::size_t>;

    // Return for how many threads this object contains execution data.
    auto thread_count() const noexcept -> int;

private:
    // How many regions this object contains execution data for.
    std::size_t region_count_{};

    // The list of region indices for each thread.
    std::vector<std::vector<std::size_t>> region_indices_threads_{};
};

template<typename T, Symmetric Symm>
class ExecData<ExecMode::atomic_planned_shared, T, Symm> {
public:
    ExecData() = default;

    // Instantiate an ‘ExecData’ object for a given thread count and
    // hierarchical matrix.
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::OneVector);

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept -> std::size_t;

    // Return the specified thread’s region indices.  Specifying an
    // out-of-bounds thread ID is undefined behaviour.
    auto region_indices(int thread_id) -> std::span<std::size_t>;

    // Return for how many threads this object contains execution data.
    auto thread_count() const noexcept -> int;

private:
    // How many regions this object contains execution data for.
    std::size_t region_count_{};

    // The list of region indices for each thread.
    std::vector<std::vector<std::size_t>> region_indices_threads_{};
};

template<typename T, Symmetric Symm>
auto swap(ExecData<ExecMode::atomic_shared, T, Symm>& lhs, ExecData<ExecMode::atomic_shared, T, Symm>& rhs) noexcept
    -> void;

template<typename T, Symmetric Symm>
class ExecData<ExecMode::atomic_shared, T, Symm> {
public:
    ExecData() = default;

    // Instantiate an ‘ExecData’ object for a given thread count and
    // hierarchical matrix.
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::OneVector);

    ExecData(ExecData<ExecMode::atomic_shared, T, Symm>&) = delete;
    ExecData(ExecData<ExecMode::atomic_shared, T, Symm>&& other) noexcept;

    auto operator=(ExecData<ExecMode::atomic_shared, T, Symm>&) = delete;
    auto operator=(ExecData<ExecMode::atomic_shared, T, Symm>&& other) noexcept
        -> ExecData<ExecMode::atomic_shared, T, Symm>&;

    // Return an iterator to the first region index in the ‘RegionIndicesDll’
    // object for the specified thread ID.  Specifying an out-of-bounds thread
    // ID is undefined behaviour.
    auto get_iterator(int thread_id) -> RegionIndicesDllIt;

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept(ndebug) -> std::size_t;

    // Reset the internal ‘RegionIndicesDll’ object and the unfinished region
    // count to their initial state.
    auto reset_status() -> void;

    // Return for how many threads this object contains execution data.
    auto thread_count() const noexcept -> int;

    // Lock this object, update the passed iterator to point to the next
    // unfinished region index, mark the corresponding region as done and
    // unlock this object.  Return ‘true’ if the iterator has been updated
    // successfully and return ‘false’ if no unfinished regions are available.
    auto update_iterator(RegionIndicesDllIt& it) -> bool;

    friend auto swap<T, Symm>(
            ExecData<ExecMode::atomic_shared, T, Symm>& lhs, ExecData<ExecMode::atomic_shared, T, Symm>& rhs
        ) noexcept -> void;

private:
    // The lock for updating the list of region indices.
    mutable omp::Mutex lock_{};

    // The list of region indices shared by all threads.
    RegionIndicesDll region_indices_{};

    // A copy of the initial state of the shared list of region indices.
    RegionIndicesDll region_indices_copy_{};

    // An iterator to the current region index of each thread.
    std::vector<RegionIndicesDllIt> region_indices_its_{};

    // The finished status of each region.
    std::vector<bool> regions_finished_{};

    // How many regions are unfinished.
    std::vector<bool>::size_type unfinished_region_count_{};
};

template<typename T, Symmetric Symm>
class ExecData<ExecMode::locks_exclusive, T, Symm> {
public:
    // Region-specific execution data of a hierarchical matrix.
    RegionsData<ExecMode::locks_exclusive, Symm> regions_data{};

    ExecData() = default;

    // Instantiate an ‘ExecData’ object for a given thread count and
    // hierarchical matrix.
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::OneVector);
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::TwoVectors);

    // Return the size of the largest list of region indices stored in this
    // object.
    auto maximum_region_index_count() const -> std::size_t;

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept -> std::size_t;

    // Return a reference to the specified thread’s ‘RegionIndicesSll’ object.
    // Specifying an out-of-bounds thread ID is undefined behaviour.
    auto region_indices(int thread_id) -> RegionIndicesSll&;

    // Reset the specified thread’s ‘RegionIndicesSll’ object to its initial
    // state.  Specifying an out-of-bounds thread ID is undefined behaviour.
    auto reset_region_indices(int thread_id) -> void;

    // Return for how many threads this object contains execution data.
    auto thread_count() const noexcept(ndebug) -> int;

private:
    // The list of region indices for each thread.
    std::vector<RegionIndicesSll> region_indices_threads_{};

    // A copy of the initial state of each thread’s list of region indices.
    std::vector<RegionIndicesSll> region_indices_threads_copy_{};
};

template<typename T, Symmetric Symm>
class ExecData<ExecMode::locks_planned_exclusive, T, Symm> {
public:
    // Region-specific execution data of a hierarchical matrix.
    RegionsData<ExecMode::locks_planned_exclusive, Symm> regions_data{};

    ExecData() = default;

    // Instantiate an ‘ExecData’ object for a given thread count and
    // hierarchical matrix.
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::OneVector);
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::TwoVectors);

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept -> std::size_t;

    // Return the specified thread’s region indices.  Specifying an
    // out-of-bounds thread ID is undefined behaviour.
    auto region_indices(int thread_id) -> std::span<std::size_t>;

    // Return for how many threads this object contains execution data.
    auto thread_count() const noexcept(ndebug) -> int;

private:
    // The list of region indices for each thread.
    std::vector<std::vector<std::size_t>> region_indices_threads_{};
};

template<typename T, Symmetric Symm>
class ExecData<ExecMode::locks_planned_shared, T, Symm> {
public:
    // Region-specific execution data of a hierarchical matrix.
    RegionsData<ExecMode::locks_planned_shared, Symm> regions_data{};

    ExecData() = default;

    // Instantiate an ‘ExecData’ object for a given thread count and
    // hierarchical matrix.
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::OneVector);
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::TwoVectors);

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept -> std::size_t;

    // Return the specified thread’s region indices.  Specifying an
    // out-of-bounds thread ID is undefined behaviour.
    auto region_indices(int thread_id) -> std::span<std::size_t>;

    // Return for how many threads this object contains execution data.
    auto thread_count() const noexcept(ndebug) -> int;

private:
    // The list of region indices for each thread.
    std::vector<std::vector<std::size_t>> region_indices_threads_{};
};

template<typename T, Symmetric Symm>
class ExecData<ExecMode::locks_shared, T, Symm> {
public:
    // Region-specific execution data of a hierarchical matrix.
    RegionsData<ExecMode::locks_shared, Symm> regions_data{};

    ExecData() = default;

    // Instantiate an ‘ExecData’ object for a given thread count and
    // hierarchical matrix.
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::OneVector);
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::TwoVectors);

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept -> std::size_t;

    // Return a reference to the specified thread’s ‘RegionIndicesSll’ object.
    // Specifying an out-of-bounds thread ID is undefined behaviour.
    auto region_indices(int thread_id) -> RegionIndicesSll&;

    // Reset the specified thread’s ‘RegionIndicesSll’ object to its initial
    // state.  Specifying an out-of-bounds thread ID is undefined behaviour.
    auto reset_region_indices(int thread_id) -> void;

    // Return for how many threads this object contains execution data.
    auto thread_count() const noexcept(ndebug) -> int;

private:
    // The list of region indices for each thread.
    std::vector<RegionIndicesSll> region_indices_threads_{};

    // A copy of the initial state of each thread’s list of region indices.
    std::vector<RegionIndicesSll> region_indices_threads_copy_{};
};

template<typename T, Symmetric Symm>
class ExecData<ExecMode::per_thread_exclusive, T, Symm> {
public:
    // Per-thread output vectors for performing SpMMᵀV.
    PerThreadVectors<T, Symm> per_thread_vectors{};

    ExecData() = default;

    // Instantiate an ‘ExecData’ object for a given thread count and
    // hierarchical matrix.
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::OneVector);

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept -> std::size_t;

    // Return the specified thread’s region indices.  Specifying an
    // out-of-bounds thread ID is undefined behaviour.
    auto region_indices(int thread_id) -> std::span<std::size_t>;

    // Return for how many threads this object contains execution data.
    auto thread_count() const noexcept -> int;

private:
    // How many regions this object contains execution data for.
    std::size_t region_count_{};

    // The list of region indices for each thread.
    std::vector<std::vector<std::size_t>> region_indices_threads_{};
};

template<typename T, Symmetric Symm>
class ExecData<ExecMode::per_thread_planned_shared, T, Symm> {
public:
    // Per-thread output vectors for performing SpMMᵀV.
    PerThreadVectors<T, Symm> per_thread_vectors{};

    ExecData() = default;

    // Instantiate an ‘ExecData’ object for a given thread count and
    // hierarchical matrix.
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::OneVector);

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept -> std::size_t;

    // Return the specified thread’s region indices.  Specifying an
    // out-of-bounds thread ID is undefined behaviour.
    auto region_indices(int thread_id) -> std::span<std::size_t>;

    // Return for how many threads this object contains execution data.
    auto thread_count() const noexcept -> int;

private:
    // How many regions this object contains execution data for.
    std::size_t region_count_{};

    // The list of region indices for each thread.
    std::vector<std::vector<std::size_t>> region_indices_threads_{};
};

template<typename T, Symmetric Symm>
auto swap(ExecData<ExecMode::per_thread_shared, T, Symm>& lhs, ExecData<ExecMode::per_thread_shared, T, Symm>& rhs)
    noexcept -> void;

template<typename T, Symmetric Symm>
class ExecData<ExecMode::per_thread_shared, T, Symm> {
public:
    // Per-thread output vectors for performing SpMMᵀV.
    PerThreadVectors<T, Symm> per_thread_vectors{};

    ExecData() = default;

    // Instantiate an ‘ExecData’ object for a given thread count and
    // hierarchical matrix.
    ExecData(int thread_count, Matrix<T, Symm> const& matrix, tags::OneVector);

    ExecData(ExecData<ExecMode::per_thread_shared, T, Symm>&) = delete;
    ExecData(ExecData<ExecMode::per_thread_shared, T, Symm>&& other) noexcept;

    auto operator=(ExecData<ExecMode::per_thread_shared, T, Symm>&) = delete;
    auto operator=(ExecData<ExecMode::per_thread_shared, T, Symm>&& other) noexcept
        -> ExecData<ExecMode::per_thread_shared, T, Symm>&;

    // Return an iterator to the first region index in the ‘RegionIndicesDll’
    // object for the specified thread ID.  Specifying an out-of-bounds thread
    // ID is undefined behaviour.
    auto get_iterator(int thread_id) -> RegionIndicesDllIt;

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept(ndebug) -> std::size_t;

    // Reset the internal ‘RegionIndicesDll’ object and the unfinished region
    // count to their initial state.
    auto reset_status() -> void;

    // Return for how many threads this object contains execution data.
    auto thread_count() const noexcept -> int;

    // Lock this object, update the passed iterator to point to the next
    // unfinished region index, mark the corresponding region as done and
    // unlock this object.  Return ‘true’ if the iterator has been updated
    // successfully and return ‘false’ if no unfinished regions are available.
    auto update_iterator(RegionIndicesDllIt& it) -> bool;

    friend auto swap<T, Symm>(
            ExecData<ExecMode::per_thread_shared, T, Symm>& lhs, ExecData<ExecMode::per_thread_shared, T, Symm>& rhs
        ) noexcept -> void;

private:
    // The lock for updating the list of region indices.
    mutable omp::Mutex lock_{};

    // The list of region indices shared by all threads.
    RegionIndicesDll region_indices_{};

    // A copy of the initial state of the shared list of region indices.
    RegionIndicesDll region_indices_copy_{};

    // An iterator to the current region index of each thread.
    std::vector<RegionIndicesDllIt> region_indices_its_{};

    // The finished status of each region.
    std::vector<bool> regions_finished_{};

    // How many regions are unfinished.
    std::vector<bool>::size_type unfinished_region_count_{};
};

}  // namespace hsf
