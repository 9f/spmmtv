#pragma once

#include <span>                 // std::span

#include <hsf/exec_mode.hpp>    // hsf:ExecMode

namespace hsf {

enum struct Symmetric;
template<ExecMode EM, typename T, Symmetric Symm> class ExecData;
template<typename T, Symmetric Symm> class Matrix;
class RegionIndicesLogs;

}  // namespace hsf

namespace hsf::compute {

// Perform an SpMMᵀV operation with a hierarchical matrix sequentially.
// Parameters ‘matrix’ and ‘x1’ are respectively the input matrix and vector.
// Parameters ‘y1’ and ‘y2’ are the resulting output vectors.
template<typename T, Symmetric Symm>
auto spmmtv_sequential(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2
    ) -> void;

// Perform an SpMMᵀV operation with a hierarchical matrix sequentially.
// Parameter ‘matrix’ is the input matrix.  Parameters ‘x1’ and ‘x2’ are input
// vectors.  Parameters ‘y1’ and ‘y2’ are the resulting output vectors.
template<typename T, Symmetric Symm>
auto spmmtv_sequential(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T> y1,
        std::span<T> y2
    ) -> void;

// The following functions perform an SpMMᵀV operation with a hierarchical
// matrix in parallel.  Parameter ‘matrix’ is the input matrix.  Parameters
// ‘x1’ and ‘x2’ are input vectors.  Parameters ‘y1’ and ‘y2’ are the resulting
// output vectors.  Parameter ‘exec_data’ is the matrix’s execution data.

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::atomic_exclusive, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::atomic_planned_shared, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::atomic_shared, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::locks_exclusive, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::locks_exclusive, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::locks_planned_exclusive, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::locks_planned_exclusive, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::locks_planned_shared, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::locks_planned_shared, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::locks_shared, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::locks_shared, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::per_thread_exclusive, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::per_thread_planned_shared, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        ExecData<ExecMode::per_thread_shared, T, Symm>& exec_data
    ) -> void;

// The following functions perform an SpMMᵀV operation with a hierarchical
// matrix in parallel and log the order in which each thread finishes regions.
// Parameter ‘matrix’ is the input matrix.  Parameters ‘x1’ and ‘x2’ are input
// vectors.  Parameters ‘y1’ and ‘y2’ are the resulting output vectors.
// Parameter ‘region_indices_logs’ is used to log the indices of each thread’s
// finished regions.  Parameter ‘exec_data’ is the matrix’s execution data.

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::atomic_shared, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_exclusive, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T> y1,
        std::span<T> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_exclusive, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_shared, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T> y1,
        std::span<T> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_shared, T, Symm>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::per_thread_shared, T, Symm>& exec_data
    ) -> void;

}  // namespace hsf::compute
