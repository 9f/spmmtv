#pragma once

#include <istream>              // std::istream
#include <ostream>              // std::ostream

namespace hsf {

enum struct RegionOrdering;

// Print parameter ‘region_ordering’ into the output stream given by parameter
// ‘os’ and return parameter ‘os’.  Throw an exception of type
// ‘hsf::LogicError’ if parameter ‘region_ordering’ has an unexpected value.
auto operator<<(std::ostream& os, RegionOrdering region_ordering) -> std::ostream&;

// Read a string from the input stream given by parameter ‘is’; if the string
// corresponds to a RegionOrdering enumeration value, assign the value to
// parameter ‘region_ordering’.  Otherwise set the ‘failbit’ of parameter ‘is’.
auto operator>>(std::istream& is, RegionOrdering& region_ordering) -> std::istream&;

}  // namespace hsf
