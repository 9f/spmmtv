#pragma once

namespace hsf {

// Specify the parallel SpMMᵀV execution mode for an ‘hsf::ExecData’ object.
enum struct ExecMode {
    atomic_exclusive,
    atomic_planned_shared,
    atomic_shared,
    locks_exclusive,
    locks_planned_exclusive,
    locks_planned_shared,
    locks_shared,
    per_thread_exclusive,
    per_thread_planned_shared,
    per_thread_shared,
};

}  // namespace hsf
