#pragma once

#include <omp.h>                // omp_lock_t

namespace hsf::omp {

// A simple ‘omp_lock_t’ wrapper class.  The class satisfies the requirements
// of ‘Lockable’.
class Mutex {
public:
    Mutex() noexcept;
    ~Mutex() noexcept;

    Mutex(Mutex const&) = delete;
    Mutex(Mutex&&) = delete;

    auto operator=(Mutex const&) -> Mutex& = delete;
    auto operator=(Mutex&&) -> Mutex& = delete;

    // Wraps function ‘omp_set_lock’.
    auto lock() -> void;

    // Wraps function ‘omp_test_lock’.
    auto try_lock() -> bool;

    // Wraps function ‘omp_unset_lock’.
    auto unlock() -> void;

private:
    omp_lock_t handle_;
};

// Call member function ‘Mutex::lock’ on all objects in the range described by
// parameters ‘begin’ (inclusive) and ‘end’ (exclusive).
auto lock(Mutex* begin, Mutex* end) -> void;

// Call member function ‘Mutex::try_lock’ on objects in the range described by
// parameters ‘begin’ (inclusive) and ‘end’ (exclusive).  Return ‘true’ if all
// of the mutexes have been locked.  Return ‘false’ if any of the mutexes can’t
// be locked; in this case all locked mutexes are unlocked again.
auto try_lock(Mutex* begin, Mutex* end) -> bool;

// Call member function ‘Mutex::unlock’ on all objects in the range described
// by parameters ‘begin’ (inclusive) and ‘end’ (exclusive).
auto unlock(Mutex* begin, Mutex* end) -> void;

}  // namespace hsf::omp
