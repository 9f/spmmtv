#pragma once

#include <cstddef>              // std::size_t
#include <vector>               // std::vector

#include <boost/align/aligned_allocator.hpp>  // boost::alignment::aligned_allocator
#include <boost/container/vector.hpp>  // boost::container::vector

#include <hsf/exec_mode.hpp>    // hsf::ExecMode
#include <hsf/priv/lock_indices.hpp>  // hsf::LockIndices
#include <hsf/priv/ndebug.hpp>  // hsf::ndebug
#include <hsf/priv/omp.hpp>     // hsf::omp
#include <hsf/symmetric.hpp>    // hsf::Symmetric

namespace hsf {

template<typename T, Symmetric Symm> class Matrix;

// A helper class with additional region-specific execution data of a
// hierarchical matrix.
template<Symmetric Symm>
class RegionsDataCommon;        // regions’ data common

template<>
class RegionsDataCommon<Symmetric::no> {
public:
    RegionsDataCommon() = default;

    // Instantiate a ‘RegionsDataCommon’ object for a given hierarchical
    // matrix.
    template<typename T>
    explicit RegionsDataCommon(Matrix<T, Symmetric::no> const& matrix);

    RegionsDataCommon(RegionsDataCommon<Symmetric::no> const& other);
    RegionsDataCommon(RegionsDataCommon<Symmetric::no>&& other) noexcept;

    auto operator=(RegionsDataCommon<Symmetric::no> const& other) -> RegionsDataCommon<Symmetric::no>&;
    auto operator=(RegionsDataCommon<Symmetric::no>&& other) noexcept -> RegionsDataCommon<Symmetric::no>&;

    // Acquire output row and column locks of the region given by parameter
    // ‘region_index’.  Specifying an out-of-bounds region index is undefined
    // behaviour.
    auto lock_output(std::size_t region_index) -> void;

    // Attempt to acquire output row and column locks of the region given by
    // parameter ‘region_index’.  Specifying an out-of-bounds region index is
    // undefined behaviour.  Return ‘true’ if the required locks have been
    // acquired.  Return ‘false’ if any of the locks couldn’t have been
    // acquired - in such a case all acquired locks are released.
    auto try_lock_output(std::size_t region_index) -> bool;

    // Release output row and column locks of the region given by parameter
    // ‘region_index’.  Specifying an out-of-bounds region index is undefined
    // behaviour.
    auto unlock_output(std::size_t region_index) -> void;

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept -> std::size_t;

    friend auto swap(RegionsDataCommon<Symmetric::no>& lhs, RegionsDataCommon<Symmetric::no>& rhs) noexcept -> void;

private:
    // Output row and column locks.
    std::vector<omp::Mutex> row_locks_{};
    std::vector<omp::Mutex> column_locks_{};

    // Each region’s output row and column lock indices.
    std::vector<LockIndices<Symmetric::no>> locks_indices_{};
};

template<>
class RegionsDataCommon<Symmetric::yes> {
public:
    RegionsDataCommon() = default;

    // Instantiate a ‘RegionsDataCommon’ object for a given hierarchical
    // matrix.
    template<typename T>
    explicit RegionsDataCommon(Matrix<T, Symmetric::yes> const& matrix);

    RegionsDataCommon(RegionsDataCommon<Symmetric::yes> const& other);
    RegionsDataCommon(RegionsDataCommon<Symmetric::yes>&& other) noexcept;

    auto operator=(RegionsDataCommon<Symmetric::yes> const& other) -> RegionsDataCommon<Symmetric::yes>&;
    auto operator=(RegionsDataCommon<Symmetric::yes>&& other) noexcept -> RegionsDataCommon<Symmetric::yes>&;

    // Acquire output row locks of the region given by parameter
    // ‘region_index’.  Specifying an out-of-bounds region index is undefined
    // behaviour.
    auto lock_output(std::size_t region_index) -> void;

    // Attempt to acquire output row locks of the region given by parameter
    // ‘region_index’.  Specifying an out-of-bounds region index is undefined
    // behaviour.  Return ‘true’ if the required locks have been acquired.
    // Return ‘false’ if any of the locks couldn’t have been acquired - in such
    // a case all acquired locks are released.
    auto try_lock_output(std::size_t region_index) -> bool;

    // Release output row locks of the region given by parameter
    // ‘region_index’.  Specifying an out-of-bounds region index is undefined
    // behaviour.
    auto unlock_output(std::size_t region_index) -> void;

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept -> std::size_t;

    friend auto swap(RegionsDataCommon<Symmetric::yes>& lhs, RegionsDataCommon<Symmetric::yes>& rhs) noexcept -> void;

private:
    // Output row locks.
    std::vector<omp::Mutex> row_locks_{};

    // Each region’s output row lock indices.
    std::vector<LockIndices<Symmetric::yes>> locks_indices_{};
};

// Region-specific execution data of a hierarchical matrix.  Only the following
// instantiations are supported: ‘ExecMode::locks_exclusive’,
// ‘ExecMode::locks_planned_exclusive’, ‘ExecMode::locks_planned_shared’ and
// ‘ExecMode::locks_shared’.
template<ExecMode EM, Symmetric Symm>
class RegionsData : public RegionsDataCommon<Symm> {  // regions’ data
public:
    RegionsData() = default;

    // Instantiate a ‘RegionsData’ object for a given hierarchical matrix.
    template<typename T>
    explicit RegionsData(Matrix<T, Symm> const& matrix);
};

template<Symmetric Symm>
class RegionsData<ExecMode::locks_shared, Symm> : public RegionsDataCommon<Symm> {
public:
    RegionsData() = default;

    // Instantiate a ‘RegionsData’ object for a given hierarchical matrix.
    template<typename T>
    explicit RegionsData(Matrix<T, Symm> const& matrix);

    // Return a reference to the lock of the region given by parameter
    // ‘region_index’.  Specifying an out-of-bounds region index is undefined
    // behaviour.
    auto get_lock(std::size_t region_index) -> omp::Mutex&;

    // Return the completion status of the region given by parameter
    // ‘region_index’.  Specifying an out-of-bounds region index is undefined
    // behaviour.
    auto is_done(std::size_t region_index) const -> bool;

    // Mark the region given by parameter ‘region_index’ as done.  Specifying
    // an out-of-bounds region index is undefined behaviour.
    auto mark_done(std::size_t region_index) -> void;

    // Return for how many regions this object contains execution data.
    auto region_count() const noexcept(ndebug) -> std::size_t;

    // Reset the completion status of all regions to unfinished.
    auto reset_status() -> void;

private:
    using AlignedAllocator = boost::alignment::aligned_allocator<bool, 32>;

    // Keep track of each region’s SpMMᵀV operation completion status.
    // Elements of this array are meant to be read and written independently by
    // different threads.  The template specialisation of ‘std::vector’ for
    // type ‘bool’ may store elements as a bit array and thus may not be
    // thread-safe.  This limitation is avoided by using ‘boost::vector’, which
    // has no such template specialisation.
    boost::container::vector<bool, AlignedAllocator> done_{};

    // Each region’s lock.
    std::vector<omp::Mutex> locks_{};
};

// Assign the contents of the object given by parameter ‘rhs’ to the object
// given by parameter ‘lhs’ and return a reference to ‘lhs’.  Because objects
// of type ‘RegionsData’ are implicitly convertible to objects of type
// ‘RegionsDataCommon’, this function can be used to assign the
// ‘RegionsDataCommon’ part of different instantiations of ‘RegionsData’
// objects.  This is merely a convenience function - the same can be achieved
// by manually calling the appropriate ‘operator=’ member function.
template<Symmetric Symm>
auto set(RegionsDataCommon<Symm>& lhs, RegionsDataCommon<Symm> const& rhs) -> RegionsDataCommon<Symm>&;

}  // namespace hsf
