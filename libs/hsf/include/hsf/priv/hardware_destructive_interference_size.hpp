#pragma once

#include <hsf/priv/cstddef.hpp>  // hsf::operator""_uz

namespace hsf {

// The minimum offset between two objects to avoid false sharing on x86 CPUs.
constexpr auto hardware_destructive_interference_size = 128_uz;

}  // namespace hsf
