#pragma once

#include <cstdint>              // std::uint32_t

#include <hsf/symmetric.hpp>    // hsf::Symmetric

namespace hsf {

template<Symmetric Symm>
class LockIndices;

// Each region of a nonsymmetric hierarchical matrix has associated row and
// column locks.  All row and column locks are stored in arrays
// ‘RegionsData::row_locks_’ and ‘RegionsData::column_locks_’ respectively.  An
// instance of this class specifies the starting and past-the-end indices of a
// region’s row and column locks in the mentioned arrays.
template<>
class LockIndices<Symmetric::no> {
public:
    std::uint32_t row_begin{};
    std::uint32_t row_end{};
    std::uint32_t column_begin{};
    std::uint32_t column_end{};
};

// Each region of a symmetric hierarchical matrix has associated row locks.
// All row locks are stored in array ‘RegionsData::row_locks_’.  The row locks
// of a region form one or two subsequences.  An instance of this class
// specifies the starting and past-the-end indices of both row lock
// subsequences of a region in the mentioned array.
template<>
class LockIndices<Symmetric::yes> {
public:
    std::uint32_t row_begin1{};
    std::uint32_t row_begin2{};
    std::uint32_t row_end1{};
    std::uint32_t row_end2{};
};

}  // namespace hsf
