#pragma once

#include <cstddef>              // std::ptrdiff_t std::size_t
#include <iterator>             // std::bidirectional_iterator_tag
#include <span>                 // std::span
#include <vector>               // std::vector

namespace hsf {

class RegionIndicesDllIt;

// A doubly linked list-like container for storing and iteration over region
// indices.  The linked list is circular.
class RegionIndicesDll {
public:
    using iterator = RegionIndicesDllIt;

    // A doubly linked list element.
    struct Element {
        Element* prev_element;
        Element* next_element;
        std::size_t region_index;
    };

    RegionIndicesDll() = default;

    // Instantiate the object according to the passed array of region indices.
    explicit RegionIndicesDll(std::span<std::size_t const> region_indices);

    RegionIndicesDll(RegionIndicesDll const&) = delete;
    RegionIndicesDll(RegionIndicesDll&&) = default;

    auto operator=(RegionIndicesDll const&) -> RegionIndicesDll& = delete;
    auto operator=(RegionIndicesDll&&) -> RegionIndicesDll& = default;

    // Return an iterator to the first region index in the list.  A
    // default-constructed iterator is returned if the container is empty.
    auto begin() noexcept -> RegionIndicesDllIt;

    // Return whether the container is empty.
    [[nodiscard]]
    auto empty() const noexcept -> bool;

    // Remove the specified region index from the linked list.  Parameter ‘it’
    // specifies the region index to erase.  Erasing a region index adjusts the
    // pointers of its neighbours in the linked list, which doesn’t invalidate
    // any iterators.  The element count of the container is decremented.
    // Calling this function is undefined behaviour if the ‘RegionIndicesDll’
    // object is empty or if ‘it’ doesn’t correspond to an element in this
    // container.
    auto erase(RegionIndicesDllIt& it) -> void;

    // Restore this object’s state from a previously created shallow copy
    // specified by parameter ‘other’.
    auto from_shallow_copy(RegionIndicesDll const& other) -> void;

    // Return a shallow copy of this object.  Due to the nature of being a
    // shallow copy, the returned object contains references to data owned by
    // the original object.  Calling member function ‘erase’ of the copied
    // object might result in unexpected behaviour.
    auto shallow_copy() const -> RegionIndicesDll;

    // Return the number of elements stored in the list.
    auto size() const noexcept -> std::size_t;

private:
    // An array of the elements in the doubly linked list.
    std::vector<Element> region_offsets_{};

    // The element count of the doubly linked list.
    std::size_t element_count_{};
};

// An iterator to an element in the ‘RegionIndicesDll’ container.  Incrementing
// or decrementing the iterator traverses elements in the circular doubly
// linked list.  This isn’t really a proper iterator because no end iterator
// exists.
class RegionIndicesDllIt {
public:
    using difference_type = std::ptrdiff_t;
    using value_type = std::size_t;
    using pointer = std::size_t*;
    using reference = std::size_t&;
    using iterator_category = std::bidirectional_iterator_tag;

    // Calling any of the member functions of this class with
    // default-constructed iterator is undefined behaviour.
    RegionIndicesDllIt() = default;

    // Instantiate an iterator to the specified list element.
    explicit RegionIndicesDllIt(RegionIndicesDll::Element* element) noexcept;

    auto operator*() -> std::size_t;
    auto operator->() -> std::size_t*;

    auto operator++() -> RegionIndicesDllIt&;
    auto operator--() -> RegionIndicesDllIt&;

    // Erase this element from the linked list.  Erasing an element adjusts the
    // pointers of the its neighbours in the linked list, which doesn’t
    // invalidate any iterators.
    auto erase() const -> void;

private:
    // The element in the linked list that this iterator represents.
    RegionIndicesDll::Element* element{};
};

}  // namespace hsf
