#pragma once

namespace hsf {

// Indicate whether NDEBUG is defined or not.  Useful for making a
// noexcept-specification depend on whether NDEBUG is defined or not (e.g. when
// using macro assert).
#ifdef NDEBUG
constexpr auto ndebug = true;
#else
constexpr auto ndebug = false;
#endif

}  // namespace hsf
