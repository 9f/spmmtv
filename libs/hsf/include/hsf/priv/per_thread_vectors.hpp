#pragma once

#include <cstddef>              // std::size_t
#include <vector>               // std::vector

#include <hsf/priv/ndebug.hpp>  // hsf::ndebug
#include <hsf/symmetric.hpp>    // hsf::Symmetric

namespace hsf {

// A helper type for performing parallel SpMMᵀV using per-thread output
// vectors.
template<typename T, Symmetric Symm>
class PerThreadVectors;

template<typename T>
class PerThreadVectors<T, Symmetric::no> {
public:
    std::vector<std::vector<T>> y1{};
    std::vector<std::vector<T>> y2{};

    PerThreadVectors() = default;
    PerThreadVectors(std::size_t column_count, std::size_t row_count, int thread_count);

    auto thread_count() const noexcept(ndebug) -> int;
};

template<typename T>
class PerThreadVectors<T, Symmetric::yes> {
public:
    std::vector<std::vector<T>> y1{};

    PerThreadVectors() = default;
    PerThreadVectors(std::size_t column_count, std::size_t row_count, int thread_count);

    auto thread_count() const noexcept -> int;
};

}  // namespace hsf
