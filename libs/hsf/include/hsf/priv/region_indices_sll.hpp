#pragma once

#include <cstddef>              // std::size_t
#include <span>                 // std::span
#include <vector>               // std::vector

#include <hsf/priv/ndebug.hpp>  // hsf::ndebug

namespace hsf {

// A singly linked list-like container for storing and iteration over region
// indices.  The linked list is circular.
class RegionIndicesSll {
public:
    RegionIndicesSll() = default;

    // Instantiate the object according to the passed array of region indices.
    explicit RegionIndicesSll(std::span<std::size_t const> region_indices);

    RegionIndicesSll(RegionIndicesSll const&) = delete;
    RegionIndicesSll(RegionIndicesSll&&) = default;

    auto operator=(RegionIndicesSll const&) -> RegionIndicesSll& = delete;
    auto operator=(RegionIndicesSll&&) -> RegionIndicesSll& = default;

    // Return whether the container is empty.
    [[nodiscard]]
    auto empty() const noexcept -> bool;

    // Remove the current region index.  The current index is set to the index
    // that followed the removed one.  Calling this function is undefined
    // behaviour if the ‘RegionIndicesSll’ object is empty.
    auto erase_index() noexcept(ndebug) -> void;

    // Restore this object’s state from a previously created shallow copy
    // specified by parameter ‘other’.
    auto from_shallow_copy(RegionIndicesSll const& other) noexcept(ndebug) -> void;

    // Return the current region index.  Calling this function is undefined
    // behaviour if the ‘RegionIndicesSll’ object is empty.
    auto get_index() const noexcept(ndebug) -> std::size_t;

    // Iterate to the next region index.  Calling this function is undefined
    // behaviour if the ‘RegionIndicesSll’ object is empty.
    auto next() -> void;

    // Return a shallow copy of this object.  Due to the nature of being a
    // shallow copy, the returned object contains references to data owned by
    // the original object.  Calling member functions ‘erase_index’ or ‘next’
    // of the copied object may result in unexpected behaviour.
    auto shallow_copy() const -> RegionIndicesSll;

    // Return the number of stored elements.
    auto size() const noexcept -> std::size_t;

private:
    // Linked list element.
    struct Element {
        Element* next_element;
        std::size_t region_index;
    };

    // Array of linked list elements.
    std::vector<Element> region_offsets_{};

    // Pointer to the current element.
    Element* current_element_{};

    // Pointer to the previous element.  This pointer is needed to facilitate
    // element removal.
    Element* previous_element_{};

    // Linked list element count.
    std::size_t element_count_{};
};

}  // namespace hsf
