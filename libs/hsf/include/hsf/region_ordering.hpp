#pragma once

#include <map>                  // std::map
#include <string_view>          // std::string_view

namespace hsf {

// The available hierarchical matrix region orderings.  The
// block-lexicographical ordering naturally arises from the way CSR matrices
// are converted to the hierarchical format used in this implementation.
// Regions are sorted lexicographically according to pairs of
// ‘Region::row_begin’ and ‘Region::column_begin’.  Sorting regions according
// to the Morton ordering entails sorting the regions according to the Morton
// codes created from pairs of ‘Region::row_begin’ and ‘Region::column_begin’.
enum struct RegionOrdering {
    block_lexicographical,
    lexicographical,
    morton,
};

// This map contains key-value pairs that can be used to look up
// ‘RegionOrdering’ enumeration values using strings.
extern std::map<std::string_view, RegionOrdering> const region_orderings;

}  // namespace hsf
