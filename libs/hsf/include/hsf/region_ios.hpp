#pragma once

#include <ostream>              // std::ostream

namespace hsf {

template<typename T> class Region;

// Print the passed region’s row, column and Morton code into the passed output
// stream.  The values are printed as binary numbers.  The function returns
// parameter ‘os’.
template<typename T>
auto print_morton_code(Region<T> const& region, std::ostream& os) -> std::ostream&;

// Print a Morton code metadata header into the passed output stream.  Such a
// header is useful when printing information about a Morton code of a
// hierarchical matrix’s region using the ‘hsf::print_morton_code’ function
// from this translation unit.  The function returns parameter ‘os’.
auto print_morton_code_header(std::ostream& os) -> std::ostream&;

// Print a region metadata header into the passed output stream.  Such a header
// is useful when printing information about a hierarchical matrix’s region
// using the ‘operator<<’ function from this translation unit.  The function
// returns parameter ‘os’.
auto print_region_header(std::ostream& os) -> std::ostream&;

// Print the passed region’s type, row, column, row count, column count and
// element count into the passed output stream.  The function returns parameter
// ‘os’.
template<typename T>
auto operator<<(std::ostream& os, Region<T> const& region) -> std::ostream&;

}  // namespace hsf
