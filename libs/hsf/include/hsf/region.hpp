#pragma once

#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t
#include <variant>              // std::variant

#include <hsf/info.hpp>         // hsf::info
#include <hsf/priv/hardware_destructive_interference_size.hpp>  // hsf::hardware_destructive_interference_size

namespace hsf {

template<typename T>
class alignas(hardware_destructive_interference_size) Region {
public:
    // Row index of the first element of this region in the source matrix.
    std::uint32_t row_begin{};

    // Column index of the first element of this region in the source matrix.
    std::uint32_t column_begin{};

    // The region’s row count.
    std::uint32_t row_count{};

    // The region’s column count.
    std::uint32_t column_count{};

    // The region’s element count.
    std::size_t element_count{};

    // The region’s COO row index array or CSR row pointer array.
    std::variant<info::RowIndex*, info::RowPointer*> rows{};

    // The region’s COO or CSR column index array.
    info::ColIndex* columns{};

    // The region’s COO or CSR element array.
    T* elements{};
};

}  // namespace hsf
