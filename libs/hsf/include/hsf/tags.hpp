#pragma once

namespace hsf::tags {

// This tag represents SpMMᵀV with one input vector.
struct OneVector {};

// This tag represents SpMMᵀV with two input vectors.
struct TwoVectors {};

// Helper constants.
constexpr auto one_vector = OneVector{};
constexpr auto two_vectors = TwoVectors{};

}  // namespace hsf::tags
