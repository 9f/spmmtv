#pragma once

#include <hsf/logic_error.hpp>  // hsf::LogicError

namespace hsf {

class SubregionBoundsCapacityError : public LogicError {
public:
    using LogicError::LogicError;
};

}  // namespace hsf
