#pragma once

#include <cstdint>              // std::uint16_t std::uint32_t

namespace hsf::info {

using ColIndex = std::uint16_t;
using RowIndex = std::uint16_t;
using RowPointer = std::uint32_t;

constexpr auto region_size = std::uint32_t{1 << 16};

}  // namespace hsf::info
