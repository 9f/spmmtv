#pragma once

namespace hsf {

// Indicate whether to optimise the CSR regions of an ‘hsf::Matrix’.
enum struct OptimiseCsrRegions {
    no,
    yes,
};

}  // namespace hsf
