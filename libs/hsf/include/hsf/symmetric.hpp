#pragma once

namespace hsf {

// Indicate the symmetry of a hierarchical matrix.
enum struct Symmetric {
    no,
    yes,
};

}  // namespace hsf
