#pragma once

#include <ostream>              // std::ostream

namespace hsf {

enum struct Symmetric;
template<typename T, Symmetric Symm> class Matrix;

// Print the passed hierarchical matrix’s element count, dimensions, symmetry,
// region count, region sort order and its CSR region optimisation status into
// the passed output stream.  The function returns parameter ‘os’.
template<typename T, Symmetric Symm>
auto operator<<(std::ostream& os, Matrix<T, Symm> const& matrix) -> std::ostream&;

}  // namespace hsf
