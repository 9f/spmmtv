#pragma once

#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t
#include <memory>               // std::unique_ptr
#include <span>                 // std::span

#include <hsf/info.hpp>         // hsf::info
#include <hsf/optimise_csr_regions.hpp>  // hsf::OptimiseCsrRegions
#include <hsf/region.hpp>       // hsf::Region

namespace hsf {

enum struct NormaliseRegions;
enum struct RegionOrdering;
enum struct Symmetric;
template<typename T, Symmetric Symm> class Matrix;

template<typename T, Symmetric Symm>
auto swap(Matrix<T, Symm>& lhs, Matrix<T, Symm>& rhs) noexcept -> void;

// A hierarchical matrix format that uses COO₁₆ in its first level and either
// COO₁₆ or CSR₁₆ in its second level.
template<typename T, Symmetric Symm>
class Matrix {
public:
    using ColIndex = info::ColIndex;
    using RowIndex = info::RowIndex;
    using RowPointer = info::RowPointer;
    using Value = T;

    static constexpr auto symmetric = Symm;

    Matrix() = default;

    // Instantiate a hierarchical matrix from a CSR matrix.  Parameter
    // ‘row_pointers’ is a CSR row pointer array.  Parameter ‘columns’ is a CSR
    // column index array.  Parameter ‘elements’ is a CSR element value array.
    // Parameters ‘row_count’, ‘column_count’ and ‘element_count’ are
    // respectively the row, column and element count the CSR matrix.
    // Parameter ‘region_ordering’ specifies the order of the instantiated
    // matrix’s regions.  Parameter ‘ocr’ specifies whether to optimise the CSR
    // regions of the instantiated matrix.  Throw ‘hsf::LogicError’ if any of
    // the passed arguments are invalid.
    Matrix(std::uint32_t const* row_pointers,
           std::uint32_t const* columns,
           T const* elements,
           std::size_t row_count,
           std::size_t column_count,
           std::size_t element_count,
           RegionOrdering region_ordering,
           OptimiseCsrRegions ocr = OptimiseCsrRegions::yes);

    // Instantiate a hierarchical matrix with normalised regions from a CSR
    // matrix.  Parameter ‘row_pointers’ is a CSR row pointer array.  Parameter
    // ‘columns’ is a CSR column index array.  Parameter ‘elements’ is a CSR
    // element value array.  Parameters ‘row_count’, ‘column_count’ and
    // ‘element_count’ are respectively the row, column and element count the
    // CSR matrix.  Parameter ‘region_ordering’ specifies the order of the
    // instantiated matrix’s regions.  The instantiated matrix’s regions are
    // normalised according to parameters ‘region_coefficient’ and
    // ‘thread_count’.  Parameter ‘region_coefficient’ is a real number in
    // interval [0, 1].  Parameter ‘thread_count’ is how many threads will be
    // used during the subsequent SpMMᵀV operations.  Parameter ‘ocr’ specifies
    // whether to optimise the CSR regions of the instantiated matrix.  Throw
    // ‘hsf::LogicError’ if any of the passed arguments are invalid.
    Matrix(std::uint32_t const* row_pointers,
           std::uint32_t const* columns,
           T const* elements,
           std::size_t row_count,
           std::size_t column_count,
           std::size_t element_count,
           RegionOrdering region_ordering,
           double region_coefficient,
           int thread_count,
           OptimiseCsrRegions ocr = OptimiseCsrRegions::yes);

    Matrix(Matrix const& other);
    Matrix(Matrix&& other) = default;

    auto operator=(Matrix const& other) -> Matrix&;
    auto operator=(Matrix&& other) -> Matrix& = default;

    // Return the matrix’s row count.
    auto row_count() const noexcept -> std::size_t;

    // Return the matrix’s column count.
    auto column_count() const noexcept -> std::size_t;

    // Return the matrix’s element count.
    auto element_count() const noexcept -> std::size_t;

    // Return a view of the matrix’s regions.
    auto regions() const noexcept -> std::span<Region<T> const>;

    // Return the matrix’s region count.
    auto region_count() const noexcept -> std::size_t;

    // Return the ordering according to which the matrix’s regions are sorted.
    auto region_ordering() const noexcept -> RegionOrdering;

    // Return whether or not the CSR region optimisation step has been
    // performed.
    auto are_csr_regions_optimised() const noexcept -> bool;

    // Return the matrix’s size in bytes.  A few data members are left out when
    // computing the matrix’s size because the size is meant to be used for
    // comparing this format with the COO and CSR formats.
    auto size_bytes() const noexcept -> std::size_t;

    friend auto swap<T, Symm>(Matrix<T, Symm>& lhs, Matrix<T, Symm>& rhs) noexcept -> void;

private:
    // The matrix’s COO row index array.
    std::unique_ptr<RowIndex[]> coo_rows_{};

    // The size of the matrix’s COO row index array.
    std::size_t coo_rows_size_{};

    // The matrix’s CSR row pointer array.
    std::unique_ptr<RowPointer[]> csr_rows_{};

    // The size of the matrix’s CSR row pointer array.
    std::size_t csr_rows_size_{};

    // The matrix’s COO and CSR column index array.
    std::unique_ptr<ColIndex[]> columns_{};

    // The matrix’s COO and CSR element value array.
    std::unique_ptr<T[]> elements_{};

    // The size of the matrix’s column index and element value arrays.
    std::size_t elements_size_{};

    // The matrix’s row count.
    std::size_t row_count_{};

    // The matrix’s column count.
    std::size_t column_count_{};

    // The matrix’s region array.
    std::unique_ptr<Region<T>[]> regions_{};

    // The size of the matrix’s region array.
    std::size_t regions_size_{};

    // The ordering according to which the matrix’s regions are sorted.
    RegionOrdering region_ordering_{};

    // Track whether the CSR region optimisation step has been performed.
    bool csr_regions_optimised_{};

    // The nondefaulted public constructors of this class forward their
    // arguments to this constructor.
    Matrix(std::uint32_t const* row_pointers,
           std::uint32_t const* columns,
           T const* elements,
           std::size_t row_count,
           std::size_t column_count,
           std::size_t element_count,
           RegionOrdering region_ordering,
           NormaliseRegions normalise_regions,
           double region_coefficient,
           int thread_count,
           OptimiseCsrRegions optimise_csr_regions);

    // Determine a target maximum amount of elements per region from parameters
    // ‘region_coefficient’ and ‘thread_count’, and split the regions of the
    // hierarchical matrix into subregions if they exceed the maximum.
    // Parameter ‘region_coefficient’ is a real number in interval [0, 1].
    // Parameter ‘thread_count’ is how many threads will be used during
    // subsequent SpMMᵀV operations.
    auto normalise_regions(double region_coefficient, int thread_count) -> void;

    // Optimise the CSR regions of the hierarchical matrix, i.e. remove any
    // leading and trailing empty rows from all regions with a CSR row pointer
    // array.
    auto optimise_csr_regions() -> void;

    // Sort the matrix’s regions according to the passed region ordering.
    // Calling this function after optimising the matrix’s CSR regions by
    // calling ‘Matrix::optimise_csr_regions’ is unsupported.  Using this
    // function to change the order of the regions from a different ordering to
    // the block-lexicographical ordering is unsupported.  Performing any of
    // the unsupported operations throws an exception of type
    // ‘hsf::LogicError’.  The function throws an exception of type
    // ‘hsf::LogicError’ if parameter ‘region_ordering’ has an unexpected
    // value.
    auto sort_regions(RegionOrdering region_ordering) -> void;
};

// Return whether or not the passed matrix’s regions are correctly sorted.
// This function is meant for testing purposes.  It iterates over all of the
// matrix’s regions and verifies that they’re ordered according to the matrix’s
// region ordering.  Calling this function after optimising the matrix’s CSR
// regions by calling ‘Matrix::optimise_csr_regions’ is unsupported and will
// throw an exception of type ‘hsf::LogicError’.  The function throws an
// exception of type ‘hsf::LogicError’ if the passed matrix has an unexpected
// region ordering.
template<typename T, Symmetric Symm>
auto are_regions_sorted(Matrix<T, Symm> const& matrix) -> bool;

}  // namespace hsf
