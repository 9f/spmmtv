#pragma once

namespace hsf {

// Indicate whether to normalise the regions of an ‘hsf::Matrix’.
enum struct NormaliseRegions {
    no,
    yes,
};

}  // namespace hsf
