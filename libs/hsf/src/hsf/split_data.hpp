#pragma once

#include <cstdint>              // std::uint32_t

namespace hsf {

template<typename T> class SubregionData;

// Data for a region that is used when splitting regions into subregions.  The
// template can only be instantiated with types ‘float’ and ‘double’.
template<typename T>
class SplitData {
public:
    // The starting index of the COO row index array of the subregions of the
    // region being split.
    std::uint32_t coo_row_index{};

    // The starting index of the CSR row pointer array of the subregions of the
    // region being split.
    std::uint32_t csr_row_index{};

    // The starting index of the column index array and element value array of
    // the subregions of the region being split.
    std::uint32_t element_index{};

    // The starting index for storing the subregions of the region being split.
    std::uint32_t region_index{};

    // A pointer to data for creating subregions.  Regions that cannot be split
    // into subregions have this member variable set to ‘nullptr’.
    SubregionData<T>* subregion_data{};
};

}  // namespace hsf
