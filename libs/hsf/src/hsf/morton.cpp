#include <hsf/morton.hpp>

#include <array>                // std::to_array
#include <cstdint>              // std::uint32_t std::uint64_t

#include <hsf/priv/cstddef.hpp>  // hsf::operator""_uz

// The Morton code creation algorithm used in this translation unit is inspired
// by the algorithm available here:
// <http://www.graphics.stanford.edu/~seander/bithacks.html#InterleaveBMN>.

namespace hsf::morton {

namespace {

auto twiddle(std::uint32_t value) noexcept -> std::uint64_t;

}  // namespace

auto encode(std::uint32_t const x, std::uint32_t const y) noexcept -> std::uint64_t
{
    return twiddle(x) | (twiddle(y) << 1);
}

namespace {

auto twiddle(std::uint32_t const value) noexcept -> std::uint64_t
{
    constexpr auto masks = std::to_array<std::uint64_t>({
            0x0000FFFF0000FFFF,
            0x00FF00FF00FF00FF,
            0x0F0F0F0F0F0F0F0F,
            0x3333333333333333,
            0x5555555555555555,
        });
    constexpr auto shifts = std::to_array<std::uint64_t>({16, 8, 4, 2, 1});

    auto value_64 = static_cast<std::uint64_t>(value);
    for (auto i = 0_uz; i < 5; ++i) {
        value_64 = (value_64 | (value_64 << shifts[i])) & masks[i];
    }
    return value_64;
}

}  // namespace

}  // namespace hsf::morton
