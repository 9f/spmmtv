#include <hsf/priv/omp.hpp>

#include <cassert>              // assert

// omp_destroy_lock omp_init_lock omp_set_lock omp_test_lock omp_unset_lock
#include <omp.h>

namespace hsf::omp {

Mutex::Mutex() noexcept
{
    omp_init_lock(&this->handle_);
}

Mutex::~Mutex() noexcept
{
    omp_destroy_lock(&this->handle_);
}

auto Mutex::lock() -> void
{
    omp_set_lock(&this->handle_);
}

auto Mutex::try_lock() -> bool
{
    return omp_test_lock(&this->handle_);
}

auto Mutex::unlock() -> void
{
    omp_unset_lock(&this->handle_);
}

auto lock(Mutex* const begin, Mutex* const end) -> void
{
    assert(begin != nullptr);
    assert(end != nullptr);
    assert(begin <= end);

    for (auto it = begin; it != end; ++it) {
        it->lock();
    }
}

auto try_lock(Mutex* const begin, Mutex* const end) -> bool
{
    assert(begin != nullptr);
    assert(end != nullptr);
    assert(begin <= end);

    // Attempt to lock all mutexes.
    auto it = begin;
    for (; it != end; ++it) {
        if (!it->try_lock()) {
            break;
        }
    }

    // Detect and indicate success.
    if (it == end) {
        return true;
    }

    // Unlock mutexes and indicate failure.
    unlock(begin, it);
    return false;
}

auto unlock(Mutex* const begin, Mutex* const end) -> void
{
    assert(begin != nullptr);
    assert(end != nullptr);
    assert(begin <= end);

    for (auto it = begin; it != end; ++it) {
        it->unlock();
    }
}

}  // namespace hsf::omp
