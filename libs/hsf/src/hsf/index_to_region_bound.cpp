#include <hsf/index_to_region_bound.hpp>

#include <cstddef>              // std::size_t

#include <hsf/info.hpp>         // hsf::info

namespace hsf {

auto index_to_region_bound(std::size_t const index) noexcept -> std::size_t
{
    return index / info::region_size;
}

}  // namespace hsf
