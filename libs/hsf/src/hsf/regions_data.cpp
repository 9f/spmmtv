#include <hsf/priv/regions_data.hpp>

#include <algorithm>            // std::sort
#include <array>                // std::array
#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t
#include <map>                  // std::map
#include <span>                 // std::span
#include <tuple>                // std::tuple
#include <utility>              // std::move
#include <vector>               // std::vector

#include <boost/align/assume_aligned.hpp>  // BOOST_ALIGN_ASSUME_ALIGNED
#include <boost/align/is_aligned.hpp>  // boost::alignment::is_aligned
#include <boost/core/swap.hpp>  // boost::swap

#include <hsf/exec_mode.hpp>    // hsf::ExecMode
#include <hsf/matrix.hpp>       // hsf::Matrix
#include <hsf/priv/cstddef.hpp>  // hsf::operator""_uz
#include <hsf/priv/ndebug.hpp>  // hsf::ndebug
#include <hsf/priv/omp.hpp>     // hsf::omp
#include <hsf/region.hpp>       // hsf::Region
#include <hsf/symmetric.hpp>    // hsf::Symmetric

namespace hsf {

namespace {

// Return a pair of maps, where the first and second maps map row and column
// indices to row and column lock indices respectively.  Parameter ‘regions’
// specifies the hierarchical matrix regions for which to compute indices.
template<typename T>
auto compute_lock_indices_nonsymmetric(std::span<Region<T> const> regions)
    -> std::tuple<std::map<std::uint32_t, std::uint32_t>, std::map<std::uint32_t, std::uint32_t>>;

// Return a map that maps row indices to row lock indices.  Parameter ‘regions’
// specifies the hierarchical matrix regions for which to compute indices.
template<typename T>
auto compute_lock_indices_symmetric(std::span<Region<T> const> regions) -> std::map<std::uint32_t, std::uint32_t>;

// Return two ranges of row indices.  Parameters ‘row_begin’ and ‘row_end’
// specify the row indices of a region.  Parameters ‘column_begin’ and
// ‘column_end’ specify the column indices of the same region.  The returned
// ranges of indices correspond to taking the passed column indices,
// interpreting them as row indices and merging them with the passed row
// indices.
auto merge_index_ranges(
        std::uint32_t row_begin, std::uint32_t row_end, std::uint32_t column_begin, std::uint32_t column_end
    ) -> std::tuple<std::uint32_t, std::uint32_t, std::uint32_t, std::uint32_t>;

}  // namespace

template<typename T>
RegionsDataCommon<Symmetric::no>::RegionsDataCommon(Matrix<T, Symmetric::no> const& matrix)
    : locks_indices_(matrix.region_count())
{
    auto const [rows_lock_indices, columns_lock_indices] = compute_lock_indices_nonsymmetric(matrix.regions());
    this->row_locks_ = std::vector<omp::Mutex>(rows_lock_indices.size());
    this->column_locks_ = std::vector<omp::Mutex>(columns_lock_indices.size());

    for (auto i = 0_uz; i < matrix.region_count(); ++i) {
        auto const& region = matrix.regions()[i];
        auto const row_begin = region.row_begin;
        auto const row_end = region.row_begin + region.row_count;
        auto const column_begin = region.column_begin;
        auto const column_end = region.column_begin + region.column_count;

        auto& lock_indices = this->locks_indices_[i];
        // None of the calls to ‘std::map::at’ should throw an exception.
        lock_indices.row_begin = rows_lock_indices.at(row_begin);
        lock_indices.row_end = rows_lock_indices.at(row_end);
        lock_indices.column_begin = columns_lock_indices.at(column_begin);
        lock_indices.column_end = columns_lock_indices.at(column_end);
    }
}

RegionsDataCommon<Symmetric::no>::RegionsDataCommon(RegionsDataCommon<Symmetric::no> const& other)
    : row_locks_(other.row_locks_.size()),
      column_locks_(other.column_locks_.size()),
      locks_indices_(other.locks_indices_)
{
}

RegionsDataCommon<Symmetric::no>::RegionsDataCommon(RegionsDataCommon<Symmetric::no>&& other) noexcept
{
    *this = std::move(other);
}

auto RegionsDataCommon<Symmetric::no>::operator=(RegionsDataCommon<Symmetric::no> const& other)
    -> RegionsDataCommon<Symmetric::no>&
{
    return *this = RegionsDataCommon<Symmetric::no>{other};
}

auto RegionsDataCommon<Symmetric::no>::operator=(RegionsDataCommon<Symmetric::no>&& other) noexcept
    -> RegionsDataCommon<Symmetric::no>&
{
    boost::swap(*this, other);
    return *this;
}

auto RegionsDataCommon<Symmetric::no>::lock_output(std::size_t const region_index) -> void
{
    assert(region_index < this->region_count());

    auto const rb = this->locks_indices_[region_index].row_begin;
    auto const re = this->locks_indices_[region_index].row_end;
    auto const row_locks_begin = this->row_locks_.data() + rb;
    auto const row_locks_end = this->row_locks_.data() + re;
    omp::lock(row_locks_begin, row_locks_end);

    auto const cb = this->locks_indices_[region_index].column_begin;
    auto const ce = this->locks_indices_[region_index].column_end;
    auto const column_locks_begin = this->column_locks_.data() + cb;
    auto const column_locks_end = this->column_locks_.data() + ce;
    omp::lock(column_locks_begin, column_locks_end);
}

auto RegionsDataCommon<Symmetric::no>::try_lock_output(std::size_t const region_index) -> bool
{
    assert(region_index < this->region_count());

    auto const rb = this->locks_indices_[region_index].row_begin;
    auto const re = this->locks_indices_[region_index].row_end;
    auto const row_locks_begin = this->row_locks_.data() + rb;
    auto const row_locks_end = this->row_locks_.data() + re;
    if (!omp::try_lock(row_locks_begin, row_locks_end)) {
        return false;
    }

    auto const cb = this->locks_indices_[region_index].column_begin;
    auto const ce = this->locks_indices_[region_index].column_end;
    auto const column_locks_begin = this->column_locks_.data() + cb;
    auto const column_locks_end = this->column_locks_.data() + ce;
    if (!omp::try_lock(column_locks_begin, column_locks_end)) {
        omp::unlock(row_locks_begin, row_locks_end);
        return false;
    }

    return true;
}

auto RegionsDataCommon<Symmetric::no>::unlock_output(std::size_t const region_index) -> void
{
    assert(region_index < this->region_count());

    auto const rb = this->locks_indices_[region_index].row_begin;
    auto const re = this->locks_indices_[region_index].row_end;
    auto const row_locks_begin = this->row_locks_.data() + rb;
    auto const row_locks_end = this->row_locks_.data() + re;
    omp::unlock(row_locks_begin, row_locks_end);

    auto const cb = this->locks_indices_[region_index].column_begin;
    auto const ce = this->locks_indices_[region_index].column_end;
    auto const column_locks_begin = this->column_locks_.data() + cb;
    auto const column_locks_end = this->column_locks_.data() + ce;
    omp::unlock(column_locks_begin, column_locks_end);
}

auto RegionsDataCommon<Symmetric::no>::region_count() const noexcept -> std::size_t
{
    return this->locks_indices_.size();
}

auto swap(RegionsDataCommon<Symmetric::no>& lhs, RegionsDataCommon<Symmetric::no>& rhs) noexcept -> void
{
    boost::swap(lhs.row_locks_, rhs.row_locks_);
    boost::swap(lhs.column_locks_, rhs.column_locks_);
    boost::swap(lhs.locks_indices_, rhs.locks_indices_);
}

template<typename T>
RegionsDataCommon<Symmetric::yes>::RegionsDataCommon(Matrix<T, Symmetric::yes> const& matrix)
    : locks_indices_(matrix.region_count())
{
    auto const rows_lock_indices = compute_lock_indices_symmetric(matrix.regions());
    this->row_locks_ = std::vector<omp::Mutex>(rows_lock_indices.size());

    for (auto i = 0_uz; i < matrix.region_count(); ++i) {
        auto const& region = matrix.regions()[i];
        auto const row_begin = region.row_begin;
        auto const row_end = region.row_begin + region.row_count;
        auto const column_begin = region.column_begin;
        auto const column_end = region.column_begin + region.column_count;

        auto const& [row_begin1, row_end1, row_begin2, row_end2]
            = merge_index_ranges(row_begin, row_end, column_begin, column_end);

        auto& lock_indices = this->locks_indices_[i];
        // None of the calls to ‘std::map::at’ should throw an exception.
        lock_indices.row_begin1 = rows_lock_indices.at(row_begin1);
        lock_indices.row_end1 = rows_lock_indices.at(row_end1);
        lock_indices.row_begin2 = rows_lock_indices.at(row_begin2);
        lock_indices.row_end2 = rows_lock_indices.at(row_end2);
    }
}

RegionsDataCommon<Symmetric::yes>::RegionsDataCommon(RegionsDataCommon<Symmetric::yes> const& other)
    : row_locks_(other.row_locks_.size()),
      locks_indices_(other.locks_indices_)
{
}

RegionsDataCommon<Symmetric::yes>::RegionsDataCommon(RegionsDataCommon<Symmetric::yes>&& other) noexcept
{
    *this = std::move(other);
}

auto RegionsDataCommon<Symmetric::yes>::operator=(RegionsDataCommon<Symmetric::yes> const& other)
    -> RegionsDataCommon<Symmetric::yes>&
{
    return *this = RegionsDataCommon<Symmetric::yes>{other};
}

auto RegionsDataCommon<Symmetric::yes>::operator=(RegionsDataCommon<Symmetric::yes>&& other) noexcept
    -> RegionsDataCommon<Symmetric::yes>&
{
    boost::swap(*this, other);
    return *this;
}

auto RegionsDataCommon<Symmetric::yes>::lock_output(std::size_t const region_index) -> void
{
    assert(region_index < this->region_count());

    auto const rb1 = this->locks_indices_[region_index].row_begin1;
    auto const re1 = this->locks_indices_[region_index].row_end1;
    auto const row_locks_begin1 = this->row_locks_.data() + rb1;
    auto const row_locks_end1 = this->row_locks_.data() + re1;
    omp::lock(row_locks_begin1, row_locks_end1);

    auto const rb2 = this->locks_indices_[region_index].row_begin2;
    auto const re2 = this->locks_indices_[region_index].row_end2;
    auto const row_locks_begin2 = this->row_locks_.data() + rb2;
    auto const row_locks_end2 = this->row_locks_.data() + re2;
    omp::lock(row_locks_begin2, row_locks_end2);
}

auto RegionsDataCommon<Symmetric::yes>::try_lock_output(std::size_t const region_index) -> bool
{
    assert(region_index < this->region_count());

    auto const rb1 = this->locks_indices_[region_index].row_begin1;
    auto const re1 = this->locks_indices_[region_index].row_end1;
    auto const row_locks_begin1 = this->row_locks_.data() + rb1;
    auto const row_locks_end1 = this->row_locks_.data() + re1;
    if (!omp::try_lock(row_locks_begin1, row_locks_end1)) {
        return false;
    }

    auto const rb2 = this->locks_indices_[region_index].row_begin2;
    auto const re2 = this->locks_indices_[region_index].row_end2;
    auto const row_locks_begin2 = this->row_locks_.data() + rb2;
    auto const row_locks_end2 = this->row_locks_.data() + re2;
    if (!omp::try_lock(row_locks_begin2, row_locks_end2)) {
        omp::unlock(row_locks_begin1, row_locks_end1);
        return false;
    }

    return true;
}

auto RegionsDataCommon<Symmetric::yes>::unlock_output(std::size_t const region_index) -> void
{
    assert(region_index < this->region_count());

    auto const rb1 = this->locks_indices_[region_index].row_begin1;
    auto const re1 = this->locks_indices_[region_index].row_end1;
    auto const row_locks_begin1 = this->row_locks_.data() + rb1;
    auto const row_locks_end1 = this->row_locks_.data() + re1;
    omp::unlock(row_locks_begin1, row_locks_end1);

    auto const rb2 = this->locks_indices_[region_index].row_begin2;
    auto const re2 = this->locks_indices_[region_index].row_end2;
    auto const row_locks_begin2 = this->row_locks_.data() + rb2;
    auto const row_locks_end2 = this->row_locks_.data() + re2;
    omp::unlock(row_locks_begin2, row_locks_end2);
}

auto RegionsDataCommon<Symmetric::yes>::region_count() const noexcept -> std::size_t
{
    return this->locks_indices_.size();
}

auto swap(RegionsDataCommon<Symmetric::yes>& lhs, RegionsDataCommon<Symmetric::yes>& rhs) noexcept -> void
{
    boost::swap(lhs.row_locks_, rhs.row_locks_);
    boost::swap(lhs.locks_indices_, rhs.locks_indices_);
}

template<ExecMode EM, Symmetric Symm>
template<typename T>
RegionsData<EM, Symm>::RegionsData(Matrix<T, Symm> const& matrix)
    : RegionsDataCommon<Symm>{matrix}
{
}

template<Symmetric Symm>
template<typename T>
RegionsData<ExecMode::locks_shared, Symm>::RegionsData(Matrix<T, Symm> const& matrix)
    : RegionsDataCommon<Symm>{matrix},
      done_(matrix.region_count()),
      locks_(matrix.region_count())
{
}

template RegionsData<ExecMode::locks_exclusive, Symmetric::no>::RegionsData(
        Matrix<float, Symmetric::no> const& matrix
    );

template RegionsData<ExecMode::locks_exclusive, Symmetric::no>::RegionsData(
        Matrix<double, Symmetric::no> const& matrix
    );

template RegionsData<ExecMode::locks_exclusive, Symmetric::yes>::RegionsData(
        Matrix<float, Symmetric::yes> const& matrix
    );

template RegionsData<ExecMode::locks_exclusive, Symmetric::yes>::RegionsData(
        Matrix<double, Symmetric::yes> const& matrix
    );

template RegionsData<ExecMode::locks_planned_exclusive, Symmetric::no>::RegionsData(
        Matrix<float, Symmetric::no> const& matrix
    );

template RegionsData<ExecMode::locks_planned_exclusive, Symmetric::no>::RegionsData(
        Matrix<double, Symmetric::no> const& matrix
    );

template RegionsData<ExecMode::locks_planned_exclusive, Symmetric::yes>::RegionsData(
        Matrix<float, Symmetric::yes> const& matrix
    );

template RegionsData<ExecMode::locks_planned_exclusive, Symmetric::yes>::RegionsData(
        Matrix<double, Symmetric::yes> const& matrix
    );

template RegionsData<ExecMode::locks_planned_shared, Symmetric::no>::RegionsData(
        Matrix<float, Symmetric::no> const& matrix
    );

template RegionsData<ExecMode::locks_planned_shared, Symmetric::no>::RegionsData(
        Matrix<double, Symmetric::no> const& matrix
    );

template RegionsData<ExecMode::locks_planned_shared, Symmetric::yes>::RegionsData(
        Matrix<float, Symmetric::yes> const& matrix
    );

template RegionsData<ExecMode::locks_planned_shared, Symmetric::yes>::RegionsData(
        Matrix<double, Symmetric::yes> const& matrix
    );

template RegionsData<ExecMode::locks_shared, Symmetric::no>::RegionsData(
        Matrix<float, Symmetric::no> const& matrix
    );

template RegionsData<ExecMode::locks_shared, Symmetric::no>::RegionsData(
        Matrix<double, Symmetric::no> const& matrix
    );

template RegionsData<ExecMode::locks_shared, Symmetric::yes>::RegionsData(
        Matrix<float, Symmetric::yes> const& matrix
    );

template RegionsData<ExecMode::locks_shared, Symmetric::yes>::RegionsData(
        Matrix<double, Symmetric::yes> const& matrix
    );

template<Symmetric Symm>
auto RegionsData<ExecMode::locks_shared, Symm>::get_lock(std::size_t const region_index) -> omp::Mutex&
{
    assert(region_index < this->region_count());
    return this->locks_[region_index];
}

template<Symmetric Symm>
auto RegionsData<ExecMode::locks_shared, Symm>::is_done(std::size_t const region_index) const -> bool
{
    assert(region_index < this->region_count());
    return this->done_[region_index];
}

template<Symmetric Symm>
auto RegionsData<ExecMode::locks_shared, Symm>::mark_done(std::size_t const region_index) -> void
{
    assert(region_index < this->region_count());
    this->done_[region_index] = true;
}

template<Symmetric Symm>
auto RegionsData<ExecMode::locks_shared, Symm>::region_count() const noexcept(ndebug) -> std::size_t
{
    assert(RegionsDataCommon<Symm>::region_count() == this->done_.size());
    assert(RegionsDataCommon<Symm>::region_count() == this->locks_.size());
    return RegionsDataCommon<Symm>::region_count();
}

template<Symmetric Symm>
auto RegionsData<ExecMode::locks_shared, Symm>::reset_status() -> void
{
    assert(boost::alignment::is_aligned(this->done_.data(), 32));

    auto* done = this->done_.data();
    BOOST_ALIGN_ASSUME_ALIGNED(done, 32);
    for (auto i = 0_uz; i < this->done_.size(); ++i) {
        done[i] = false;
    }
}

template class RegionsData<ExecMode::locks_exclusive, Symmetric::no>;
template class RegionsData<ExecMode::locks_exclusive, Symmetric::yes>;
template class RegionsData<ExecMode::locks_planned_exclusive, Symmetric::no>;
template class RegionsData<ExecMode::locks_planned_exclusive, Symmetric::yes>;
template class RegionsData<ExecMode::locks_planned_shared, Symmetric::no>;
template class RegionsData<ExecMode::locks_planned_shared, Symmetric::yes>;
template class RegionsData<ExecMode::locks_shared, Symmetric::no>;
template class RegionsData<ExecMode::locks_shared, Symmetric::yes>;

template<Symmetric Symm>
auto set(RegionsDataCommon<Symm>& lhs, RegionsDataCommon<Symm> const& rhs) -> RegionsDataCommon<Symm>&
{
    return lhs = rhs;
}

template auto set(RegionsDataCommon<Symmetric::no>& lhs, RegionsDataCommon<Symmetric::no> const& rhs)
    -> RegionsDataCommon<Symmetric::no>&;

template auto set(RegionsDataCommon<Symmetric::yes>& lhs, RegionsDataCommon<Symmetric::yes> const& rhs)
    -> RegionsDataCommon<Symmetric::yes>&;

namespace {

template<typename T>
auto compute_lock_indices_nonsymmetric(std::span<Region<T> const> const regions)
    -> std::tuple<std::map<std::uint32_t, std::uint32_t>, std::map<std::uint32_t, std::uint32_t>>
{
    auto rows_lock_indices = std::map<std::uint32_t, std::uint32_t>{};
    auto columns_lock_indices = std::map<std::uint32_t, std::uint32_t>{};

    for (auto const& region : regions) {
        auto const row_begin = region.row_begin;
        auto const row_end = region.row_begin + region.row_count;
        auto const column_begin = region.column_begin;
        auto const column_end = region.column_begin + region.column_count;

        rows_lock_indices.try_emplace(row_begin);
        rows_lock_indices.try_emplace(row_end);
        columns_lock_indices.try_emplace(column_begin);
        columns_lock_indices.try_emplace(column_end);
    }

    auto i = std::uint32_t{};
    for (auto& [row, lock_index] : rows_lock_indices) {
        lock_index = i++;
    }

    i = 0;
    for (auto& [column, lock_index] : columns_lock_indices) {
        lock_index = i++;
    }

    return {std::move(rows_lock_indices), std::move(columns_lock_indices)};
}

template<typename T>
auto compute_lock_indices_symmetric(std::span<Region<T> const> const regions) -> std::map<std::uint32_t, std::uint32_t>
{
    auto rows_lock_indices = std::map<std::uint32_t, std::uint32_t>{};

    for (auto const& region : regions) {
        auto const row_begin = region.row_begin;
        auto const row_end = region.row_begin + region.row_count;
        auto const column_begin = region.column_begin;
        auto const column_end = region.column_begin + region.column_count;

        rows_lock_indices.try_emplace(row_begin);
        rows_lock_indices.try_emplace(row_end);
        rows_lock_indices.try_emplace(column_begin);
        rows_lock_indices.try_emplace(column_end);
    }

    auto i = std::uint32_t{};
    for (auto& [row, lock_index] : rows_lock_indices) {
        lock_index = i++;
    }

    return rows_lock_indices;
}

auto merge_index_ranges(
        std::uint32_t const row_begin,
        std::uint32_t const row_end,
        std::uint32_t const column_begin,
        std::uint32_t const column_end
    ) -> std::tuple<std::uint32_t, std::uint32_t, std::uint32_t, std::uint32_t>
{
    auto indices = std::array{row_begin, row_end, column_begin, column_end};
    std::sort(indices.begin(), indices.end());

    if ((row_begin == indices[0] && row_end == indices[3])
        || (column_begin == indices[0] && column_end == indices[3])
        || row_end == indices[2]
        || column_end == indices[2]) {
        return {indices[0], indices[3], 0, 0};
    }

    return {indices[0], indices[1], indices[2], indices[3]};
}

}  // namespace

}  // namespace hsf
