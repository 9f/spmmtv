#pragma once

#include <cstddef>              // std::size_t
#include <vector>               // std::vector

#include <hsf/region_indices_log.hpp>  // hsf::RegionIndicesLog

namespace hsf {

// Region indices logs for multiple threads.
class RegionIndicesLogs {
public:
    RegionIndicesLogs() = default;

    // Instantiate an object with a given amount region indices logs.
    // Parameter ‘maximum_region_indices_count’ is the maximum amount of region
    // indices of each log.  Parameter ‘thread_count’ specifies the amount of
    // region indices logs.
    RegionIndicesLogs(std::size_t maximum_region_indices_count, int thread_count);

    // Return the region indices log of a given thread.  Parameter ‘thread_id’
    // specifies the thread’s ID.  Specifying a thread ID greater than the
    // thread count this object was constructed for is undefined behaviour.
    auto get(int thread_id) -> RegionIndicesLog&;
    auto get(int thread_id) const -> RegionIndicesLog const&;

    // Return the thread count that was specified during this object’s
    // construction.
    auto thread_count() const noexcept -> int;

private:
    // Threads’ region indices logs.
    std::vector<RegionIndicesLog> region_indices_logs_{};
};

}  // namespace hsf
