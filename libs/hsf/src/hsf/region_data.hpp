#pragma once

#include <cstdint>              // std::uint32_t

#include <hsf/priv/hardware_destructive_interference_size.hpp>  // hsf::hardware_destructive_interference_size
#include <hsf/region_type.hpp>  // hsf::RegionType

namespace hsf {

// Metadata of a region.  A ‘RegionInfo’ object keeps a reference to an array
// of ‘RegionData’ objects.
struct alignas(hardware_destructive_interference_size) RegionData {
    // The type of the region.
    RegionType region_type;

    // An index into the row bounds object used by the associated ‘RegionInfo’
    // object.
    std::uint32_t row_bound;

    // An index into the column bounds object used by the associated
    // ‘RegionInfo’ object.
    std::uint32_t col_bound;

    // The number of nonzero elements of the region.
    std::uint32_t element_count;

    // The index of the current region among regions with at least one element
    // (i.e. among nonempty regions).
    std::uint32_t nonempty_region_index;

    // A helper member variable for accessing the column index array and
    // element value array of the region.  It initially holds the index of
    // the first element of this region determined by summing the element
    // counts of predecessing regions.
    std::uint32_t element_index;

    // A helper member variable for accessing the COO row index array of
    // the region.  It initially holds the index of the first COO row index
    // of this region determined by summing the element counts of
    // predecessing COO regions.
    std::uint32_t coo_row_index;

    // The starting index of the CSR row pointer array of this region.
    std::uint32_t csr_row_index;

    // The previous row index, which is used when updating CSR regions.
    std::uint32_t previous_row;
};

}  // namespace hsf
