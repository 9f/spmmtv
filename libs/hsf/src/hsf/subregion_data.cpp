#include <hsf/subregion_data.hpp>

#include <cstdint>              // std::uint32_t

#include <hsf/region.hpp>       // hsf::Region

namespace hsf {

template<typename T>
SubregionData<T>::SubregionData(
        Region<T> const* const region,
        std::uint32_t const row_pointers_index,
        std::uint32_t const column_pointers_index
    ) noexcept
    : region{region},
      row_pointers_index{row_pointers_index},
      column_pointers_index{column_pointers_index}
{
}

template class SubregionData<float>;
template class SubregionData<double>;

}  // namespace hsf
