#pragma once

#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t
#include <span>                 // std::span

#include <boost/container/static_vector.hpp>  // boost::container::static_vector

#include <hsf/info.hpp>         // hsf::info
#include <hsf/priv/ndebug.hpp>  // hsf::ndebug
#include <hsf/sub_bounds_capacity.hpp>  // hsf::sub_bounds_capacity

namespace hsf {

// Store subregion bounds of a region along one of its axes as an array of
// indices.  Each subregion is bounded by a pair of indices.  The first index
// of the pair is the first index of the subregion.  The second index of the
// pair is the index following the last index of the subregion.  The second
// index is therefore the first index of the following subregion.
class SubBounds {
public:
    // Return the maximum number of subregions that can be stored using the
    // internal statically allocated array of ‘SubBounds’ objects.
    static auto get_subregion_capacity() noexcept(ndebug) -> std::size_t;

    // Find a subregion bound according to parameter ‘value’.  The stored bound
    // indices are searched for the largest index less than or equal to
    // ‘value’.  The bound with the found index is returned.
    auto find_subregion_bound(info::RowPointer value) const -> std::size_t;

    // Return the starting index of the subregion specified by parameter
    // ‘bound’.  Specifying an out-of-bounds ‘bound’ is undefined behaviour.
    auto get_index(std::size_t bound) const -> info::RowPointer;

    // Get the starting index of the subregion specified by parameter ‘bound’,
    // convert it to an index in the original matrix and return the result.
    // Specifying an out-of-bounds ‘bound’ is undefined behaviour.
    auto get_original_index(std::size_t bound) const -> info::RowPointer;

    // Return the difference between the starting index of the subregion
    // specified by parameter ‘bound’ and the starting index of the following
    // subregion.  This can be interpreted as the subregion’s size (e.g. row or
    // column count).  Specifying an out-of-bounds ‘bound’ is undefined
    // behaviour.
    auto get_size(std::size_t bound) const -> info::RowPointer;

    // Return the number of stored subregions.  For a nonempty ‘SubBounds’
    // object, the subregion count is one less than the number of stored bound
    // indices.
    auto get_subregion_count() const noexcept -> std::size_t;

    // Initialise the ‘SubBounds’ object to contain subregion bounds of an
    // existing region.  Parameter ‘pointers’ is the existing region’s CSC or
    // CSR pointer array.  Parameter ‘element_count’ is the existing region’s
    // element count.  Parameter ‘index_offset’ is the first row or column
    // index of the existing region in the original matrix.  Parameter
    // ‘region_threshold’ is the maximum amount of elements that the newly
    // created subregions should have.  Parameter ‘thread_count’ affects the
    // final subregion’s element count.  The member function throws
    // ‘hsf::SubregionBoundsCapacityError’ if the capacity of the ‘indices_’
    // array is insufficient.
    auto init(
            std::span<info::RowPointer const> pointers,
            std::size_t element_count,
            std::uint32_t index_offset,
            std::size_t region_threshold,
            int thread_count
        ) -> void;

private:
    using StaticVector = boost::container::static_vector<info::RowPointer, sub_bounds_capacity>;

    // An array of bound indices.  The first array bound index is always equal
    // to zero.
    StaticVector indices_{};

    // The subregion count is one less than the number of bound indices in
    // array ‘indices_’.
    StaticVector::size_type subregion_count_{};

    // Bound indices in ‘SubBounds::indices_’ might not correspond to index
    // values in the original matrix, because ‘SubBounds::indices_’ begins with
    // zero whilst the ‘SubBounds’ object can represent any matrix region.
    // This member variable stores the offset that needs to be added to each
    // index in ‘SubBounds::indices_’ to obtain index values from the original
    // matrix.
    std::uint32_t index_offset_{};
};

}  // namespace hsf
