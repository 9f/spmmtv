#pragma once

#include <cstddef>              // std::size_t

namespace hsf {

// Return an index mapped to a region bound.  The implementation assumes that
// each bound covers ‘meta::region_size’ elements.
auto index_to_region_bound(std::size_t index) noexcept -> std::size_t;

}  // namespace hsf
