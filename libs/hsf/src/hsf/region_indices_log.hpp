#pragma once

#include <cstddef>              // std::size_t
#include <memory>               // std::unique_ptr
#include <vector>               // std::vector

namespace hsf {

// Region indices log.
class RegionIndicesLog {
public:
    RegionIndicesLog() = default;

    // Instantiate an object that can log up to a given number of region
    // indices.  Parameter ‘maximum_region_indices_count’ is the maximum amount
    // of region indices that the object can log.
    explicit RegionIndicesLog(std::size_t maximum_region_indices_count);

    // Return a vector of all logged region indices.
    auto region_indices() const -> std::vector<std::size_t>;

    // Append a region index to the log.  Parameter ‘region_index’ is the
    // region index to append.  Appending more region indices than has been
    // specified during this object’s construction is undefined behaviour.
    auto push_back(std::size_t region_index) -> void;

private:
    // Logged region indices.
    std::unique_ptr<std::size_t[]> region_indices_{};

    // Logged region index’s count.
    std::size_t region_indices_count_{};
};

}  // namespace hsf
