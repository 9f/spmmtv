#include <hsf/region_type.hpp>

#include <variant>              // std::visit

#include <hsf/info.hpp>         // hsf::info
#include <hsf/region.hpp>       // hsf::Region

namespace hsf {

namespace {

class RegionTypeVisitor {
public:
    auto operator()(info::RowIndex*) const noexcept -> RegionType;
    auto operator()(info::RowPointer*) const noexcept -> RegionType;
};

}  // namespace

template<typename T>
auto get_region_type(Region<T> const& region) -> RegionType
{
    return std::visit(RegionTypeVisitor{}, region.rows);
}

template auto get_region_type(Region<float> const& region) -> RegionType;
template auto get_region_type(Region<double> const& region) -> RegionType;

namespace {

auto RegionTypeVisitor::operator()(info::RowIndex*) const noexcept -> RegionType
{
    return RegionType::COO;
}

auto RegionTypeVisitor::operator()(info::RowPointer*) const noexcept -> RegionType
{
    return RegionType::CSR;
}

}  // namespace

}  // namespace hsf
