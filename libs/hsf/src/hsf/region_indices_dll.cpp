#include <hsf/priv/region_indices_dll.hpp>

#include <algorithm>            // std::copy
#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <span>                 // std::span

#include <hsf/priv/cstddef.hpp>  // hsf::operator""_uz

namespace hsf {

RegionIndicesDll::RegionIndicesDll(std::span<std::size_t const> const region_indices)
    : region_offsets_(region_indices.size()),
      element_count_{region_indices.size()}
{
    if (region_indices.empty()) {
        return;
    }

    this->region_offsets_[0].prev_element = &this->region_offsets_.back();
    for (auto i = 1_uz; i < this->region_offsets_.size(); ++i) {
        this->region_offsets_[i].prev_element = &this->region_offsets_[i - 1];
    }

    for (auto i = 0_uz; i < this->region_offsets_.size() - 1; ++i) {
        this->region_offsets_[i].next_element = &this->region_offsets_[i + 1];
    }
    this->region_offsets_.back().next_element = &this->region_offsets_[0];

    for (auto i = 0_uz; i < this->region_offsets_.size(); ++i) {
        this->region_offsets_[i].region_index = region_indices[i];
    }
}

auto RegionIndicesDll::begin() noexcept -> RegionIndicesDllIt
{
    if (this->empty()) {
        return {};
    }
    return RegionIndicesDllIt{&region_offsets_.front()};
}

[[nodiscard]]
auto RegionIndicesDll::empty() const noexcept -> bool
{
    return this->size() == 0;
}

auto RegionIndicesDll::erase(RegionIndicesDllIt& it) -> void
{
    assert(!this->empty());

    it.erase();
    --this->element_count_;
}

auto RegionIndicesDll::from_shallow_copy(RegionIndicesDll const& other) -> void
{
    assert(this->region_offsets_.size() == other.region_offsets_.size());

    std::copy(other.region_offsets_.begin(), other.region_offsets_.end(), this->region_offsets_.begin());
    this->element_count_ = other.element_count_;
}

auto RegionIndicesDll::shallow_copy() const -> RegionIndicesDll
{
    auto copy = RegionIndicesDll{};
    copy.region_offsets_ = this->region_offsets_;
    copy.element_count_ = this->element_count_;
    return copy;
}

auto RegionIndicesDll::size() const noexcept -> std::size_t
{
    return this->element_count_;
}

RegionIndicesDllIt::RegionIndicesDllIt(RegionIndicesDll::Element* const element) noexcept
    : element{element}
{
}

auto RegionIndicesDllIt::operator*() -> std::size_t
{
    assert(this->element != nullptr);
    return this->element->region_index;
}


auto RegionIndicesDllIt::operator->() -> std::size_t*
{
    assert(this->element != nullptr);
    return &this->element->region_index;
}

auto RegionIndicesDllIt::operator++() -> RegionIndicesDllIt&
{
    assert(this->element != nullptr);
    this->element = this->element->next_element;
    return *this;
}

auto RegionIndicesDllIt::operator--() -> RegionIndicesDllIt&
{
    assert(this->element != nullptr);
    this->element = this->element->prev_element;
    return *this;
}

auto RegionIndicesDllIt::erase() const -> void
{
    assert(this->element != nullptr);
    assert(this->element->prev_element != nullptr);
    assert(this->element->next_element != nullptr);

    auto* const prev_element = this->element->prev_element;
    auto* const next_element = this->element->next_element;

    prev_element->next_element = next_element;
    next_element->prev_element = prev_element;
}

}  // namespace hsf
