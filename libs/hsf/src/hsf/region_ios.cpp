#include <hsf/region_ios.hpp>

#include <array>                // std::array
#include <charconv>             // std::to_chars
#include <cstddef>              // std::size_t
#include <ios>                  // std::left
#include <iomanip>              // std::setw
#include <ostream>              // std::ostream
#include <string_view>          // std::string_view

#include <boost/io/ios_state.hpp>  // boost::io::ios_flags_saver

#include <hsf/morton.hpp>       // hsf::morton
#include <hsf/region.hpp>       // hsf::Region
#include <hsf/region_type.hpp>  // hsf::get_region_type
#include <hsf/region_type_ios.hpp>  // hsf::operator<<

namespace hsf {

template<typename T>
auto print_morton_code(Region<T> const& region, std::ostream& os) -> std::ostream&
{
    auto row_begin = std::array<char, 32>{};
    auto column_begin = std::array<char, 32>{};
    auto morton_code = std::array<char, 64>{};

    auto const [end_pointer1, error_contition1] = std::to_chars(
            row_begin.data(),
            row_begin.data() + row_begin.size(),
            region.row_begin,
            2
        );
    auto const row_begin_size = static_cast<std::size_t>(end_pointer1 - row_begin.data());
    auto const row_begin_sv = std::string_view{row_begin.data(), row_begin_size};

    auto const [end_pointer2, error_contition2] = std::to_chars(
            column_begin.data(),
            column_begin.data() + column_begin.size(),
            region.column_begin,
            2
        );
    auto const column_begin_size = static_cast<std::size_t>(end_pointer2 - column_begin.data());
    auto const column_begin_sv = std::string_view{column_begin.data(), column_begin_size};

    auto const [end_pointer3, error_contition3] = std::to_chars(
            morton_code.data(),
            morton_code.data() + morton_code.size(),
            morton::encode(region.column_begin, region.row_begin),
            2
        );
    auto const morton_code_size = static_cast<std::size_t>(end_pointer3 - morton_code.data());
    auto const morton_code_sv = std::string_view{morton_code.data(), morton_code_size};

    os << std::setw(32) << row_begin_sv << "  "
       << std::setw(32) << column_begin_sv << "  "
       << std::setw(64) << morton_code_sv;
    return os;
}

template auto print_morton_code(Region<float> const& region, std::ostream& os) -> std::ostream&;
template auto print_morton_code(Region<double> const& region, std::ostream& os) -> std::ostream&;

auto print_morton_code_header(std::ostream& os) -> std::ostream&
{
    auto const ifs = boost::io::ios_flags_saver{os};
    os << std::left
       << std::setw(32) << "row beginning" << "  "
       << std::setw(32) << "column beginning" << "  "
       << std::setw(64) << "morton code";
    return os;
}

auto print_region_header(std::ostream& os) -> std::ostream&
{
    auto const ifs = boost::io::ios_flags_saver{os};
    os << std::left
       << std::setw(11) << "region type" << "  "
       << std::setw(13) << "row beginning" << "  "
       << std::setw(16) << "column beginning" << "  "
       << std::setw(13) << "row count" << "  "
       << std::setw(13) << "column count" << "  "
       << std::setw(13) << "element count";
    return os;
}

template<typename T>
auto operator<<(std::ostream& os, Region<T> const& region) -> std::ostream&
{
    {
        auto const ifs = boost::io::ios_flags_saver{os};
        os << std::left << std::setw(11) << get_region_type(region) << "  ";
    }

    os << std::setw(13) << region.row_begin << "  "
       << std::setw(16) << region.column_begin << "  "
       << std::setw(13) << region.row_count << "  "
       << std::setw(13) << region.column_count << "  "
       << std::setw(13) << region.element_count;
    return os;
}

template auto operator<<(std::ostream& os, Region<float> const& region) -> std::ostream&;
template auto operator<<(std::ostream& os, Region<double> const& region) -> std::ostream&;

}  // namespace hsf
