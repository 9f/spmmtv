#include <hsf/compute.hpp>

#include <algorithm>            // std::copy std::max
#include <array>                // std::array
#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t
#include <span>                 // std::span
#include <variant>              // std::visit

#include <omp.h>                // omp_get_max_threads omp_get_num_threads omp_get_thread_num

#include <hsf/exec_data.hpp>    // hsf::ExecData
#include <hsf/exec_mode.hpp>    // hsf::ExecMode
#include <hsf/info.hpp>         // hsf::info
#include <hsf/matrix.hpp>       // hsf::Matrix
#include <hsf/priv/cstddef.hpp>  // hsf::operator""_uz
#include <hsf/priv/ndebug.hpp>  // hsf::ndebug
#include <hsf/priv/per_thread_vectors.hpp>  // hsf::PerThreadVectors
#include <hsf/region.hpp>       // hsf::Region
#include <hsf/region_indices_logs.hpp>  // hsf::RegionIndicesLogs
#include <hsf/symmetric.hpp>    // hsf::Symmetric

namespace hsf::compute {

namespace {

// Call the appropriate SpMMᵀV function with one input vector depending on
// whether the region of the hierarchical matrix uses the COO or CSR format.
// Parameter ‘Symm’ indicates whether or not to call an SpMMᵀV function for
// symmetric matrices.
template<typename T, Symmetric Symm>
class SpmmtvOneVec {
public:
    SpmmtvOneVec(
            Region<T> const& region,
            std::span<T const> x1,
            std::span<T> y1,
            std::span<T> y2
        ) noexcept(ndebug);

    SpmmtvOneVec(
            Region<T> const& region,
            std::span<T const> x1,
            PerThreadVectors<T, Symmetric::no>& per_thread_vectors,
            int thread_id
        );

    SpmmtvOneVec(
            Region<T> const& region,
            std::span<T const> x1,
            PerThreadVectors<T, Symmetric::yes>& per_thread_vectors,
            int thread_id
        );

    auto operator()(info::RowIndex* row_indices) -> void;
    auto operator()(info::RowPointer* row_pointers) -> void;

private:
    Region<T> const& region_;
    std::span<T const> x1_{};
    std::span<T> y1_{};
    std::span<T> y2_{};
};

// Call the appropriate SpMMᵀV function with two input vectors depending on
// whether the region of the hierarchical matrix uses the COO or CSR format.
// Parameter ‘Symm’ indicates whether or not to call an SpMMᵀV function for
// symmetric matrices.
template<typename T, Symmetric Symm>
class SpmmtvTwoVecs {
public:
    SpmmtvTwoVecs(
            Region<T> const& region,
            std::span<T const> x1,
            std::span<T const> x2,
            std::span<T> y1,
            std::span<T> y2
        ) noexcept(ndebug);

    auto operator()(info::RowIndex* row_indices) -> void;
    auto operator()(info::RowPointer* row_pointers) -> void;

private:
    Region<T> const& region_;
    std::span<T const> x1_{};
    std::span<T const> x2_{};
    std::span<T> y1_{};
    std::span<T> y2_{};
};

// Call the appropriate atomic SpMMᵀV function with one input vector depending
// on whether the region of the hierarchical matrix uses the COO or CSR format.
// Parameter ‘Symm’ indicates whether or not to call an SpMMᵀV function for
// symmetric matrices.
template<typename T, Symmetric Symm>
class SpmmtvOneVecAtomic {
public:
    SpmmtvOneVecAtomic(
            Region<T> const& region,
            std::span<T const> x1,
            std::span<T> y1,
            std::span<T> y2
        ) noexcept(ndebug);

    auto operator()(info::RowIndex* row_indices) -> void;
    auto operator()(info::RowPointer* row_pointers) -> void;

private:
    Region<T> const& region_;
    std::span<T const> x1_{};
    std::span<T> y1_{};
    std::span<T> y2_{};
};

// Verify that the passed pointer arguments don’t alias.  The function returns
// ‘true’ if no pair of arguments alias, or ‘false’ otherwise.  Arguments are
// treated as ‘void’ pointers.
template<typename... Args>
auto do_not_alias(Args... args) noexcept -> bool;

// Copy the contents of parameter ‘y1’ to parameter ‘y2’ sequentially.  The
// contents is copied only if parameter ‘Symm’ is equal to ‘Symmetric::yes’.
template<typename T, Symmetric Symm>
auto finish_sequential_one_vec(std::span<T const> y1, std::span<T> y2) -> void;

// Copy the contents of parameter ‘y1’ to parameter ‘y2’ in parallel.  The
// contents is copied only if parameter ‘Symm’ is equal to ‘Symmetric::yes’,
template<typename T, Symmetric Symm>
auto finish_parallel_one_vec(std::span<T const> y1, std::span<T> y2) -> void;

// Merge and reset per-thread output vectors.  Parameter ‘per_thread_vectors’
// specifies the per-thread output vectors to merge.  Parameters ‘y1’ and ‘y2’
// specify the output vectors that will contain merged values.
template<typename T>
auto merge_reset_per_thread_vectors_one_vec(
        PerThreadVectors<T, Symmetric::no>& per_thread_vectors, std::span<T> y1, std::span<T> y2
    ) -> void;

// Merge and reset per-thread output vectors.  Parameter ‘per_thread_vectors’
// specifies the per-thread output vectors to merge.  Parameters ‘y1’ and ‘y2’
// specify the output vectors that will contain merged values.
template<typename T>
auto merge_reset_per_thread_vectors_one_vec(
        PerThreadVectors<T, Symmetric::yes>& per_thread_vectors, std::span<T> y1, std::span<T> y2
    ) -> void;

// Perform assertions to verify that the arguments have the correct properties
// for performing sequential SpMMᵀV.
template<typename T, Symmetric Symm>
auto perform_sequential_checks(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> y1,
        std::span<T const> y2
    ) -> void;

// Perform assertions to verify that the arguments have the correct properties
// for performing sequential SpMMᵀV.
template<typename T, Symmetric Symm>
auto perform_sequential_checks(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T const> y1,
        std::span<T const> y2
    ) -> void;

// Perform assertions to verify that the arguments have the correct properties
// for performing parallel SpMMᵀV.  The function also calls function
// ‘perform_sequential_checks’.
template<typename T, Symmetric Symm, ExecMode EM>
auto perform_parallel_checks(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> y1,
        std::span<T const> y2,
        ExecData<EM, T, Symm> const& exec_data
    ) -> void;

// Perform assertions to verify that the arguments have the correct properties
// for performing parallel SpMMᵀV.  The function also calls function
// ‘perform_sequential_checks’.
template<typename T, Symmetric Symm, ExecMode EM>
auto perform_parallel_checks(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T const> y1,
        std::span<T const> y2,
        ExecData<EM, T, Symm> const& exec_data
    ) -> void;

// Perform assertions to verify that the arguments have the correct properties
// for performing logged parallel SpMMᵀV.  The function also calls function
// ‘perform_parallel_checks’.
template<typename T, Symmetric Symm, ExecMode EM>
auto perform_parallel_logged_checks(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> y1,
        std::span<T const> y2,
        RegionIndicesLogs const& region_indices_logs,
        ExecData<EM, T, Symm> const& exec_data
    ) -> void;

// Perform assertions to verify that the arguments have the correct properties
// for performing logged parallel SpMMᵀV.  The function also calls function
// ‘perform_parallel_checks’.
template<typename T, Symmetric Symm, ExecMode EM>
auto perform_parallel_logged_checks(
        Matrix<T, Symm> const& matrix,
        std::span<T const> x1,
        std::span<T const> x2,
        std::span<T const> y1,
        std::span<T const> y2,
        RegionIndicesLogs const& region_indices_logs,
        ExecData<EM, T, Symm> const& exec_data
    ) -> void;

// Perform SpMMᵀV with a region of a hierarchical matrix where the region uses
// the COO format.
template<typename T>
auto spmmtv_coo(
        info::RowIndex const* rows,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t element_count,
        T const* x1,
        T* y1,
        T* y2,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV with a region of a hierarchical matrix where the region uses
// the COO format.
template<typename T>
auto spmmtv_coo(
        info::RowIndex const* rows,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t element_count,
        T const* x1,
        T const* x2,
        T* y1,
        T* y2,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV using atomic instructions with a region of a hierarchical
// matrix where the region uses the COO format.
template<typename T>
auto spmmtv_coo_atomic(
        info::RowIndex const* rows,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t element_count,
        T const* x1,
        T* y1,
        T* y2,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV with a region of a symmetric hierarchical matrix where the
// region uses the COO format.  Copying the contents of output array ‘y1’ to
// output array ‘y2’ is left to the caller.
template<typename T>
auto spmmtv_coo_symmetric(
        info::RowIndex const* rows,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t element_count,
        T const* x1,
        T* y1,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV with a region of a symmetric hierarchical matrix where the
// region uses the COO format.
template<typename T>
auto spmmtv_coo_symmetric(
        info::RowIndex const* rows,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t element_count,
        T const* x1,
        T const* x2,
        T* y1,
        T* y2,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV using atomic instructions with a region of a symmetric
// hierarchical matrix where the region uses the COO format.  Copying the
// contents of output array ‘y1’ to output array ‘y2’ is left to the caller.
template<typename T>
auto spmmtv_coo_symmetric_atomic(
        info::RowIndex const* rows,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t element_count,
        T const* x1,
        T* y1,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV with a region of a symmetric hierarchical matrix where the
// region uses the COO format.  The function assumes the region contains no
// diagonal elements.  Copying the contents of output array ‘y1’ to output
// array ‘y2’ is left to the caller.
template<typename T>
auto spmmtv_coo_symmetric_nondiagonal(
        info::RowIndex const* rows,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t element_count,
        T const* x1,
        T* y1,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV with a region of a symmetric hierarchical matrix where the
// region uses the COO format.  The function assumes the region contains no
// diagonal elements.
template<typename T>
auto spmmtv_coo_symmetric_nondiagonal(
        info::RowIndex const* rows,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t element_count,
        T const* x1,
        T const* x2,
        T* y1,
        T* y2,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV using atomic instructions with a region of a symmetric
// hierarchical matrix where the region uses the COO format.  The function
// assumes the region contains no diagonal elements.  Copying the contents of
// output array ‘y1’ to output array ‘y2’ is left to the caller.
template<typename T>
auto spmmtv_coo_symmetric_nondiagonal_atomic(
        info::RowIndex const* rows,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t element_count,
        T const* x1,
        T* y1,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV with a region of a hierarchical matrix where the region uses
// the CSR format.
template<typename T>
auto spmmtv_csr(
        info::RowPointer const* row_pointers,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T* y1,
        T* y2,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV with a region of a hierarchical matrix where the region uses
// the CSR format.
template<typename T>
auto spmmtv_csr(
        info::RowPointer const* row_pointers,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T const* x2,
        T* y1,
        T* y2,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV using atomic instructions with a region of a hierarchical
// matrix where the region uses the CSR format.
template<typename T>
auto spmmtv_csr_atomic(
        info::RowPointer const* row_pointers,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T* y1,
        T* y2,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV with a region of a symmetric hierarchical matrix where the
// region uses the CSR format.  Copying the contents of output array ‘y1’ to
// output array ‘y2’ is left to the caller.
template<typename T>
auto spmmtv_csr_symmetric(
        info::RowPointer const* row_pointers,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T* y1,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV with a region of a symmetric hierarchical matrix where the
// region uses the CSR format.
template<typename T>
auto spmmtv_csr_symmetric(
        info::RowPointer const* row_pointers,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T const* x2,
        T* y1,
        T* y2,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV using atomic instructions with a region of a symmetric
// hierarchical matrix where the region uses the CSR format.  Copying the
// contents of output array ‘y1’ to output array ‘y2’ is left to the caller.
template<typename T>
auto spmmtv_csr_symmetric_atomic(
        info::RowPointer const* row_pointers,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T* y1,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV with a region of a symmetric hierarchical matrix where the
// region uses the CSR format.  The function assumes the region contains no
// diagonal elements.  Copying the contents of output array ‘y1’ to output
// array ‘y2’ is left to the caller.
template<typename T>
auto spmmtv_csr_symmetric_nondiagonal(
        info::RowPointer const* row_pointers,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T* y1,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV with a region of a symmetric hierarchical matrix where the
// region uses the CSR format.  The function assumes the region contains no
// diagonal elements.
template<typename T>
auto spmmtv_csr_symmetric_nondiagonal(
        info::RowPointer const* row_pointers,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T const* x2,
        T* y1,
        T* y2,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

// Perform SpMMᵀV using atomic instructions with a region of a symmetric
// hierarchical matrix where the region uses the CSR format.  The function
// assumes the region contains no diagonal elements.  Copying the contents of
// output array ‘y1’ to output array ‘y2’ is left to the caller.
template<typename T>
auto spmmtv_csr_symmetric_nondiagonal_atomic(
        info::RowPointer const* row_pointers,
        info::ColIndex const* columns,
        T const* elements,
        std::size_t row_count,
        T const* x1,
        T* y1,
        std::uint32_t row_offset,
        std::uint32_t column_offset
    ) -> void;

}  // namespace

template<typename T, Symmetric Symm>
auto spmmtv_sequential(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2
    ) -> void
{
    perform_sequential_checks<T>(matrix, x1, y1, y2);

    for (auto const& region : matrix.regions()) {
        std::visit(SpmmtvOneVec<T, Symm>{region, x1, y1, y2}, region.rows);
    }

    // Copy the contents of ‘y1’ to ‘y2’ if necessary.
    finish_sequential_one_vec<T, Symm>(y1, y2);
}

template auto spmmtv_sequential(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2
    ) -> void;

template auto spmmtv_sequential(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2
    ) -> void;

template auto spmmtv_sequential(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2
    ) -> void;

template auto spmmtv_sequential(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_sequential(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T> const y1,
        std::span<T> const y2
    ) -> void
{
    perform_sequential_checks<T>(matrix, x1, x2, y1, y2);

    for (auto const& region : matrix.regions()) {
        std::visit(SpmmtvTwoVecs<T, Symm>{region, x1, x2, y1, y2}, region.rows);
    }
}

template auto spmmtv_sequential(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2
    ) -> void;

template auto spmmtv_sequential(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2
    ) -> void;

template auto spmmtv_sequential(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2
    ) -> void;

template auto spmmtv_sequential(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::atomic_exclusive, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto const& region_indices = exec_data.region_indices(thread_id);

        // Iterate over all region indices.
        for (auto const& region_index : region_indices) {
            // Perform SpMMᵀV with this region.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvOneVecAtomic<T, Symm>{region, x1, y1, y2};
            std::visit(visitor, region.rows);
        }

        // Copy the contents of ‘y1’ to ‘y2’ if necessary.
        finish_parallel_one_vec<T, Symm>(y1, y2);
    }
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::atomic_exclusive, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::atomic_exclusive, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::atomic_exclusive, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::atomic_exclusive, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::atomic_planned_shared, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto const& region_indices = exec_data.region_indices(thread_id);

        // Iterate over all region indices.
        for (auto const& region_index : region_indices) {
            // Perform SpMMᵀV with this region.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvOneVecAtomic<T, Symm>{region, x1, y1, y2};
            std::visit(visitor, region.rows);
        }

        // Copy the contents of ‘y1’ to ‘y2’ if necessary.
        finish_parallel_one_vec<T, Symm>(y1, y2);
    }
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::atomic_planned_shared, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::atomic_planned_shared, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::atomic_planned_shared, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::atomic_planned_shared, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::atomic_shared, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto it = exec_data.get_iterator(thread_id);

        // Iterate over unfinished region indices.
        while (exec_data.update_iterator(it)) {
            // Perform SpMMᵀV with this region.
            auto const& region = matrix.regions()[*it];
            auto visitor = SpmmtvOneVecAtomic<T, Symm>{region, x1, y1, y2};
            std::visit(visitor, region.rows);
        }

        // Copy the contents of ‘y1’ to ‘y2’ if necessary.
        finish_parallel_one_vec<T, Symm>(y1, y2);
    }

    // Prepare the ‘ExecData’ object for the next potential SpMMᵀV iteration.
    exec_data.reset_status();
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::atomic_shared, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::atomic_shared, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::atomic_shared, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::atomic_shared, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::locks_exclusive, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto& region_indices = exec_data.region_indices(thread_id);
        auto& regions_data = exec_data.regions_data;

        while (!region_indices.empty()) {
            auto const region_index = region_indices.get_index();

            // If we fail to lock this region’s output row and column locks,
            // try the next region.
            if (!regions_data.try_lock_output(region_index)) {
                region_indices.next();
                continue;
            }

            // Perform SpMMᵀV with this region and release its output row and
            // column locks as soon as possible.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvOneVec<T, Symm>{region, x1, y1, y2};
            std::visit(visitor, region.rows);
            regions_data.unlock_output(region_index);

            // Remove this region’s index from this thread’s list of region
            // indices and try the next region.
            region_indices.erase_index();
        }

        // Copy the contents of ‘y1’ to ‘y2’ if necessary.
        finish_parallel_one_vec<T, Symm>(y1, y2);

        // Reset this thread’s ‘RegionIndices’ object to its initial state for
        // the next potential SpMMᵀV iteration.
        exec_data.reset_region_indices(thread_id);
    }
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_exclusive, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_exclusive, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_exclusive, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_exclusive, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::locks_exclusive, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, x2, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, x2, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto& region_indices = exec_data.region_indices(thread_id);
        auto& regions_data = exec_data.regions_data;

        while (!region_indices.empty()) {
            auto const region_index = region_indices.get_index();

            // If we fail to lock this region’s output row and column locks,
            // try the next region.
            if (!regions_data.try_lock_output(region_index)) {
                region_indices.next();
                continue;
            }

            // Perform SpMMᵀV with this region and release its output row and
            // column locks as soon as possible.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvTwoVecs<T, Symm>{region, x1, x2, y1, y2};
            std::visit(visitor, region.rows);
            regions_data.unlock_output(region_index);

            // Remove this region’s index from this thread’s list of region
            // indices and try the next region.
            region_indices.erase_index();
        }

        // Reset this thread’s ‘RegionIndices’ object to its initial state for
        // the next potential SpMMᵀV iteration.
        exec_data.reset_region_indices(thread_id);
    }
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_exclusive, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_exclusive, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_exclusive, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_exclusive, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::locks_planned_exclusive, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto const& region_indices = exec_data.region_indices(thread_id);
        auto& regions_data = exec_data.regions_data;

        // Iterate over all region indices.
        for (auto const& region_index : region_indices) {
            // Lock this region’s output row and column locks.
            regions_data.lock_output(region_index);

            // Perform SpMMᵀV with this region and release its output row and
            // column locks as soon as possible.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvOneVec<T, Symm>{region, x1, y1, y2};
            std::visit(visitor, region.rows);
            regions_data.unlock_output(region_index);
        }

        // Copy the contents of ‘y1’ to ‘y2’ if necessary.
        finish_parallel_one_vec<T, Symm>(y1, y2);
    }
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_planned_exclusive, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_planned_exclusive, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_planned_exclusive, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_planned_exclusive, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::locks_planned_exclusive, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, x2, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, x2, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto const& region_indices = exec_data.region_indices(thread_id);
        auto& regions_data = exec_data.regions_data;

        // Iterate over all region indices.
        for (auto const& region_index : region_indices) {
            // Lock this region’s output row and column locks.
            regions_data.lock_output(region_index);

            // Perform SpMMᵀV with this region and release its output row and
            // column locks as soon as possible.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvTwoVecs<T, Symm>{region, x1, x2, y1, y2};
            std::visit(visitor, region.rows);
            regions_data.unlock_output(region_index);
        }
    }
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_planned_exclusive, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_planned_exclusive, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_planned_exclusive, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_planned_exclusive, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::locks_planned_shared, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto const& region_indices = exec_data.region_indices(thread_id);
        auto& regions_data = exec_data.regions_data;

        // Iterate over all region indices.
        for (auto const& region_index : region_indices) {
            // Lock this region’s output row and column locks.
            regions_data.lock_output(region_index);

            // Perform SpMMᵀV with this region and release its output row and
            // column locks as soon as possible.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvOneVec<T, Symm>{region, x1, y1, y2};
            std::visit(visitor, region.rows);
            regions_data.unlock_output(region_index);
        }

        // Copy the contents of ‘y1’ to ‘y2’ if necessary.
        finish_parallel_one_vec<T, Symm>(y1, y2);
    }
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_planned_shared, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_planned_shared, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_planned_shared, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_planned_shared, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::locks_planned_shared, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, x2, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, x2, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto const& region_indices = exec_data.region_indices(thread_id);
        auto& regions_data = exec_data.regions_data;

        // Iterate over all region indices.
        for (auto const& region_index : region_indices) {
            // Lock this region’s output row and column locks.
            regions_data.lock_output(region_index);

            // Perform SpMMᵀV with this region and release its output row and
            // column locks as soon as possible.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvTwoVecs<T, Symm>{region, x1, x2, y1, y2};
            std::visit(visitor, region.rows);
            regions_data.unlock_output(region_index);
        }
    }
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_planned_shared, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_planned_shared, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_planned_shared, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_planned_shared, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::locks_shared, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto& region_indices = exec_data.region_indices(thread_id);
        auto& regions_data = exec_data.regions_data;

        while (!region_indices.empty()) {
            auto const region_index = region_indices.get_index();
            auto& region_lock = regions_data.get_lock(region_index);

            // If this region is already locked, try the next region.
            if (!region_lock.try_lock()) {
                region_indices.next();
                continue;
            }

            // If this region is finished, release its lock as soon as
            // possible, remove its index from this thread’s list of region
            // indices and try the next region.
            if (regions_data.is_done(region_index)) {
                region_lock.unlock();
                region_indices.erase_index();
                continue;
            }

            // If we fail to lock this region’s output row and column locks,
            // release its lock as soon as possible and try the next region.
            if (!regions_data.try_lock_output(region_index)) {
                region_lock.unlock();
                region_indices.next();
                continue;
            }

            // Mark this region as finished and release its lock as soon as
            // possible.
            regions_data.mark_done(region_index);
            region_lock.unlock();

            // Perform SpMMᵀV with this region and release its output row and
            // column locks as soon as possible.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvOneVec<T, Symm>{region, x1, y1, y2};
            std::visit(visitor, region.rows);
            regions_data.unlock_output(region_index);

            // Remove this region’s index from this thread’s list of region
            // indices and try the next region.
            region_indices.erase_index();
        }

        // Copy the contents of ‘y1’ to ‘y2’ if necessary.
        finish_parallel_one_vec<T, Symm>(y1, y2);

        // Reset this thread’s ‘RegionIndices’ object to its initial state for
        // the next potential SpMMᵀV iteration.
        exec_data.reset_region_indices(thread_id);
    }

    // Mark all regions as unfinished for the next potential SpMMᵀV iteration.
    exec_data.regions_data.reset_status();
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_shared, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_shared, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_shared, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_shared, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::locks_shared, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, x2, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, x2, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto& region_indices = exec_data.region_indices(thread_id);
        auto& regions_data = exec_data.regions_data;

        while (!region_indices.empty()) {
            auto const region_index = region_indices.get_index();
            auto& region_lock = regions_data.get_lock(region_index);

            // If this region is already locked, try the next region.
            if (!region_lock.try_lock()) {
                region_indices.next();
                continue;
            }

            // If this region is finished, release its lock as soon as
            // possible, remove its index from this thread’s list of region
            // indices and try the next region.
            if (regions_data.is_done(region_index)) {
                region_lock.unlock();
                region_indices.erase_index();
                continue;
            }

            // If we fail to lock this region’s output row and column locks,
            // release its lock as soon as possible and try the next region.
            if (!regions_data.try_lock_output(region_index)) {
                region_lock.unlock();
                region_indices.next();
                continue;
            }

            // Mark this region as finished and release its lock as soon as
            // possible.
            regions_data.mark_done(region_index);
            region_lock.unlock();

            // Perform SpMMᵀV with this region and release its output row and
            // column locks as soon as possible.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvTwoVecs<T, Symm>{region, x1, x2, y1, y2};
            std::visit(visitor, region.rows);
            regions_data.unlock_output(region_index);

            // Remove this region’s index from this thread’s list of region
            // indices and try the next region.
            region_indices.erase_index();
        }

        // Reset this thread’s ‘RegionIndices’ object to its initial state for
        // the next potential SpMMᵀV iteration.
        exec_data.reset_region_indices(thread_id);
    }

    // Mark all regions as unfinished for the next potential SpMMᵀV iteration.
    exec_data.regions_data.reset_status();
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_shared, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::locks_shared, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_shared, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::locks_shared, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::per_thread_exclusive, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto const& region_indices = exec_data.region_indices(thread_id);

        // Iterate over all region indices.
        for (auto const& region_index : region_indices) {
            // Perform SpMMᵀV with this region.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvOneVec<T, Symm>{region, x1, exec_data.per_thread_vectors, thread_id};
            std::visit(visitor, region.rows);
        }

        // Merge and reset per-thread vectors.
        merge_reset_per_thread_vectors_one_vec(exec_data.per_thread_vectors, y1, y2);
    }
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::per_thread_exclusive, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::per_thread_exclusive, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::per_thread_exclusive, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::per_thread_exclusive, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::per_thread_planned_shared, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto const& region_indices = exec_data.region_indices(thread_id);

        // Iterate over all region indices.
        for (auto const& region_index : region_indices) {
            // Perform SpMMᵀV with this region.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvOneVec<T, Symm>{region, x1, exec_data.per_thread_vectors, thread_id};
            std::visit(visitor, region.rows);
        }

        // Merge and reset per-thread vectors.
        merge_reset_per_thread_vectors_one_vec(exec_data.per_thread_vectors, y1, y2);
    }
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::per_thread_planned_shared, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::per_thread_planned_shared, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::per_thread_planned_shared, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::per_thread_planned_shared, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        ExecData<ExecMode::per_thread_shared, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_checks<T>(matrix, x1, y1, y2, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto it = exec_data.get_iterator(thread_id);

        // Iterate over unfinished region indices.
        while (exec_data.update_iterator(it)) {
            // Perform SpMMᵀV with this region.
            auto const& region = matrix.regions()[*it];
            auto visitor = SpmmtvOneVec<T, Symm>{region, x1, exec_data.per_thread_vectors, thread_id};
            std::visit(visitor, region.rows);
        }

        // Merge and reset per-thread vectors.
        merge_reset_per_thread_vectors_one_vec(exec_data.per_thread_vectors, y1, y2);
    }

    // Prepare the ‘ExecData’ object for the next potential SpMMᵀV iteration.
    exec_data.reset_status();
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::per_thread_shared, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        ExecData<ExecMode::per_thread_shared, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::per_thread_shared, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        ExecData<ExecMode::per_thread_shared, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::atomic_shared, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_logged_checks<T>(matrix, x1, y1, y2, region_indices_logs, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, region_indices_logs, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto it = exec_data.get_iterator(thread_id);
        auto& region_indices_log = region_indices_logs.get(thread_id);

        // Iterate over unfinished region indices.
        while (exec_data.update_iterator(it)) {
            // Perform SpMMᵀV with this region.
            auto const& region = matrix.regions()[*it];
            auto visitor = SpmmtvOneVecAtomic<T, Symm>{region, x1, y1, y2};
            std::visit(visitor, region.rows);

            // Record the index of the finished region.
            region_indices_log.push_back(*it);
        }

        // Copy the contents of ‘y1’ to ‘y2’ if necessary.
        finish_parallel_one_vec<T, Symm>(y1, y2);
    }

    // Prepare the ‘ExecData’ object for the next potential SpMMᵀV iteration.
    exec_data.reset_status();
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::atomic_shared, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::atomic_shared, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::atomic_shared, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::atomic_shared, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_exclusive, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_logged_checks<T>(matrix, x1, y1, y2, region_indices_logs, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, region_indices_logs, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto& region_indices = exec_data.region_indices(thread_id);
        auto& region_indices_log = region_indices_logs.get(thread_id);
        auto& regions_data = exec_data.regions_data;

        while (!region_indices.empty()) {
            auto const region_index = region_indices.get_index();

            // If we fail to lock this region’s output row and column locks,
            // try the next region.
            if (!regions_data.try_lock_output(region_index)) {
                region_indices.next();
                continue;
            }

            // Perform SpMMᵀV with this region and release its output row and
            // column locks as soon as possible.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvOneVec<T, Symm>{region, x1, y1, y2};
            std::visit(visitor, region.rows);
            regions_data.unlock_output(region_index);

            // Record the index of the finished region.
            region_indices_log.push_back(region_index);

            // Remove this region’s index from this thread’s list of region
            // indices and try the next region.
            region_indices.erase_index();
        }

        // Copy the contents of ‘y1’ to ‘y2’ if necessary.
        finish_parallel_one_vec<T, Symm>(y1, y2);

        // Reset this thread’s ‘RegionIndices’ object to its initial state for
        // the next potential SpMMᵀV iteration.
        exec_data.reset_region_indices(thread_id);
    }
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_exclusive, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_exclusive, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_exclusive, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_exclusive, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T> const y1,
        std::span<T> const y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_exclusive, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_logged_checks<T>(matrix, x1, x2, y1, y2, region_indices_logs, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, x2, y1, y2, region_indices_logs, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto& region_indices = exec_data.region_indices(thread_id);
        auto& region_indices_log = region_indices_logs.get(thread_id);
        auto& regions_data = exec_data.regions_data;

        while (!region_indices.empty()) {
            auto const region_index = region_indices.get_index();

            // If we fail to lock this region’s output row and column locks,
            // try the next region.
            if (!regions_data.try_lock_output(region_index)) {
                region_indices.next();
                continue;
            }

            // Perform SpMMᵀV with this region and release its output row and
            // column locks as soon as possible.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvTwoVecs<T, Symm>{region, x1, x2, y1, y2};
            std::visit(visitor, region.rows);
            regions_data.unlock_output(region_index);

            // Record the index of the finished region.
            region_indices_log.push_back(region_index);

            // Remove this region’s index from this thread’s list of region
            // indices and try the next region.
            region_indices.erase_index();
        }

        // Reset this thread’s ‘RegionIndices’ object to its initial state for
        // the next potential SpMMᵀV iteration.
        exec_data.reset_region_indices(thread_id);
    }
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_exclusive, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_exclusive, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_exclusive, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_exclusive, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_shared, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_logged_checks<T>(matrix, x1, y1, y2, region_indices_logs, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, region_indices_logs, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto& region_indices = exec_data.region_indices(thread_id);
        auto& region_indices_log = region_indices_logs.get(thread_id);
        auto& regions_data = exec_data.regions_data;

        while (!region_indices.empty()) {
            auto const region_index = region_indices.get_index();
            auto& region_lock = regions_data.get_lock(region_index);

            // If this region is already locked, try the next region.
            if (!region_lock.try_lock()) {
                region_indices.next();
                continue;
            }

            // If this region is finished, release its lock as soon as
            // possible, remove its index from this thread’s list of region
            // indices and try the next region.
            if (regions_data.is_done(region_index)) {
                region_lock.unlock();
                region_indices.erase_index();
                continue;
            }

            // If we fail to lock this region’s output row and column locks,
            // release its lock as soon as possible and try the next region.
            if (!regions_data.try_lock_output(region_index)) {
                region_lock.unlock();
                region_indices.next();
                continue;
            }

            // Mark this region as finished and release its lock as soon as
            // possible.
            regions_data.mark_done(region_index);
            region_lock.unlock();

            // Perform SpMMᵀV with this region and release its output row and
            // column locks as soon as possible.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvOneVec<T, Symm>{region, x1, y1, y2};
            std::visit(visitor, region.rows);
            regions_data.unlock_output(region_index);

            // Record the index of the finished region.
            region_indices_log.push_back(region_index);

            // Remove this region’s index from this thread’s list of region
            // indices and try the next region.
            region_indices.erase_index();
        }

        // Copy the contents of ‘y1’ to ‘y2’ if necessary.
        finish_parallel_one_vec<T, Symm>(y1, y2);

        // Reset this thread’s ‘RegionIndices’ object to its initial state for
        // the next potential SpMMᵀV iteration.
        exec_data.reset_region_indices(thread_id);
    }

    // Mark all regions as unfinished for the next potential SpMMᵀV iteration.
    exec_data.regions_data.reset_status();
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_shared, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_shared, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_shared, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_shared, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T> const y1,
        std::span<T> const y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_shared, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_logged_checks<T>(matrix, x1, x2, y1, y2, region_indices_logs, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, x2, y1, y2, region_indices_logs, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto& region_indices = exec_data.region_indices(thread_id);
        auto& region_indices_log = region_indices_logs.get(thread_id);
        auto& regions_data = exec_data.regions_data;

        while (!region_indices.empty()) {
            auto const region_index = region_indices.get_index();
            auto& region_lock = regions_data.get_lock(region_index);

            // If this region is already locked, try the next region.
            if (!region_lock.try_lock()) {
                region_indices.next();
                continue;
            }

            // If this region is finished, release its lock as soon as
            // possible, remove its index from this thread’s list of region
            // indices and try the next region.
            if (regions_data.is_done(region_index)) {
                region_lock.unlock();
                region_indices.erase_index();
                continue;
            }

            // If we fail to lock this region’s output row and column locks,
            // release its lock as soon as possible and try the next region.
            if (!regions_data.try_lock_output(region_index)) {
                region_lock.unlock();
                region_indices.next();
                continue;
            }

            // Mark this region as finished and release its lock as soon as
            // possible.
            regions_data.mark_done(region_index);
            region_lock.unlock();

            // Perform SpMMᵀV with this region and release its output row and
            // column locks as soon as possible.
            auto const& region = matrix.regions()[region_index];
            auto visitor = SpmmtvTwoVecs<T, Symm>{region, x1, x2, y1, y2};
            std::visit(visitor, region.rows);
            regions_data.unlock_output(region_index);

            // Record the index of the finished region.
            region_indices_log.push_back(region_index);

            // Remove this region’s index from this thread’s list of region
            // indices and try the next region.
            region_indices.erase_index();
        }

        // Reset this thread’s ‘RegionIndices’ object to its initial state for
        // the next potential SpMMᵀV iteration.
        exec_data.reset_region_indices(thread_id);
    }

    // Mark all regions as unfinished for the next potential SpMMᵀV iteration.
    exec_data.regions_data.reset_status();
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_shared, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float const> x2,
        std::span<float> y1,
        std::span<float> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_shared, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_shared, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double const> x2,
        std::span<double> y1,
        std::span<double> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::locks_shared, double, Symmetric::yes>& exec_data
    ) -> void;

template<typename T, Symmetric Symm>
auto spmmtv_parallel(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::per_thread_shared, T, Symm>& exec_data
    ) -> void
{
    perform_parallel_logged_checks<T>(matrix, x1, y1, y2, region_indices_logs, exec_data);

    #pragma omp parallel default(none) shared(matrix, x1, y1, y2, region_indices_logs, exec_data)
    {
        auto const thread_id = omp_get_thread_num();
        auto it = exec_data.get_iterator(thread_id);
        auto& region_indices_log = region_indices_logs.get(thread_id);

        // Iterate over unfinished region indices.
        while (exec_data.update_iterator(it)) {
            // Perform SpMMᵀV with this region.
            auto const& region = matrix.regions()[*it];
            auto visitor = SpmmtvOneVec<T, Symm>{region, x1, exec_data.per_thread_vectors, thread_id};
            std::visit(visitor, region.rows);

            // Record the index of the finished region.
            region_indices_log.push_back(*it);
        }

        // Merge and reset per-thread vectors.
        merge_reset_per_thread_vectors_one_vec(exec_data.per_thread_vectors, y1, y2);
    }

    // Prepare the ‘ExecData’ object for the next potential SpMMᵀV iteration.
    exec_data.reset_status();
}

template auto spmmtv_parallel(
        Matrix<float, Symmetric::no> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::per_thread_shared, float, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<float, Symmetric::yes> const& matrix,
        std::span<float const> x1,
        std::span<float> y1,
        std::span<float> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::per_thread_shared, float, Symmetric::yes>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::no> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::per_thread_shared, double, Symmetric::no>& exec_data
    ) -> void;

template auto spmmtv_parallel(
        Matrix<double, Symmetric::yes> const& matrix,
        std::span<double const> x1,
        std::span<double> y1,
        std::span<double> y2,
        RegionIndicesLogs& region_indices_logs,
        ExecData<ExecMode::per_thread_shared, double, Symmetric::yes>& exec_data
    ) -> void;

namespace {

template<typename T, Symmetric Symm>
SpmmtvOneVec<T, Symm>::SpmmtvOneVec(
        Region<T> const& region,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2
    ) noexcept(ndebug)
    : region_{region},
      x1_{x1},
      y1_{y1},
      y2_{y2}
{
    assert(x1.size() == y1.size());
    assert(x1.size() == y2.size());
}

template<typename T, Symmetric Symm>
SpmmtvOneVec<T, Symm>::SpmmtvOneVec(
        Region<T> const& region,
        std::span<T const> const x1,
        PerThreadVectors<T, Symmetric::no>& per_thread_vectors,
        int const thread_id
    ) : region_{region},
        x1_{x1},
        y1_{per_thread_vectors.y1[static_cast<std::size_t>(thread_id)]},
        y2_{per_thread_vectors.y2[static_cast<std::size_t>(thread_id)]}
{
    assert(thread_id >= 0);
    #ifndef NDEBUG
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);
    #endif
    assert(x1.size() == per_thread_vectors.y1[thread_id_uz].size());
    assert(x1.size() == per_thread_vectors.y2[thread_id_uz].size());
}

template<typename T, Symmetric Symm>
SpmmtvOneVec<T, Symm>::SpmmtvOneVec(
        Region<T> const& region,
        std::span<T const> const x1,
        PerThreadVectors<T, Symmetric::yes>& per_thread_vectors,
        int const thread_id
    ) : region_{region},
        x1_{x1},
        y1_{per_thread_vectors.y1[static_cast<std::size_t>(thread_id)]}
{
    assert(thread_id >= 0);
    #ifndef NDEBUG
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);
    #endif
    assert(x1.size() == per_thread_vectors.y1[thread_id_uz].size());
}

template<typename T, Symmetric Symm>
auto SpmmtvOneVec<T, Symm>::operator()(info::RowIndex* const row_indices) -> void
{
    assert(row_indices != nullptr);

    if constexpr (Symm == Symmetric::no) {
        spmmtv_coo(
                row_indices,
                this->region_.columns,
                this->region_.elements,
                this->region_.element_count,
                this->x1_.data(),
                this->y1_.data(),
                this->y2_.data(),
                this->region_.row_begin,
                this->region_.column_begin
            );

    } else if constexpr (Symm == Symmetric::yes) {
        auto const row_end = this->region_.row_begin + this->region_.row_count;
        if (row_end > this->region_.column_begin) {
            spmmtv_coo_symmetric(
                    row_indices,
                    this->region_.columns,
                    this->region_.elements,
                    this->region_.element_count,
                    this->x1_.data(),
                    this->y1_.data(),
                    this->region_.row_begin,
                    this->region_.column_begin
                );
        } else {                // row_end <= this->region_.column_begin
            spmmtv_coo_symmetric_nondiagonal(
                    row_indices,
                    this->region_.columns,
                    this->region_.elements,
                    this->region_.element_count,
                    this->x1_.data(),
                    this->y1_.data(),
                    this->region_.row_begin,
                    this->region_.column_begin
                );
        }
    }
}

template<typename T, Symmetric Symm>
auto SpmmtvOneVec<T, Symm>::operator()(info::RowPointer* const row_pointers) -> void
{
    assert(row_pointers != nullptr);

    if constexpr (Symm == Symmetric::no) {
        spmmtv_csr(
                row_pointers,
                this->region_.columns,
                this->region_.elements,
                this->region_.row_count,
                this->x1_.data(),
                this->y1_.data(),
                this->y2_.data(),
                this->region_.row_begin,
                this->region_.column_begin
            );

    } else if constexpr (Symm == Symmetric::yes) {
        auto const row_end = this->region_.row_begin + this->region_.row_count;
        if (row_end > this->region_.column_begin) {
            spmmtv_csr_symmetric(
                    row_pointers,
                    this->region_.columns,
                    this->region_.elements,
                    this->region_.row_count,
                    this->x1_.data(),
                    this->y1_.data(),
                    this->region_.row_begin,
                    this->region_.column_begin
                );
        } else {                // row_end <= this->region_.column_begin
            spmmtv_csr_symmetric_nondiagonal(
                    row_pointers,
                    this->region_.columns,
                    this->region_.elements,
                    this->region_.row_count,
                    this->x1_.data(),
                    this->y1_.data(),
                    this->region_.row_begin,
                    this->region_.column_begin
                );
        }
    }
}

template<typename T, Symmetric Symm>
SpmmtvTwoVecs<T, Symm>::SpmmtvTwoVecs(
        Region<T> const& region,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T> const y1,
        std::span<T> const y2
    ) noexcept(ndebug)
    : region_{region},
      x1_{x1},
      x2_{x2},
      y1_{y1},
      y2_{y2}
{
    if constexpr (Symm == Symmetric::no) {
        assert(x1.size() == y2.size());
        assert(x2.size() == y1.size());

    } else if constexpr (Symm == Symmetric::yes) {
        assert(x1.size() == x2.size());
        assert(x1.size() == y1.size());
        assert(x1.size() == y2.size());
    }
}

template<typename T, Symmetric Symm>
auto SpmmtvTwoVecs<T, Symm>::operator()(info::RowIndex* const row_indices) -> void
{
    assert(row_indices != nullptr);

    if constexpr (Symm == Symmetric::no) {
        spmmtv_coo(
                row_indices,
                this->region_.columns,
                this->region_.elements,
                this->region_.element_count,
                this->x1_.data(),
                this->x2_.data(),
                this->y1_.data(),
                this->y2_.data(),
                this->region_.row_begin,
                this->region_.column_begin
            );

    } else if constexpr (Symm == Symmetric::yes) {
        auto const row_end = this->region_.row_begin + this->region_.row_count;
        if (row_end > this->region_.column_begin) {
            spmmtv_coo_symmetric(
                    row_indices,
                    this->region_.columns,
                    this->region_.elements,
                    this->region_.element_count,
                    this->x1_.data(),
                    this->x2_.data(),
                    this->y1_.data(),
                    this->y2_.data(),
                    this->region_.row_begin,
                    this->region_.column_begin
                );
        } else {                // row_end <= this->region_.column_begin
            spmmtv_coo_symmetric_nondiagonal(
                    row_indices,
                    this->region_.columns,
                    this->region_.elements,
                    this->region_.element_count,
                    this->x1_.data(),
                    this->x2_.data(),
                    this->y1_.data(),
                    this->y2_.data(),
                    this->region_.row_begin,
                    this->region_.column_begin
                );
        }
    }
}

template<typename T, Symmetric Symm>
auto SpmmtvTwoVecs<T, Symm>::operator()(info::RowPointer* const row_pointers) -> void
{
    assert(row_pointers != nullptr);

    if constexpr (Symm == Symmetric::no) {
        spmmtv_csr(
                row_pointers,
                this->region_.columns,
                this->region_.elements,
                this->region_.row_count,
                this->x1_.data(),
                this->x2_.data(),
                this->y1_.data(),
                this->y2_.data(),
                this->region_.row_begin,
                this->region_.column_begin
            );

    } else if constexpr (Symm == Symmetric::yes) {
        auto const row_end = this->region_.row_begin + this->region_.row_count;
        if (row_end > this->region_.column_begin) {
            spmmtv_csr_symmetric(
                    row_pointers,
                    this->region_.columns,
                    this->region_.elements,
                    this->region_.row_count,
                    this->x1_.data(),
                    this->x2_.data(),
                    this->y1_.data(),
                    this->y2_.data(),
                    this->region_.row_begin,
                    this->region_.column_begin
                );
        } else {                // row_end <= this->region_.column_begin
            spmmtv_csr_symmetric_nondiagonal(
                    row_pointers,
                    this->region_.columns,
                    this->region_.elements,
                    this->region_.row_count,
                    this->x1_.data(),
                    this->x2_.data(),
                    this->y1_.data(),
                    this->y2_.data(),
                    this->region_.row_begin,
                    this->region_.column_begin
                );
        }
    }
}

template<typename T, Symmetric Symm>
SpmmtvOneVecAtomic<T, Symm>::SpmmtvOneVecAtomic(
        Region<T> const& region,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2
    ) noexcept(ndebug)
    : region_{region},
      x1_{x1},
      y1_{y1},
      y2_{y2}
{
    assert(x1.size() == y1.size());
    assert(x1.size() == y2.size());
}

template<typename T, Symmetric Symm>
auto SpmmtvOneVecAtomic<T, Symm>::operator()(info::RowIndex* const row_indices) -> void
{
    assert(row_indices != nullptr);

    if constexpr (Symm == Symmetric::no) {
        spmmtv_coo_atomic(
                row_indices,
                this->region_.columns,
                this->region_.elements,
                this->region_.element_count,
                this->x1_.data(),
                this->y1_.data(),
                this->y2_.data(),
                this->region_.row_begin,
                this->region_.column_begin
            );

    } else if constexpr (Symm == Symmetric::yes) {
        auto const row_end = this->region_.row_begin + this->region_.row_count;
        if (row_end > this->region_.column_begin) {
            spmmtv_coo_symmetric_atomic(
                    row_indices,
                    this->region_.columns,
                    this->region_.elements,
                    this->region_.element_count,
                    this->x1_.data(),
                    this->y1_.data(),
                    this->region_.row_begin,
                    this->region_.column_begin
                );
        } else {                // row_end <= this->region_.column_begin
            spmmtv_coo_symmetric_nondiagonal_atomic(
                    row_indices,
                    this->region_.columns,
                    this->region_.elements,
                    this->region_.element_count,
                    this->x1_.data(),
                    this->y1_.data(),
                    this->region_.row_begin,
                    this->region_.column_begin
                );
        }
    }
}

template<typename T, Symmetric Symm>
auto SpmmtvOneVecAtomic<T, Symm>::operator()(info::RowPointer* const row_pointers) -> void
{
    assert(row_pointers != nullptr);

    if constexpr (Symm == Symmetric::no) {
        spmmtv_csr_atomic(
                row_pointers,
                this->region_.columns,
                this->region_.elements,
                this->region_.row_count,
                this->x1_.data(),
                this->y1_.data(),
                this->y2_.data(),
                this->region_.row_begin,
                this->region_.column_begin
            );

    } else if constexpr (Symm == Symmetric::yes) {
        auto const row_end = this->region_.row_begin + this->region_.row_count;
        if (row_end > this->region_.column_begin) {
            spmmtv_csr_symmetric_atomic(
                    row_pointers,
                    this->region_.columns,
                    this->region_.elements,
                    this->region_.row_count,
                    this->x1_.data(),
                    this->y1_.data(),
                    this->region_.row_begin,
                    this->region_.column_begin
                );
        } else {                // row_end <= this->region_.column_begin
            spmmtv_csr_symmetric_nondiagonal_atomic(
                    row_pointers,
                    this->region_.columns,
                    this->region_.elements,
                    this->region_.row_count,
                    this->x1_.data(),
                    this->y1_.data(),
                    this->region_.row_begin,
                    this->region_.column_begin
                );
        }
    }
}

template<typename... Args>
auto do_not_alias(Args... args) noexcept -> bool
{
    auto const ps = std::array<void const*, sizeof...(args)>{args...};
    for (auto i = 0_uz; i < ps.size() - 1; ++i) {
        for (auto j = i + 1; j < ps.size(); ++j) {
            if (ps[i] == ps[j]) {
                return false;
            }
        }
    }
    return true;
}

template<typename T, Symmetric Symm>
auto finish_sequential_one_vec(std::span<T const> const y1, std::span<T> const y2) -> void
{
    if constexpr (Symm == Symmetric::yes) {
        assert(y1.size() == y2.size());
        std::copy(y1.begin(), y1.end(), y2.begin());
    }
}

template<typename T, Symmetric Symm>
auto finish_parallel_one_vec(std::span<T const> const y1, std::span<T> const y2) -> void
{
    if constexpr (Symm == Symmetric::yes) {
        assert(y1.size() == y2.size());

        #pragma omp barrier
        #pragma omp for schedule(static) nowait
        for (auto i = 0_uz; i < y1.size(); ++i) {
            y2[i] = y1[i];
        }
    }
}

template<typename T>
auto merge_reset_per_thread_vectors_one_vec(
        PerThreadVectors<T, Symmetric::no>& per_thread_vectors, std::span<T> const y1, std::span<T> const y2
    ) -> void
{
    #pragma omp barrier
    #pragma omp for schedule(static) nowait
    for (auto i = 0_uz; i < y1.size(); ++i) {
        auto y1_sum = T{};
        auto y2_sum = T{};

        for (auto j = 0_uz; j < static_cast<std::size_t>(omp_get_num_threads()); ++j) {
            y1_sum += per_thread_vectors.y1[j][i];
            y2_sum += per_thread_vectors.y2[j][i];

            // Zero out the per-thread vectors for next potential SpMMᵀV
            // iteration.
            per_thread_vectors.y1[j][i] = 0;
            per_thread_vectors.y2[j][i] = 0;
        }

        y1[i] = y1_sum;
        y2[i] = y2_sum;
    }
}

template<typename T>
auto merge_reset_per_thread_vectors_one_vec(
        PerThreadVectors<T, Symmetric::yes>& per_thread_vectors, std::span<T> const y1, std::span<T> const y2
    ) -> void
{
    #pragma omp barrier
    #pragma omp for schedule(static) nowait
    for (auto i = 0_uz; i < y1.size(); ++i) {
        auto y1_sum = T{};

        for (auto j = 0_uz; j < static_cast<std::size_t>(omp_get_num_threads()); ++j) {
            y1_sum += per_thread_vectors.y1[j][i];

            // Zero out the per-thread vector for next potential SpMMᵀV
            // iteration.
            per_thread_vectors.y1[j][i] = 0;
        }

        y1[i] = y1_sum;
        y2[i] = y1_sum;
    }
}

template<typename T, Symmetric Symm>
auto perform_sequential_checks(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const y1,
        std::span<T const> const y2
    ) -> void
{
    assert(matrix.column_count() == matrix.row_count());
    assert(matrix.column_count() == x1.size());
    assert(matrix.column_count() == y1.size());
    assert(matrix.column_count() == y2.size());
}

template<typename T, Symmetric Symm>
auto perform_sequential_checks(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T const> const y1,
        std::span<T const> const y2
    ) -> void
{
    if constexpr (Symm == Symmetric::no) {
        assert(matrix.column_count() == x1.size());
        assert(matrix.row_count() == y1.size());
        assert(matrix.row_count() == x2.size());
        assert(matrix.column_count() == y2.size());

    } else if constexpr (Symm == Symmetric::yes) {
        assert(matrix.column_count() == matrix.row_count());
        assert(matrix.column_count() == x1.size());
        assert(matrix.column_count() == x2.size());
        assert(matrix.column_count() == y1.size());
        assert(matrix.column_count() == y2.size());
    }
}

template<typename T, Symmetric Symm, ExecMode EM>
auto perform_parallel_checks(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const y1,
        std::span<T const> const y2,
        ExecData<EM, T, Symm> const& exec_data
    ) -> void
{
    perform_sequential_checks(matrix, x1, y1, y2);

    assert(exec_data.region_count() == matrix.region_count());
    assert(exec_data.thread_count() == omp_get_max_threads());

    if constexpr (EM == ExecMode::per_thread_exclusive
                  || EM == ExecMode::per_thread_planned_shared
                  || EM == ExecMode::per_thread_shared) {
        assert(exec_data.per_thread_vectors.thread_count() == omp_get_max_threads());
    }
}

template<typename T, Symmetric Symm, ExecMode EM>
auto perform_parallel_checks(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T const> const y1,
        std::span<T const> const y2,
        ExecData<EM, T, Symm> const& exec_data
    ) -> void
{
    perform_sequential_checks(matrix, x1, x2, y1, y2);
    assert(exec_data.region_count() == matrix.region_count());
    assert(exec_data.thread_count() == omp_get_max_threads());
}

template<typename T, Symmetric Symm, ExecMode EM>
auto perform_parallel_logged_checks(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const y1,
        std::span<T const> const y2,
        RegionIndicesLogs const& region_indices_logs,
        ExecData<EM, T, Symm> const& exec_data
    ) -> void
{
    perform_parallel_checks(matrix, x1, y1, y2, exec_data);
    assert(region_indices_logs.thread_count() == omp_get_max_threads());
}

template<typename T, Symmetric Symm, ExecMode EM>
auto perform_parallel_logged_checks(
        Matrix<T, Symm> const& matrix,
        std::span<T const> const x1,
        std::span<T const> const x2,
        std::span<T const> const y1,
        std::span<T const> const y2,
        RegionIndicesLogs const& region_indices_logs,
        ExecData<EM, T, Symm> const& exec_data
    ) -> void
{
    perform_parallel_checks(matrix, x1, x2, y1, y2, exec_data);
    assert(region_indices_logs.thread_count() == omp_get_max_threads());
}

template<typename T>
auto spmmtv_coo(
        info::RowIndex const* __restrict const rows,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const element_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        T* __restrict const y2,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(rows, columns, elements, x1, y1, y2));

    for (auto i = 0_uz; i < element_count; ++i) {
        auto const row = row_offset + rows[i];
        auto const column = column_offset + columns[i];
        y1[row] += elements[i] * x1[column];
        y2[column] += elements[i] * x1[row];
    }
}

template<typename T>
auto spmmtv_coo(
        info::RowIndex const* __restrict const rows,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const element_count,
        T const* __restrict const x1,
        T const* __restrict const x2,
        T* __restrict const y1,
        T* __restrict const y2,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(rows, columns, elements, x1, x2, y1, y2));

    for (auto i = 0_uz; i < element_count; ++i) {
        auto const row = row_offset + rows[i];
        auto const column = column_offset + columns[i];
        y1[row] += elements[i] * x1[column];
        y2[column] += elements[i] * x2[row];
    }
}

template<typename T>
auto spmmtv_coo_atomic(
        info::RowIndex const* __restrict const rows,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const element_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        T* __restrict const y2,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(rows, columns, elements, x1, y1, y2));

    for (auto i = 0_uz; i < element_count; ++i) {
        auto const row = row_offset + rows[i];
        auto const column = column_offset + columns[i];
        #pragma omp atomic update
        y1[row] += elements[i] * x1[column];
        #pragma omp atomic update
        y2[column] += elements[i] * x1[row];
    }
}

template<typename T>
auto spmmtv_coo_symmetric(
        info::RowIndex const* __restrict const rows,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const element_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(rows, columns, elements, x1, y1));

    for (auto i = 0_uz; i < element_count; ++i) {
        auto const row = row_offset + rows[i];
        auto const column = column_offset + columns[i];
        y1[row] += elements[i] * x1[column];
        if (row != column) {
            y1[column] += elements[i] * x1[row];
        }
    }
}

template<typename T>
auto spmmtv_coo_symmetric(
        info::RowIndex const* __restrict const rows,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const element_count,
        T const* __restrict const x1,
        T const* __restrict const x2,
        T* __restrict const y1,
        T* __restrict const y2,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(rows, columns, elements, x1, x2, y1, y2));

    for (auto i = 0_uz; i < element_count; ++i) {
        auto const row = row_offset + rows[i];
        auto const column = column_offset + columns[i];
        y1[row] += elements[i] * x1[column];
        y2[row] += elements[i] * x2[column];
        if (row != column) {
            y1[column] += elements[i] * x1[row];
            y2[column] += elements[i] * x2[row];
        }
    }
}

template<typename T>
auto spmmtv_coo_symmetric_atomic(
        info::RowIndex const* __restrict const rows,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const element_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(rows, columns, elements, x1, y1));

    for (auto i = 0_uz; i < element_count; ++i) {
        auto const row = row_offset + rows[i];
        auto const column = column_offset + columns[i];
        #pragma omp atomic update
        y1[row] += elements[i] * x1[column];
        if (row != column) {
            #pragma omp atomic update
            y1[column] += elements[i] * x1[row];
        }
    }
}

template<typename T>
auto spmmtv_coo_symmetric_nondiagonal(
        info::RowIndex const* __restrict const rows,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const element_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(rows, columns, elements, x1, y1));

    for (auto i = 0_uz; i < element_count; ++i) {
        auto const row = row_offset + rows[i];
        auto const column = column_offset + columns[i];
        y1[row] += elements[i] * x1[column];
        y1[column] += elements[i] * x1[row];
    }
}

template<typename T>
auto spmmtv_coo_symmetric_nondiagonal(
        info::RowIndex const* __restrict const rows,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const element_count,
        T const* __restrict const x1,
        T const* __restrict const x2,
        T* __restrict const y1,
        T* __restrict const y2,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(rows, columns, elements, x1, x2, y1, y2));

    for (auto i = 0_uz; i < element_count; ++i) {
        auto const row = row_offset + rows[i];
        auto const column = column_offset + columns[i];
        y1[row] += elements[i] * x1[column];
        y2[row] += elements[i] * x2[column];
        y1[column] += elements[i] * x1[row];
        y2[column] += elements[i] * x2[row];
    }
}

template<typename T>
auto spmmtv_coo_symmetric_nondiagonal_atomic(
        info::RowIndex const* __restrict const rows,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const element_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(rows, columns, elements, x1, y1));

    for (auto i = 0_uz; i < element_count; ++i) {
        auto const row = row_offset + rows[i];
        auto const column = column_offset + columns[i];
        #pragma omp atomic update
        y1[row] += elements[i] * x1[column];
        #pragma omp atomic update
        y1[column] += elements[i] * x1[row];
    }
}

template<typename T>
auto spmmtv_csr(
        info::RowPointer const* __restrict const row_pointers,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        T* __restrict const y2,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, y1, y2));

    for (auto region_row = 0_uz; region_row < row_count; ++region_row) {
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto const row = row_offset + region_row;
        auto column = column_offset + columns[i];
        auto y1_element = elements[i] * x1[column];
        y2[column] += elements[i] * x1[row];

        for (++i; i < row_end; ++i) {
            column = column_offset + columns[i];
            y1_element += elements[i] * x1[column];
            y2[column] += elements[i] * x1[row];
        }

        y1[row] += y1_element;
    }
}

template<typename T>
auto spmmtv_csr(
        info::RowPointer const* __restrict const row_pointers,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T const* __restrict const x2,
        T* __restrict const y1,
        T* __restrict const y2,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, x2, y1, y2));

    for (auto region_row = 0_uz; region_row < row_count; ++region_row) {
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto const row = row_offset + region_row;
        auto column = column_offset + columns[i];
        auto y1_element = elements[i] * x1[column];
        y2[column] += elements[i] * x2[row];

        for (++i; i < row_end; ++i) {
            column = column_offset + columns[i];
            y1_element += elements[i] * x1[column];
            y2[column] += elements[i] * x2[row];
        }

        y1[row] += y1_element;
    }
}

template<typename T>
auto spmmtv_csr_atomic(
        info::RowPointer const* __restrict const row_pointers,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        T* __restrict const y2,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, y1, y2));

    for (auto region_row = 0_uz; region_row < row_count; ++region_row) {
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto const row = row_offset + region_row;
        auto column = column_offset + columns[i];
        auto y1_element = elements[i] * x1[column];
        #pragma omp atomic update
        y2[column] += elements[i] * x1[row];

        for (++i; i < row_end; ++i) {
            column = column_offset + columns[i];
            y1_element += elements[i] * x1[column];
            #pragma omp atomic update
            y2[column] += elements[i] * x1[row];
        }

        #pragma omp atomic update
        y1[row] += y1_element;
    }
}

template<typename T>
auto spmmtv_csr_symmetric(
        info::RowPointer const* __restrict const row_pointers,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, y1));

    auto const diagonal_row = column_offset;
    auto const rows_begin = row_offset;
    auto const rows_end = row_offset + row_count;
    // This should’ve been checked by the caller.
    assert(diagonal_row < rows_end);

    auto region_row = 0_uz;
    auto const rows_bound = std::max(diagonal_row, rows_begin);

    // Iterate over rows that can’t contain diagonal elements.
    for (auto row = rows_begin; row < rows_bound; ++row) {
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];
        ++region_row;
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto column = column_offset + columns[i];
        auto y1_element = elements[i] * x1[column];
        y1[column] += elements[i] * x1[row];

        for (++i; i < row_end; ++i) {
            column = column_offset + columns[i];
            y1_element += elements[i] * x1[column];
            y1[column] += elements[i] * x1[row];
        }

        y1[row] += y1_element;
    }

    // Iterate over rows that might contain diagonal elements.
    for (auto row = rows_bound; row < rows_end; ++row) {
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];
        ++region_row;
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto column = column_offset + columns[i];
        auto y1_element = elements[i] * x1[column];
        if (row != column) {
            y1[column] += elements[i] * x1[row];
        }

        for (++i; i < row_end; ++i) {
            column = column_offset + columns[i];
            y1_element += elements[i] * x1[column];
            y1[column] += elements[i] * x1[row];
        }

        y1[row] += y1_element;
    }
}

template<typename T>
auto spmmtv_csr_symmetric(
        info::RowPointer const* __restrict const row_pointers,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T const* __restrict const x2,
        T* __restrict const y1,
        T* __restrict const y2,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, x2, y1, y2));

    auto const diagonal_row = column_offset;
    auto const rows_begin = row_offset;
    auto const rows_end = row_offset + row_count;
    // This should’ve been checked by the caller.
    assert(diagonal_row < rows_end);

    auto region_row = 0_uz;
    auto const rows_bound = std::max(diagonal_row, rows_begin);

    // Iterate over rows that can’t contain diagonal elements.
    for (auto row = rows_begin; row < rows_bound; ++row) {
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];
        ++region_row;
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto column = column_offset + columns[i];
        auto y1_element = elements[i] * x1[column];
        auto y2_element = elements[i] * x2[column];
        y1[column] += elements[i] * x1[row];
        y2[column] += elements[i] * x2[row];

        for (++i; i < row_end; ++i) {
            column = column_offset + columns[i];
            y1_element += elements[i] * x1[column];
            y2_element += elements[i] * x2[column];
            y1[column] += elements[i] * x1[row];
            y2[column] += elements[i] * x2[row];
        }

        y1[row] += y1_element;
        y2[row] += y2_element;
    }

    // Iterate over rows that might contain diagonal elements.
    for (auto row = rows_bound; row < rows_end; ++row) {
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];
        ++region_row;
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto column = column_offset + columns[i];
        auto y1_element = elements[i] * x1[column];
        auto y2_element = elements[i] * x2[column];
        if (row != column) {
            y1[column] += elements[i] * x1[row];
            y2[column] += elements[i] * x2[row];
        }

        for (++i; i < row_end; ++i) {
            column = column_offset + columns[i];
            y1_element += elements[i] * x1[column];
            y2_element += elements[i] * x2[column];
            y1[column] += elements[i] * x1[row];
            y2[column] += elements[i] * x2[row];
        }

        y1[row] += y1_element;
        y2[row] += y2_element;
    }
}

template<typename T>
auto spmmtv_csr_symmetric_atomic(
        info::RowPointer const* __restrict const row_pointers,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, y1));

    auto const diagonal_row = column_offset;
    auto const rows_begin = row_offset;
    auto const rows_end = row_offset + row_count;
    // This should’ve been checked by the caller.
    assert(diagonal_row < rows_end);

    auto region_row = 0_uz;
    auto const rows_bound = std::max(diagonal_row, rows_begin);

    // Iterate over rows that can’t contain diagonal elements.
    for (auto row = rows_begin; row < rows_bound; ++row) {
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];
        ++region_row;
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto column = column_offset + columns[i];
        auto y1_element = elements[i] * x1[column];
        #pragma omp atomic update
        y1[column] += elements[i] * x1[row];

        for (++i; i < row_end; ++i) {
            column = column_offset + columns[i];
            y1_element += elements[i] * x1[column];
            #pragma omp atomic update
            y1[column] += elements[i] * x1[row];
        }

        #pragma omp atomic update
        y1[row] += y1_element;
    }

    // Iterate over rows that might contain diagonal elements.
    for (auto row = rows_bound; row < rows_end; ++row) {
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];
        ++region_row;
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto column = column_offset + columns[i];
        auto y1_element = elements[i] * x1[column];
        if (row != column) {
            #pragma omp atomic update
            y1[column] += elements[i] * x1[row];
        }

        for (++i; i < row_end; ++i) {
            column = column_offset + columns[i];
            y1_element += elements[i] * x1[column];
            #pragma omp atomic update
            y1[column] += elements[i] * x1[row];
        }

        #pragma omp atomic update
        y1[row] += y1_element;
    }
}

template<typename T>
auto spmmtv_csr_symmetric_nondiagonal(
        info::RowPointer const* __restrict const row_pointers,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, y1));

    for (auto region_row = 0_uz; region_row < row_count; ++region_row) {
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto const row = row_offset + region_row;
        auto column = column_offset + columns[i];
        auto y1_element = elements[i] * x1[column];
        y1[column] += elements[i] * x1[row];

        for (++i; i < row_end; ++i) {
            column = column_offset + columns[i];
            y1_element += elements[i] * x1[column];
            y1[column] += elements[i] * x1[row];
        }

        y1[row] += y1_element;
    }
}

template<typename T>
auto spmmtv_csr_symmetric_nondiagonal(
        info::RowPointer const* __restrict const row_pointers,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T const* __restrict const x2,
        T* __restrict const y1,
        T* __restrict const y2,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, x2, y1, y2));

    for (auto region_row = 0_uz; region_row < row_count; ++region_row) {
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto const row = row_offset + region_row;
        auto column = column_offset + columns[i];
        auto y1_element = elements[i] * x1[column];
        auto y2_element = elements[i] * x2[column];
        y1[column] += elements[i] * x1[row];
        y2[column] += elements[i] * x2[row];

        for (++i; i < row_end; ++i) {
            column = column_offset + columns[i];
            y1_element += elements[i] * x1[column];
            y2_element += elements[i] * x2[column];
            y1[column] += elements[i] * x1[row];
            y2[column] += elements[i] * x2[row];
        }

        y1[row] += y1_element;
        y2[row] += y2_element;
    }
}

template<typename T>
auto spmmtv_csr_symmetric_nondiagonal_atomic(
        info::RowPointer const* __restrict const row_pointers,
        info::ColIndex const* __restrict const columns,
        T const* __restrict const elements,
        std::size_t const row_count,
        T const* __restrict const x1,
        T* __restrict const y1,
        std::uint32_t const row_offset,
        std::uint32_t const column_offset
    ) -> void
{
    assert(do_not_alias(row_pointers, columns, elements, x1, y1));

    for (auto region_row = 0_uz; region_row < row_count; ++region_row) {
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];
        if (row_begin == row_end) {
            continue;
        }

        auto i = row_begin;
        auto const row = row_offset + region_row;
        auto column = column_offset + columns[i];
        auto y1_element = elements[i] * x1[column];
        #pragma omp atomic update
        y1[column] += elements[i] * x1[row];

        for (++i; i < row_end; ++i) {
            column = column_offset + columns[i];
            y1_element += elements[i] * x1[column];
            #pragma omp atomic update
            y1[column] += elements[i] * x1[row];
        }

        #pragma omp atomic update
        y1[row] += y1_element;
    }
}

}  // namespace

}  // namespace hsf::compute
