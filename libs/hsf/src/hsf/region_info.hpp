#pragma once

#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t
#include <span>                 // std::span

#include <hsf/bounds.hpp>       // hsf::Bounds
#include <hsf/info.hpp>         // hsf::info
#include <hsf/region.hpp>       // hsf::Region
#include <hsf/region_data.hpp>  // hsf::RegionData
#include <hsf/region_type.hpp>  // hsf::RegionType
#include <hsf/sub_bounds.hpp>   // hsf::SubBounds

namespace hsf {

// A helper class used when creating new regions.
class RegionInfo {
public:
    RegionInfo() = default;

    // Instantiate a ‘RegionInfo’ object according to the specified parameters.
    // Parameters ‘row_pointers’ (a CSR row pointer array) and ‘columns’ (a CSR
    // column index array) can be viewed as arrays of a CSR matrix.  Parameters
    // ‘row_bounds’ and ‘col_bounds’ specify how to split the matrix into
    // regions.  Metadata of the regions is stored into the array given by
    // parameter ‘region_data’.
    RegionInfo(
            Bounds const& row_bounds,
            Bounds const& col_bounds,
            std::span<std::uint32_t const> row_pointers,
            std::span<std::uint32_t const> columns,
            std::span<RegionData> region_data
        );

    // Instantiate a ‘RegionInfo’ object according to the specified parameters.
    // Parameters ‘row_pointers’ (a CSR row pointer array) and ‘columns’ (a CSR
    // column index array) can be viewed as arrays of a CSR matrix.  Parameters
    // ‘row_bounds’ and ‘col_bounds’ specify how to split the matrix into
    // regions.  Metadata of the regions is stored into the array given by
    // parameter ‘region_data’.  This constructor is optimised for the case
    // when the CSR matrix is a CSR region that is being split into subregions.
    RegionInfo(
            SubBounds const& row_bounds,
            SubBounds const& col_bounds,
            std::span<info::RowPointer const> row_pointers,
            std::span<info::ColIndex const> columns,
            std::span<RegionData> region_data
        );

    // Finalise possibly incomplete CSR row pointer arrays.  Parameter
    // ‘row_bounds’ is used to obtain additional information about each region.
    // Parameter ‘csr_rows’ contains CSR row pointer arrays of each region,
    // which will be updated.
    template<typename BoundsType>
    auto finalise_row_pointers(BoundsType const& row_bounds, std::span<info::RowPointer> csr_rows) const -> void;

    // Return the total number of COO row indices in all regions.
    auto get_coo_rows_element_count() const noexcept -> std::size_t;

    // Return the current index into the COO row index array of the specified
    // region.  See ‘RegionData::coo_row_index’ for details.  The specified
    // region is determined from parameters ‘row_bound’ and ‘col_bound’.
    // Specifying an out-of-bounds region is undefined behaviour.
    auto get_coo_row_index(std::size_t row_bound, std::size_t col_bound) const
        -> std::size_t;

    // Return the total number of CSR row pointers in all regions.
    auto get_csr_rows_element_count() const noexcept -> std::size_t;

    // Return the starting index of the CSR row pointer array of the specified
    // region.  See ‘RegionData::csr_row_index’ for details.  The specified
    // region is determined from parameters ‘row_bound’ and ‘col_bound’.
    // Specifying an out-of-bounds region is undefined behaviour.
    auto get_csr_row_index(std::size_t row_bound, std::size_t col_bound) const
        -> std::size_t;

    // Return the current index into the column index array and element value
    // array of the specified region.  See ‘RegionData::element_index’ for
    // details.  The specified region is determined from parameters ‘row_bound’
    // and ‘col_bound’.  Specifying an out-of-bounds region is undefined
    // behaviour.
    auto get_element_index(std::size_t row_bound, std::size_t col_bound) const
        -> std::size_t;

    // Return the number of regions that contain at least one element.
    auto get_nonempty_region_count() const noexcept -> std::size_t;

    // Return the currently stored previous row index for the region specified
    // by parameters ‘row_bound’ and ‘col_bound’.  Specifying an out-of-bounds
    // region is undefined behaviour.
    auto get_previous_row(std::size_t row_bound, std::size_t col_bound) const
        -> std::size_t;

    // Return the region type of the region specified by parameters ‘row_bound’
    // and ‘col_bound’.  Specifying an out-of-bounds region is undefined
    // behaviour.
    auto get_region_type(std::size_t row_bound, std::size_t col_bound) const
        -> RegionType;

    // Increment the current index into the COO row index array of the
    // specified region.  See ‘RegionData::coo_row_index’ for details.  The
    // specified region is determined from parameters ‘row_bound’ and
    // ‘col_bound’.  Specifying an out-of-bounds region is undefined behaviour.
    auto increment_coo_row_index(std::size_t row_bound, std::size_t col_bound)
        -> void;

    // Increment the current index into the column index array and element
    // value array of the specified region.  See ‘RegionData::element_index’
    // for details.  The specified region is determined from parameters
    // ‘row_bound’ and ‘col_bound’.  Specifying an out-of-bounds region is
    // undefined behaviour.
    auto increment_element_index(std::size_t row_bound, std::size_t col_bound)
        -> void;

    // Initialise the regions given by parameter ‘regions’.  The regions are
    // initialised using information from the ‘RegionInfo’ object and using
    // parameters ‘row_bounds’ and ‘col_bounds’.  The regions are assigned
    // pointers into the arrays specified by parameters ‘coo_rows’, ‘csr_rows’,
    // ‘columns’ and ‘elements’.
    template<typename BoundsType, typename T>
    auto init_regions(
            BoundsType const& row_bounds,
            BoundsType const& col_bounds,
            std::span<info::RowIndex> coo_rows,
            std::span<info::RowPointer> csr_rows,
            std::span<info::ColIndex> columns,
            std::span<T> elements,
            std::span<Region<T>> regions
        ) -> void;

    // Set the currently stored previous row index for the specified region to
    // the value given by parameter ‘row_index’.  The specified region is
    // determined from parameters ‘row_bound’ and ‘col_bound’.  Specifying an
    // out-of-bounds region is undefined behaviour.
    auto set_previous_row(
            std::size_t row_bound, std::size_t col_bound, std::uint32_t row_index
        ) -> void;

private:
    // The regions described by the ‘RegionInfo’ object can be divided into
    // columns.  This member variable stores their count.
    std::size_t col_region_count_{};

    // The total number of COO row indices in all regions.
    std::size_t coo_rows_element_count_{};

    // The total number of CSR row pointers in all regions.
    std::size_t csr_rows_element_count_{};

    // The number of regions that contain at least one element.
    std::size_t nonempty_region_count_{};

    // Metadata of all regions of the ‘RegionInfo’ object.  Even empty regions
    // are included.
    std::span<RegionData> region_data_{};

    // Increment the stored element count of the specified region.  Specifying
    // an out-of-bounds region is undefined behaviour.
    auto increment_element_count(std::size_t row_bound, std::size_t col_bound)
        -> void;

    // Determine the number of nonzero elements in the specified blocks.
    // Parameters ‘row_bound’, ‘row_bounds’ and ‘column_region_count’ specify a
    // row of regions.  Parameters ‘row_pointers’ (a CSR row pointer array) and
    // ‘columns’ (a CSR column index array) specify the row and column indices
    // of nonzero elements.  The nonzero element counts are zeroed out at the
    // start of the member function.  No bounds checking of the parameters is
    // performed.
    auto init_element_counts(
            std::size_t row_bound,
            Bounds const& row_bounds,
            std::size_t column_region_count,
            std::span<std::uint32_t const> row_pointers,
            std::span<std::uint32_t const> columns
        ) -> void;

    // Initialise member variables ‘coo_rows_element_count_’ and
    // ‘csr_rows_element_count_’ using information from the ‘RegionInfo’ object
    // and the ‘row_bounds’ parameter.  This member function is meant to be
    // invoked at the end of the constructor.
    template<typename BoundsType>
    auto init_row_element_counts(BoundsType const& row_bounds) -> void;

    // Initialise member variable ‘nonempty_region_count_’ using information
    // from the ‘RegionInfo’ object.  This member function is meant to be
    // invoked at the end of the constructor.
    auto init_nonempty_region_count() noexcept -> void;
};

}  // namespace hsf
