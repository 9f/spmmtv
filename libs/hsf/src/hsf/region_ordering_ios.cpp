#include <hsf/region_ordering_ios.hpp>

#include <istream>              // std::istream
#include <ostream>              // std::ostream
#include <stdexcept>            // std::out_of_range
#include <string>               // std::string

#include <hsf/logic_error.hpp>  // hsf::LogicError
#include <hsf/region_ordering.hpp>  // hsf::region_orderings hsf::RegionOrdering

namespace hsf {

auto operator<<(std::ostream& os, RegionOrdering const region_ordering) -> std::ostream&
{
    switch (region_ordering) {
    case RegionOrdering::block_lexicographical:
        return os << "block-lexicographical";
    case RegionOrdering::lexicographical:
        return os << "lexicographical";
    case RegionOrdering::morton:
        return os << "Morton";
    }

    throw LogicError{"Unexpected enumeration value."};
}

auto operator>>(std::istream& is, RegionOrdering& region_ordering) -> std::istream&
{
    auto token = std::string{};
    is >> token;
    try {
        region_ordering = region_orderings.at(token);
    } catch (std::out_of_range const& e) {
        is.setstate(std::istream::failbit);
    }
    return is;
}

}  // namespace hsf
