#include <hsf/region_indices_log.hpp>

#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <memory>               // std::make_unique
#include <vector>               // std::vector

namespace hsf {

RegionIndicesLog::RegionIndicesLog(
        std::size_t const maximum_region_indices_count
    ) : region_indices_{std::make_unique<std::size_t[]>(maximum_region_indices_count)}
{
}

auto RegionIndicesLog::region_indices() const -> std::vector<std::size_t>
{
    if (!this->region_indices_) {
        return {};
    }

    auto const begin = this->region_indices_.get();
    auto const end = this->region_indices_.get() + this->region_indices_count_;
    return {begin, end};
}

auto RegionIndicesLog::push_back(std::size_t const region_index) -> void
{
    assert(this->region_indices_);
    this->region_indices_[this->region_indices_count_++] = region_index;
}

}  // namespace hsf
