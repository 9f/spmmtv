#include <hsf/priv/region_indices_sll.hpp>

#include <algorithm>            // std::copy
#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <span>                 // std::span

#include <hsf/priv/cstddef.hpp>  // hsf::operator""_uz
#include <hsf/priv/ndebug.hpp>  // hsf::ndebug

namespace hsf {

RegionIndicesSll::RegionIndicesSll(std::span<std::size_t const> const region_indices)
    : region_offsets_(region_indices.size()),
      element_count_{region_indices.size()}
{
    if (region_indices.empty()) {
        return;
    }

    for (auto i = 0_uz; i < this->region_offsets_.size() - 1; ++i) {
        this->region_offsets_[i].next_element = &this->region_offsets_[i + 1];
        this->region_offsets_[i].region_index = region_indices[i];
    }

    this->region_offsets_.back().next_element = &this->region_offsets_.front();
    this->region_offsets_.back().region_index = region_indices.back();

    this->current_element_ = &this->region_offsets_.front();
    this->previous_element_ = &this->region_offsets_.back();
}

[[nodiscard]]
auto RegionIndicesSll::empty() const noexcept -> bool
{
    return this->size() == 0;
}

auto RegionIndicesSll::erase_index() noexcept(ndebug) -> void
{
    assert(!this->empty());
    assert(this->current_element_ != nullptr);
    assert(this->previous_element_ != nullptr);

    auto* const next_element = this->current_element_->next_element;
    this->current_element_ = next_element;
    this->previous_element_->next_element = next_element;

    --this->element_count_;
}

auto RegionIndicesSll::from_shallow_copy(RegionIndicesSll const& other) noexcept(ndebug) -> void
{
    assert(this->region_offsets_.size() == other.region_offsets_.size());

    std::copy(other.region_offsets_.begin(), other.region_offsets_.end(), this->region_offsets_.begin());
    this->current_element_ = other.current_element_;
    this->previous_element_ = other.previous_element_;
    this->element_count_ = other.element_count_;
}

auto RegionIndicesSll::get_index() const noexcept(ndebug) -> std::size_t
{
    assert(this->current_element_ != nullptr);
    return this->current_element_->region_index;
}

auto RegionIndicesSll::next() -> void
{
    assert(!this->empty());
    assert(this->current_element_ != nullptr);

    this->previous_element_ = this->current_element_;
    this->current_element_ = this->current_element_->next_element;
}

auto RegionIndicesSll::shallow_copy() const -> RegionIndicesSll
{
    auto copy = RegionIndicesSll{};
    copy.region_offsets_ = this->region_offsets_;
    copy.current_element_ = this->current_element_;
    copy.previous_element_ = this->previous_element_;
    copy.element_count_ = this->element_count_;
    return copy;
}

auto RegionIndicesSll::size() const noexcept -> std::size_t
{
    return this->element_count_;
}

}  // namespace hsf
