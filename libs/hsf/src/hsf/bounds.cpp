#include <hsf/bounds.hpp>

#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t

#include <hsf/info.hpp>         // hsf::info

namespace hsf {

namespace {

// Return how many bound indices does a ‘Bounds’ object need to divide
// ‘element_count’ rows (or columns) into blocks with ‘info::region_size’ rows
// (or columns) each.
auto get_bound_count(std::uint32_t element_count) noexcept -> std::uint32_t;

}  // namespace

Bounds::Bounds(std::uint32_t const row_or_column_count)
    : indices_(get_bound_count(row_or_column_count)),
      region_count_{this->indices_.size() - 1}
{
    assert(row_or_column_count >= 1);

    // The following assertion should follow from the fact that the row (or
    // column) count is greater than or equal to one.
    assert(this->indices_.size() >= 2);

    auto const begin = this->indices_.begin() + 1;
    auto const end = this->indices_.end() - 1;
    for (auto it = begin; it != end; ++it) {
        *it = *(it - 1) + info::region_size;
    }
    this->indices_.back() = row_or_column_count;
}

auto Bounds::get_index(std::size_t const bound) const -> std::uint32_t
{
    assert(bound < this->indices_.size());
    return this->indices_[bound];
}

auto Bounds::get_region_count() const noexcept -> std::size_t
{
    return this->region_count_;
}

auto Bounds::get_size(std::size_t const bound) const -> std::uint32_t
{
    assert(bound < this->get_region_count());
    return this->get_index(bound + 1) - this->get_index(bound);
}

namespace {

auto get_bound_count(std::uint32_t const element_count) noexcept -> std::uint32_t
{
    auto const full_region_count = element_count / info::region_size;
    auto const remaining_element_count = element_count % info::region_size;
    if (remaining_element_count > 0) {
        return full_region_count + 2;
    }
    return full_region_count + 1;
}

}  // namespace

}  // namespace hsf
