#pragma once

#include <ostream>              // std::ostream

namespace hsf {

enum struct RegionType;

// Print parameter ‘region_type’ into the output stream given by parameter ‘os’
// and return parameter ‘os’.  Throw an exception of type ‘hsf::LogicError’ if
// parameter ‘region_type’ has an unexpected value.
auto operator<<(std::ostream& os, RegionType region_type) -> std::ostream&;

}  // namespace hsf
