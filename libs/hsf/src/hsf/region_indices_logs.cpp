#include <hsf/region_indices_logs.hpp>

#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <limits>               // std::numeric_limits
#include <vector>               // std::vector

#include <hsf/region_indices_log.hpp>  // hsf::RegionIndicesLog

namespace hsf {

namespace {

auto init_logs(std::size_t maximum_region_indices_count, int thread_count) -> std::vector<RegionIndicesLog>;

}  // namespace

RegionIndicesLogs::RegionIndicesLogs(std::size_t const maximum_region_indices_count, int const thread_count)
    : region_indices_logs_(init_logs(maximum_region_indices_count, thread_count))
{
}

auto RegionIndicesLogs::get(int const thread_id) -> RegionIndicesLog&
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);
    return this->region_indices_logs_[thread_id_uz];
}

auto RegionIndicesLogs::get(int const thread_id) const -> RegionIndicesLog const&
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);
    return this->region_indices_logs_[thread_id_uz];
}

auto RegionIndicesLogs::thread_count() const noexcept -> int
{
    auto const thread_count_uz = this->region_indices_logs_.size();
    assert(thread_count_uz <= std::numeric_limits<int>::max());
    return static_cast<int>(thread_count_uz);
}

namespace {

auto init_logs(std::size_t const maximum_region_indices_count, int const thread_count)
    -> std::vector<RegionIndicesLog>
{
    assert(thread_count > 0);
    auto const thread_count_uz = static_cast<std::size_t>(thread_count);

    auto region_indices_logs = std::vector<RegionIndicesLog>{};
    region_indices_logs.reserve(thread_count_uz);

    for (auto i = 0; i < thread_count; ++i) {
        region_indices_logs.emplace_back(maximum_region_indices_count);
    }

    return region_indices_logs;
}

}  // namespace

}  // namespace hsf
