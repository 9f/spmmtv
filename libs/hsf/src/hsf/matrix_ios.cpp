#include <hsf/matrix_ios.hpp>

#include <functional>           // std::invoke
#include <ostream>              // std::ostream
#include <string_view>          // std::string_view

#include <hsf/matrix.hpp>       // hsf::Matrix
#include <hsf/region_ordering_ios.hpp>  // hsf::operator<<
#include <hsf/symmetric.hpp>    // hsf::Symmetric
#include <hsf/symmetric_ios.hpp>  // hsf::operator<<

namespace hsf {

namespace {

// Return a string representation of template argument ‘T’.  The supported
// template arguments are ‘float’ and ‘double’.
template<typename T>
constexpr auto get_type_name() noexcept -> std::string_view;

template<> constexpr auto get_type_name<float>() noexcept -> std::string_view;
template<> constexpr auto get_type_name<double>() noexcept -> std::string_view;

}  // namespace

template<typename T, Symmetric Symm>
auto operator<<(std::ostream& os, Matrix<T, Symm> const& matrix) -> std::ostream&
{
    auto const csr_regions_optimised = std::invoke([&] {
        if (matrix.are_csr_regions_optimised()) {
            return "yes";
        }
        return "no";
    });
    os << "element type: " << get_type_name<T>() << '\n'
       << "element count: " << matrix.element_count() << '\n'
       << "dimensions: " << matrix.row_count() << " × " << matrix.column_count() << '\n'
       << "symmetric: " << Symm << '\n'
       << "region count: " << matrix.region_count() << '\n'
       << "region sort order: " << matrix.region_ordering() << '\n'
       << "CSR regions optimised: " << csr_regions_optimised << '\n';
    return os;
}

template auto operator<<(std::ostream& os, Matrix<float, Symmetric::no> const& matrix) -> std::ostream&;
template auto operator<<(std::ostream& os, Matrix<float, Symmetric::yes> const& matrix) -> std::ostream&;
template auto operator<<(std::ostream& os, Matrix<double, Symmetric::no> const& matrix) -> std::ostream&;
template auto operator<<(std::ostream& os, Matrix<double, Symmetric::yes> const& matrix) -> std::ostream&;

namespace {

template<>
constexpr auto get_type_name<float>() noexcept -> std::string_view
{
    return "float";
}

template<>
constexpr auto get_type_name<double>() noexcept -> std::string_view
{
    return "double";
}

}  // namespace

}  // namespace hsf
