#pragma once

#include <hsf/config.hpp>       // SPMMTV_HSF_SUB_BOUNDS_CAPACITY

#include <cstddef>              // std::size_t

namespace hsf {

// The capacity of the statically allocated array for storing subregion bounds.
#ifndef SPMMTV_HSF_SUB_BOUNDS_CAPACITY
constexpr std::size_t sub_bounds_capacity = 512;
#else
constexpr std::size_t sub_bounds_capacity = SPMMTV_HSF_SUB_BOUNDS_CAPACITY;
#endif  // SPMMTV_HSF_SUB_BOUNDS_CAPACITY

static_assert(sub_bounds_capacity > 0, "‘sub_bounds_capacity’ is required to be greater than zero");

}  // namespace hsf
