#include <hsf/sub_bounds.hpp>

#include <algorithm>            // std::lower_bound std::max std::min std::upper_bound
#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t
#include <functional>           // std::invoke
#include <iterator>             // std::distance
#include <new>                  // std::bad_alloc
#include <span>                 // std::span
#include <string>               // std::string_literals std::to_string

#include <hsf/info.hpp>         // hsf::info
#include <hsf/priv/ndebug.hpp>  // hsf::ndebug
#include <hsf/sub_bounds_capacity.hpp>  // hsf::sub_bounds_capacity
#include <hsf/subregion_bounds_capacity_error.hpp>  // hsf::SubregionBoundsCapacityError

using namespace std::string_literals;

namespace hsf {

auto SubBounds::get_subregion_capacity() noexcept(ndebug) -> std::size_t
{
    assert(decltype(SubBounds::indices_)::static_capacity > 0);
    return decltype(SubBounds::indices_)::static_capacity - 1;
}

auto SubBounds::find_subregion_bound(info::RowPointer const value) const -> std::size_t
{
    auto const begin = this->indices_.begin();
    auto const end = this->indices_.end();
    auto const it = std::lower_bound(begin, end, value);

    // According to the specification of ‘std::lower_bound’, the following
    // assertion should hold.
    assert(*it >= value);

    if (*it == value) {
        return static_cast<std::size_t>(std::distance(begin, it));
    }
    // *it > value
    return static_cast<std::size_t>(std::distance(begin, it)) - 1;
}

auto SubBounds::get_index(std::size_t const bound) const -> info::RowPointer
{
    assert(bound < this->indices_.size());
    return this->indices_[bound];
}

auto SubBounds::get_original_index(std::size_t const bound) const -> info::RowPointer
{
    return this->get_index(bound) + this->index_offset_;
}

auto SubBounds::get_size(std::size_t const bound) const -> info::RowPointer
{
    assert(bound < this->get_subregion_count());
    return this->get_index(bound + 1) - this->get_index(bound);
}

auto SubBounds::get_subregion_count() const noexcept -> std::size_t
{
    return this->subregion_count_;
}

auto SubBounds::init(
        std::span<info::RowPointer const> const pointers,
        std::size_t const element_count,
        std::uint32_t const index_offset,
        std::size_t const region_threshold,
        int const thread_count
    ) -> void try
{
    this->indices_.clear();
    this->indices_.emplace_back(0);
    this->index_offset_ = index_offset;

    if (pointers.empty()) {
        this->subregion_count_ = 0;
        return;
    }

    // Depending on whether ‘pointers’ is a CSC or CSR pointer array,
    // ‘last_bound’ is equal to the row count or column count of the original
    // matrix.
    auto const last_bound = pointers.size() - 1;

    while (this->indices_.back() < last_bound) {
        auto const subregion_index = this->indices_.size();
        auto const last_element = pointers[this->indices_.back()];
        auto const target_element = std::invoke([&] {
            auto const target_element = std::max(subregion_index * region_threshold, last_element + region_threshold);
            return std::min(target_element, element_count);
        });

        // To do: there might be better ways to adjust the final subregion’s
        // element count.
        auto const remaining_element_count = element_count - target_element;
        constexpr auto remaining_coef = 10;
        auto const element_count_threshold = remaining_coef * static_cast<std::size_t>(thread_count);
        if (remaining_element_count < element_count_threshold) {
            this->indices_.emplace_back(last_bound);
            break;
        }

        auto const pointers_begin = pointers.begin() + this->indices_.back() + 1;
        auto const found_bound = std::upper_bound(pointers_begin, pointers.end(), target_element);

        if (found_bound == pointers_begin || found_bound == pointers.end()) {
            this->indices_.emplace_back(found_bound - pointers.begin());

        } else {
            // As long as found_bound isn’t equal to ‘pointers_begin’ or
            // ‘pointers.end()’, the following are a corollary of the
            // specification of ‘std::upper_bound’.
            assert(target_element < *found_bound);
            assert(*(found_bound - 1) <= target_element);

            auto const diff1 = *found_bound - target_element;
            auto const diff2 = target_element - *(found_bound - 1);
            if (diff1 <= diff2) {
                this->indices_.emplace_back(found_bound - pointers.begin());
            } else {
                this->indices_.emplace_back(found_bound - 1 - pointers.begin());
            }
        }
    }

    this->subregion_count_ = this->indices_.size() - 1;

} catch (std::bad_alloc const& e) {
    #undef SPMMTV_HSF_SUB_BOUNDS_CAPACITY

    auto message =
        "The statically allocated array for storing subregion bounds isn’t large enough for the current sparse matrix."
        " The capacity of the array can be changed by setting CMake variable SPMMTV_HSF_SUB_BOUNDS_CAPACITY to the "
        "desired value. The current capacity is "s;

    if (sub_bounds_capacity == 1) {
        message += "1 element.";
    } else {
        message += std::to_string(sub_bounds_capacity) + " elements.";
    }

    throw SubregionBoundsCapacityError{message};
}

}  // namespace hsf
