#include <hsf/matrix.hpp>

#include <algorithm>            // std::copy std::fill std::is_sorted std::sort std::upper_bound
#include <cassert>              // assert
#include <cstddef>              // std::ptrdiff_t std::size_t
#include <cstdint>              // std::uint32_t
#include <iterator>             // std::distance
#include <limits>               // std::numeric_limits
#include <memory>               // std::make_unique std::unique_ptr
#include <numeric>              // std::exclusive_scan
#include <span>                 // std::span
#include <string>               // std::to_string
#include <tuple>                // std::tie std::tuple
#include <utility>              // std::move
#include <variant>              // std::variant std::visit
#include <vector>               // std::vector

#include <boost/core/swap.hpp>  // boost::swap
#include <boost/smart_ptr/make_unique.hpp>  // boost::make_unique_noinit

#include <omp.h>                // omp_get_max_threads

#include <hsf/bounds.hpp>       // hsf::Bounds
#include <hsf/index_to_region_bound.hpp>  // hsf::index_to_region_bound
#include <hsf/info.hpp>         // hsf::info
#include <hsf/logic_error.hpp>  // hsf::LogicError
#include <hsf/morton.hpp>       // hsf::morton
#include <hsf/normalise_regions.hpp>  // hsf::NormaliseRegions
#include <hsf/optimise_csr_regions.hpp>  // hsf::OptimiseCsrRegions
#include <hsf/priv/cstddef.hpp>  // hsf::operator""_uz
#include <hsf/priv/ndebug.hpp>  // hsf::ndebug
#include <hsf/region.hpp>       // hsf::Region
#include <hsf/region_data.hpp>  // hsf::RegionData
#include <hsf/region_info.hpp>  // hsf::RegionInfo
#include <hsf/region_ordering.hpp>  // hsf::RegionOrdering
#include <hsf/region_type.hpp>  // hsf::get_region_type hsf::RegionType
#include <hsf/split_data.hpp>   // hsf::SplitData
#include <hsf/sub_bounds.hpp>   // hsf::SubBounds
#include <hsf/subregion_data.hpp>  // hsf::SubregionData
#include <hsf/symmetric.hpp>    // hsf::Symmetric

namespace hsf {

namespace {

// A visitor class for copying the COO row index or CSR row pointer array of
// one region to another region.  This is done when performing region
// normalisation.
class CopyRegionDataVisitor {
public:
    CopyRegionDataVisitor() = default;

    CopyRegionDataVisitor(
            std::variant<info::RowIndex*, info::RowPointer*>& new_region_rows,
            info::RowIndex* new_coo_rows,
            info::RowPointer* new_csr_rows,
            std::size_t coo_row_index,
            std::size_t csr_row_index,
            std::size_t row_count,
            std::size_t element_count
        ) noexcept;

    auto operator()(info::RowIndex* old_region_row_indices) -> void;
    auto operator()(info::RowPointer* old_region_row_pointers) -> void;

private:
    std::variant<info::RowIndex*, info::RowPointer*>* new_region_rows_{};
    info::RowIndex* new_coo_rows_{};
    info::RowPointer* new_csr_rows_{};
    std::size_t coo_row_index_{};
    std::size_t csr_row_index_{};
    std::size_t row_count_{};
    std::size_t element_count_{};
};

// A visitor class for copying the COO rows pointer or CSR rows pointer from
// one region to another region.  This is done when copy constructing an
// ‘hsf::Matrix’ object.
template<typename T>
class CopyRegionVisitor {
public:
    CopyRegionVisitor() = default;

    CopyRegionVisitor(
            info::RowIndex* const& lhs_coo_rows,
            info::RowPointer* const& lhs_csr_rows,
            Region<T>& lhs_region,
            info::RowIndex* const& rhs_coo_rows,
            info::RowPointer* const& rhs_csr_rows
        ) noexcept;

    auto operator()(info::RowIndex* rhs_region_coo_rows) const -> void;
    auto operator()(info::RowPointer* rhs_region_csr_rows) const -> void;

private:
    info::RowIndex* lhs_coo_rows_{};
    info::RowPointer* lhs_csr_rows_{};
    Region<T>* lhs_region_{};
    info::RowIndex const* rhs_coo_rows_{};
    info::RowPointer const* rhs_csr_rows_{};
};

// A visitor class for removing leading and trailing empty rows from regions
// with a CSR row pointer array.
template<typename T>
class OptimiseCsrRegionVisitor {
public:
    OptimiseCsrRegionVisitor() = default;
    explicit OptimiseCsrRegionVisitor(Region<T>& region) noexcept;

    auto operator()(info::RowIndex* row_indices) const noexcept -> void;
    auto operator()(info::RowPointer*& row_pointers) const noexcept(ndebug) -> void;

private:
    Region<T>* region_{};
};

// A visitor class for obtaining a CSR row pointer array.
class RowPointersVisitor {
public:
    RowPointersVisitor() = default;

    RowPointersVisitor(std::size_t element_count, std::uint32_t row_count, info::RowPointer* row_pointers_offset)
        noexcept;

    auto operator()(info::RowIndex* row_indices) const -> info::RowPointer*;
    auto operator()(info::RowPointer* row_pointers) const noexcept -> info::RowPointer*;

private:
    std::size_t element_count_{};
    std::uint32_t row_count_{};
    info::RowPointer* row_pointers_offset_{};
};

// Return whether or not the passed regions are block-lexicographically
// ordered.
template<typename T>
auto are_block_lexicographically_ordered(std::span<Region<T> const> regions) -> bool;

// Return whether or not the passed regions were created by normalising the
// same parent region.
template<typename T>
auto are_from_same_parent_region(Region<T> const& region1, Region<T> const& region2) noexcept -> bool;

// Return whether or not the passed regions are lexicographically ordered.
template<typename T>
auto are_lexicographically_ordered(std::span<Region<T> const> regions) -> bool;

// Return whether or not the passed regions are Morton ordered.
template<typename T>
auto are_morton_ordered(std::span<Region<T> const> regions) -> bool;

// Return the total region count, the total COO row element count and the total
// CSR row element count using the final ‘Region’ object and final ‘SplitData’
// object of the matrix being normalised.
template<typename T>
auto calculate_total_counts(Region<T> const& region, SplitData<T> const& split_data)
    -> std::tuple<std::size_t, std::size_t, std::size_t>;

// Initialise a CSC column pointer array from a COO column index array.
// Parameter ‘column_indices’ is a COO column index array of a matrix.
// Parameter ‘column_pointers’ is used to store the resulting CSC column
// pointer array.  The function’s parameter types have been chosen to match the
// types of corresponding member variables of class ‘Region’.
auto column_indices_to_pointers(
        std::span<info::ColIndex const> column_indices, std::span<info::RowPointer> column_pointers
    ) -> void;

// Return the region element count threshold for a matrix.  The threshold is
// the maximum amount of elements that a region should contain and is always
// greater than or equal to ‘info::region_size’.  Parameter
// ‘nonzero_element_count’ is the nonzero element count of the matrix.
// Parameter ‘thread_count’ is how many threads will be used during the SpMMᵀV
// operation.  Parameter ‘region_coefficient’ is a real number in interval
// [0, 1].
auto compute_region_threshold(std::size_t nonzero_element_count, int thread_count, double region_coefficient)
    noexcept(ndebug) -> std::size_t;

// Forward the passed arguments to function ‘copy_region’ or ‘copy_subregions’.
// The arguments are forwarded to function ‘copy_region’ if parameter
// ‘split_data.subregion_data’ is equal to ‘nullptr’.  Otherwise the arguments
// are forwarded to function ‘copy_subregions’.
template<typename T>
auto copy(
        Region<T> const& old_region,
        SplitData<T> const& split_data,
        std::span<info::RowIndex> new_coo_rows,
        std::span<info::RowPointer> new_csr_rows,
        std::span<info::ColIndex> new_columns,
        std::span<T> new_elements,
        std::span<Region<T>> new_regions
    ) -> void;

// Copy the region specified by parameter ‘old_region’ to the region specified
// by parameter ‘new_region’.  Parameters ‘new_coo_rows’, ‘new_csr_rows’,
// ‘new_columns’ and ‘new_elements’ are used to store the elements of the new
// region.  Parameter ‘split_data’ contains indices into the aforementioned
// arrays for storing elements of the new region.
template<typename T>
auto copy_region(
        Region<T> const& old_region,
        SplitData<T> const& split_data,
        info::RowIndex* new_coo_rows,
        info::RowPointer* new_csr_rows,
        info::ColIndex* new_columns,
        T* new_elements,
        Region<T>& new_region
    ) -> void;

// Copy the elements of a region and store them as subregions.  Parameter
// ‘split_data’ gives information about the region to copy and information
// about how to create the subregions.  Parameters ‘new_coo_rows’,
// ‘new_csr_rows’, ‘new_columns’, ‘new_elements’ and ‘new_regions’ are used to
// store the data of the subregions.
template<typename T>
auto copy_subregions(
        SplitData<T> const& split_data,
        std::span<info::RowIndex> new_coo_rows,
        std::span<info::RowPointer> new_csr_rows,
        std::span<info::ColIndex> new_columns,
        std::span<T> new_elements,
        std::span<Region<T>> new_regions
    ) -> void;

// Return a column index converted to an index for the second level of the
// hierarchical storage format.
auto index_to_second_level(std::uint32_t index) noexcept -> info::ColIndex;

// Initialise the ‘RegionInfo’ object of the passed ‘SubregionData’ object.  If
// necessary, parameter ‘all_row_pointers’ is used as underlying storage for a
// row pointer array.  Parameter ‘all_column_pointers’ is used as underlying
// storage for a column pointer array.  Parameter ‘all_region_data’ is used as
// storage for the ‘RegionData’ array of the ‘RegionInfo’ object being
// initialised.  Parameter ‘region_data_index’ is used to determine the
// starting position of the ‘RegionData’ array.  Parameters ‘region_threshold’
// and ‘thread_count’ influence how the row and column bounds of the
// ‘RegionInfo’ object being initialised are created.  No bounds checking of
// the parameters is performed.
template<typename T>
auto init_subregion_data(
        SubregionData<T>& subregion_data,
        info::RowPointer* all_row_pointers,
        info::RowPointer* all_column_pointers,
        RegionData* all_region_data,
        std::size_t region_data_index,
        std::size_t region_threshold,
        int thread_count
    ) -> void;

// Insert an element into its region.  Parameters ‘row_index’, ‘col_index’ and
// ‘element’ specify the element to insert.  Parameters ‘row_bound’ and
// ‘col_bound’ are used to access parameter ‘region_info’.  Parameter
// ‘region_info’ maintains information about where to store inserted elements.
// Parameters ‘coo_rows’, ‘csr_rows’, ‘columns’ and ‘elements’ are used to
// store the inserted element.
template<typename T>
auto insert_element(
        info::RowIndex row_index,
        info::ColIndex col_index,
        T element,
        std::size_t row_bound,
        std::size_t col_bound,
        RegionInfo& region_info,
        std::span<info::RowIndex> coo_rows,
        std::span<info::RowPointer> csr_rows,
        std::span<info::ColIndex> columns,
        std::span<T> elements
    ) -> void;

// Insert elements into their regions.  Parameters ‘input_row_pointers’ (a CSR
// row pointer array), ‘input_columns’ (a CSR column index array) and
// ‘input_elements’ (a CSR element value array) specify input elements.
// Parameters ‘row_bounds’ and ‘row_bound’ specify the subset of the input
// elements that is to be copied.  Parameter ‘region_info’ maintains
// information about where to store inserted elements.  Parameters
// ‘output_coo_rows’, ‘output_csr_rows’, ‘output_columns’ and ‘output_elements’
// are used to store the inserted elements.
template<typename T>
auto insert_elements(
        std::span<std::uint32_t const> input_row_pointers,
        std::span<std::uint32_t const> input_columns,
        std::span<T const> input_elements,
        Bounds const& row_bounds,
        std::size_t row_bound,
        RegionInfo& region_info,
        std::span<info::RowIndex> output_coo_rows,
        std::span<info::RowPointer> output_csr_rows,
        std::span<info::ColIndex> output_columns,
        std::span<T> output_elements
    ) -> void;

// Initialise a CSR row pointer array from a COO row index array.  Parameter
// ‘row_indices’ is a COO row index array of a matrix.  Parameter
// ‘row_pointers’ is used to store the resulting CSR row pointer array.  The
// function’s parameter types have been chosen to match the types of
// corresponding member variables of class ‘Region’.
auto row_indices_to_pointers(std::span<info::RowIndex const> row_indices, std::span<info::RowPointer> row_pointers)
    -> void;

// Sort the passed regions in the Morton order.
template<typename T>
auto sort_in_morton_order(std::unique_ptr<Region<T>[]>& regions, std::size_t region_count) -> void;

// Sort the passed regions lexicographically.
template<typename T>
auto sort_lexicographically(std::unique_ptr<Region<T>[]>& regions, std::size_t region_count) -> void;

}  // namespace

template<typename T, Symmetric Symm>
Matrix<T, Symm>::Matrix(
        std::uint32_t const* const row_pointers,
        std::uint32_t const* const columns,
        T const* const elements,
        std::size_t const row_count,
        std::size_t const column_count,
        std::size_t const element_count,
        RegionOrdering const region_ordering,
        OptimiseCsrRegions const optimise_csr_regions
    ) : Matrix{
            row_pointers,
            columns,
            elements,
            row_count,
            column_count,
            element_count,
            region_ordering,
            NormaliseRegions::no,
            0,
            0,
            optimise_csr_regions
        }
{
}

template<typename T, Symmetric Symm>
Matrix<T, Symm>::Matrix(
        std::uint32_t const* const row_pointers,
        std::uint32_t const* const columns,
        T const* const elements,
        std::size_t const row_count,
        std::size_t const column_count,
        std::size_t const element_count,
        RegionOrdering const region_ordering,
        double const region_coefficient,
        int const thread_count,
        OptimiseCsrRegions const optimise_csr_regions
    ) : Matrix{
            row_pointers,
            columns,
            elements,
            row_count,
            column_count,
            element_count,
            region_ordering,
            NormaliseRegions::yes,
            region_coefficient,
            thread_count,
            optimise_csr_regions
        }
{
}

template<typename T, Symmetric Symm>
Matrix<T, Symm>::Matrix(Matrix const& other)
    : coo_rows_{boost::make_unique_noinit<RowIndex[]>(other.coo_rows_size_)},
      coo_rows_size_{other.coo_rows_size_},
      csr_rows_{boost::make_unique_noinit<RowPointer[]>(other.csr_rows_size_)},
      csr_rows_size_{other.csr_rows_size_},
      columns_{boost::make_unique_noinit<ColIndex[]>(other.element_count())},
      elements_{boost::make_unique_noinit<T[]>(other.element_count())},
      elements_size_{other.element_count()},
      row_count_{other.row_count()},
      column_count_{other.column_count()},
      regions_{std::make_unique<Region<T>[]>(other.region_count())},
      regions_size_{other.region_count()},
      region_ordering_{other.region_ordering()},
      csr_regions_optimised_{other.are_csr_regions_optimised()}
{
    std::copy(other.coo_rows_.get(), other.coo_rows_.get() + other.coo_rows_size_, this->coo_rows_.get());
    std::copy(other.csr_rows_.get(), other.csr_rows_.get() + other.csr_rows_size_, this->csr_rows_.get());
    std::copy(other.columns_.get(), other.columns_.get() + other.element_count(), this->columns_.get());
    std::copy(other.elements_.get(), other.elements_.get() + other.element_count(), this->elements_.get());

    for (auto i = 0_uz; i < other.region_count(); ++i) {
        this->regions_[i].row_begin = other.regions_[i].row_begin;
        this->regions_[i].column_begin = other.regions_[i].column_begin;
        this->regions_[i].row_count = other.regions_[i].row_count;
        this->regions_[i].column_count = other.regions_[i].column_count;
        this->regions_[i].element_count = other.regions_[i].element_count;

        auto const visitor = CopyRegionVisitor{
                this->coo_rows_.get(),
                this->csr_rows_.get(),
                this->regions_[i],
                other.coo_rows_.get(),
                other.csr_rows_.get()
            };
        std::visit(visitor, other.regions_[i].rows);

        auto const columns_offset = other.regions_[i].columns - other.columns_.get();
        this->regions_[i].columns = this->columns_.get() + columns_offset;

        auto const elements_offset = other.regions_[i].elements - other.elements_.get();
        this->regions_[i].elements = this->elements_.get() + elements_offset;
    }
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::operator=(Matrix const& other) -> Matrix&
{
    return *this = Matrix{other};
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::row_count() const noexcept -> std::size_t
{
    return this->row_count_;
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::column_count() const noexcept -> std::size_t
{
    return this->column_count_;
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::element_count() const noexcept -> std::size_t
{
    return this->elements_size_;
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::regions() const noexcept -> std::span<Region<T> const>
{
    return {this->regions_.get(), this->region_count()};
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::region_count() const noexcept -> std::size_t
{
    return this->regions_size_;
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::region_ordering() const noexcept -> RegionOrdering
{
    return this->region_ordering_;
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::are_csr_regions_optimised() const noexcept -> bool
{
    return this->csr_regions_optimised_;
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::size_bytes() const noexcept -> std::size_t
{
    // The size of the COO row index array.
    auto sum = this->coo_rows_size_ * sizeof(typename decltype(this->coo_rows_)::element_type);

    // The size of the CSR row pointer array.
    sum += this->csr_rows_size_ * sizeof(typename decltype(this->csr_rows_)::element_type);

    // The size of the column index array.
    sum += this->elements_size_ * sizeof(typename decltype(this->columns_)::element_type);

    // The size of the element value array.
    sum += this->elements_size_ * sizeof(typename decltype(this->elements_)::element_type);

    // The size of the region array.
    return sum + this->regions_size_ * sizeof(typename decltype(this->regions_)::element_type);
}

template<typename T, Symmetric Symm>
Matrix<T, Symm>::Matrix(
        std::uint32_t const* const row_pointers,
        std::uint32_t const* const columns,
        T const* const elements,
        std::size_t const row_count,
        std::size_t const column_count,
        std::size_t const element_count,
        RegionOrdering const region_ordering,
        NormaliseRegions const normalise_regions_enum,
        double const region_coefficient,
        int const thread_count,
        OptimiseCsrRegions const optimise_csr_regions_enum
    ) : columns_{boost::make_unique_noinit<ColIndex[]>(element_count)},
        elements_{boost::make_unique_noinit<T[]>(element_count)},
        elements_size_{element_count},
        row_count_{row_count},
        column_count_{column_count},
        region_ordering_{RegionOrdering::block_lexicographical}
{
    auto const max_csr_element_count = std::numeric_limits<std::uint32_t>::max();
    if (element_count > max_csr_element_count) {
        throw LogicError{
                "The element count of the passed CSR matrix can’t be greater than "
                + std::to_string(max_csr_element_count) + '.'
            };
    }

    if (element_count > row_count * column_count
        || (row_count == 0 && column_count != 0)
        || (row_count != 0 && column_count == 0)) {
        throw LogicError{"Attempting to instantiate hsf::Matrix with invalid dimensions."};
    }

    if (element_count == 0) {
        return;
    }

    if (row_pointers == nullptr) {
        throw LogicError{"The row pointer array of the passed CSR matrix can’t be nullptr."};
    }
    if (columns == nullptr) {
        throw LogicError{"The column index array of the passed CSR matrix can’t be nullptr."};
    }
    if (elements == nullptr) {
        throw LogicError{"The element value array of the passed CSR matrix can’t be nullptr."};
    }

    auto const max_row_count = std::numeric_limits<std::uint32_t>::max() - 1;
    if (row_count > max_row_count) {
        throw LogicError{
                "hsf::Matrix doesn’t support row counts greater than " + std::to_string(max_row_count) + '.'
            };
    }

    auto const max_column_count = std::numeric_limits<std::uint32_t>::max() - 1;
    if (column_count > max_column_count) {
        throw LogicError{
                "hsf::Matrix doesn’t support column counts greater than " + std::to_string(max_column_count) + '.'
            };
    }

    if (Symm == Symmetric::yes && row_count != column_count) {
        throw LogicError{"hsf::Matrix can’t be symmetric and rectangular simultaneously."};
    }

    if (normalise_regions_enum == NormaliseRegions::yes) {
        if (region_coefficient < 0 || region_coefficient > 1) {
            throw LogicError{"The passed region coefficient has to be in interval [0, 1]."};
        }
        if (thread_count <= 0) {
            throw LogicError{"The passed thread count has to be greater than zero."};
        }
    }

    auto const row_bounds = Bounds{static_cast<std::uint32_t>(row_count)};
    auto const col_bounds = Bounds{static_cast<std::uint32_t>(column_count)};

    auto const input_row_pointers = std::span<std::uint32_t const>{row_pointers, row_count + 1};
    auto const input_columns = std::span<std::uint32_t const>{columns, element_count};
    auto const input_elements = std::span<T const>{elements, element_count};

    auto const region_data_size = row_bounds.get_region_count() * col_bounds.get_region_count();
    auto region_data = boost::make_unique_noinit<RegionData[]>(region_data_size);
    auto region_info = RegionInfo{
            row_bounds,
            col_bounds,
            input_row_pointers,
            input_columns,
            {region_data.get(), region_data_size}
        };

    this->coo_rows_size_ = region_info.get_coo_rows_element_count();
    this->coo_rows_ = boost::make_unique_noinit<RowIndex[]>(this->coo_rows_size_);

    this->csr_rows_size_ = region_info.get_csr_rows_element_count();
    this->csr_rows_ = boost::make_unique_noinit<RowPointer[]>(this->csr_rows_size_);

    this->regions_size_ = region_info.get_nonempty_region_count();
    this->regions_ = std::make_unique<Region<T>[]>(this->regions_size_);

    region_info.init_regions(
            row_bounds,
            col_bounds,
            {this->coo_rows_.get(), this->coo_rows_size_},
            {this->csr_rows_.get(), this->csr_rows_size_},
            {this->columns_.get(), this->elements_size_},
            std::span<T>{this->elements_.get(), this->elements_size_},
            {this->regions_.get(), this->regions_size_}
        );

    auto const row_bound_count = row_bounds.get_region_count();
    if (row_bound_count > static_cast<std::size_t>(omp_get_max_threads())) {
        #pragma omp parallel for schedule(dynamic) default(none) shared(input_columns, input_elements, \
            input_row_pointers, region_info, row_bound_count, row_bounds)
        for (auto row_bound = 0_uz; row_bound < row_bound_count; ++row_bound) {
            insert_elements(
                    input_row_pointers,
                    input_columns,
                    input_elements,
                    row_bounds,
                    row_bound,
                    region_info,
                    {this->coo_rows_.get(), this->coo_rows_size_},
                    {this->csr_rows_.get(), this->csr_rows_size_},
                    {this->columns_.get(), this->elements_size_},
                    {this->elements_.get(), this->elements_size_}
                );
        }

    } else if (row_bound_count > 1) {
        #pragma omp parallel for num_threads(row_bound_count) schedule(static) default(none) shared(input_columns, \
            input_elements, input_row_pointers, region_info, row_bound_count, row_bounds)
        for (auto row_bound = 0_uz; row_bound < row_bound_count; ++row_bound) {
            insert_elements(
                    input_row_pointers,
                    input_columns,
                    input_elements,
                    row_bounds,
                    row_bound,
                    region_info,
                    {this->coo_rows_.get(), this->coo_rows_size_},
                    {this->csr_rows_.get(), this->csr_rows_size_},
                    {this->columns_.get(), this->elements_size_},
                    {this->elements_.get(), this->elements_size_}
                );
        }

    } else if (row_bound_count == 1) {
        insert_elements(
                input_row_pointers,
                input_columns,
                input_elements,
                row_bounds,
                0,
                region_info,
                {this->coo_rows_.get(), this->coo_rows_size_},
                {this->csr_rows_.get(), this->csr_rows_size_},
                {this->columns_.get(), this->elements_size_},
                {this->elements_.get(), this->elements_size_}
            );
    }

    region_info.finalise_row_pointers(row_bounds, {this->csr_rows_.get(), this->csr_rows_size_});

    if (normalise_regions_enum == NormaliseRegions::yes) {
        this->normalise_regions(region_coefficient, thread_count);
    }

    this->sort_regions(region_ordering);

    if (optimise_csr_regions_enum == OptimiseCsrRegions::yes) {
        this->optimise_csr_regions();
    }
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::normalise_regions(double const region_coefficient, int const thread_count) -> void
{
    assert(region_coefficient >= 0);
    assert(region_coefficient <= 1);
    assert(thread_count > 0);

    auto const region_threshold = compute_region_threshold(this->element_count(), thread_count, region_coefficient);

    auto subregion_data_v = std::vector<SubregionData<T>>{};
    subregion_data_v.reserve(this->region_count());

    auto split_data_v = std::vector<SplitData<T>>(this->region_count());

    auto all_row_pointer_size = 0_uz;
    auto all_column_pointer_size = 0_uz;

    for (auto i = 0_uz; i < this->region_count(); ++i) {
        if (this->regions_[i].element_count < region_threshold) {
            continue;
        }

        auto& subregion_data = subregion_data_v.emplace_back(
                &this->regions_[i],
                all_row_pointer_size,
                all_column_pointer_size
            );
        split_data_v[i].subregion_data = &subregion_data;

        if (get_region_type(this->regions_[i]) == RegionType::COO) {
            all_row_pointer_size += this->regions_[i].row_count + 1;
        }
        all_column_pointer_size += this->regions_[i].column_count + 1;
    }

    if (subregion_data_v.empty()) {
        return;
    }

    auto const subregion_capacity = SubBounds::get_subregion_capacity() * SubBounds::get_subregion_capacity();
    auto const all_region_data_size = subregion_capacity * subregion_data_v.size();
    auto all_region_data = boost::make_unique_noinit<RegionData[]>(all_region_data_size);
    auto all_row_pointers = boost::make_unique_noinit<RowPointer[]>(all_row_pointer_size);
    auto all_column_pointers = boost::make_unique_noinit<RowPointer[]>(all_column_pointer_size);

    if (subregion_data_v.size() > static_cast<std::size_t>(omp_get_max_threads())) {
        #pragma omp parallel for schedule(dynamic) default(none) shared(all_column_pointers, all_region_data, \
            all_row_pointers, region_threshold, subregion_data_v, thread_count)
        for (auto i = 0_uz; i < subregion_data_v.size(); ++i) {
            init_subregion_data(
                    subregion_data_v[i],
                    all_row_pointers.get(),
                    all_column_pointers.get(),
                    all_region_data.get(),
                    i,
                    region_threshold,
                    thread_count
                );
        }

    } else if (subregion_data_v.size() > 1) {
        #pragma omp parallel for num_threads(subregion_data_v.size()) schedule(static) default(none) \
            shared(all_column_pointers, all_region_data, all_row_pointers, region_threshold, subregion_data_v, \
            thread_count)
        for (auto i = 0_uz; i < subregion_data_v.size(); ++i) {
            init_subregion_data(
                    subregion_data_v[i],
                    all_row_pointers.get(),
                    all_column_pointers.get(),
                    all_region_data.get(),
                    i,
                    region_threshold,
                    thread_count
                );
        }

    } else if (subregion_data_v.size() == 1) {
        init_subregion_data(
                subregion_data_v[0],
                all_row_pointers.get(),
                all_column_pointers.get(),
                all_region_data.get(),
                0,
                region_threshold,
                thread_count
            );
    }

    for (auto i = 1_uz; i < split_data_v.size(); ++i) {
        auto const i_z = static_cast<std::ptrdiff_t>(i);
        auto const it = split_data_v.begin() + i_z;
        auto const prev_it = split_data_v.cbegin() + i_z - 1;

        auto const& region = this->regions_[i - 1];
        assert(region.element_count <= std::numeric_limits<std::uint32_t>::max());
        auto const region_element_count_u32 = static_cast<std::uint32_t>(region.element_count);
        it->element_index = prev_it->element_index + region_element_count_u32;

        if (prev_it->subregion_data == nullptr) {
            it->region_index = prev_it->region_index + 1;

            switch (get_region_type(region)) {
            case RegionType::COO:
                it->coo_row_index = prev_it->coo_row_index + region_element_count_u32;
                it->csr_row_index = prev_it->csr_row_index;
                break;

            case RegionType::CSR:
                it->coo_row_index = prev_it->coo_row_index;
                it->csr_row_index = prev_it->csr_row_index + region.row_count + 1;
                break;
            }

        } else {                // prev_it->subregion_data != nullptr
            auto const& prev_region_info = prev_it->subregion_data->region_info;
            #ifndef NDEBUG
            auto const max = std::numeric_limits<std::uint32_t>::max();
            #endif

            assert(prev_region_info.get_nonempty_region_count() <= max);
            it->region_index
                = prev_it->region_index
                + static_cast<std::uint32_t>(prev_region_info.get_nonempty_region_count());

            assert(prev_region_info.get_coo_rows_element_count() <= max);
            it->coo_row_index
                = prev_it->coo_row_index
                + static_cast<std::uint32_t>(prev_region_info.get_coo_rows_element_count());

            assert(prev_region_info.get_csr_rows_element_count() <= max);
            it->csr_row_index
                = prev_it->csr_row_index
                + static_cast<std::uint32_t>(prev_region_info.get_csr_rows_element_count());
        }
    }

    auto const& last_region = this->regions_[this->region_count() - 1];
    auto const& last_split_data = split_data_v.back();

    auto new_coo_rows_size = std::size_t{};
    auto new_csr_rows_size = std::size_t{};
    auto new_regions_size = std::size_t{};
    std::tie(new_coo_rows_size, new_csr_rows_size, new_regions_size)
        = calculate_total_counts(last_region, last_split_data);

    auto new_coo_rows = boost::make_unique_noinit<RowIndex[]>(new_coo_rows_size);
    auto new_csr_rows = boost::make_unique_noinit<RowPointer[]>(new_csr_rows_size);
    auto new_columns = boost::make_unique_noinit<ColIndex[]>(this->element_count());
    auto new_elements = boost::make_unique_noinit<T[]>(this->element_count());
    auto new_regions = std::make_unique<Region<T>[]>(new_regions_size);

    if (this->region_count() > static_cast<std::size_t>(omp_get_max_threads())) {
        #pragma omp parallel for schedule(dynamic) default(none) shared(split_data_v, new_coo_rows, \
            new_coo_rows_size, new_csr_rows, new_csr_rows_size, new_columns, new_elements, new_regions, \
            new_regions_size)
        for (auto i = 0_uz; i < this->region_count(); ++i) {
            copy(this->regions_[i],
                 split_data_v[i],
                 {new_coo_rows.get(), new_coo_rows_size},
                 {new_csr_rows.get(), new_csr_rows_size},
                 {new_columns.get(), this->element_count()},
                 {new_elements.get(), this->element_count()},
                 {new_regions.get(), new_regions_size});
        }

    } else if (this->region_count() > 1) {
        #pragma omp parallel for num_threads(this->region_count()) schedule(static) default(none) \
            shared(split_data_v, new_coo_rows, new_coo_rows_size, new_csr_rows, new_csr_rows_size, new_columns, \
            new_elements, new_regions, new_regions_size)
        for (auto i = 0_uz; i < this->region_count(); ++i) {
            copy(this->regions_[i],
                 split_data_v[i],
                 {new_coo_rows.get(), new_coo_rows_size},
                 {new_csr_rows.get(), new_csr_rows_size},
                 {new_columns.get(), this->element_count()},
                 {new_elements.get(), this->element_count()},
                 {new_regions.get(), new_regions_size});
        }

    } else if (this->region_count() == 1) {
        copy(this->regions_[0],
             split_data_v[0],
             {new_coo_rows.get(), new_coo_rows_size},
             {new_csr_rows.get(), new_csr_rows_size},
             {new_columns.get(), this->element_count()},
             {new_elements.get(), this->element_count()},
             {new_regions.get(), new_regions_size});
    }

    this->coo_rows_ = std::move(new_coo_rows);
    this->csr_rows_ = std::move(new_csr_rows);
    this->columns_ = std::move(new_columns);
    this->elements_ = std::move(new_elements);
    this->regions_ = std::move(new_regions);

    this->coo_rows_size_ = new_coo_rows_size;
    this->csr_rows_size_ = new_csr_rows_size;
    this->regions_size_ = new_regions_size;
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::optimise_csr_regions() -> void
{
    if (this->are_csr_regions_optimised()) {
        return;
    }

    for (auto i = 0_uz; i < this->region_count(); ++i) {
        auto& region = this->regions_[i];
        std::visit(OptimiseCsrRegionVisitor{region}, region.rows);
    }
    this->csr_regions_optimised_ = true;
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::sort_regions(RegionOrdering const region_ordering) -> void
{
    // The regions have already been sorted.
    if (this->region_ordering() == region_ordering) {
        return;
    }

    if (this->are_csr_regions_optimised()) {
        throw LogicError{
                "Sorting the regions of a hierarchical matrix after performing the CSR region optimisation "
                "step isn’t supported."
            };
    }

    switch (region_ordering) {
    case RegionOrdering::block_lexicographical:
        throw LogicError{
                "Sorting the regions of a hierarchical matrix block-lexicographically isn’t supported."
            };

    case RegionOrdering::lexicographical:
        sort_lexicographically(this->regions_, this->region_count());
        this->region_ordering_ = RegionOrdering::lexicographical;
        return;

    case RegionOrdering::morton:
        sort_in_morton_order(this->regions_, this->region_count());
        this->region_ordering_ = RegionOrdering::morton;
        return;
    }

    throw LogicError{"Unexpected enumeration value."};
}

template class Matrix<float, Symmetric::no>;
template class Matrix<float, Symmetric::yes>;
template class Matrix<double, Symmetric::no>;
template class Matrix<double, Symmetric::yes>;

template<typename T, Symmetric Symm>
auto are_regions_sorted(Matrix<T, Symm> const& matrix) -> bool
{
    if (matrix.are_csr_regions_optimised()) {
        throw LogicError{
                "Checking whether the regions of a hierarchical matrix are sorted is unsupported after "
                "performing the CSR region optimisation step."
            };
    }

    switch (matrix.region_ordering()) {
    case RegionOrdering::block_lexicographical:
        return are_block_lexicographically_ordered(matrix.regions());
    case RegionOrdering::lexicographical:
        return are_lexicographically_ordered(matrix.regions());
    case RegionOrdering::morton:
        return are_morton_ordered(matrix.regions());
    }

    throw LogicError{"Unexpected enumeration value."};
}

template auto are_regions_sorted(Matrix<float, Symmetric::no> const& matrix) -> bool;
template auto are_regions_sorted(Matrix<float, Symmetric::yes> const& matrix) -> bool;
template auto are_regions_sorted(Matrix<double, Symmetric::no> const& matrix) -> bool;
template auto are_regions_sorted(Matrix<double, Symmetric::yes> const& matrix) -> bool;

template<typename T, Symmetric Symm>
auto swap(Matrix<T, Symm>& lhs, Matrix<T, Symm>& rhs) noexcept -> void
{
    boost::swap(lhs.coo_rows_, rhs.coo_rows_);
    boost::swap(lhs.coo_rows_size_, rhs.coo_rows_size_);
    boost::swap(lhs.csr_rows_, rhs.csr_rows_);
    boost::swap(lhs.csr_rows_size_, rhs.csr_rows_size_);
    boost::swap(lhs.columns_, rhs.columns_);
    boost::swap(lhs.elements_, rhs.elements_);
    boost::swap(lhs.elements_size_, rhs.elements_size_);
    boost::swap(lhs.row_count_, rhs.row_count_);
    boost::swap(lhs.column_count_, rhs.column_count_);
    boost::swap(lhs.regions_, rhs.regions_);
    boost::swap(lhs.regions_size_, rhs.regions_size_);
    boost::swap(lhs.region_ordering_, rhs.region_ordering_);
    boost::swap(lhs.csr_regions_optimised_, rhs.csr_regions_optimised_);
}

template auto swap(Matrix<float, Symmetric::no>& lhs, Matrix<float, Symmetric::no>& rhs) noexcept -> void;
template auto swap(Matrix<float, Symmetric::yes>& lhs, Matrix<float, Symmetric::yes>& rhs) noexcept -> void;
template auto swap(Matrix<double, Symmetric::no>& lhs, Matrix<double, Symmetric::no>& rhs) noexcept -> void;
template auto swap(Matrix<double, Symmetric::yes>& lhs, Matrix<double, Symmetric::yes>& rhs) noexcept -> void;

namespace {

CopyRegionDataVisitor::CopyRegionDataVisitor(
        std::variant<info::RowIndex*, info::RowPointer*>& new_region_rows,
        info::RowIndex* const new_coo_rows,
        info::RowPointer* const new_csr_rows,
        std::size_t const coo_row_index,
        std::size_t const csr_row_index,
        std::size_t const row_count,
        std::size_t const element_count
    ) noexcept
    : new_region_rows_{&new_region_rows},
      new_coo_rows_{new_coo_rows},
      new_csr_rows_{new_csr_rows},
      coo_row_index_{coo_row_index},
      csr_row_index_{csr_row_index},
      row_count_{row_count},
      element_count_{element_count}
{
}

auto CopyRegionDataVisitor::operator()(info::RowIndex* const old_region_row_indices) -> void
{
    assert(this->new_region_rows_ != nullptr);
    assert(this->new_coo_rows_ != nullptr);
    assert(this->new_csr_rows_ != nullptr);

    *this->new_region_rows_ = this->new_coo_rows_ + this->coo_row_index_;
    std::copy(
            old_region_row_indices,
            old_region_row_indices + this->element_count_,
            this->new_coo_rows_ + this->coo_row_index_
        );
}

auto CopyRegionDataVisitor::operator()(info::RowPointer* const old_region_row_pointers) -> void
{
    assert(this->new_region_rows_ != nullptr);
    assert(this->new_coo_rows_ != nullptr);
    assert(this->new_csr_rows_ != nullptr);

    *this->new_region_rows_ = this->new_csr_rows_ + this->csr_row_index_;
    std::copy(
            old_region_row_pointers,
            old_region_row_pointers + this->row_count_ + 1,
            this->new_csr_rows_ + this->csr_row_index_
        );
}

template<typename T>
CopyRegionVisitor<T>::CopyRegionVisitor(
        info::RowIndex* const& lhs_coo_rows,
        info::RowPointer* const& lhs_csr_rows,
        Region<T>& lhs_region,
        info::RowIndex* const& rhs_coo_rows,
        info::RowPointer* const& rhs_csr_rows
    ) noexcept
    : lhs_coo_rows_{lhs_coo_rows},
      lhs_csr_rows_{lhs_csr_rows},
      lhs_region_{&lhs_region},
      rhs_coo_rows_{rhs_coo_rows},
      rhs_csr_rows_{rhs_csr_rows}
{
}

template<typename T>
auto CopyRegionVisitor<T>::operator()(info::RowIndex* const rhs_region_coo_rows) const -> void
{
    assert(this->lhs_coo_rows_ != nullptr);
    assert(this->lhs_region_ != nullptr);
    assert(this->rhs_coo_rows_ != nullptr);

    auto const coo_rows_offset = rhs_region_coo_rows - this->rhs_coo_rows_;
    this->lhs_region_->rows = this->lhs_coo_rows_ + coo_rows_offset;
}

template<typename T>
auto CopyRegionVisitor<T>::operator()(info::RowPointer* const rhs_region_csr_rows) const -> void
{
    assert(this->lhs_csr_rows_ != nullptr);
    assert(this->lhs_region_ != nullptr);
    assert(this->rhs_csr_rows_ != nullptr);

    auto const csr_rows_offset = rhs_region_csr_rows - this->rhs_csr_rows_;
    this->lhs_region_->rows = this->lhs_csr_rows_ + csr_rows_offset;
}

template<typename T>
OptimiseCsrRegionVisitor<T>::OptimiseCsrRegionVisitor(Region<T>& region) noexcept
    : region_{&region}
{
}

template<typename T>
auto OptimiseCsrRegionVisitor<T>::operator()(info::RowIndex*) const noexcept -> void
{
}

template<typename T>
auto OptimiseCsrRegionVisitor<T>::operator()(info::RowPointer*& row_pointers) const noexcept(ndebug) -> void
{
    assert(this->region_ != nullptr);

    auto const row_pointers_span = std::span<info::RowPointer const>{row_pointers, this->region_->row_count + 1};
    // A CSR row pointer array has at least one element.
    assert(!row_pointers_span.empty());

    if (row_pointers_span.size() <= 1) {
        // This branch should never be taken because the implementation
        // shouldn’t create regions with zero elements.
        return;
    }

    // The first and last indices of a CSR row pointer array can’t be the same
    // if the array belongs to a region with at least one element.
    assert(row_pointers_span.front() != row_pointers_span.back());

    auto const begin = row_pointers_span.begin();
    auto const end = row_pointers_span.end();
    auto new_begin = begin;
    for (auto it = begin + 1; it != end; ++it) {
        if (*it != row_pointers_span.front()) {
            break;
        }
        ++new_begin;
    }

    auto const rbegin = row_pointers_span.rbegin();
    auto const rend = row_pointers_span.rend();
    auto new_rbegin = rbegin;
    for (auto it = rbegin + 1; it != rend; ++it) {
        if (*it != row_pointers_span.back()) {
            break;
        }
        ++new_rbegin;
    }

    if (new_begin == begin && new_rbegin == rbegin) {
        return;                 // Nothing can be optimised.
    }

    auto const new_end = new_rbegin.base();
    // The following assertion holds if ‘row_pointers_span’ is a valid CSR
    // array with at least one element.
    assert(new_begin < new_end - 1);
    auto const new_row_pointers_size = std::distance(new_begin, new_end);

    this->region_->row_begin += static_cast<std::uint32_t>(new_begin - begin);
    this->region_->row_count = static_cast<std::uint32_t>(new_row_pointers_size - 1);
    row_pointers += new_begin - begin;
}

RowPointersVisitor::RowPointersVisitor(
        std::size_t const element_count,
        std::uint32_t const row_count,
        info::RowPointer* const row_pointers_offset
    ) noexcept
    : element_count_{element_count},
      row_count_{row_count},
      row_pointers_offset_{row_pointers_offset}
{
}

auto RowPointersVisitor::operator()(info::RowIndex* const row_indices) const -> info::RowPointer*
{
    row_indices_to_pointers({row_indices, this->element_count_}, {this->row_pointers_offset_, this->row_count_ + 1});
    return this->row_pointers_offset_;
}

auto RowPointersVisitor::operator()(info::RowPointer* const row_pointers) const noexcept -> info::RowPointer*
{
    return row_pointers;
}

template<typename T>
auto are_block_lexicographically_ordered(std::span<Region<T> const> const regions) -> bool
{
    if (regions.size() <= 1) {
        return true;
    }

    for (auto it = regions.begin() + 1; it != regions.end(); ++it) {
        auto const& prev_region = *(it - 1);
        auto const& this_region = *it;

        // The following row index check should be performed only for a pair of
        // regions that were created by normalising the same parent region.
        auto const check_row = are_from_same_parent_region(prev_region, this_region);

        // Apart from the aforementioned exception, a region can’t begin with a
        // lower-numbered row than the previous region.
        if (check_row && prev_region.row_begin > this_region.row_begin) {
            return false;
        }

        // A region that begins with the same row as the previous region has to
        // begin with a higher-numbered column.
        if (prev_region.row_begin == this_region.row_begin && prev_region.column_begin >= this_region.column_begin) {
            return false;
        }
    }

    return true;
}

template<typename T>
auto are_from_same_parent_region(Region<T> const& region1, Region<T> const& region2) noexcept -> bool
{
    auto const parent_row1 = region1.row_begin % info::region_size;
    auto const parent_row2 = region2.row_begin % info::region_size;
    if (parent_row1 != parent_row2) {
        return false;
    }

    auto const parent_column1 = region1.column_begin % info::region_size;
    auto const parent_column2 = region2.column_begin % info::region_size;
    if (parent_column1 != parent_column2) {
        return false;
    }

    return true;
}

template<typename T>
auto are_lexicographically_ordered(std::span<Region<T> const> const regions) -> bool
{
    auto const comp = [](Region<T> const& lhs, Region<T> const& rhs) {
        if (lhs.row_begin != rhs.row_begin) {
            return lhs.row_begin < rhs.row_begin;
        }
        // lhs.row_begin == rhs.row_begin
        return lhs.column_begin < rhs.column_begin;
    };
    return std::is_sorted(regions.begin(), regions.end(), comp);
}

template<typename T>
auto are_morton_ordered(std::span<Region<T> const> const regions) -> bool
{
    if (regions.size() <= 1) {
        return true;
    }

    auto previous_morton_code = morton::encode(regions.front().column_begin, regions.front().row_begin);
    for (auto it = regions.begin() + 1; it != regions.end(); ++it) {
        auto const current_morton_code = morton::encode(it->column_begin, it->row_begin);
        if (previous_morton_code >= current_morton_code) {
            return false;
        }
        previous_morton_code = current_morton_code;
    }

    return true;
}

template<typename T>
auto calculate_total_counts(Region<T> const& region, SplitData<T> const& split_data)
    -> std::tuple<std::size_t, std::size_t, std::size_t>
{
    auto coo_rows_element_count = 0_uz;
    auto csr_rows_element_count = 0_uz;
    auto region_count = 0_uz;

    if (split_data.subregion_data == nullptr) {
        switch (get_region_type(region)) {
        case RegionType::COO:
            coo_rows_element_count = split_data.coo_row_index + region.element_count;
            csr_rows_element_count = split_data.csr_row_index;
            break;

        case RegionType::CSR:
            coo_rows_element_count = split_data.coo_row_index;
            csr_rows_element_count = split_data.csr_row_index + region.row_count + 1;
            break;
        }

        region_count = split_data.region_index + 1;

    } else {                    // split_data.subregion_data != nullptr
        auto const& region_info = split_data.subregion_data->region_info;
        coo_rows_element_count = split_data.coo_row_index + region_info.get_coo_rows_element_count();
        csr_rows_element_count = split_data.csr_row_index + region_info.get_csr_rows_element_count();
        region_count = split_data.region_index + region_info.get_nonempty_region_count();
    }

    return {coo_rows_element_count, csr_rows_element_count, region_count};
}

auto column_indices_to_pointers(
        std::span<info::ColIndex const> const column_indices, std::span<info::RowPointer> const column_pointers
    ) -> void
{
    row_indices_to_pointers(column_indices, column_pointers);
}

auto compute_region_threshold(
        std::size_t const nonzero_element_count, int const thread_count, double const region_coefficient
    ) noexcept(ndebug) -> std::size_t
{
    assert(thread_count > 0);

    auto const threshold = region_coefficient * static_cast<double>(nonzero_element_count) / thread_count;

    // Don’t let the threshold become too low.
    if (threshold < info::region_size) {
        return info::region_size;
    }

    return static_cast<std::size_t>(threshold);
}

template<typename T>
auto copy(
        Region<T> const& old_region,
        SplitData<T> const& split_data,
        std::span<info::RowIndex> const new_coo_rows,
        std::span<info::RowPointer> const new_csr_rows,
        std::span<info::ColIndex> const new_columns,
        std::span<T> const new_elements,
        std::span<Region<T>> const new_regions
    ) -> void
{
    if (split_data.subregion_data == nullptr) {
        copy_region(
                old_region,
                split_data,
                new_coo_rows.data(),
                new_csr_rows.data(),
                new_columns.data(),
                new_elements.data(),
                new_regions[split_data.region_index]
            );

    } else {                    // split_data.subregion_data != nullptr
        copy_subregions(
                split_data,
                new_coo_rows,
                new_csr_rows,
                new_columns,
                new_elements,
                new_regions
            );
    }
}

template<typename T>
auto copy_region(
        Region<T> const& old_region,
        SplitData<T> const& split_data,
        info::RowIndex* const new_coo_rows,
        info::RowPointer* const new_csr_rows,
        info::ColIndex* const new_columns,
        T* const new_elements,
        Region<T>& new_region
    ) -> void
{
    assert(new_coo_rows != nullptr);
    assert(new_csr_rows != nullptr);
    assert(new_columns != nullptr);
    assert(new_elements != nullptr);

    new_region.row_begin = old_region.row_begin;
    new_region.column_begin = old_region.column_begin;
    new_region.row_count = old_region.row_count;
    new_region.column_count = old_region.column_count;
    new_region.element_count = old_region.element_count;

    new_region.columns = new_columns + split_data.element_index;
    new_region.elements = new_elements + split_data.element_index;

    auto visitor = CopyRegionDataVisitor{
            new_region.rows,
            new_coo_rows,
            new_csr_rows,
            split_data.coo_row_index,
            split_data.csr_row_index,
            old_region.row_count,
            old_region.element_count
        };
    std::visit(visitor, old_region.rows);

    std::copy(old_region.columns, old_region.columns + old_region.element_count, new_region.columns);
    std::copy(old_region.elements, old_region.elements + old_region.element_count, new_region.elements);
}

template<typename T>
auto copy_subregions(
        SplitData<T> const& split_data,
        std::span<info::RowIndex> const new_coo_rows,
        std::span<info::RowPointer> const new_csr_rows,
        std::span<info::ColIndex> const new_columns,
        std::span<T> const new_elements,
        std::span<Region<T>> const new_regions
    ) -> void
{
    assert(split_data.subregion_data != nullptr);
    assert(split_data.subregion_data->region != nullptr);

    auto const& row_bounds = split_data.subregion_data->row_bounds;
    auto const& column_bounds = split_data.subregion_data->column_bounds;
    auto const& region = *split_data.subregion_data->region;
    auto& region_info = split_data.subregion_data->region_info;
    auto const& row_pointers = split_data.subregion_data->row_pointers;

    auto const new_coo_rows_subspan = new_coo_rows.subspan(
            split_data.coo_row_index, region_info.get_coo_rows_element_count()
        );
    auto const new_csr_rows_subspan = new_csr_rows.subspan(
            split_data.csr_row_index, region_info.get_csr_rows_element_count()
        );
    auto const new_columns_subspan = new_columns.subspan(
            split_data.element_index, region.element_count
        );
    auto const new_elements_subspan = new_elements.subspan(
            split_data.element_index, region.element_count
        );
    auto const new_regions_subspan = new_regions.subspan(
            split_data.region_index, region_info.get_nonempty_region_count()
        );

    region_info.init_regions(
            row_bounds,
            column_bounds,
            new_coo_rows_subspan,
            new_csr_rows_subspan,
            new_columns_subspan,
            new_elements_subspan,
            new_regions_subspan
        );

    auto const rb_count = row_bounds.get_subregion_count();
    for (auto row_bound = 0_uz; row_bound < rb_count; ++row_bound) {

        auto const row_bounds_begin = row_bounds.get_index(row_bound);
        auto const row_bounds_end = row_bounds.get_index(row_bound + 1);
        for (auto row = row_bounds_begin; row < row_bounds_end; ++row) {

            auto const new_row = row - row_bounds_begin;
            auto const row_begin = row_pointers[row];
            auto const row_end = row_pointers[row + 1];
            for (auto i = row_begin; i < row_end; ++i) {

                auto const col = region.columns[i];
                auto const col_bound = column_bounds.find_subregion_bound(col);
                auto const new_col = col - column_bounds.get_index(col_bound);
                insert_element(
                        static_cast<info::RowIndex>(new_row),
                        static_cast<info::ColIndex>(new_col),
                        region.elements[i],
                        row_bound,
                        col_bound,
                        region_info,
                        new_coo_rows_subspan,
                        new_csr_rows_subspan,
                        new_columns_subspan,
                        new_elements_subspan
                    );
            }
        }
    }

    region_info.finalise_row_pointers(row_bounds, new_csr_rows_subspan);
}

auto index_to_second_level(std::uint32_t const index) noexcept -> info::ColIndex
{
    return index & 0xffff;
}

template<typename T>
auto init_subregion_data(
        SubregionData<T>& subregion_data,
        info::RowPointer* const all_row_pointers,
        info::RowPointer* const all_column_pointers,
        RegionData* const all_region_data,
        std::size_t const region_data_index,
        std::size_t const region_threshold,
        int const thread_count
    ) -> void
{
    assert(subregion_data.region != nullptr);

    auto& row_bounds = subregion_data.row_bounds;
    auto& column_bounds = subregion_data.column_bounds;
    auto const& region = *subregion_data.region;
    auto& region_info = subregion_data.region_info;
    auto& row_pointers = subregion_data.row_pointers;
    auto const& row_pointers_index = subregion_data.row_pointers_index;
    auto const& column_pointers_index = subregion_data.column_pointers_index;

    auto* const row_pointers_offset = all_row_pointers + row_pointers_index;
    auto const visitor = RowPointersVisitor{region.element_count, region.row_count, row_pointers_offset};
    row_pointers = std::visit(visitor, region.rows);
    row_bounds.init(
            {row_pointers, region.row_count + 1},
            region.element_count,
            region.row_begin,
            region_threshold,
            thread_count
        );

    auto* const column_pointers_offset = all_column_pointers + column_pointers_index;
    auto const column_pointers = std::span<info::RowPointer>{column_pointers_offset, region.column_count + 1};
    column_indices_to_pointers({region.columns, region.element_count}, column_pointers);
    column_bounds.init(
            column_pointers,
            region.element_count,
            region.column_begin,
            region_threshold,
            thread_count
        );

    auto const subregion_capacity = SubBounds::get_subregion_capacity() * SubBounds::get_subregion_capacity();
    auto* const region_data_offset = all_region_data + region_data_index * subregion_capacity;
    auto const region_data_size = row_bounds.get_subregion_count() * column_bounds.get_subregion_count();
    region_info = RegionInfo{
            row_bounds,
            column_bounds,
            {row_pointers, region.row_count + 1},
            {region.columns, region.element_count},
            {region_data_offset, region_data_size}
        };
}

template<typename T>
auto insert_element(
        info::RowIndex const row_index,
        info::ColIndex const col_index,
        T const element,
        std::size_t const row_bound,
        std::size_t const col_bound,
        RegionInfo& region_info,
        std::span<info::RowIndex> const coo_rows,
        std::span<info::RowPointer> const csr_rows,
        std::span<info::ColIndex> const columns,
        std::span<T> const elements
    ) -> void
{
    auto const elem_i = region_info.get_element_index(row_bound, col_bound);
    region_info.increment_element_index(row_bound, col_bound);
    columns[elem_i] = col_index;
    elements[elem_i] = element;

    switch (region_info.get_region_type(row_bound, col_bound)) {
    case RegionType::COO:
        {
            auto const coo_row_i = region_info.get_coo_row_index(row_bound, col_bound);
            region_info.increment_coo_row_index(row_bound, col_bound);
            coo_rows[coo_row_i] = row_index;
            break;
        }

    case RegionType::CSR:
        {
            auto const csr_row_i = region_info.get_csr_row_index(row_bound, col_bound);
            auto const prev_row = region_info.get_previous_row(row_bound, col_bound);

            // No rows have been skipped.  Increment the next row’s element
            // count.
            if (row_index == prev_row) {
                ++csr_rows[csr_row_i + row_index + 1];
                break;
            }

            // Copy the previous element count to skipped rows.
            auto const prev_element_count = csr_rows[csr_row_i + prev_row + 1];
            for (auto i = prev_row + 2; i <= row_index; ++i) {
                csr_rows[csr_row_i + i] = prev_element_count;
            }

            // Set the next row’s element count.
            csr_rows[csr_row_i + row_index + 1] = prev_element_count + 1;
            // Set the previous row for the next potential element insertion.
            region_info.set_previous_row(row_bound, col_bound, row_index);

            break;
        }
    }
}

template<typename T>
auto insert_elements(
        std::span<std::uint32_t const> const input_row_pointers,
        std::span<std::uint32_t const> const input_columns,
        std::span<T const> const input_elements,
        Bounds const& row_bounds,
        std::size_t const row_bound,
        RegionInfo& region_info,
        std::span<info::RowIndex> const output_coo_rows,
        std::span<info::RowPointer> const output_csr_rows,
        std::span<info::ColIndex> const output_columns,
        std::span<T> const output_elements
    ) -> void
{
    auto const row_bounds_begin = row_bounds.get_index(row_bound);
    auto const row_bounds_end = row_bounds.get_index(row_bound + 1);
    for (auto row = row_bounds_begin; row < row_bounds_end; ++row) {

        auto const new_row = row - row_bounds_begin;
        assert(new_row <= std::numeric_limits<info::RowIndex>::max());

        for (auto i = input_row_pointers[row]; i < input_row_pointers[row + 1]; ++i) {

            auto const column = input_columns[i];
            insert_element(
                    static_cast<info::RowIndex>(new_row),
                    index_to_second_level(column),
                    input_elements[i],
                    row_bound,
                    index_to_region_bound(column),
                    region_info,
                    output_coo_rows,
                    output_csr_rows,
                    output_columns,
                    output_elements
                );
        }
    }
}

auto row_indices_to_pointers(
        std::span<info::RowIndex const> const row_indices, std::span<info::RowPointer> const row_pointers
    ) -> void
{
    std::fill(row_pointers.begin(), row_pointers.end(), 0);

    for (auto i = 0_uz; i < row_indices.size(); ++i) {
        auto const row = row_indices[i];
        ++row_pointers[row];
    }

    std::exclusive_scan(row_pointers.begin(), row_pointers.end(), row_pointers.begin(), 0);
}

template<typename T>
auto sort_in_morton_order(std::unique_ptr<Region<T>[]>& regions, std::size_t const region_count) -> void
{
    struct MCodeRegion {
        std::uint64_t mcode;
        Region<T>* region;
    };

    auto mcodes_regions = std::vector<MCodeRegion>(region_count);
    for (auto i = 0_uz; i < region_count; ++i) {
        auto const row = regions[i].row_begin;
        auto const column = regions[i].column_begin;
        mcodes_regions[i].mcode = morton::encode(column, row);
        mcodes_regions[i].region = &regions[i];
    }

    auto const comp = [](MCodeRegion const& lhs, MCodeRegion const& rhs) {
        return lhs.mcode < rhs.mcode;
    };
    std::sort(mcodes_regions.begin(), mcodes_regions.end(), comp);

    auto sorted_regions = std::make_unique<Region<T>[]>(region_count);
    for (auto i = 0_uz; i < region_count; ++i) {
        sorted_regions[i] = std::move(*mcodes_regions[i].region);
    }
    regions = std::move(sorted_regions);
}

template<typename T>
auto sort_lexicographically(std::unique_ptr<Region<T>[]>& regions, std::size_t const region_count) -> void
{
    auto region_ptrs = std::vector<Region<T>*>(region_count);
    for (auto i = 0_uz; i < region_count; ++i) {
        region_ptrs[i] = &regions[i];
    }

    auto const comp = [](Region<T> const* const lhs, Region<T> const* const rhs) {
        if (lhs->row_begin != rhs->row_begin) {
            return lhs->row_begin < rhs->row_begin;
        }
        // lhs->row_begin == rhs->row_begin
        return lhs->column_begin < rhs->column_begin;
    };
    std::sort(region_ptrs.begin(), region_ptrs.end(), comp);

    auto sorted_regions = std::make_unique<Region<T>[]>(region_count);
    for (auto i = 0_uz; i < region_count; ++i) {
        sorted_regions[i] = std::move(*region_ptrs[i]);
    }
    regions = std::move(sorted_regions);
}

}  // namespace

}  // namespace hsf
