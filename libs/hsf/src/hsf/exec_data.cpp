#include <hsf/exec_data.hpp>

#include <algorithm>            // std::fill std::max std::rotate
#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <functional>           // std::invoke
#include <iterator>             // std::advance
#include <limits>               // std::numeric_limits
#include <mutex>                // std::lock_guard
#include <numeric>              // std::iota
#include <span>                 // std::span
#include <utility>              // std::move
#include <vector>               // std::vector

#include <boost/core/swap.hpp>  // boost::swap

#include <hsf/compute.hpp>      // hsf::compute
#include <hsf/exec_mode.hpp>    // hsf::ExecMode
#include <hsf/matrix.hpp>       // hsf::Matrix
#include <hsf/priv/cstddef.hpp>  // hsf::operator""_uz
#include <hsf/priv/ndebug.hpp>  // hsf::ndebug
#include <hsf/priv/region_indices_dll.hpp>  // hsf::RegionIndicesDll hsf::RegionIndicesDllIt
#include <hsf/priv/region_indices_sll.hpp>  // hsf::RegionIndicesSll
#include <hsf/priv/regions_data.hpp>  // hsf::set
#include <hsf/region_indices_logs.hpp>  // hsf::RegionIndicesLogs
#include <hsf/symmetric.hpp>    // hsf::Symmetric
#include <hsf/tags.hpp>         // hsf::tags

namespace hsf {

namespace {

auto init_region_indices_dll_shared(std::size_t region_count) -> RegionIndicesDll;

auto init_region_indices_dll_its_shared(int thread_count, RegionIndicesDll& region_indices)
    -> std::vector<RegionIndicesDllIt>;

auto init_region_indices_threads_exclusive(std::size_t region_count, int thread_count)
    -> std::vector<std::vector<std::size_t>>;

auto init_region_indices_threads_sll_exclusive(std::size_t region_count, int thread_count)
    -> std::vector<RegionIndicesSll>;

auto init_region_indices_threads_sll_shared(std::size_t region_count, int thread_count)
    -> std::vector<RegionIndicesSll>;

// Return an array of shallow copies of ‘RegionIndicesSll’ objects.  Parameter
// ‘region_indices_threads’ is the array of objects to copy.
auto shallow_copy(std::span<RegionIndicesSll const> region_indices_threads) -> std::vector<RegionIndicesSll>;

}  // namespace

template<typename T, Symmetric Symm>
ExecData<ExecMode::atomic_exclusive, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::OneVector
    ) : region_count_{matrix.region_count()},
        region_indices_threads_(init_region_indices_threads_exclusive(matrix.region_count(), thread_count))
{
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::atomic_exclusive, T, Symm>::region_count() const noexcept -> std::size_t
{
    return this->region_count_;
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::atomic_exclusive, T, Symm>::region_indices(int const thread_id) -> std::span<std::size_t>
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);

    assert(thread_id_uz < this->region_indices_threads_.size());
    return this->region_indices_threads_[thread_id_uz];
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::atomic_exclusive, T, Symm>::thread_count() const noexcept -> int
{
    auto const thread_count_uz = this->region_indices_threads_.size();
    assert(thread_count_uz <= std::numeric_limits<int>::max());
    return static_cast<int>(thread_count_uz);
}

template class ExecData<ExecMode::atomic_exclusive, float, Symmetric::no>;
template class ExecData<ExecMode::atomic_exclusive, float, Symmetric::yes>;
template class ExecData<ExecMode::atomic_exclusive, double, Symmetric::no>;
template class ExecData<ExecMode::atomic_exclusive, double, Symmetric::yes>;

template<typename T, Symmetric Symm>
ExecData<ExecMode::atomic_planned_shared, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::OneVector
    ) : region_count_{matrix.region_count()},
        region_indices_threads_(static_cast<std::size_t>(thread_count))
{
    assert(thread_count > 0);
    assert(matrix.column_count() == matrix.row_count());

    auto exec_data = ExecData<ExecMode::atomic_shared, T, Symm>{thread_count, matrix, tags::one_vector};
    auto logs = RegionIndicesLogs{matrix.region_count(), thread_count};
    auto const x1 = std::vector<T>(matrix.column_count(), static_cast<T>(3.14));
    auto y1 = std::vector<T>(x1.size());
    auto y2 = std::vector<T>(x1.size());

    // A warm-up iteration.
    compute::spmmtv_parallel<T>(matrix, x1, y1, y2, exec_data);
    std::fill(y1.begin(), y1.end(), 0);
    std::fill(y2.begin(), y2.end(), 0);

    compute::spmmtv_parallel<T>(matrix, x1, y1, y2, logs, exec_data);

    for (auto th = 0; th < logs.thread_count(); ++th) {
        auto region_indices = logs.get(th).region_indices();
        auto const th_uz = static_cast<std::size_t>(th);
        this->region_indices_threads_[th_uz] = std::move(region_indices);
    }
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::atomic_planned_shared, T, Symm>::region_count() const noexcept -> std::size_t
{
    return this->region_count_;
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::atomic_planned_shared, T, Symm>::region_indices(int const thread_id) -> std::span<std::size_t>
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);

    assert(thread_id_uz < this->region_indices_threads_.size());
    return this->region_indices_threads_[thread_id_uz];
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::atomic_planned_shared, T, Symm>::thread_count() const noexcept -> int
{
    auto const thread_count_uz = this->region_indices_threads_.size();
    assert(thread_count_uz <= std::numeric_limits<int>::max());
    return static_cast<int>(thread_count_uz);
}

template class ExecData<ExecMode::atomic_planned_shared, float, Symmetric::no>;
template class ExecData<ExecMode::atomic_planned_shared, float, Symmetric::yes>;
template class ExecData<ExecMode::atomic_planned_shared, double, Symmetric::no>;
template class ExecData<ExecMode::atomic_planned_shared, double, Symmetric::yes>;

template<typename T, Symmetric Symm>
ExecData<ExecMode::atomic_shared, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::OneVector
    ) : region_indices_{init_region_indices_dll_shared(matrix.region_count())},
        region_indices_copy_{this->region_indices_.shallow_copy()},
        region_indices_its_(init_region_indices_dll_its_shared(thread_count, this->region_indices_)),
        regions_finished_(matrix.region_count()),
        unfinished_region_count_{matrix.region_count()}
{
}

template<typename T, Symmetric Symm>
ExecData<ExecMode::atomic_shared, T, Symm>::ExecData(ExecData<ExecMode::atomic_shared, T, Symm>&& other) noexcept
{
    *this = std::move(other);
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::atomic_shared, T, Symm>::operator=(ExecData<ExecMode::atomic_shared, T, Symm>&& other) noexcept
    -> ExecData<ExecMode::atomic_shared, T, Symm>&
{
    boost::swap(*this, other);
    return *this;
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::atomic_shared, T, Symm>::get_iterator(int const thread_id) -> RegionIndicesDllIt
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);

    assert(thread_id_uz < this->region_indices_its_.size());
    return this->region_indices_its_[thread_id_uz];
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::atomic_shared, T, Symm>::region_count() const noexcept(ndebug) -> std::size_t
{
    auto const size = this->regions_finished_.size();
    assert(size == this->region_indices_copy_.size());
    return size;
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::atomic_shared, T, Symm>::reset_status() -> void
{
    this->region_indices_.from_shallow_copy(this->region_indices_copy_);
    std::fill(this->regions_finished_.begin(), this->regions_finished_.end(), 0);
    this->unfinished_region_count_ = this->region_count();
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::atomic_shared, T, Symm>::thread_count() const noexcept -> int
{
    auto const thread_count_uz = this->region_indices_its_.size();
    assert(thread_count_uz <= std::numeric_limits<int>::max());
    return static_cast<int>(thread_count_uz);
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::atomic_shared, T, Symm>::update_iterator(RegionIndicesDllIt& it) -> bool
{
    auto const lock = std::lock_guard{this->lock_};

    if (this->unfinished_region_count_ == 0) {
        return false;
    }

    while (this->regions_finished_[*it]) {
        ++it;
    }
    this->regions_finished_[*it] = true;
    --this->unfinished_region_count_;
    this->region_indices_.erase(it);

    return true;
}

template class ExecData<ExecMode::atomic_shared, float, Symmetric::no>;
template class ExecData<ExecMode::atomic_shared, float, Symmetric::yes>;
template class ExecData<ExecMode::atomic_shared, double, Symmetric::no>;
template class ExecData<ExecMode::atomic_shared, double, Symmetric::yes>;

template<typename T, Symmetric Symm>
auto swap(ExecData<ExecMode::atomic_shared, T, Symm>& lhs, ExecData<ExecMode::atomic_shared, T, Symm>& rhs) noexcept
    -> void
{
    boost::swap(lhs.region_indices_, rhs.region_indices_);
    boost::swap(lhs.region_indices_copy_, rhs.region_indices_copy_);
    boost::swap(lhs.region_indices_its_, rhs.region_indices_its_);
    boost::swap(lhs.regions_finished_, rhs.regions_finished_);
    boost::swap(lhs.unfinished_region_count_, rhs.unfinished_region_count_);
}

template auto swap(
        ExecData<ExecMode::atomic_shared, float, Symmetric::no>& lhs,
        ExecData<ExecMode::atomic_shared, float, Symmetric::no>& rhs
    ) noexcept -> void;

template auto swap(
        ExecData<ExecMode::atomic_shared, float, Symmetric::yes>& lhs,
        ExecData<ExecMode::atomic_shared, float, Symmetric::yes>& rhs
    ) noexcept -> void;

template auto swap(
        ExecData<ExecMode::atomic_shared, double, Symmetric::no>& lhs,
        ExecData<ExecMode::atomic_shared, double, Symmetric::no>& rhs
    ) noexcept -> void;

template auto swap(
        ExecData<ExecMode::atomic_shared, double, Symmetric::yes>& lhs,
        ExecData<ExecMode::atomic_shared, double, Symmetric::yes>& rhs
    ) noexcept -> void;

template<typename T, Symmetric Symm>
ExecData<ExecMode::locks_exclusive, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::OneVector
    ) : ExecData{thread_count, matrix, tags::two_vectors}
{
}

template<typename T, Symmetric Symm>
ExecData<ExecMode::locks_exclusive, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::TwoVectors
    ) : regions_data{matrix},
        region_indices_threads_(init_region_indices_threads_sll_exclusive(matrix.region_count(), thread_count)),
        region_indices_threads_copy_(shallow_copy(this->region_indices_threads_))
{
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_exclusive, T, Symm>::maximum_region_index_count() const -> std::size_t
{
    auto maximum = 0_uz;
    for (auto const& region_indices : this->region_indices_threads_) {
        maximum = std::max(maximum, region_indices.size());
    }
    return maximum;
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_exclusive, T, Symm>::region_count() const noexcept -> std::size_t
{
    return this->regions_data.region_count();
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_exclusive, T, Symm>::region_indices(int const thread_id) -> RegionIndicesSll&
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);

    assert(thread_id_uz < this->region_indices_threads_.size());
    return this->region_indices_threads_[thread_id_uz];
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_exclusive, T, Symm>::reset_region_indices(int const thread_id) -> void
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);

    auto const& copy = this->region_indices_threads_copy_[thread_id_uz];
    this->region_indices_threads_[thread_id_uz].from_shallow_copy(copy);
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_exclusive, T, Symm>::thread_count() const noexcept(ndebug) -> int
{
    auto const count_uz = this->region_indices_threads_.size();
    assert(count_uz == this->region_indices_threads_copy_.size());
    assert(count_uz <= std::numeric_limits<int>::max());
    return static_cast<int>(count_uz);
}

template class ExecData<ExecMode::locks_exclusive, float, Symmetric::no>;
template class ExecData<ExecMode::locks_exclusive, float, Symmetric::yes>;
template class ExecData<ExecMode::locks_exclusive, double, Symmetric::no>;
template class ExecData<ExecMode::locks_exclusive, double, Symmetric::yes>;

template<typename T, Symmetric Symm>
ExecData<ExecMode::locks_planned_exclusive, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::OneVector
    ) : region_indices_threads_(static_cast<std::size_t>(thread_count))
{
    assert(thread_count > 0);
    assert(matrix.column_count() == matrix.row_count());

    auto exec_data = ExecData<ExecMode::locks_exclusive, T, Symm>{thread_count, matrix, tags::one_vector};
    auto const x1 = std::vector<T>(matrix.column_count(), static_cast<T>(3.14));
    auto y1 = std::vector<T>(x1.size());
    auto y2 = std::vector<T>(x1.size());

    auto const maximum_count = exec_data.maximum_region_index_count();
    auto logs = RegionIndicesLogs{maximum_count, thread_count};

    // A warm-up iteration.
    compute::spmmtv_parallel<T>(matrix, x1, y1, y2, exec_data);
    std::fill(y1.begin(), y1.end(), 0);
    std::fill(y2.begin(), y2.end(), 0);

    compute::spmmtv_parallel<T>(matrix, x1, y1, y2, logs, exec_data);

    for (auto th = 0; th < logs.thread_count(); ++th) {
        auto region_indices = logs.get(th).region_indices();
        auto const th_uz = static_cast<std::size_t>(th);
        this->region_indices_threads_[th_uz] = std::move(region_indices);
    }

    set(this->regions_data, exec_data.regions_data);
}

template<typename T, Symmetric Symm>
ExecData<ExecMode::locks_planned_exclusive, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::TwoVectors
    ) : region_indices_threads_(static_cast<std::size_t>(thread_count))
{
    assert(thread_count > 0);
    if constexpr (Symm == Symmetric::yes) {
        assert(matrix.column_count() == matrix.row_count());
    }

    auto exec_data = ExecData<ExecMode::locks_exclusive, T, Symm>{thread_count, matrix, tags::two_vectors};
    auto const x1 = std::vector<T>(matrix.column_count(), static_cast<T>(3.14));
    auto const x2 = std::vector<T>(matrix.row_count(), static_cast<T>(-2.72));
    auto y1 = std::vector<T>(matrix.row_count());
    auto y2 = std::vector<T>(matrix.column_count());

    auto const maximum_count = exec_data.maximum_region_index_count();
    auto logs = RegionIndicesLogs{maximum_count, thread_count};

    // A warm-up iteration.
    compute::spmmtv_parallel<T>(matrix, x1, x2, y1, y2, exec_data);
    std::fill(y1.begin(), y1.end(), 0);
    std::fill(y2.begin(), y2.end(), 0);

    compute::spmmtv_parallel<T>(matrix, x1, x2, y1, y2, logs, exec_data);

    for (auto th = 0; th < logs.thread_count(); ++th) {
        auto region_indices = logs.get(th).region_indices();
        auto const th_uz = static_cast<std::size_t>(th);
        this->region_indices_threads_[th_uz] = std::move(region_indices);
    }

    set(this->regions_data, exec_data.regions_data);
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_planned_exclusive, T, Symm>::region_count() const noexcept -> std::size_t
{
    return this->regions_data.region_count();
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_planned_exclusive, T, Symm>::region_indices(int const thread_id)
    -> std::span<std::size_t>
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);

    assert(thread_id_uz < this->region_indices_threads_.size());
    return this->region_indices_threads_[thread_id_uz];
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_planned_exclusive, T, Symm>::thread_count() const noexcept(ndebug) -> int
{
    auto const thread_count_uz = this->region_indices_threads_.size();
    assert(thread_count_uz <= std::numeric_limits<int>::max());
    return static_cast<int>(thread_count_uz);
}

template class ExecData<ExecMode::locks_planned_exclusive, float, Symmetric::no>;
template class ExecData<ExecMode::locks_planned_exclusive, float, Symmetric::yes>;
template class ExecData<ExecMode::locks_planned_exclusive, double, Symmetric::no>;
template class ExecData<ExecMode::locks_planned_exclusive, double, Symmetric::yes>;

template<typename T, Symmetric Symm>
ExecData<ExecMode::locks_planned_shared, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::OneVector
    ) : region_indices_threads_(static_cast<std::size_t>(thread_count))
{
    assert(thread_count > 0);
    assert(matrix.column_count() == matrix.row_count());

    auto exec_data = ExecData<ExecMode::locks_shared, T, Symm>{thread_count, matrix, tags::one_vector};
    auto logs = RegionIndicesLogs{matrix.region_count(), thread_count};
    auto const x1 = std::vector<T>(matrix.column_count(), static_cast<T>(3.14));
    auto y1 = std::vector<T>(x1.size());
    auto y2 = std::vector<T>(x1.size());

    // A warm-up iteration.
    compute::spmmtv_parallel<T>(matrix, x1, y1, y2, exec_data);
    std::fill(y1.begin(), y1.end(), 0);
    std::fill(y2.begin(), y2.end(), 0);

    compute::spmmtv_parallel<T>(matrix, x1, y1, y2, logs, exec_data);

    for (auto th = 0; th < logs.thread_count(); ++th) {
        auto region_indices = logs.get(th).region_indices();
        auto const th_uz = static_cast<std::size_t>(th);
        this->region_indices_threads_[th_uz] = std::move(region_indices);
    }

    set(this->regions_data, exec_data.regions_data);
}

template<typename T, Symmetric Symm>
ExecData<ExecMode::locks_planned_shared, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::TwoVectors
    ) : region_indices_threads_(static_cast<std::size_t>(thread_count))
{
    assert(thread_count > 0);
    if constexpr (Symm == Symmetric::yes) {
        assert(matrix.column_count() == matrix.row_count());
    }

    auto exec_data = ExecData<ExecMode::locks_shared, T, Symm>{thread_count, matrix, tags::two_vectors};
    auto logs = RegionIndicesLogs{matrix.region_count(), thread_count};
    auto const x1 = std::vector<T>(matrix.column_count(), static_cast<T>(3.14));
    auto const x2 = std::vector<T>(matrix.row_count(), static_cast<T>(-2.72));
    auto y1 = std::vector<T>(matrix.row_count());
    auto y2 = std::vector<T>(matrix.column_count());

    // A warm-up iteration.
    compute::spmmtv_parallel<T>(matrix, x1, x2, y1, y2, exec_data);
    std::fill(y1.begin(), y1.end(), 0);
    std::fill(y2.begin(), y2.end(), 0);

    compute::spmmtv_parallel<T>(matrix, x1, x2, y1, y2, logs, exec_data);

    for (auto th = 0; th < logs.thread_count(); ++th) {
        auto region_indices = logs.get(th).region_indices();
        auto const th_uz = static_cast<std::size_t>(th);
        this->region_indices_threads_[th_uz] = std::move(region_indices);
    }

    set(this->regions_data, exec_data.regions_data);
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_planned_shared, T, Symm>::region_count() const noexcept -> std::size_t
{
    return this->regions_data.region_count();
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_planned_shared, T, Symm>::region_indices(int const thread_id) -> std::span<std::size_t>
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);

    assert(thread_id_uz < this->region_indices_threads_.size());
    return this->region_indices_threads_[thread_id_uz];
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_planned_shared, T, Symm>::thread_count() const noexcept(ndebug) -> int
{
    auto const thread_count_uz = this->region_indices_threads_.size();
    assert(thread_count_uz <= std::numeric_limits<int>::max());
    return static_cast<int>(thread_count_uz);
}

template class ExecData<ExecMode::locks_planned_shared, float, Symmetric::no>;
template class ExecData<ExecMode::locks_planned_shared, float, Symmetric::yes>;
template class ExecData<ExecMode::locks_planned_shared, double, Symmetric::no>;
template class ExecData<ExecMode::locks_planned_shared, double, Symmetric::yes>;

template<typename T, Symmetric Symm>
ExecData<ExecMode::locks_shared, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::OneVector
    ) : ExecData{thread_count, matrix, tags::two_vectors}
{
}

template<typename T, Symmetric Symm>
ExecData<ExecMode::locks_shared, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::TwoVectors
    ) : regions_data{matrix},
        region_indices_threads_(init_region_indices_threads_sll_shared(matrix.region_count(), thread_count)),
        region_indices_threads_copy_(shallow_copy(this->region_indices_threads_))
{
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_shared, T, Symm>::region_count() const noexcept -> std::size_t
{
    return this->regions_data.region_count();
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_shared, T, Symm>::region_indices(int const thread_id) -> RegionIndicesSll&
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);

    assert(thread_id_uz < this->region_indices_threads_.size());
    return this->region_indices_threads_[thread_id_uz];
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_shared, T, Symm>::reset_region_indices(int const thread_id) -> void
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);

    auto const& copy = this->region_indices_threads_copy_[thread_id_uz];
    this->region_indices_threads_[thread_id_uz].from_shallow_copy(copy);
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::locks_shared, T, Symm>::thread_count() const noexcept(ndebug) -> int
{
    auto const count_uz = this->region_indices_threads_.size();
    assert(count_uz == this->region_indices_threads_copy_.size());
    assert(count_uz <= std::numeric_limits<int>::max());
    return static_cast<int>(count_uz);
}

template class ExecData<ExecMode::locks_shared, float, Symmetric::no>;
template class ExecData<ExecMode::locks_shared, float, Symmetric::yes>;
template class ExecData<ExecMode::locks_shared, double, Symmetric::no>;
template class ExecData<ExecMode::locks_shared, double, Symmetric::yes>;

template<typename T, Symmetric Symm>
ExecData<ExecMode::per_thread_exclusive, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::OneVector
    ) : per_thread_vectors{matrix.column_count(), matrix.row_count(), thread_count},
        region_count_{matrix.region_count()},
        region_indices_threads_(init_region_indices_threads_exclusive(matrix.region_count(), thread_count))
{
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::per_thread_exclusive, T, Symm>::region_count() const noexcept -> std::size_t
{
    return this->region_count_;
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::per_thread_exclusive, T, Symm> ::region_indices(int const thread_id) -> std::span<std::size_t>
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);

    assert(thread_id_uz < this->region_indices_threads_.size());
    return this->region_indices_threads_[thread_id_uz];
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::per_thread_exclusive, T, Symm>::thread_count() const noexcept -> int
{
    auto const thread_count_uz = this->region_indices_threads_.size();
    assert(thread_count_uz <= std::numeric_limits<int>::max());
    return static_cast<int>(thread_count_uz);
}

template class ExecData<ExecMode::per_thread_exclusive, float, Symmetric::no>;
template class ExecData<ExecMode::per_thread_exclusive, float, Symmetric::yes>;
template class ExecData<ExecMode::per_thread_exclusive, double, Symmetric::no>;
template class ExecData<ExecMode::per_thread_exclusive, double, Symmetric::yes>;

template<typename T, Symmetric Symm>
ExecData<ExecMode::per_thread_planned_shared, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::OneVector
    ) : per_thread_vectors{matrix.column_count(), matrix.row_count(), thread_count},
        region_count_{matrix.region_count()},
        region_indices_threads_(static_cast<std::size_t>(thread_count))
{
    assert(thread_count > 0);
    assert(matrix.column_count() == matrix.row_count());

    auto exec_data = ExecData<ExecMode::per_thread_shared, T, Symm>{thread_count, matrix, tags::one_vector};
    auto logs = RegionIndicesLogs{matrix.region_count(), thread_count};
    auto const x1 = std::vector<T>(matrix.column_count(), static_cast<T>(3.14));
    auto y1 = std::vector<T>(x1.size());
    auto y2 = std::vector<T>(x1.size());

    // A warm-up iteration.
    compute::spmmtv_parallel<T>(matrix, x1, y1, y2, exec_data);
    std::fill(y1.begin(), y1.end(), 0);
    std::fill(y2.begin(), y2.end(), 0);

    compute::spmmtv_parallel<T>(matrix, x1, y1, y2, logs, exec_data);

    for (auto th = 0; th < logs.thread_count(); ++th) {
        auto region_indices = logs.get(th).region_indices();
        auto const th_uz = static_cast<std::size_t>(th);
        this->region_indices_threads_[th_uz] = std::move(region_indices);
    }
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::per_thread_planned_shared, T, Symm>::region_count() const noexcept -> std::size_t
{
    return this->region_count_;
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::per_thread_planned_shared, T, Symm> ::region_indices(int const thread_id)
    -> std::span<std::size_t>
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);

    assert(thread_id_uz < this->region_indices_threads_.size());
    return this->region_indices_threads_[thread_id_uz];
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::per_thread_planned_shared, T, Symm>::thread_count() const noexcept -> int
{
    auto const thread_count_uz = this->region_indices_threads_.size();
    assert(thread_count_uz <= std::numeric_limits<int>::max());
    return static_cast<int>(thread_count_uz);
}

template class ExecData<ExecMode::per_thread_planned_shared, float, Symmetric::no>;
template class ExecData<ExecMode::per_thread_planned_shared, float, Symmetric::yes>;
template class ExecData<ExecMode::per_thread_planned_shared, double, Symmetric::no>;
template class ExecData<ExecMode::per_thread_planned_shared, double, Symmetric::yes>;

template<typename T, Symmetric Symm>
ExecData<ExecMode::per_thread_shared, T, Symm>::ExecData(
        int const thread_count, Matrix<T, Symm> const& matrix, tags::OneVector
    ) : per_thread_vectors{matrix.column_count(), matrix.row_count(), thread_count},
        region_indices_{init_region_indices_dll_shared(matrix.region_count())},
        region_indices_copy_{this->region_indices_.shallow_copy()},
        region_indices_its_(init_region_indices_dll_its_shared(thread_count, this->region_indices_)),
        regions_finished_(matrix.region_count()),
        unfinished_region_count_{matrix.region_count()}
{
}

template<typename T, Symmetric Symm>
ExecData<ExecMode::per_thread_shared, T, Symm>::ExecData(ExecData<ExecMode::per_thread_shared, T, Symm>&& other)
    noexcept
{
    *this = std::move(other);
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::per_thread_shared, T, Symm>::operator=(ExecData<ExecMode::per_thread_shared, T, Symm>&& other)
    noexcept -> ExecData<ExecMode::per_thread_shared, T, Symm>&
{
    boost::swap(*this, other);
    return *this;
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::per_thread_shared, T, Symm>::get_iterator(int const thread_id) -> RegionIndicesDllIt
{
    assert(thread_id >= 0);
    assert(thread_id < this->thread_count());
    auto const thread_id_uz = static_cast<std::size_t>(thread_id);

    assert(thread_id_uz < this->region_indices_its_.size());
    return this->region_indices_its_[thread_id_uz];
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::per_thread_shared, T, Symm>::region_count() const noexcept(ndebug) -> std::size_t
{
    auto const size = this->regions_finished_.size();
    assert(size == this->region_indices_copy_.size());
    return size;
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::per_thread_shared, T, Symm>::reset_status() -> void
{
    this->region_indices_.from_shallow_copy(this->region_indices_copy_);
    std::fill(this->regions_finished_.begin(), this->regions_finished_.end(), 0);
    this->unfinished_region_count_ = this->region_count();
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::per_thread_shared, T, Symm>::thread_count() const noexcept -> int
{
    auto const thread_count_uz = this->region_indices_its_.size();
    assert(thread_count_uz <= std::numeric_limits<int>::max());
    return static_cast<int>(thread_count_uz);
}

template<typename T, Symmetric Symm>
auto ExecData<ExecMode::per_thread_shared, T, Symm>::update_iterator(RegionIndicesDllIt& it) -> bool
{
    auto const lock = std::lock_guard{this->lock_};

    if (this->unfinished_region_count_ == 0) {
        return false;
    }

    while (this->regions_finished_[*it]) {
        ++it;
    }
    this->regions_finished_[*it] = true;
    --this->unfinished_region_count_;
    this->region_indices_.erase(it);

    return true;
}

template class ExecData<ExecMode::per_thread_shared, float, Symmetric::no>;
template class ExecData<ExecMode::per_thread_shared, float, Symmetric::yes>;
template class ExecData<ExecMode::per_thread_shared, double, Symmetric::no>;
template class ExecData<ExecMode::per_thread_shared, double, Symmetric::yes>;

template<typename T, Symmetric Symm>
auto swap(ExecData<ExecMode::per_thread_shared, T, Symm>& lhs, ExecData<ExecMode::per_thread_shared, T, Symm>& rhs)
    noexcept -> void
{
    boost::swap(lhs.per_thread_vectors, rhs.per_thread_vectors);
    boost::swap(lhs.region_indices_, rhs.region_indices_);
    boost::swap(lhs.region_indices_copy_, rhs.region_indices_copy_);
    boost::swap(lhs.region_indices_its_, rhs.region_indices_its_);
    boost::swap(lhs.regions_finished_, rhs.regions_finished_);
    boost::swap(lhs.unfinished_region_count_, rhs.unfinished_region_count_);
}

template auto swap(
        ExecData<ExecMode::per_thread_shared, float, Symmetric::no>& lhs,
        ExecData<ExecMode::per_thread_shared, float, Symmetric::no>& rhs
    ) noexcept -> void;

template auto swap(
        ExecData<ExecMode::per_thread_shared, float, Symmetric::yes>& lhs,
        ExecData<ExecMode::per_thread_shared, float, Symmetric::yes>& rhs
    ) noexcept -> void;

template auto swap(
        ExecData<ExecMode::per_thread_shared, double, Symmetric::no>& lhs,
        ExecData<ExecMode::per_thread_shared, double, Symmetric::no>& rhs
    ) noexcept -> void;

template auto swap(
        ExecData<ExecMode::per_thread_shared, double, Symmetric::yes>& lhs,
        ExecData<ExecMode::per_thread_shared, double, Symmetric::yes>& rhs
    ) noexcept -> void;

namespace {

auto init_region_indices_dll_shared(std::size_t const region_count) -> RegionIndicesDll
{
    auto region_indices = std::vector<std::size_t>(region_count);
    std::iota(region_indices.begin(), region_indices.end(), 0);
    return RegionIndicesDll{region_indices};
}

auto init_region_indices_dll_its_shared(int const thread_count, RegionIndicesDll& region_indices)
    -> std::vector<RegionIndicesDllIt>
{
    assert(thread_count > 0);
    auto const thread_count_uz = static_cast<std::size_t>(thread_count);

    auto region_indices_its = std::vector<RegionIndicesDllIt>{};
    region_indices_its.reserve(thread_count_uz);

    auto const offset = std::invoke([&] {
        auto const region_count = region_indices.size();
        auto const offset = region_count / thread_count_uz;
        if (offset == 0 && region_count != 0) {
            return 1_uz;
        }
        return offset;
    });

    auto it = region_indices.begin();
    region_indices_its.emplace_back(it);
    for (auto i = 1; i < thread_count; ++i) {
        std::advance(it, offset);
        region_indices_its.emplace_back(it);
    }

    assert(region_indices_its.size() == thread_count_uz);
    return region_indices_its;
}

auto init_region_indices_threads_exclusive(std::size_t const region_count, int const thread_count)
    -> std::vector<std::vector<std::size_t>>
{
    assert(thread_count > 0);
    auto const thread_count_uz = static_cast<std::size_t>(thread_count);

    auto region_indices_threads = std::vector<std::vector<std::size_t>>{};
    region_indices_threads.reserve(thread_count_uz);

    auto const per_thread_region_count = region_count / thread_count_uz;
    auto const remaining_region_count = region_count % thread_count_uz;
    auto region_begin = 0_uz;

    for (auto th = 0_uz; th < thread_count_uz; ++th) {
        auto const region_end = std::invoke([&] {
            if (th < remaining_region_count) {
                return region_begin + per_thread_region_count + 1;
            } else {            // th >= remaining_region_count
                return region_begin + per_thread_region_count;
            }
        });

        auto region_indices = std::vector<std::size_t>(region_end - region_begin);
        std::iota(region_indices.begin(), region_indices.end(), region_begin);
        region_indices_threads.emplace_back(region_indices);

        region_begin = region_end;
    }

    assert(region_begin == region_count);
    return region_indices_threads;
}

auto init_region_indices_threads_sll_exclusive(std::size_t const region_count, int const thread_count)
    -> std::vector<RegionIndicesSll>
{
    assert(thread_count > 0);
    auto const thread_count_uz = static_cast<std::size_t>(thread_count);

    auto region_indices_threads = std::vector<RegionIndicesSll>{};
    region_indices_threads.reserve(thread_count_uz);

    auto const per_thread_region_count = region_count / thread_count_uz;
    auto const remaining_region_count = region_count % thread_count_uz;
    auto region_begin = 0_uz;

    for (auto th = 0_uz; th < thread_count_uz; ++th) {
        auto const region_end = std::invoke([&] {
            if (th < remaining_region_count) {
                return region_begin + per_thread_region_count + 1;
            } else {            // th >= remaining_region_count
                return region_begin + per_thread_region_count;
            }
        });

        auto region_indices = std::vector<std::size_t>(region_end - region_begin);
        std::iota(region_indices.begin(), region_indices.end(), region_begin);
        region_indices_threads.emplace_back(region_indices);

        region_begin = region_end;
    }

    assert(region_begin == region_count);
    return region_indices_threads;
}

auto init_region_indices_threads_sll_shared(std::size_t const region_count, int const thread_count)
    -> std::vector<RegionIndicesSll>
{
    assert(thread_count > 0);
    auto const thread_count_uz = static_cast<std::size_t>(thread_count);

    auto region_indices_threads = std::vector<RegionIndicesSll>{};
    region_indices_threads.reserve(thread_count_uz);

    auto region_indices = std::vector<std::size_t>(region_count);
    std::iota(region_indices.begin(), region_indices.end(), 0);
    region_indices_threads.emplace_back(region_indices);

    auto const rotate_by = std::invoke([&] {
        assert(region_count <= std::numeric_limits<int>::max());
        auto const region_count_i = static_cast<int>(region_count);

        auto const rotate_by = region_count_i / thread_count;
        if (rotate_by == 0 && region_count != 0) {
            return 1;
        }
        return rotate_by;
    });

    for (auto th = 1; th < thread_count; ++th) {
        std::rotate(region_indices.begin(), region_indices.begin() + rotate_by, region_indices.end());
        region_indices_threads.emplace_back(region_indices);
    }

    return region_indices_threads;
}

auto shallow_copy(std::span<RegionIndicesSll const> const region_indices_threads) -> std::vector<RegionIndicesSll>
{
    auto copies = std::vector<RegionIndicesSll>(region_indices_threads.size());
    for (auto i = 0_uz; i < region_indices_threads.size(); ++i) {
        copies[i] = region_indices_threads[i].shallow_copy();
    }
    return copies;
}

}  // namespace

}  // namespace hsf
