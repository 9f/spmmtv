#include <hsf/region_ordering.hpp>

#include <map>                  // std::map
#include <string_view>          // std::string_view

namespace hsf {

std::map<std::string_view, RegionOrdering> const region_orderings = {
    {"block-lexicographical", RegionOrdering::block_lexicographical},
    {"lexicographical",       RegionOrdering::lexicographical},
    {"morton",                RegionOrdering::morton},
};

}  // namespace hsf
