#include <hsf/symmetric_ios.hpp>

#include <ostream>              // std::ostream

#include <hsf/logic_error.hpp>  // hsf::LogicError
#include <hsf/symmetric.hpp>    // hsf::Symmetric

namespace hsf {

auto operator<<(std::ostream& os, Symmetric const symmetric) -> std::ostream&
{
    switch (symmetric) {
    case Symmetric::no:
        return os << "no";
    case Symmetric::yes:
        return os << "yes";
    }

    throw LogicError{"Unexpected enumeration value."};
}

}  // namespace hsf
