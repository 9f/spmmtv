#pragma once

#include <cstdint>              // std::uint32_t

#include <hsf/info.hpp>         // hsf::info
#include <hsf/region_info.hpp>  // hsf::RegionInfo
#include <hsf/sub_bounds.hpp>   // hsf::SubBounds

namespace hsf {

template<typename T> class Region;

// Data for creating subregions.  The template can only be instantiated with
// types ‘float’ and ‘double’.
template<typename T>
class SubregionData {
public:
    // The row bounds of the subregions.
    SubBounds row_bounds{};

    // The column bounds of the subregions.
    SubBounds column_bounds{};

    // The region that is being split into subregions.
    Region<T> const* region{};

    // Additional data for creating the subregions.
    RegionInfo region_info{};

    // The CSR row pointer array of the region being split.
    info::RowPointer* row_pointers{};

    // The starting index of the CSR row pointer array for the region being
    // split.
    std::uint32_t row_pointers_index{};

    // The starting index of the CSC row pointer array for the region being
    // split.
    std::uint32_t column_pointers_index{};

    SubregionData() = default;

    // Instantiate a new ‘SubregionData’ object and initialise its ‘region’,
    // ‘row_pointers_index’ and ‘column_pointers_index’ member variables with
    // the passed arguments.
    SubregionData(Region<T> const* region, std::uint32_t row_pointers_index, std::uint32_t column_pointers_index)
        noexcept;
};

}  // namespace hsf
