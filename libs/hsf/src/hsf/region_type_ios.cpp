#include <hsf/region_type_ios.hpp>

#include <ostream>              // std::ostream

#include <hsf/logic_error.hpp>  // hsf::LogicError
#include <hsf/region_type.hpp>  // hsf::RegionType

namespace hsf {

auto operator<<(std::ostream& os, RegionType const region_type) -> std::ostream&
{
    switch (region_type) {
    case RegionType::COO:
        return os << "COO";
    case RegionType::CSR:
        return os << "CSR";
    }

    throw LogicError{"Unexpected enumeration value."};
}

}  // namespace hsf
