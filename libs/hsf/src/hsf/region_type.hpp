#pragma once

namespace hsf {

template<typename T> class Region;

// Regions can be of type COO or of type CSR.
enum struct RegionType {
    COO,
    CSR,
};

// Return the type of the passed ‘Region’.
template<typename T>
auto get_region_type(Region<T> const& region) -> RegionType;

}  // namespace hsf
