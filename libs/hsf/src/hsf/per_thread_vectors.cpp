#include <hsf/priv/per_thread_vectors.hpp>

#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <limits>               // std::numeric_limits
#include <vector>               // std::vector

#include <hsf/priv/ndebug.hpp>  // hsf::ndebug
#include <hsf/symmetric.hpp>    // hsf::Symmetric

namespace hsf {

namespace {

template<typename T>
auto init_per_thread_vectors(int thread_count, std::size_t vector_length) -> std::vector<std::vector<T>>;

}  // namespace

template<typename T>
PerThreadVectors<T, Symmetric::no>::PerThreadVectors(
        std::size_t const column_count, std::size_t const row_count, int const thread_count
    ) : y1(init_per_thread_vectors<T>(thread_count, row_count)),
        y2(init_per_thread_vectors<T>(thread_count, column_count))
{
}

template<typename T>
auto PerThreadVectors<T, Symmetric::no>::thread_count() const noexcept(ndebug) -> int
{
    assert(this->y1.size() == this->y2.size());
    assert(this->y1.size() <= std::numeric_limits<int>::max());
    return static_cast<int>(this->y1.size());
}

template<typename T>
PerThreadVectors<T, Symmetric::yes>::PerThreadVectors(
        std::size_t const column_count, std::size_t const row_count, int const thread_count
    ) : y1(init_per_thread_vectors<T>(thread_count, row_count))
{
}

template<typename T>
auto PerThreadVectors<T, Symmetric::yes>::thread_count() const noexcept -> int
{
    assert(this->y1.size() <= std::numeric_limits<int>::max());
    return static_cast<int>(this->y1.size());
}

template class PerThreadVectors<float, Symmetric::no>;
template class PerThreadVectors<float, Symmetric::yes>;
template class PerThreadVectors<double, Symmetric::no>;
template class PerThreadVectors<double, Symmetric::yes>;

namespace {

template<typename T>
auto init_per_thread_vectors(int const thread_count, std::size_t const vector_length)
    -> std::vector<std::vector<T>>
{
    assert(thread_count >= 0);
    auto const thread_count_uz = static_cast<std::size_t>(thread_count);

    auto per_thread_vectors = std::vector<std::vector<T>>(thread_count_uz);
    for (auto& per_thread_vector : per_thread_vectors) {
        per_thread_vector = std::vector<T>(vector_length);
    }
    return per_thread_vectors;
}

}  // namespace

}  // namespace hsf
