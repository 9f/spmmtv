#pragma once

#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t
#include <vector>               // std::vector

namespace hsf {

// Store region bounds of a matrix along one of its axes as an array of
// indices.  Each region is bounded by a pair of indices.  The first index of
// the pair is the first index of the region.  The second index of the pair is
// the index following the last index of the region.  The second index is
// therefore the first index of the following region.
class Bounds {
public:
    Bounds() = default;

    // Initialise equally spaced region bounds along a single axis of a matrix.
    // Parameter ‘row_or_column_count’ specifies the axis size and must be
    // greater than or equal to one.  Each created region bound spans over
    // ‘info::region_size’ rows (or columns), apart from the final region
    // bounds when ‘row_or_column_count’ isn’t a multiple of
    // ‘info::region_size’.
    explicit Bounds(std::uint32_t row_or_column_count);

    // Return the starting index of the region specified by parameter ‘bound’.
    // Specifying an out-of-bounds ‘bound’ is undefined behaviour.
    auto get_index(std::size_t bound) const -> std::uint32_t;

    // Return the number of stored regions.  For a nonempty ‘Bounds’ object,
    // the region count is one less than the number of stored bound indices.
    auto get_region_count() const noexcept -> std::size_t;

    // Return the difference between the starting index of the region specified
    // by parameter ‘bound’ and the starting index of the following region.
    // This can be interpreted as the region’s size (e.g. row or column count).
    // Specifying an out-of-bounds ‘bound’ is undefined behaviour.
    auto get_size(std::size_t bound) const -> std::uint32_t;

private:
    // An array of bound indices.  The first array bound index is always equal
    // to zero.
    std::vector<std::uint32_t> indices_{};

    // The region count is one less than the number of bound indices in array
    // ‘indices_’.
    std::vector<std::uint32_t>::size_type region_count_{};
};

}  // namespace hsf
