#pragma once

#include <cstdint>              // std::uint32_t std::uint64_t

namespace hsf::morton {

// Return a Morton code created from a point in two-dimensional space.
// Parameter ‘x’ is the point’s first coordinate and ‘y’ is the point’s second
// coordinate.
auto encode(std::uint32_t x, std::uint32_t y) noexcept -> std::uint64_t;

}  // namespace hsf::morton
