#include <hsf/region_info.hpp>

#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t
#include <span>                 // std::span

#include <omp.h>                // omp_get_max_threads

#include <hsf/bounds.hpp>       // hsf::Bounds
#include <hsf/index_to_region_bound.hpp>  // hsf::index_to_region_bound
#include <hsf/info.hpp>         // hsf::info
#include <hsf/logic_error.hpp>  // hsf::LogicError
#include <hsf/priv/cstddef.hpp>  // hsf::operator""_uz
#include <hsf/region.hpp>       // hsf::Region
#include <hsf/region_data.hpp>  // hsf::RegionData
#include <hsf/region_type.hpp>  // hsf::RegionType
#include <hsf/sub_bounds.hpp>   // hsf::SubBounds

namespace hsf {

namespace {

// Return the index of the block described by ‘row_bound’ and ‘column_bound’
// assuming that each row of blocks consists of ‘column_region_count’ columns
// of blocks.
auto bounds_to_index(
        std::size_t row_bound, std::size_t column_bound, std::size_t column_region_count
    ) noexcept -> std::size_t;

// Return ‘bounds.get_index(bound)’.
auto get_original_index(Bounds const& bounds, std::size_t const bound) -> info::RowPointer;

// Return ‘sub_bounds.get_original_index(bound)’.
auto get_original_index(SubBounds const& sub_bounds, std::size_t const bound) -> info::RowPointer;

// Return whether it’s preferable to store a matrix with the passed element and
// row count in the CSR format instead of the COO format.  The CSR format is
// preferred as long as it isn’t less space efficient than the COO format.
auto is_csr_preferred(std::size_t element_count, std::size_t row_count) noexcept -> bool;

}  // namespace

RegionInfo::RegionInfo(
        Bounds const& row_bounds,
        Bounds const& col_bounds,
        std::span<std::uint32_t const> const row_pointers,
        std::span<std::uint32_t const> const columns,
        std::span<RegionData> const region_data
    ) : col_region_count_{col_bounds.get_region_count()},
        region_data_{region_data}
{
    if (this->region_data_.empty()) {
        return;
    }

    auto const row_bound_count = row_bounds.get_region_count();
    if (row_bound_count > static_cast<std::size_t>(omp_get_max_threads())) {
        #pragma omp parallel for schedule(dynamic) default(none) shared(columns, row_bound_count, row_bounds, \
            row_pointers)
        for (auto row_bound = 0_uz; row_bound < row_bound_count; ++row_bound) {
            this->init_element_counts(row_bound, row_bounds, this->col_region_count_, row_pointers, columns);
        }

    } else if (row_bound_count > 1) {
        #pragma omp parallel for num_threads(row_bound_count) schedule(static) default(none) shared(columns, \
            row_bound_count, row_bounds, row_pointers)
        for (auto row_bound = 0_uz; row_bound < row_bound_count; ++row_bound) {
            this->init_element_counts(row_bound, row_bounds, this->col_region_count_, row_pointers, columns);
        }

    } else if (row_bound_count == 1) {
        this->init_element_counts(0, row_bounds, this->col_region_count_, row_pointers, columns);
    }

    // The remainder of this function is implemented as a series of loops where
    // each of them should be straightforward to parallelise.  Profiling
    // results have however shown that the loops get executed so quickly that
    // it’s not worth parallelising them.

    for (auto row_bound = std::uint32_t{}; row_bound < row_bound_count; ++row_bound) {
        for (auto col_bound = std::uint32_t{}; col_bound < this->col_region_count_; ++col_bound) {
            auto const i = bounds_to_index(row_bound, col_bound, this->col_region_count_);
            auto const element_count = this->region_data_[i].element_count;
            if (element_count == 0) {
                continue;
            }

            this->region_data_[i].row_bound = row_bound;
            this->region_data_[i].col_bound = col_bound;

            auto const row_count = row_bounds.get_size(row_bound);
            if (is_csr_preferred(element_count, row_count)) {
                this->region_data_[i].region_type = RegionType::CSR;
                this->region_data_[i].previous_row = 0;
            } else {
                this->region_data_[i].region_type = RegionType::COO;
            }
        }
    }

    this->region_data_.front().nonempty_region_index = 0;
    this->region_data_.front().element_index = 0;
    this->region_data_.front().coo_row_index = 0;
    this->region_data_.front().csr_row_index = 0;

    for (auto i = 1_uz; i < this->region_data_.size(); ++i) {
        auto& region_data = this->region_data_[i];
        auto& prev_region_data = this->region_data_[i - 1];

        if (prev_region_data.element_count == 0) {
            region_data.nonempty_region_index = 0;
            region_data.element_index = 0;
            region_data.coo_row_index = 0;
            region_data.csr_row_index = 0;
            continue;
        }

        region_data.nonempty_region_index = 1;
        region_data.element_index = prev_region_data.element_count;

        switch (prev_region_data.region_type) {
        case RegionType::COO:
            region_data.coo_row_index = prev_region_data.element_count;
            region_data.csr_row_index = 0;
            break;

        case RegionType::CSR:
            {
                region_data.coo_row_index = 0;
                auto const p_row_bound = prev_region_data.row_bound;
                auto const p_row_count = row_bounds.get_size(p_row_bound);
                region_data.csr_row_index = static_cast<std::uint32_t>(p_row_count + 1);
                break;
            }
        }
    }

    for (auto i = 1_uz; i < this->region_data_.size(); ++i) {
        auto& region_data = this->region_data_[i];
        auto& prev_region_data = this->region_data_[i - 1];

        region_data.nonempty_region_index += prev_region_data.nonempty_region_index;
        region_data.element_index += prev_region_data.element_index;
        region_data.coo_row_index += prev_region_data.coo_row_index;
        region_data.csr_row_index += prev_region_data.csr_row_index;
    }

    this->init_row_element_counts(row_bounds);
    this->init_nonempty_region_count();
}

RegionInfo::RegionInfo(
        SubBounds const& row_bounds,
        SubBounds const& col_bounds,
        std::span<info::RowPointer const> const row_pointers,
        std::span<info::ColIndex const> const columns,
        std::span<RegionData> const region_data
    ) : col_region_count_{col_bounds.get_subregion_count()},
        region_data_{region_data}
{
    if (this->region_data_.empty()) {
        return;
    }

    this->region_data_.front().element_count = 0;
    this->region_data_.front().nonempty_region_index = 0;
    this->region_data_.front().element_index = 0;
    this->region_data_.front().coo_row_index = 0;
    this->region_data_.front().csr_row_index = 0;

    for (auto i = 1_uz; i < this->region_data_.size(); ++i) {
        this->region_data_[i].element_count = 0;
    }

    // Determine the number of nonzero elements in each region.
    auto const row_region_count = row_bounds.get_subregion_count();
    for (auto row_bound = 0_uz; row_bound < row_region_count; ++row_bound) {

        auto const row_bounds_begin = row_bounds.get_index(row_bound);
        auto const row_bounds_end = row_bounds.get_index(row_bound + 1);
        for (auto row = row_bounds_begin; row < row_bounds_end; ++row) {

            auto const row_begin = row_pointers[row];
            auto const row_end = row_pointers[row + 1];
            for (auto i = row_begin; i < row_end; ++i) {

                auto const col = columns[i];
                auto const col_bound = col_bounds.find_subregion_bound(col);
                this->increment_element_count(row_bound, col_bound);
            }
        }
    }

    for (auto row_bound = std::uint32_t{}; row_bound < row_region_count; ++row_bound) {
        for (auto col_bound = std::uint32_t{}; col_bound < this->col_region_count_; ++col_bound) {
            auto const i = bounds_to_index(row_bound, col_bound, this->col_region_count_);
            auto const element_count = this->region_data_[i].element_count;
            if (element_count == 0) {
                continue;
            }

            this->region_data_[i].row_bound = row_bound;
            this->region_data_[i].col_bound = col_bound;

            auto const row_count = row_bounds.get_size(row_bound);
            if (is_csr_preferred(element_count, row_count)) {
                this->region_data_[i].region_type = RegionType::CSR;
                this->region_data_[i].previous_row = 0;
            } else {
                this->region_data_[i].region_type = RegionType::COO;
            }
        }
    }

    for (auto i = 1_uz; i < this->region_data_.size(); ++i) {
        auto& region_data = this->region_data_[i];
        auto& prev_region_data = this->region_data_[i - 1];

        if (prev_region_data.element_count == 0) {
            region_data.nonempty_region_index = 0;
            region_data.element_index = 0;
            region_data.coo_row_index = 0;
            region_data.csr_row_index = 0;
            continue;
        }

        region_data.nonempty_region_index = 1;
        region_data.element_index = prev_region_data.element_count;

        switch (prev_region_data.region_type) {
        case RegionType::COO:
            region_data.coo_row_index = prev_region_data.element_count;
            region_data.csr_row_index = 0;
            break;

        case RegionType::CSR:
            {
                region_data.coo_row_index = 0;
                auto const p_row_bound = prev_region_data.row_bound;
                auto const p_row_count = row_bounds.get_size(p_row_bound);
                region_data.csr_row_index = static_cast<std::uint32_t>(p_row_count + 1);
                break;
            }
        }
    }

    for (auto i = 1_uz; i < this->region_data_.size(); ++i) {
        auto& region_data = this->region_data_[i];
        auto& prev_region_data = this->region_data_[i - 1];

        region_data.nonempty_region_index += prev_region_data.nonempty_region_index;
        region_data.element_index += prev_region_data.element_index;
        region_data.coo_row_index += prev_region_data.coo_row_index;
        region_data.csr_row_index += prev_region_data.csr_row_index;
    }

    this->init_row_element_counts(row_bounds);
    this->init_nonempty_region_count();
}

template<typename BoundsType>
auto RegionInfo::finalise_row_pointers(BoundsType const& row_bounds, std::span<info::RowPointer> const csr_rows) const
    -> void
{
    for (auto const& region_data : this->region_data_) {
        if (region_data.element_count == 0) {
            continue;
        }

        if (region_data.region_type != RegionType::CSR) {
            continue;
        }

        auto const row_count = row_bounds.get_size(region_data.row_bound);
        assert(region_data.previous_row + 1 <= row_count);

        // Stop if the last row’s element count has already been written.
        if (region_data.previous_row + 1 >= row_count) {
            continue;
        }

        // Copy the element counts of remaining rows.
        auto const prev_element_count = csr_rows[region_data.csr_row_index + region_data.previous_row + 1];
        for (auto i = region_data.previous_row + 2; i <= row_count; ++i) {
            csr_rows[region_data.csr_row_index + i] = prev_element_count;
        }
    }
}

template auto RegionInfo::finalise_row_pointers(Bounds const& row_bounds, std::span<info::RowPointer> csr_rows) const
    -> void;

template auto RegionInfo::finalise_row_pointers(SubBounds const& row_bounds, std::span<info::RowPointer> csr_rows) const
    -> void;

auto RegionInfo::get_coo_rows_element_count() const noexcept -> std::size_t
{
    return this->coo_rows_element_count_;
}

auto RegionInfo::get_coo_row_index(std::size_t const row_bound, std::size_t const col_bound) const
    -> std::size_t
{
    auto const i = bounds_to_index(row_bound, col_bound, this->col_region_count_);
    return this->region_data_[i].coo_row_index;
}

auto RegionInfo::get_csr_rows_element_count() const noexcept -> std::size_t
{
    return this->csr_rows_element_count_;
}

auto RegionInfo::get_csr_row_index(std::size_t const row_bound, std::size_t const col_bound) const
    -> std::size_t
{
    auto const i = bounds_to_index(row_bound, col_bound, this->col_region_count_);
    return this->region_data_[i].csr_row_index;
}

auto RegionInfo::get_element_index(std::size_t const row_bound, std::size_t const col_bound) const
    -> std::size_t
{
    auto const i = bounds_to_index(row_bound, col_bound, this->col_region_count_);
    return this->region_data_[i].element_index;
}

auto RegionInfo::get_nonempty_region_count() const noexcept -> std::size_t
{
    return this->nonempty_region_count_;
}

auto RegionInfo::get_previous_row(std::size_t const row_bound, std::size_t const col_bound) const
    -> std::size_t
{
    auto const i = bounds_to_index(row_bound, col_bound, this->col_region_count_);
    return this->region_data_[i].previous_row;
}

auto RegionInfo::get_region_type(std::size_t const row_bound, std::size_t const col_bound) const
    -> RegionType
{
    auto const i = bounds_to_index(row_bound, col_bound, this->col_region_count_);
    return this->region_data_[i].region_type;
}

auto RegionInfo::increment_coo_row_index(std::size_t const row_bound, std::size_t const col_bound) -> void
{
    auto const i = bounds_to_index(row_bound, col_bound, this->col_region_count_);
    ++this->region_data_[i].coo_row_index;
}

auto RegionInfo::increment_element_index(std::size_t const row_bound, std::size_t const col_bound) -> void
{
    auto const i = bounds_to_index(row_bound, col_bound, this->col_region_count_);
    ++this->region_data_[i].element_index;
}

template<typename BoundsType, typename T>
auto RegionInfo::init_regions(
        BoundsType const& row_bounds,
        BoundsType const& col_bounds,
        std::span<info::RowIndex> const coo_rows,
        std::span<info::RowPointer> const csr_rows,
        std::span<info::ColIndex> const columns,
        std::span<T> const elements,
        std::span<Region<T>> const regions
    ) -> void
{
    assert(columns.size() == elements.size());
    assert(columns.size() >= coo_rows.size());
    assert(columns.size() >= csr_rows.size());

    for (auto const& region_data : this->region_data_) {
        if (region_data.element_count == 0) {
            continue;
        }

        auto& region = regions[region_data.nonempty_region_index];
        auto const row_bound = region_data.row_bound;
        auto const col_bound = region_data.col_bound;
        region.row_begin = get_original_index(row_bounds, row_bound);
        region.column_begin = get_original_index(col_bounds, col_bound);
        region.row_count = row_bounds.get_size(row_bound);
        region.column_count = col_bounds.get_size(col_bound);
        region.element_count = region_data.element_count;
        region.columns = columns.data() + region_data.element_index;
        region.elements = elements.data() + region_data.element_index;

        switch (region_data.region_type) {
        case RegionType::COO:
            region.rows = coo_rows.data() + region_data.coo_row_index;
            break;

        case RegionType::CSR:
            {
                auto const rows = csr_rows.data() + region_data.csr_row_index;
                region.rows = rows;

                // A nonempty region has at least one row and thus its row
                // pointer array has at least two elements.
                rows[0] = 0;
                rows[1] = 0;

                break;
            }
        }
    }
}

template auto RegionInfo::init_regions(
        Bounds const& row_bounds,
        Bounds const& col_bounds,
        std::span<info::RowIndex> coo_rows,
        std::span<info::RowPointer> csr_rows,
        std::span<info::ColIndex> columns,
        std::span<float> elements,
        std::span<Region<float>> regions
    ) -> void;

template auto RegionInfo::init_regions(
        Bounds const& row_bounds,
        Bounds const& col_bounds,
        std::span<info::RowIndex> coo_rows,
        std::span<info::RowPointer> csr_rows,
        std::span<info::ColIndex> columns,
        std::span<double> elements,
        std::span<Region<double>> regions
    ) -> void;

template auto RegionInfo::init_regions(
        SubBounds const& row_bounds,
        SubBounds const& col_bounds,
        std::span<info::RowIndex> coo_rows,
        std::span<info::RowPointer> csr_rows,
        std::span<info::ColIndex> columns,
        std::span<float> elements,
        std::span<Region<float>> regions
    ) -> void;

template auto RegionInfo::init_regions(
        SubBounds const& row_bounds,
        SubBounds const& col_bounds,
        std::span<info::RowIndex> coo_rows,
        std::span<info::RowPointer> csr_rows,
        std::span<info::ColIndex> columns,
        std::span<double> elements,
        std::span<Region<double>> regions
    ) -> void;

auto RegionInfo::set_previous_row(
        std::size_t const row_bound, std::size_t const col_bound, std::uint32_t const row_index
    ) -> void
{
    auto const i = bounds_to_index(row_bound, col_bound, this->col_region_count_);
    this->region_data_[i].previous_row = row_index;
}

auto RegionInfo::increment_element_count(std::size_t const row_bound, std::size_t const col_bound) -> void
{
    auto const i = bounds_to_index(row_bound, col_bound, this->col_region_count_);
    ++this->region_data_[i].element_count;
}

auto RegionInfo::init_element_counts(
        std::size_t const row_bound,
        Bounds const& row_bounds,
        std::size_t const column_region_count,
        std::span<std::uint32_t const> const row_pointers,
        std::span<std::uint32_t const> const columns
    ) -> void
{
    auto const rd_begin = bounds_to_index(row_bound, 0, column_region_count);
    auto const rd_end = bounds_to_index(row_bound, column_region_count, column_region_count);
    for (auto i = rd_begin; i < rd_end; ++i) {
        this->region_data_[i].element_count = 0;
    }

    auto const row_bounds_begin = row_bounds.get_index(row_bound);
    auto const row_bounds_end = row_bounds.get_index(row_bound + 1);
    for (auto row = row_bounds_begin; row < row_bounds_end; ++row) {
        for (auto i = row_pointers[row]; i < row_pointers[row + 1]; ++i) {
            auto const column = columns[i];
            auto const column_bound = index_to_region_bound(column);
            this->increment_element_count(row_bound, column_bound);
        }
    }
}

template<typename BoundsType>
auto RegionInfo::init_row_element_counts(BoundsType const& row_bounds) -> void
{
    if (this->region_data_.empty()) {
        return;
    }

    auto const& region_data = this->region_data_.back();
    auto const coo_row_index = region_data.coo_row_index;
    auto const csr_row_index = region_data.csr_row_index;

    if (region_data.element_count == 0) {
        this->coo_rows_element_count_ = coo_row_index;
        this->csr_rows_element_count_ = csr_row_index;
        return;
    }

    switch (region_data.region_type) {
    case RegionType::COO:
        {
            auto const element_count = region_data.element_count;
            this->coo_rows_element_count_ = coo_row_index + element_count;
            this->csr_rows_element_count_ = csr_row_index;
            return;
        }

    case RegionType::CSR:
        {
            auto const row_bound = region_data.row_bound;
            auto const row_count = row_bounds.get_size(row_bound);
            this->coo_rows_element_count_ = coo_row_index;
            this->csr_rows_element_count_ = csr_row_index + row_count + 1;
            return;
        }
    }

    throw LogicError{"Unexpected enumeration value."};
}

template auto RegionInfo::init_row_element_counts(Bounds const& row_bounds) -> void;
template auto RegionInfo::init_row_element_counts(SubBounds const& row_bounds) -> void;

auto RegionInfo::init_nonempty_region_count() noexcept -> void
{
    auto region_count = 0_uz;

    for (auto const& region_data : this->region_data_) {
        if (region_data.element_count > 0) {
            ++region_count;
        }
    }

    this->nonempty_region_count_ = region_count;
}

namespace {

auto bounds_to_index(
        std::size_t const row_bound, std::size_t const column_bound, std::size_t const column_region_count
    ) noexcept -> std::size_t
{
    return row_bound * column_region_count + column_bound;
}

auto get_original_index(Bounds const& bounds, std::size_t const bound) -> info::RowPointer
{
    return bounds.get_index(bound);
}

auto get_original_index(SubBounds const& sub_bounds, std::size_t const bound) -> info::RowPointer
{
    return sub_bounds.get_original_index(bound);
}

auto is_csr_preferred(std::size_t const element_count, std::size_t const row_count) noexcept -> bool
{
    if (element_count >= 2 * row_count + 2) {
        return true;
    }
    return false;
}

}  // namespace

}  // namespace hsf
