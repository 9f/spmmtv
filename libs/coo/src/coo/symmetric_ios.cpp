#include <coo/symmetric_ios.hpp>

#include <ostream>              // std::ostream

#include <coo/logic_error.hpp>  // coo::LogicError
#include <coo/symmetric.hpp>    // coo::Symmetric

namespace coo {

auto operator<<(std::ostream& os, Symmetric const symmetric) -> std::ostream&
{
    switch (symmetric) {
    case Symmetric::no:
        return os << "no";
    case Symmetric::yes:
        return os << "yes";
    }

    throw LogicError{"Unexpected enumeration value."};
}

}  // namespace coo
