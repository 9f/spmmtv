#include <coo/matrix.hpp>

#include <algorithm>            // std::sort
#include <array>                // std::to_array
#include <cassert>              // assert
#include <cinttypes>            // SCNu32
#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint64_t
#include <cstdio>               // EOF std::fclose std::FILE std::fopen std::fscanf
#include <filesystem>           // std::filesystem
#include <limits>               // std::numeric_limits
#include <numeric>              // std::iota
#include <string>               // std::string std::to_string
#include <tuple>                // std::tie std::tuple
#include <vector>               // std::vector

#include <boost/core/swap.hpp>  // boost::swap
#include <boost/scope_exit.hpp>  // BOOST_SCOPE_EXIT_END BOOST_SCOPE_EXIT_TPL

#include <H5Cpp.h>              // H5

#include <coo/cstddef.hpp>      // coo::operator""_uz
#include <coo/info.hpp>         // coo::info
#include <coo/logic_error.hpp>  // coo::LogicError
#include <coo/runtime_error.hpp>  // coo::RuntimeError
#include <coo/symmetric.hpp>    // coo::Symmetric

extern "C" {
    #include <mmio.h>           // MM_* mm_*
}

namespace coo {

namespace {

// Return ‘H5::PredType::NATIVE_FLOAT’.
auto get_h5_pred_type(float) noexcept -> H5::PredType const&;

// Return ‘H5::PredType::NATIVE_DOUBLE’.
auto get_h5_pred_type(double) noexcept -> H5::PredType const&;

// Read a line from the Matrix Market file specified by ‘file’, parse the line
// and return a triple of row index, column index and element value.  Throw
// ‘coo::RuntimeError’ if a read error occurs.
auto read_mm_triple(std::FILE* file, float) -> std::tuple<info::Index, info::Index, float>;
auto read_mm_triple(std::FILE* file, double) -> std::tuple<info::Index, info::Index, double>;

}  // namespace

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::load_from_hdf5(std::filesystem::path const& file_path) -> Matrix try
{
    auto const file = H5::H5File{file_path, H5F_ACC_RDONLY};

    auto const matrix_format_attribute = file.openAttribute("matrix format");
    if (matrix_format_attribute.getSpace().getSelectNpoints() != 1) {
        throw RuntimeError{"Failed to read the matrix format from the specified HDF5 file."};
    }
    auto const matrix_format_str_size = matrix_format_attribute.getDataType().getSize();
    auto matrix_format_type = H5::StrType{H5::PredType::C_S1};
    matrix_format_type.setCset(H5T_CSET_UTF8);
    matrix_format_type.setSize(matrix_format_str_size + 1);
    auto matrix_format = std::string{};
    matrix_format_attribute.read(matrix_format_type, matrix_format);
    if (matrix_format != "COO") {
        throw RuntimeError{"The specified HDF5 file doesn’t contain a COO matrix."};
    }

    auto const symmetric_attribute = file.openAttribute("symmetric");
    if (symmetric_attribute.getSpace().getSelectNpoints() != 1) {
        throw RuntimeError{"Failed to read the symmetry of the COO matrix from the specified HDF5 file."};
    }
    auto const symmetric_type = H5::EnumType{H5::PredType::NATIVE_INT};
    auto symmetric = static_cast<int>(Symmetric::no);
    symmetric_type.insert("FALSE", &symmetric);
    symmetric = static_cast<int>(Symmetric::yes);
    symmetric_type.insert("TRUE", &symmetric);
    symmetric_attribute.read(symmetric_type, &symmetric);
    switch (Symm) {
    case Symmetric::no:
        if (symmetric != static_cast<int>(Symmetric::no)) {
            throw RuntimeError{"The COO matrix in the specified HDF5 file is symmetric instead of general."};
        }
        break;
    case Symmetric::yes:
        if (symmetric != static_cast<int>(Symmetric::yes)) {
            throw RuntimeError{"The COO matrix in the specified HDF5 file is general instead of symmetric."};
        }
        break;
    }

    auto const row_count_attribute = file.openAttribute("row count");
    if (row_count_attribute.getSpace().getSelectNpoints() != 1) {
        throw RuntimeError{"Failed to read the row count of the COO matrix from the specified HDF5 file."};
    }
    auto row_count = std::uint64_t{};
    row_count_attribute.read(H5::PredType::NATIVE_UINT64, &row_count);

    auto const column_count_attribute = file.openAttribute("column count");
    if (column_count_attribute.getSpace().getSelectNpoints() != 1) {
        throw RuntimeError{"Failed to read the column count of the COO matrix from the specified HDF5 file."};
    }
    auto column_count = std::uint64_t{};
    column_count_attribute.read(H5::PredType::NATIVE_UINT64, &column_count);

    auto const element_count_attribute = file.openAttribute("element count");
    if (element_count_attribute.getSpace().getSelectNpoints() != 1) {
        throw RuntimeError{"Failed to read the element count of the COO matrix from the specified HDF5 file."};
    }
    auto element_count = std::uint64_t{};
    element_count_attribute.read(H5::PredType::NATIVE_UINT64, &element_count);

    auto matrix = Matrix{row_count, column_count, element_count};

    auto const rows_dataset = file.openDataSet("row indices");
    auto const rows_mem_dims = std::to_array<hsize_t>({matrix.rows.size()});
    auto const rows_mem_space = H5::DataSpace{1, rows_mem_dims.data()};
    rows_dataset.read(matrix.rows.data(), H5::PredType::NATIVE_UINT32, rows_mem_space);

    auto const columns_dataset = file.openDataSet("column indices");
    auto const columns_mem_dims = std::to_array<hsize_t>({matrix.columns.size()});
    auto const columns_mem_space = H5::DataSpace{1, columns_mem_dims.data()};
    columns_dataset.read(matrix.columns.data(), H5::PredType::NATIVE_UINT32, columns_mem_space);

    auto const elements_dataset = file.openDataSet("element values");
    auto const elements_mem_dims = std::to_array<hsize_t>({matrix.elements.size()});
    auto const elements_mem_space = H5::DataSpace{1, elements_mem_dims.data()};
    elements_dataset.read(matrix.elements.data(), get_h5_pred_type(T{}), elements_mem_space);

    return matrix;

} catch (H5::Exception const&) {
    throw RuntimeError{"Reading the COO matrix from the specified HDF5 file has failed."};
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::load_from_mm(std::filesystem::path const& file_path) -> Matrix
{
    auto* const file = std::fopen(file_path.c_str(), "r");
    if (file == nullptr) {
        throw RuntimeError{"Failed to open the specified MM file."};
    }

    BOOST_SCOPE_EXIT_TPL(&file) {
        std::fclose(file);
    } BOOST_SCOPE_EXIT_END;

    MM_typecode typecode{};
    switch (mm_read_banner(file, &typecode)) {
    case 0:
        break;
    case MM_NO_HEADER:
        throw RuntimeError{"The specified MM file doesn’t contain the required header line."};
    case MM_PREMATURE_EOF:
        throw RuntimeError{"The header line of the specified MM file is incomplete."};
    case MM_UNSUPPORTED_TYPE:
        throw RuntimeError{"The header line of the specified MM file describes an unsupported matrix format."};
    default:
        throw RuntimeError{"Failed to parse the header line of the specified MM file."};
    }

    if (!mm_is_matrix(typecode) || !mm_is_sparse(typecode)) {
        throw RuntimeError{"Only sparse matrices in the coordinate format are supported."};
    }
    if (!mm_is_real(typecode)) {
        throw RuntimeError{"Only real-valued sparse matrices are supported."};
    }

    switch (Symm) {
    case Symmetric::no:
        if (!mm_is_general(typecode)) {
            throw RuntimeError{"The matrix in the specified MM file isn’t general."};
        }
        break;
    case Symmetric::yes:
        if (!mm_is_symmetric(typecode)) {
            throw RuntimeError{"The matrix in the specified MM file isn’t symmetric."};
        }
        break;
    }

    auto row_count = int{};
    auto column_count = int{};
    auto element_count = int{};
    switch (mm_read_mtx_crd_size(file, &row_count, &column_count, &element_count)) {
    case 0:
        break;
    case MM_PREMATURE_EOF:
        throw RuntimeError{"Parsing the specified MM file has failed before encountering matrix size information."};
    default:
        throw RuntimeError{"Failed to parse matrix size information from the specified MM file."};
    }

    auto rows = std::vector<info::Index>(static_cast<std::size_t>(element_count));
    auto columns = std::vector<info::Index>(static_cast<std::size_t>(element_count));
    auto elements = std::vector<T>(static_cast<std::size_t>(element_count));

    for (auto i = 0_uz; i < elements.size(); ++i) {
        std::tie(rows[i], columns[i], elements[i]) = read_mm_triple(file, T{});
        // Element indices in the Matrix Market format are one-based; subtract
        // one to obtain zero-based indices.
        --rows[i];
        --columns[i];
        // The MM format stores symmetric matrices as triangular matrices.
        // Convert the loaded matrix to upper-triangular form to match the way
        // symmetric matrices are created by the program in ‘extras/mm2h5’.
        if (Symm == Symmetric::yes && rows[i] > columns[i]) {
            boost::swap(rows[i], columns[i]);
        }
    }

    auto const row_major_less_than = [&](std::size_t const lhs, std::size_t const rhs) {
        if (rows[lhs] == rows[rhs]) {
            return columns[lhs] < columns[rhs];
        }
        return rows[lhs] < rows[rhs];
    };

    auto indices = std::vector<std::size_t>(elements.size());
    std::iota(indices.begin(), indices.end(), 0);
    std::sort(indices.begin(), indices.end(), row_major_less_than);

    auto matrix = Matrix{
        static_cast<std::size_t>(row_count),
        static_cast<std::size_t>(column_count),
        static_cast<std::size_t>(element_count)
    };

    for (auto i = 0_uz; i < indices.size(); ++i) {
        matrix.rows[i] = rows[indices[i]];
        matrix.columns[i] = columns[indices[i]];
        matrix.elements[i] = elements[indices[i]];
    }

    return matrix;
}

template<typename T, Symmetric Symm>
Matrix<T, Symm>::Matrix(std::size_t const row_count, std::size_t const column_count, std::size_t const element_count)
    : rows(element_count),
      columns(element_count),
      elements(element_count),
      row_count_{row_count},
      column_count_{column_count}
{
    auto const max_row_count = std::numeric_limits<Index>::max();
    if (row_count > max_row_count) {
        throw LogicError{
                "coo::Matrix doesn’t support row counts greater than " + std::to_string(max_row_count) + '.'
            };
    }

    auto const max_column_count = std::numeric_limits<Index>::max();
    if (column_count > max_column_count) {
        throw LogicError{
                "coo::Matrix doesn’t support column counts greater than " + std::to_string(max_column_count) + '.'
            };
    }

    if (Symm == Symmetric::yes && row_count != column_count) {
        throw LogicError{"coo::Matrix can’t be symmetric and rectangular simultaneously."};
    }

    if (element_count > row_count * column_count
        || (row_count == 0 && column_count != 0)
        || (row_count != 0 && column_count == 0)) {
        throw LogicError{"Attempting to instantiate coo::Matrix with invalid dimensions."};
    }
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::row_count() const noexcept -> std::size_t
{
    return this->row_count_;
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::column_count() const noexcept -> std::size_t
{
    return this->column_count_;
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::element_count() const noexcept -> std::size_t
{
    assert(this->elements.size() == this->rows.size());
    assert(this->elements.size() == this->columns.size());
    return this->elements.size();
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::is_row_major_ordered() const -> bool
{
    for (auto i = 0_uz; i < this->element_count() - 1; ++i) {
        auto const row = this->rows[i];
        auto const column = this->columns[i];
        auto const next_row = this->rows[i + 1];
        auto const next_column = this->columns[i + 1];

        if (row == next_row) {
            if (column >= next_column) {
                return false;
            }
        } else if (row > next_row) {
            return false;
        }
    }

    return true;
}

template<typename T, Symmetric Symm>
auto Matrix<T, Symm>::is_valid() const -> bool
{
    // Check whether the following invariants hold:
    //
    // 1.  All row and column indices are greater than or equal to zero.
    //
    // 2.  No row or column indices exceed row count or column count
    // respectively.

    for (auto i = 0_uz; i < this->element_count(); ++i) {
        auto const row = this->rows[i];
        auto const column = this->columns[i];

        // Check invariant no. 1.
        if (row < 0 || column < 0) {
            return false;
        }

        // Check invariant no. 2.
        if (row >= this->row_count() || column >= this->column_count()) {
            return false;
        }
    }

    return true;
}

template class Matrix<float, Symmetric::no>;
template class Matrix<float, Symmetric::yes>;
template class Matrix<double, Symmetric::no>;
template class Matrix<double, Symmetric::yes>;

namespace {

auto get_h5_pred_type(float) noexcept -> H5::PredType const&
{
    return H5::PredType::NATIVE_FLOAT;
}

auto get_h5_pred_type(double) noexcept -> H5::PredType const&
{
    return H5::PredType::NATIVE_DOUBLE;
}

auto read_mm_triple(std::FILE* const file, float) -> std::tuple<info::Index, info::Index, float>
{
    auto row = info::Index{};
    auto column = info::Index{};
    auto element_value = float{};

    auto const ret = std::fscanf(file, "%" SCNu32 " %" SCNu32 " %f\n", &row, &column, &element_value);
    if (ret == EOF) {
        throw RuntimeError{
                "Encountered end-of-file when parsing a triple of row index, column index and element value "
                "from the specified MM file."
            };
    }
    if (ret != 3) {
        throw RuntimeError{
                "Encountered unexpected data when parsing a triple of row index, column index and element "
                "value from the specified MM file."
            };
    }

    return {row, column, element_value};
}

auto read_mm_triple(std::FILE* const file, double) -> std::tuple<info::Index, info::Index, double>
{
    auto row = info::Index{};
    auto column = info::Index{};
    auto element_value = double{};

    auto const ret = std::fscanf(file, "%" SCNu32 " %" SCNu32 " %lf\n", &row, &column, &element_value);
    if (ret == EOF) {
        throw RuntimeError{
                "Encountered end-of-file when parsing a triple of row index, column index and element value "
                "from the specified MM file."
            };
    }
    if (ret != 3) {
        throw RuntimeError{
                "Encountered unexpected data when parsing a triple of row index, column index and element "
                "value from the specified MM file."
            };
    }

    return {row, column, element_value};
}

}  // namespace

}  // namespace coo
