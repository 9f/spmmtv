#pragma once

namespace coo {

// Indicate the symmetry of a COO matrix.
enum struct Symmetric {
    no,
    yes,
};

}  // namespace coo
