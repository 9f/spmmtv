#pragma once

#include <ostream>              // std::ostream

namespace coo {

enum struct Symmetric;

// Print parameter ‘symmetric’ into the output stream given by parameter ‘os’
// and return parameter ‘os’.  Throw an exception of type ‘coo::LogicError’ if
// parameter ‘symmetric’ has an unexpected value.
auto operator<<(std::ostream& os, Symmetric symmetric) -> std::ostream&;

}  // namespace coo
