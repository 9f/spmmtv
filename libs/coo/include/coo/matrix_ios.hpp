#pragma once

#include <ostream>              // std::ostream

namespace coo {

enum struct Symmetric;
template<typename T, Symmetric Symm> class Matrix;

// Print a COO matrix’s row, column and element count to an output stream.
// Return parameter ‘os’.
template<typename T, Symmetric Symm>
auto operator<<(std::ostream& os, Matrix<T, Symm> const& matrix) -> std::ostream&;

}  // namespace coo
