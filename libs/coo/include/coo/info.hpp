#pragma once

#include <cstdint>              // std::uint32_t

namespace coo::info {

using Index = std::uint32_t;

}  // namespace coo::info
