#pragma once

#include <cstddef>              // std::size_t
#include <filesystem>           // std::filesystem
#include <vector>               // std::vector

#include <coo/info.hpp>         // coo::info

namespace coo {

enum struct Symmetric;

template<typename T, Symmetric Symm>
class Matrix {
public:
    using Index = info::Index;
    using Value = T;

    static constexpr auto symmetric = Symm;

    // The matrix’s COO row index array.
    std::vector<Index> rows{};

    // The matrix’s COO column index array.
    std::vector<Index> columns{};

    // The matrix’s COO element array.
    std::vector<T> elements{};

    // Load a COO matrix from the HDF5 file specified by ‘file_path’.  Throw
    // ‘coo::RuntimeError’ if loading the matrix fails.
    static auto load_from_hdf5(std::filesystem::path const& file_path) -> Matrix;

    // Load a COO matrix from the Matrix Market file specified by ‘file_path’.
    // Throw ‘coo::RuntimeError’ if loading the matrix fails.
    static auto load_from_mm(std::filesystem::path const& file_path) -> Matrix;

    Matrix() = default;

    // Instantiate a COO matrix with row count, column count and element count
    // respectively specified by parameters ‘row_count’, ‘column_count’ and
    // ‘element_count’.  Throw ‘coo::LogicError’ if any of the passed arguments
    // are invalid.
    Matrix(std::size_t row_count, std::size_t column_count, std::size_t element_count);

    auto row_count() const noexcept -> std::size_t;
    auto column_count() const noexcept -> std::size_t;
    auto element_count() const noexcept -> std::size_t;

    auto is_row_major_ordered() const -> bool;
    auto is_valid() const -> bool;

private:
    std::size_t row_count_{};
    std::size_t column_count_{};
};

}  // namespace coo
