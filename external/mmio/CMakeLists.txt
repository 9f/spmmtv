project(mmio LANGUAGES C)

add_library("${PROJECT_NAME}" src/mmio.c)


# Compilation Settings

set_target_properties("${PROJECT_NAME}" PROPERTIES
  C_EXTENSIONS FALSE
  C_STANDARD_REQUIRED TRUE
  INTERPROCEDURAL_OPTIMIZATION_RELEASE TRUE
  INTERPROCEDURAL_OPTIMIZATION_RELWITHDEBINFO TRUE
  )

target_compile_features("${PROJECT_NAME}" PUBLIC c_std_90)
target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")


# Compiler-Specific Settings

if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
  target_compile_options("${PROJECT_NAME}" PRIVATE
    -Wpedantic
    $<$<CONFIG:Debug>:-Og>
    $<$<CONFIG:RelWithDebInfo>:-fno-omit-frame-pointer>
    $<$<STREQUAL:"${CMAKE_GENERATOR}","Ninja">:-fcolor-diagnostics>
    )

  target_link_options("${PROJECT_NAME}" PRIVATE
    $<$<CONFIG:Debug>:-Og>
    $<$<CONFIG:RelWithDebInfo>:-fno-omit-frame-pointer>
    $<$<STREQUAL:"${CMAKE_GENERATOR}","Ninja">:-fcolor-diagnostics>
    )

elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")
  target_compile_options("${PROJECT_NAME}" PRIVATE
    -Wno-unused-result
    -Wpedantic
    $<$<CONFIG:Debug>:-Og>
    $<$<CONFIG:RelWithDebInfo>:-fno-omit-frame-pointer>
    $<$<STREQUAL:"${CMAKE_GENERATOR}","Ninja">:-fdiagnostics-color=always>
    )

  target_link_options("${PROJECT_NAME}" PRIVATE
    $<$<CONFIG:Debug>:-Og>
    $<$<CONFIG:RelWithDebInfo>:-fno-omit-frame-pointer>
    $<$<STREQUAL:"${CMAKE_GENERATOR}","Ninja">:-fdiagnostics-color=always>
    )

endif()

treat_warnings_as_errors("${PROJECT_NAME}")
