#pragma once

namespace meta {

template<typename ToSymmetry, typename FromSymmetry>
constexpr auto convert_symmetry(FromSymmetry from_symmetry) noexcept -> ToSymmetry;

}  // namespace meta

#include <meta/convert_symmetry.ipp>
