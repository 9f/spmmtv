#include <meta/are_equal.hpp>

#include <cmath>                // std::abs
#include <cstddef>              // std::size_t
#include <span>                 // std::span

#include <boost/test/tools/assertion_result.hpp>  // boost::test_tools::predicate_result

#include <cstddef.hpp>          // operator""_uz

namespace meta {

template<typename T>
auto are_equal(std::span<T const> const y, std::span<T const> const model, T const tolerance)
    -> boost::test_tools::predicate_result
{
    auto result = boost::test_tools::predicate_result{true};
    if (y.size() != model.size()) {
        result = false;
        result.message() << "\nArray size mismatch: " << y.size() << " != " << model.size();
        return result;
    }

    for (auto i = 0_uz; i < y.size(); ++i) {
        auto const difference = y[i] - model[i];
        if (std::abs(difference) <= tolerance) {
            continue;
        }
        result.message() << "\n  - mismatch at position " << i << ": "
            "[" << y[i] << " == " << model[i] << "] is false: "
            "difference exceeds tolerance [|" << difference << "| > " << tolerance << "]";
    }

    if (!result.has_empty_message()) {
        result = false;
    }
    return result;
}

template auto are_equal(std::span<float const> y, std::span<float const> model, float tolerance)
    -> boost::test_tools::predicate_result;

template auto are_equal(std::span<double const> y, std::span<double const> model, double tolerance)
    -> boost::test_tools::predicate_result;

}  // namespace meta
