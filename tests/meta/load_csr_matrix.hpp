#pragma once

#include <string_view>          // std::string_view

#include <csr/matrix.hpp>       // csr::Matrix

namespace csr {

enum struct Symmetric;

}  // namespace csr

namespace meta {

template<typename T, csr::Symmetric Symm>
auto load_csr_matrix(std::string_view name) -> csr::Matrix<T, Symm>;

}  // namespace meta
