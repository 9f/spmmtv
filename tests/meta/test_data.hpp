#pragma once

#include <string_view>          // std::string_view
#include <vector>               // std::vector

namespace meta {

template<typename T>
class TestData {
public:
    static auto load_from_hdf5_one_vec(std::string_view const matrix_name) -> TestData<T>;
    static auto load_from_hdf5_two_vecs(std::string_view const matrix_name) -> TestData<T>;

    std::vector<T> y1{};
    std::vector<T> y2{};
    T mean_nonzero_element_magnitude{};
};

}  // namespace meta
