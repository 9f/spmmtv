#include <meta/test_data.hpp>

#include <cstddef>              // std::size_t
#include <stdexcept>            // std::runtime_error
#include <string>               // std::string
#include <string_view>          // std::string_view
#include <utility>              // std::move
#include <vector>               // std::vector

#include <H5Cpp.h>              // H5

#include <meta/info.hpp>        // meta::info

namespace meta {

namespace {

// Return ‘H5::PredType::NATIVE_FLOAT’.
auto get_h5_pred_type(float) noexcept -> H5::PredType const&;

// Return ‘H5::PredType::NATIVE_DOUBLE’.
auto get_h5_pred_type(double) noexcept -> H5::PredType const&;

}  // namespace

template<typename T>
auto TestData<T>::load_from_hdf5_one_vec(std::string_view const matrix_name) -> TestData<T>
{
    auto const filename = std::string{matrix_name} + ".h5";
    auto const file_path = info::dataset_path / info::test_data_path<T> / filename;
    auto const file = H5::H5File{file_path, H5F_ACC_RDONLY};

    auto const y1_dataset = file.openDataSet("y1 SpMV");
    auto const y1_size = y1_dataset.getSpace().getSelectNpoints();
    auto y1 = std::vector<T>(static_cast<std::size_t>(y1_size));
    y1_dataset.read(y1.data(), get_h5_pred_type(T{}));

    auto const y2_dataset = file.openDataSet("y1 SpMTV");
    auto const y2_size = y2_dataset.getSpace().getSelectNpoints();
    auto y2 = std::vector<T>(static_cast<std::size_t>(y2_size));
    y2_dataset.read(y2.data(), get_h5_pred_type(T{}));

    auto const magnitude_dataset = file.openDataSet("mean nonzero element magnitude");
    auto const magnitude_size = magnitude_dataset.getSpace().getSelectNpoints();
    if (magnitude_size != 1) {
        throw std::runtime_error{"Error: failed to read the mean nonzero element magnitude from the test data file."};
    }
    auto magnitude = T{};
    magnitude_dataset.read(&magnitude, get_h5_pred_type(T{}));

    return {std::move(y1), std::move(y2), magnitude};
}

template<typename T>
auto TestData<T>::load_from_hdf5_two_vecs(std::string_view const matrix_name) -> TestData<T>
{
    auto const filename = std::string{matrix_name} + ".h5";
    auto const file_path = info::dataset_path / info::test_data_path<T> / filename;
    auto const file = H5::H5File{file_path, H5F_ACC_RDONLY};

    auto const y1_dataset = file.openDataSet("y1 SpMV");
    auto const y1_size = y1_dataset.getSpace().getSelectNpoints();
    auto y1 = std::vector<T>(static_cast<std::size_t>(y1_size));
    y1_dataset.read(y1.data(), get_h5_pred_type(T{}));

    auto const y2_dataset = file.openDataSet("y2 SpMTV");
    auto const y2_size = y2_dataset.getSpace().getSelectNpoints();
    auto y2 = std::vector<T>(static_cast<std::size_t>(y2_size));
    y2_dataset.read(y2.data(), get_h5_pred_type(T{}));

    auto const magnitude_dataset = file.openDataSet("mean nonzero element magnitude");
    auto const magnitude_size = magnitude_dataset.getSpace().getSelectNpoints();
    if (magnitude_size != 1) {
        throw std::runtime_error{"Error: failed to read the mean nonzero element magnitude from the test data file."};
    }
    auto magnitude = T{};
    magnitude_dataset.read(&magnitude, get_h5_pred_type(T{}));

    return {std::move(y1), std::move(y2), magnitude};
}

template class TestData<float>;
template class TestData<double>;

namespace {

auto get_h5_pred_type(float) noexcept -> H5::PredType const&
{
    return H5::PredType::NATIVE_FLOAT;
}

auto get_h5_pred_type(double) noexcept -> H5::PredType const&
{
    return H5::PredType::NATIVE_DOUBLE;
}

}  // namespace

}  // namespace meta
