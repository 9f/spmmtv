#pragma once
#include <meta/convert_symmetry.hpp>

namespace meta {

template<typename ToSymmetry, typename FromSymmetry>
constexpr auto convert_symmetry(FromSymmetry const from_symmetry) noexcept -> ToSymmetry
{
    switch (from_symmetry) {
    case FromSymmetry::no:
        return ToSymmetry::no;
    case FromSymmetry::yes:
        return ToSymmetry::yes;
    }
}

}  // namespace meta
