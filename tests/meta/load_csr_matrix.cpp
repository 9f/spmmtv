#include <meta/load_csr_matrix.hpp>

#include <string>               // std::string
#include <string_view>          // std::string_view

#include <csr/matrix.hpp>       // csr::Matrix
#include <csr/symmetric.hpp>    // csr::Symmetric

#include <meta/convert_symmetry.hpp>  // meta::convert_symmetry
#include <meta/info.hpp>        // meta::info
#include <meta/symmetric.hpp>   // meta::Symmetric

namespace meta {

template<typename T, csr::Symmetric Symm>
auto load_csr_matrix(std::string_view const name) -> csr::Matrix<T, Symm>
{
    constexpr auto symmetry = convert_symmetry<Symmetric>(Symm);
    auto const filename = std::string{name} + ".csr.h5";
    auto const path = info::dataset_path / info::matrix_path<T, symmetry> / filename;
    return csr::Matrix<T, Symm>::load_from_hdf5(path);
}

template auto load_csr_matrix(std::string_view name) -> csr::Matrix<float, csr::Symmetric::no>;
template auto load_csr_matrix(std::string_view name) -> csr::Matrix<float, csr::Symmetric::yes>;
template auto load_csr_matrix(std::string_view name) -> csr::Matrix<double, csr::Symmetric::no>;
template auto load_csr_matrix(std::string_view name) -> csr::Matrix<double, csr::Symmetric::yes>;

}  // namespace meta
