#include <meta/load_hsf_matrix.hpp>

#include <string_view>          // std::string_view
#include <tuple>                // std::tuple
#include <utility>              // std::move

#include <omp.h>                // omp_get_max_threads

#include <csr/matrix.hpp>       // csr::Matrix
#include <csr/symmetric.hpp>    // csr::Symmetric

#include <hsf/matrix.hpp>       // hsf::Matrix
#include <hsf/symmetric.hpp>    // hsf::Symmetric

#include <meta/convert_symmetry.hpp>  // meta::convert_symmetry
#include <meta/load_csr_matrix.hpp>  // meta::load_csr_matrix

namespace hsf {

enum struct OptimiseCsrRegions;
enum struct RegionOrdering;

}  // namespace hsf

namespace meta {

template<typename T, hsf::Symmetric Symm>
auto load_hsf_matrix_sequential(
        std::string_view const name,
        hsf::RegionOrdering const region_ordering,
        hsf::OptimiseCsrRegions const optimise_csr_regions
    ) -> hsf::Matrix<T, Symm>
{
    constexpr auto csr_symmetry = convert_symmetry<csr::Symmetric>(Symm);
    auto [hsf_matrix, csr_matrix]
        = load_hsf_matrix_sequential_both<T, Symm, csr_symmetry>(name, region_ordering, optimise_csr_regions);
    return hsf_matrix;
}

template auto load_hsf_matrix_sequential(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> hsf::Matrix<float, hsf::Symmetric::no>;

template auto load_hsf_matrix_sequential(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> hsf::Matrix<float, hsf::Symmetric::yes>;

template auto load_hsf_matrix_sequential(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> hsf::Matrix<double, hsf::Symmetric::no>;

template auto load_hsf_matrix_sequential(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> hsf::Matrix<double, hsf::Symmetric::yes>;

template<typename T, hsf::Symmetric HsfSymm, csr::Symmetric CsrSymm>
auto load_hsf_matrix_sequential_both(
        std::string_view const name,
        hsf::RegionOrdering const region_ordering,
        hsf::OptimiseCsrRegions const optimise_csr_regions
    ) -> std::tuple<hsf::Matrix<T, HsfSymm>, csr::Matrix<T, CsrSymm>>
{
    auto csr_matrix = load_csr_matrix<T, CsrSymm>(name);
    auto hsf_matrix = hsf::Matrix<T, HsfSymm>{
        csr_matrix.row_pointers.data(),
        csr_matrix.columns.data(),
        csr_matrix.elements.data(),
        csr_matrix.row_count(),
        csr_matrix.column_count(),
        csr_matrix.element_count(),
        region_ordering,
        optimise_csr_regions
    };
    return {std::move(hsf_matrix), std::move(csr_matrix)};
}

template auto load_hsf_matrix_sequential_both(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> std::tuple<hsf::Matrix<float, hsf::Symmetric::no>, csr::Matrix<float, csr::Symmetric::no>>;

template auto load_hsf_matrix_sequential_both(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> std::tuple<hsf::Matrix<float, hsf::Symmetric::yes>, csr::Matrix<float, csr::Symmetric::yes>>;

template auto load_hsf_matrix_sequential_both(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> std::tuple<hsf::Matrix<double, hsf::Symmetric::no>, csr::Matrix<double, csr::Symmetric::no>>;

template auto load_hsf_matrix_sequential_both(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> std::tuple<hsf::Matrix<double, hsf::Symmetric::yes>, csr::Matrix<double, csr::Symmetric::yes>>;

template<typename T, hsf::Symmetric Symm>
auto load_hsf_matrix_parallel(
        std::string_view const name,
        hsf::RegionOrdering const region_ordering,
        hsf::OptimiseCsrRegions const optimise_csr_regions
    ) -> hsf::Matrix<T, Symm>
{
    constexpr auto csr_symmetry = convert_symmetry<csr::Symmetric>(Symm);
    auto [hsf_matrix, csr_matrix]
        = load_hsf_matrix_parallel_both<T, Symm, csr_symmetry>(name, region_ordering, optimise_csr_regions);
    return hsf_matrix;
}

template auto load_hsf_matrix_parallel(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> hsf::Matrix<float, hsf::Symmetric::no>;

template auto load_hsf_matrix_parallel(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> hsf::Matrix<float, hsf::Symmetric::yes>;

template auto load_hsf_matrix_parallel(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> hsf::Matrix<double, hsf::Symmetric::no>;

template auto load_hsf_matrix_parallel(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> hsf::Matrix<double, hsf::Symmetric::yes>;

template<typename T, hsf::Symmetric HsfSymm, csr::Symmetric CsrSymm>
auto load_hsf_matrix_parallel_both(
        std::string_view const name,
        hsf::RegionOrdering const region_ordering,
        hsf::OptimiseCsrRegions const optimise_csr_regions
    ) -> std::tuple<hsf::Matrix<T, HsfSymm>, csr::Matrix<T, CsrSymm>>
{
    auto csr_matrix = load_csr_matrix<T, CsrSymm>(name);
    auto const region_coefficient = .12;
    auto const thread_count = omp_get_max_threads();
    auto hsf_matrix = hsf::Matrix<T, HsfSymm>{
        csr_matrix.row_pointers.data(),
        csr_matrix.columns.data(),
        csr_matrix.elements.data(),
        csr_matrix.row_count(),
        csr_matrix.column_count(),
        csr_matrix.element_count(),
        region_ordering,
        region_coefficient,
        thread_count,
        optimise_csr_regions
    };
    return {std::move(hsf_matrix), std::move(csr_matrix)};
}

template auto load_hsf_matrix_parallel_both(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> std::tuple<hsf::Matrix<float, hsf::Symmetric::no>, csr::Matrix<float, csr::Symmetric::no>>;

template auto load_hsf_matrix_parallel_both(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> std::tuple<hsf::Matrix<float, hsf::Symmetric::yes>, csr::Matrix<float, csr::Symmetric::yes>>;

template auto load_hsf_matrix_parallel_both(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> std::tuple<hsf::Matrix<double, hsf::Symmetric::no>, csr::Matrix<double, csr::Symmetric::no>>;

template auto load_hsf_matrix_parallel_both(
        std::string_view name, hsf::RegionOrdering region_ordering, hsf::OptimiseCsrRegions optimise_csr_regions
    ) -> std::tuple<hsf::Matrix<double, hsf::Symmetric::yes>, csr::Matrix<double, csr::Symmetric::yes>>;

}  // namespace meta
