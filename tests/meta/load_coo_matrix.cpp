#include <meta/load_coo_matrix.hpp>

#include <string>               // std::string
#include <string_view>          // std::string_view

#include <coo/matrix.hpp>       // coo::Matrix
#include <coo/symmetric.hpp>    // coo::Symmetric

#include <meta/convert_symmetry.hpp>  // meta::convert_symmetry
#include <meta/info.hpp>        // meta::info
#include <meta/symmetric.hpp>   // meta::Symmetric

namespace meta {

template<typename T, coo::Symmetric Symm>
auto load_coo_matrix(std::string_view const name) -> coo::Matrix<T, Symm>
{
    constexpr auto symmetry = convert_symmetry<Symmetric>(Symm);
    auto const filename = std::string{name} + ".coo.h5";
    auto const path = info::dataset_path / info::matrix_path<T, symmetry> / filename;
    return coo::Matrix<T, Symm>::load_from_hdf5(path);
}

template auto load_coo_matrix(std::string_view name) -> coo::Matrix<float, coo::Symmetric::no>;
template auto load_coo_matrix(std::string_view name) -> coo::Matrix<float, coo::Symmetric::yes>;
template auto load_coo_matrix(std::string_view name) -> coo::Matrix<double, coo::Symmetric::no>;
template auto load_coo_matrix(std::string_view name) -> coo::Matrix<double, coo::Symmetric::yes>;

}  // namespace meta
