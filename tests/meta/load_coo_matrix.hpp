#pragma once

#include <string_view>          // std::string_view

#include <coo/matrix.hpp>       // coo::Matrix

namespace coo {

enum struct Symmetric;

}  // namespace coo

namespace meta {

template<typename T, coo::Symmetric Symm>
auto load_coo_matrix(std::string_view name) -> coo::Matrix<T, Symm>;

}  // namespace meta
