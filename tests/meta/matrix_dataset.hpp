#pragma once

#include <string_view>          // std::string_view
#include <vector>               // std::vector

#include <boost/test/unit_test.hpp>  // boost::unit_test
#include <boost/test/data/monomorphic.hpp>

namespace meta {

enum struct Symmetric;

class MatrixDataset {
public:
    enum {
        arity = 1,
    };

    MatrixDataset() = default;
    MatrixDataset(Symmetric symmetric);

    auto begin() const -> std::vector<std::string_view>::const_iterator;
    auto size() const -> boost::unit_test::data::size_t;

private:
    std::vector<std::string_view> matrix_names_{};
};

}  // namespace meta

namespace boost::unit_test::data::monomorphic {

// Register MatrixDataset as a dataset into the Unit Test Framework.
template<>
struct is_dataset<meta::MatrixDataset> : boost::mpl::true_
{
};

}  // namespace boost::unit_test::data::monomorphic
