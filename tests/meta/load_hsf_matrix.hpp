#pragma once

#include <string_view>          // std::string_view
#include <tuple>                // std::tuple

#include <csr/matrix.hpp>       // csr::Matrix

#include <hsf/matrix.hpp>       // hsf::Matrix
#include <hsf/optimise_csr_regions.hpp>  // hsf::OptimiseCsrRegions

namespace csr {

enum struct Symmetric;

}  // namespace csr

namespace hsf {

enum struct RegionOrdering;
enum struct Symmetric;

}  // namespace hsf

namespace meta {

template<typename T, hsf::Symmetric Symm>
auto load_hsf_matrix_sequential(
        std::string_view name,
        hsf::RegionOrdering region_ordering,
        hsf::OptimiseCsrRegions ocr = hsf::OptimiseCsrRegions::yes
    ) -> hsf::Matrix<T, Symm>;

template<typename T, hsf::Symmetric HsfSymm, csr::Symmetric CsrSymm>
auto load_hsf_matrix_sequential_both(
        std::string_view name,
        hsf::RegionOrdering region_ordering,
        hsf::OptimiseCsrRegions ocr = hsf::OptimiseCsrRegions::yes
    ) -> std::tuple<hsf::Matrix<T, HsfSymm>, csr::Matrix<T, CsrSymm>>;

template<typename T, hsf::Symmetric Symm>
auto load_hsf_matrix_parallel(
        std::string_view name,
        hsf::RegionOrdering region_ordering,
        hsf::OptimiseCsrRegions ocr = hsf::OptimiseCsrRegions::yes
    ) -> hsf::Matrix<T, Symm>;

template<typename T, hsf::Symmetric HsfSymm, csr::Symmetric CsrSymm>
auto load_hsf_matrix_parallel_both(
        std::string_view name,
        hsf::RegionOrdering region_ordering,
        hsf::OptimiseCsrRegions ocr = hsf::OptimiseCsrRegions::yes
    ) -> std::tuple<hsf::Matrix<T, HsfSymm>, csr::Matrix<T, CsrSymm>>;

}  // namespace meta
