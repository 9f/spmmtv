#include <meta/matrix_dataset.hpp>

#include <map>                  // std::map
#include <stdexcept>            // std::logic_error
#include <string_view>          // std::string_view
#include <vector>               // std::vector

#include <boost/test/unit_test.hpp>  // boost::unit_test

#include <meta/symmetric.hpp>   // meta::Symmetric

using boost::unit_test::framework::master_test_suite;

namespace meta {

namespace {

enum struct DatasetName {
    tiny,
    small,
    medium,
    all,
    invalid,
};

auto const dataset_names = std::map<std::string_view, DatasetName>{
    {"tiny",   DatasetName::tiny},
    {"small",  DatasetName::small},
    {"medium", DatasetName::medium},
    {"all",    DatasetName::all},
};

auto init_matrix_names(Symmetric symmetric) -> std::vector<std::string_view>;

auto init_matrix_names_general() -> std::vector<std::string_view>;

auto init_matrix_names_symmetric() -> std::vector<std::string_view>;

auto to_dataset_name(std::string_view dataset_name) -> DatasetName;

auto matrix_names_tiny_general() -> std::vector<std::string_view>;
auto matrix_names_tiny_symmetric() -> std::vector<std::string_view>;

auto matrix_names_small_general() -> std::vector<std::string_view>;
auto matrix_names_small_symmetric() -> std::vector<std::string_view>;

auto matrix_names_medium_general() -> std::vector<std::string_view>;
auto matrix_names_medium_symmetric() -> std::vector<std::string_view>;

auto matrix_names_all_general() -> std::vector<std::string_view>;
auto matrix_names_all_symmetric() -> std::vector<std::string_view>;

}  // namespace

MatrixDataset::MatrixDataset(Symmetric const symmetric)
    : matrix_names_(init_matrix_names(symmetric))
{
}

auto MatrixDataset::begin() const -> std::vector<std::string_view>::const_iterator
{
    return this->matrix_names_.begin();
}

auto MatrixDataset::size() const -> boost::unit_test::data::size_t
{
    return this->matrix_names_.size();
}

namespace {

auto init_matrix_names(Symmetric const symmetric) -> std::vector<std::string_view>
{
    if (master_test_suite().argc != 3) {
        throw std::logic_error{"Usage: tests [OPTIONS] -- DATASET_NAME DATASET_DIR"};
    }

    switch (symmetric) {
    case Symmetric::no:
        return init_matrix_names_general();

    case Symmetric::yes:
        return init_matrix_names_symmetric();
    }

    throw std::logic_error{"Unexpected enumeration value."};
}

auto init_matrix_names_general() -> std::vector<std::string_view>
{
    switch (to_dataset_name(master_test_suite().argv[1])) {
    case DatasetName::tiny:
        return matrix_names_tiny_general();

    case DatasetName::small:
        return matrix_names_small_general();

    case DatasetName::medium:
        return matrix_names_medium_general();

    case DatasetName::all:
        return matrix_names_all_general();

    case DatasetName::invalid:
        throw std::logic_error{"Invalid dataset name. Expected ‘tiny’, ‘small’, ‘medium’ or ‘all’."};
    }

    throw std::logic_error{"Unexpected enumeration value."};
}

auto init_matrix_names_symmetric() -> std::vector<std::string_view>
{
    switch (to_dataset_name(master_test_suite().argv[1])) {
    case DatasetName::tiny:
        return matrix_names_tiny_symmetric();

    case DatasetName::small:
        return matrix_names_small_symmetric();

    case DatasetName::medium:
        return matrix_names_medium_symmetric();

    case DatasetName::all:
        return matrix_names_all_symmetric();

    case DatasetName::invalid:
        throw std::logic_error{"Invalid dataset name. Expected ‘tiny’, ‘small’, ‘medium’ or ‘all’."};
    }

    throw std::logic_error{"Unexpected enumeration value."};
}

auto to_dataset_name(std::string_view const dataset_name) -> DatasetName
{
    auto const it = dataset_names.find(dataset_name);
    if (it == dataset_names.end()) {
        return DatasetName::invalid;
    }
    return it->second;
}

auto matrix_names_tiny_general() -> std::vector<std::string_view>
{
    return {
        "b1_ss",
        "LFAT5",
    };
}

auto matrix_names_tiny_symmetric() -> std::vector<std::string_view>
{
    return {
        "LFAT5",
    };
}

auto matrix_names_small_general() -> std::vector<std::string_view>
{
    return {
        "b1_ss",
        "LFAT5",
        "thread",
    };
}

auto matrix_names_small_symmetric() -> std::vector<std::string_view>
{
    return {
        "LFAT5",
        "thread",
    };
}

auto matrix_names_medium_general() -> std::vector<std::string_view>
{
    return {
        "b1_ss",
        "bundle_adj",
        "cage14",
        "CoupCons3D",
        "CurlCurl_4",
        "Freescale1",
        "Freescale2",
        "FullChip",
        "Ga41As41H72",
        "gsm_106857",
        "human_gene1",
        "LFAT5",
        "ML_Laplace",
        "mouse_gene",
        "rajat31",
        "StocF-1465",
        "thread",
        "Transport",
        "TSOPF_RS_b2383",
    };
}

auto matrix_names_medium_symmetric() -> std::vector<std::string_view>
{
    return {
        "bundle_adj",
        "CurlCurl_4",
        "Ga41As41H72",
        "gsm_106857",
        "human_gene1",
        "LFAT5",
        "mouse_gene",
        "StocF-1465",
        "thread",
    };
}

auto matrix_names_all_general() -> std::vector<std::string_view>
{
    return {
        "b1_ss",
        "bone010",
        "boneS10",
        "Bump_2911",
        "bundle_adj",
        "cage14",
        "cage15",
        "circuit5M",
        "CoupCons3D",
        "Cube_Coup_dt0",
        "CurlCurl_4",
        "dgreen",
        "dielFilterV2real",
        "dielFilterV3real",
        "Flan_1565",
        "Freescale1",
        "Freescale2",
        "FullChip",
        "Ga41As41H72",
        "gsm_106857",
        "human_gene1",
        "HV15R",
        "LFAT5",
        "Long_Coup_dt0",
        "ML_Geer",
        "ML_Laplace",
        "mouse_gene",
        "nlpkkt120",
        "nlpkkt160",
        "nlpkkt200",
        "nv2",
        "PFlow_742",
        "Queen_4147",
        "rajat31",
        "RM07R",
        "ss",
        "StocF-1465",
        "stokes",
        "thread",
        "Transport",
        "TSOPF_RS_b2383",
        "vas_stokes_2M",
        "vas_stokes_4M",
    };
}

auto matrix_names_all_symmetric() -> std::vector<std::string_view>
{
    return {
        "bone010",
        "boneS10",
        "Bump_2911",
        "bundle_adj",
        "Cube_Coup_dt0",
        "CurlCurl_4",
        "dielFilterV2real",
        "dielFilterV3real",
        "Flan_1565",
        "Ga41As41H72",
        "gsm_106857",
        "human_gene1",
        "LFAT5",
        "Long_Coup_dt0",
        "mouse_gene",
        "nlpkkt120",
        "nlpkkt160",
        "nlpkkt200",
        "PFlow_742",
        "Queen_4147",
        "StocF-1465",
        "thread",
    };
}

}  // namespace

}  // namespace meta
