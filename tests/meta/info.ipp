#pragma once
#include <meta/info.hpp>

#include <filesystem>           // std::filesystem

#include <meta/symmetric.hpp>   // meta::Symmetric

namespace meta::info {

namespace {

template<>
std::filesystem::path const
matrix_path<float, Symmetric::no> = "matrices-hdf5/general/float32/";

template<>
std::filesystem::path const
matrix_path<float, Symmetric::yes> = "matrices-hdf5/symmetric/float32/";

template<>
std::filesystem::path const
matrix_path<double, Symmetric::no> = "matrices-hdf5/general/float64/";

template<>
std::filesystem::path const
matrix_path<double, Symmetric::yes> = "matrices-hdf5/symmetric/float64/";

template<>
std::filesystem::path const
test_data_path<float> = "test-data/float32/";

template<>
std::filesystem::path const
test_data_path<double> = "test-data/float64/";

}  // namespace

}  // namespace meta::info
