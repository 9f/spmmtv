#pragma once

#include <span>                 // std::span

#include <boost/test/tools/assertion_result.hpp>  // boost::test_tools::predicate_result

namespace meta {

template<typename T>
auto are_equal(std::span<T const> y, std::span<T const> model, T const tolerance)
    -> boost::test_tools::predicate_result;

}  // namespace meta
