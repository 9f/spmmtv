#include <meta/info.hpp>

#include <filesystem>           // std::filesystem

#include <boost/test/unit_test.hpp>  // boost::unit_test

using boost::unit_test::framework::master_test_suite;

namespace meta::info {

std::filesystem::path dataset_path{};

namespace {

class CommandLineInitFixture {
public:
    CommandLineInitFixture();
    auto setup() -> void;
};

CommandLineInitFixture::CommandLineInitFixture()
{
    BOOST_TEST_REQUIRE(master_test_suite().argc == 3, "Usage: tests [OPTIONS] -- DATASET_NAME DATASET_DIR");

    auto const dataset_dir = std::filesystem::path{master_test_suite().argv[2]};
    BOOST_TEST_REQUIRE(std::filesystem::is_directory(dataset_dir), "Error: DATASET_DIR isn’t a directory.");

    auto const perms = std::filesystem::status(dataset_dir).permissions();
    auto const traverse = (perms & std::filesystem::perms::owner_exec) == std::filesystem::perms::owner_exec;
    BOOST_TEST_REQUIRE(traverse, "Error: The user doesn’t have permission to traverse DATASET_DIR.");
}

auto CommandLineInitFixture::setup() -> void
{
    dataset_path = master_test_suite().argv[2];
}

BOOST_TEST_GLOBAL_FIXTURE(CommandLineInitFixture);

}  // namespace

}  // namespace meta::info
