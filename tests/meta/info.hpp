#pragma once

#include <array>                // std::array
#include <filesystem>           // std::filesystem

#include <hsf/region_ordering.hpp>  // hsf::RegionOrdering

namespace meta {

enum struct Symmetric;

}  // namespace meta

namespace meta::info {

extern std::filesystem::path dataset_path;

constexpr auto region_orderings = std::array{
        hsf::RegionOrdering::block_lexicographical,
        hsf::RegionOrdering::lexicographical,
        hsf::RegionOrdering::morton,
    };

namespace {

template<typename T, Symmetric Symm>
std::filesystem::path const matrix_path;

template<typename T>
std::filesystem::path const test_data_path;

}  // namespace

}  // namespace meta::info

#include <meta/info.ipp>
