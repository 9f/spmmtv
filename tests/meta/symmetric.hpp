#pragma once

namespace meta {

// Indicate the symmetry of a matrix.
enum struct Symmetric {
    no,
    yes,
};

}  // namespace meta
