#pragma once

#include <cstddef>              // std::size_t

constexpr auto operator""_uz(unsigned long long const n) noexcept -> std::size_t
{
    return static_cast<std::size_t>(n);
}
