#include <cstdint>              // std::uint32_t
#include <functional>           // std::invoke
#include <stdexcept>            // std::logic_error
#include <string_view>          // std::string_view
#include <tuple>                // std::tuple
#include <unordered_map>        // std::unordered_map
#include <utility>              // std::move
#include <variant>              // std::visit

#include <boost/container_hash/hash.hpp>  // boost::hash
#include <boost/test/unit_test.hpp>  // The Unit Test Framework.
#include <boost/test/data/monomorphic.hpp>  // Dataset-related declarations.
#include <boost/test/data/test_case.hpp>  // Test-case-related declarations.

#include <csr/symmetric.hpp>    // csr::Symmetric

#include <hsf/info.hpp>         // hsf::info
#include <hsf/matrix.hpp>       // hsf::are_regions_sorted hsf::Matrix
#include <hsf/optimise_csr_regions.hpp>  // hsf::OptimiseCsrRegions
#include <hsf/region.hpp>       // hsf::Region
#include <hsf/region_ordering.hpp>  // hsf::RegionOrdering
#include <hsf/region_ordering_ios.hpp>  // hsf::operator<<
#include <hsf/symmetric.hpp>    // hsf::Symmetric

#include <cstddef.hpp>          // operator""_uz
#include <meta/convert_symmetry.hpp>  // meta::convert_symmetry
#include <meta/info.hpp>        // meta::info
// meta::load_hsf_matrix_parallel meta::load_hsf_matrix_parallel_both meta::load_hsf_matrix_sequential
// meta::load_hsf_matrix_sequential_both
#include <meta/load_hsf_matrix.hpp>
#include <meta/matrix_dataset.hpp>  // meta::MatrixDataset
#include <meta/symmetric.hpp>   // meta::Symmetric

using boost::unit_test::data::make;
using boost::unit_test::data::make_delayed;
using meta::info::region_orderings;

namespace {

auto const general_matrices = make_delayed<meta::MatrixDataset>(meta::Symmetric::no);
auto const symmetric_matrices = make_delayed<meta::MatrixDataset>(meta::Symmetric::yes);

// A pair of row and column coordinates.
using CoordPair = std::tuple<std::uint32_t, std::uint32_t>;

using MatrixMap = std::unordered_map<CoordPair, float, boost::hash<CoordPair>>;

enum struct Mode {
    sequential,
    parallel,
};

template<typename T, hsf::Symmetric Symm>
class SortFixture {
public:
    hsf::Matrix<T, Symm> matrix{};
};

template<typename T, hsf::Symmetric Symm>
class MatchFixture {
public:
    hsf::Matrix<T, Symm> matrix{};
    MatrixMap matrix_map{};
};

template<typename T>
class CheckRegion {
public:
    CheckRegion(hsf::Region<T> const& region, MatrixMap& matrix_map) noexcept;

    auto operator()(hsf::info::RowIndex* row_indices) -> void;
    auto operator()(hsf::info::RowPointer* row_pointers) -> void;

private:
    hsf::Region<T> const& region_;
    MatrixMap& matrix_map_;
};

template<typename T, hsf::Symmetric Symm>
auto initialise(
        std::string_view matrix_name, hsf::RegionOrdering region_ordering, Mode mode, SortFixture<T, Symm>& f
    ) -> void;

template<typename T, hsf::Symmetric Symm>
auto initialise(std::string_view matrix_name, Mode mode, MatchFixture<T, Symm>& f) -> void;

}  // namespace

BOOST_AUTO_TEST_SUITE(matrix_integrity)
BOOST_AUTO_TEST_SUITE(HSF)
BOOST_AUTO_TEST_SUITE(general)

using GeneralRegionsSortedSequentialFixture = SortFixture<float, hsf::Symmetric::no>;
BOOST_DATA_TEST_CASE_F(
        GeneralRegionsSortedSequentialFixture,
        regions_sorted_sequential,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, Mode::sequential, *this);
    BOOST_TEST_CHECK(hsf::are_regions_sorted(this->matrix));
}

using GeneralRegionsSortedParallelFixture = SortFixture<float, hsf::Symmetric::no>;
BOOST_DATA_TEST_CASE_F(
        GeneralRegionsSortedParallelFixture,
        regions_sorted_parallel,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, Mode::parallel, *this);
    BOOST_TEST_CHECK(hsf::are_regions_sorted(this->matrix));
}

using GeneralMatrixMatchesSequentialFixture = MatchFixture<float, hsf::Symmetric::no>;
BOOST_DATA_TEST_CASE_F(
        GeneralMatrixMatchesSequentialFixture,
        matrix_matches_sequential,
        general_matrices,
        matrix_name
    )
{
    initialise(matrix_name, Mode::sequential, *this);
    BOOST_TEST_CHECK(this->matrix.element_count() == this->matrix_map.size());
    for (auto const& region : this->matrix.regions()) {
        std::visit(CheckRegion{region, this->matrix_map}, region.rows);
    }
    BOOST_TEST_CHECK(this->matrix_map.empty());
}

using GeneralMatrixMatchesParallelFixture = MatchFixture<float, hsf::Symmetric::no>;
BOOST_DATA_TEST_CASE_F(
        GeneralMatrixMatchesParallelFixture,
        matrix_matches_parallel,
        general_matrices,
        matrix_name
    )
{
    initialise(matrix_name, Mode::parallel, *this);
    BOOST_TEST_CHECK(this->matrix.element_count() == this->matrix_map.size());
    for (auto const& region : this->matrix.regions()) {
        std::visit(CheckRegion{region, this->matrix_map}, region.rows);
    }
    BOOST_TEST_CHECK(this->matrix_map.empty());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(symmetric)

using SymmetricRegionsSortedSequentialFixture = SortFixture<float, hsf::Symmetric::yes>;
BOOST_DATA_TEST_CASE_F(
        SymmetricRegionsSortedSequentialFixture,
        regions_sorted_sequential,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, Mode::sequential, *this);
    BOOST_TEST_CHECK(hsf::are_regions_sorted(this->matrix));
}

using SymmetricRegionsSortedParallelFixture = SortFixture<float, hsf::Symmetric::yes>;
BOOST_DATA_TEST_CASE_F(
        SymmetricRegionsSortedParallelFixture,
        regions_sorted_parallel,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, Mode::parallel, *this);
    BOOST_TEST_CHECK(hsf::are_regions_sorted(this->matrix));
}

using SymmetricMatrixMatchesSequentialFixture = MatchFixture<float, hsf::Symmetric::yes>;
BOOST_DATA_TEST_CASE_F(
        SymmetricMatrixMatchesSequentialFixture,
        matrix_matches_sequential,
        symmetric_matrices,
        matrix_name
    )
{
    initialise(matrix_name, Mode::sequential, *this);
    BOOST_TEST_CHECK(this->matrix.element_count() == this->matrix_map.size());
    for (auto const& region : this->matrix.regions()) {
        std::visit(CheckRegion{region, this->matrix_map}, region.rows);
    }
    BOOST_TEST_CHECK(this->matrix_map.empty());
}

using SymmetricMatrixMatchesParallelFixture = MatchFixture<float, hsf::Symmetric::yes>;
BOOST_DATA_TEST_CASE_F(
        SymmetricMatrixMatchesParallelFixture,
        matrix_matches_parallel,
        symmetric_matrices,
        matrix_name
    )
{
    initialise(matrix_name, Mode::parallel, *this);
    BOOST_TEST_CHECK(this->matrix.element_count() == this->matrix_map.size());
    for (auto const& region : this->matrix.regions()) {
        std::visit(CheckRegion{region, this->matrix_map}, region.rows);
    }
    BOOST_TEST_CHECK(this->matrix_map.empty());
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()

namespace {

template<typename T>
CheckRegion<T>::CheckRegion(hsf::Region<T> const& region, MatrixMap& matrix_map) noexcept
    : region_{region},
      matrix_map_{matrix_map}
{
}

template<typename T>
auto CheckRegion<T>::operator()(hsf::info::RowIndex* const row_indices) -> void
{
    assert(row_indices != nullptr);

    auto const row_offset = this->region_.row_begin;
    auto const column_offset = this->region_.column_begin;

    for (auto i = 0_uz; i < this->region_.element_count; ++i) {
        auto const row = row_indices[i] + row_offset;
        auto const column = this->region_.columns[i] + column_offset;
        auto const element = this->region_.elements[i];

        auto const it = this->matrix_map_.find(CoordPair{row, column});
        auto const is_element_present = it != this->matrix_map_.cend();

        if (is_element_present) {
            BOOST_TEST_CHECK(element == it->second);
            this->matrix_map_.erase(it);
        } else {
            BOOST_TEST_CHECK(false, "missing element: (" << row << ", " << column << ')');
        }
    }
}

template<typename T>
auto CheckRegion<T>::operator()(hsf::info::RowPointer* const row_pointers) -> void
{
    assert(row_pointers != nullptr);

    auto const row_count = this->region_.row_count;
    auto const row_offset = this->region_.row_begin;
    auto const column_offset = this->region_.column_begin;

    for (auto region_row = 0_uz; region_row < row_count; ++region_row) {
        auto const row = region_row + row_offset;
        auto const row_begin = row_pointers[region_row];
        auto const row_end = row_pointers[region_row + 1];

        for (auto i = row_begin; i < row_end; ++i) {
            auto const column = this->region_.columns[i] + column_offset;
            auto const element = this->region_.elements[i];

            auto const it = this->matrix_map_.find(CoordPair{row, column});
            auto const is_element_present = it != this->matrix_map_.cend();

            if (is_element_present) {
                BOOST_TEST_CHECK(element == it->second);
                this->matrix_map_.erase(it);
            } else {
                BOOST_TEST_CHECK(false, "missing element: (" << row << ", " << column << ')');
            }
        }
    }
}

template<typename T, hsf::Symmetric Symm>
auto initialise(
        std::string_view const matrix_name,
        hsf::RegionOrdering const region_ordering,
        Mode const mode,
        SortFixture<T, Symm>& f
    ) -> void
{
    auto const optimise_csr_regions = hsf::OptimiseCsrRegions::no;
    switch (mode) {
    case Mode::sequential:
        f.matrix = meta::load_hsf_matrix_sequential<T, Symm>(
                matrix_name, region_ordering, optimise_csr_regions
            );
        break;
    case Mode::parallel:
        f.matrix = meta::load_hsf_matrix_parallel<T, Symm>(
                matrix_name, region_ordering, optimise_csr_regions
            );
        break;
    }
}

template<typename T, hsf::Symmetric Symm>
auto initialise(std::string_view const matrix_name, Mode const mode, MatchFixture<T, Symm>& f) -> void
{
    constexpr auto csr_symmetry = meta::convert_symmetry<csr::Symmetric>(Symm);
    auto const ro = hsf::RegionOrdering::block_lexicographical;
    auto [hsf_matrix, csr_matrix] = std::invoke([&] {
        switch (mode) {
        case Mode::sequential:
            return meta::load_hsf_matrix_sequential_both<T, Symm, csr_symmetry>(matrix_name, ro);
        case Mode::parallel:
            return meta::load_hsf_matrix_parallel_both<T, Symm, csr_symmetry>(matrix_name, ro);
        }
        throw std::logic_error{"Unexpected enumeration value."};
    });

    auto matrix_map = MatrixMap{};
    matrix_map.reserve(csr_matrix.element_count());

    for (auto row = 0_uz; row < csr_matrix.row_count(); ++row) {
        auto const row_begin = csr_matrix.row_pointers[row];
        auto const row_end = csr_matrix.row_pointers[row + 1];

        for (auto i = row_begin; i < row_end; ++i) {
            auto const column = csr_matrix.columns[i];
            auto const element = csr_matrix.elements[i];
            matrix_map.emplace(std::make_pair(row, column), element);
        }
    }

    f.matrix = std::move(hsf_matrix);
    f.matrix_map = std::move(matrix_map);
}

}  // namespace
