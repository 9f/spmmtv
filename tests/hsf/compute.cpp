#include <algorithm>            // std::fill
#include <string_view>          // std::string_view
#include <utility>              // std::move
#include <vector>               // std::vector

#include <boost/test/unit_test.hpp>  // The Unit Test Framework.
#include <boost/test/data/monomorphic.hpp>  // Dataset-related declarations.
#include <boost/test/data/test_case.hpp>  // Test-case-related declarations.

#include <omp.h>                // omp_get_max_threads

#include <hsf/compute.hpp>      // hsf::compute
#include <hsf/exec_data.hpp>    // hsf::ExecData
#include <hsf/exec_mode.hpp>    // hsf::ExecMode
#include <hsf/matrix.hpp>       // hsf::Matrix
#include <hsf/region_ordering.hpp>  // hsf::RegionOrdering
#include <hsf/region_ordering_ios.hpp>  // hsf::operator<<
#include <hsf/symmetric.hpp>    // hsf::Symmetric
#include <hsf/tags.hpp>         // hsf::tags

#include <meta/are_equal.hpp>   // meta::are_equal
#include <meta/info.hpp>        // meta::info
#include <meta/load_hsf_matrix.hpp>  // meta::load_hsf_matrix_sequential meta::load_hsf_matrix_parallel
#include <meta/matrix_dataset.hpp>  // meta::MatrixDataset
#include <meta/symmetric.hpp>   // meta::Symmetric
#include <meta/test_data.hpp>   // meta::TestData

using boost::unit_test::data::make;
using boost::unit_test::data::make_delayed;
using meta::info::region_orderings;

namespace {

auto const general_matrices = make_delayed<meta::MatrixDataset>(meta::Symmetric::no);
auto const symmetric_matrices = make_delayed<meta::MatrixDataset>(meta::Symmetric::yes);

template<typename T, hsf::Symmetric Symm>
class SeqFixture {
public:
    hsf::Matrix<T, Symm> matrix{};
    std::vector<T> x1{};
    std::vector<T> x2{};
    std::vector<T> y1{};
    std::vector<T> y2{};
    std::vector<T> y1_model{};
    std::vector<T> y2_model{};
    T tolerance{};
};

using GeneralSequentialFixture = SeqFixture<double, hsf::Symmetric::no>;
using SymmetricSequentialFixture = SeqFixture<double, hsf::Symmetric::yes>;

template<hsf::ExecMode EM, typename T, hsf::Symmetric Symm>
class ParFixture {
public:
    hsf::Matrix<T, Symm> matrix{};
    hsf::ExecData<EM, T, Symm> exec_data{};
    std::vector<T> x1{};
    std::vector<T> x2{};
    std::vector<T> y1{};
    std::vector<T> y2{};
    std::vector<T> y1_model{};
    std::vector<T> y2_model{};
    T tolerance{};
};

using GeneralAtomicExclusiveFixture = ParFixture<hsf::ExecMode::atomic_exclusive, double, hsf::Symmetric::no>;
using GeneralAtomicPlannedSharedFixture = ParFixture<hsf::ExecMode::atomic_planned_shared, double, hsf::Symmetric::no>;
using GeneralAtomicSharedFixture = ParFixture<hsf::ExecMode::atomic_shared, double, hsf::Symmetric::no>;
using GeneralLocksExclusiveFixture = ParFixture<hsf::ExecMode::locks_exclusive, double, hsf::Symmetric::no>;
using GeneralLocksPlannedExclusiveFixture = ParFixture<hsf::ExecMode::locks_planned_exclusive, double, hsf::Symmetric::no>;
using GeneralLocksPlannedSharedFixture = ParFixture<hsf::ExecMode::locks_planned_shared, double, hsf::Symmetric::no>;
using GeneralLocksSharedFixture = ParFixture<hsf::ExecMode::locks_shared, double, hsf::Symmetric::no>;
using GeneralPerThreadExclusiveFixture = ParFixture<hsf::ExecMode::per_thread_exclusive, double, hsf::Symmetric::no>;
using GeneralPerThreadPlannedSharedFixture = ParFixture<hsf::ExecMode::per_thread_planned_shared, double, hsf::Symmetric::no>;
using GeneralPerThreadSharedFixture = ParFixture<hsf::ExecMode::per_thread_shared, double, hsf::Symmetric::no>;

using SymmetricAtomicExclusiveFixture = ParFixture<hsf::ExecMode::atomic_exclusive, double, hsf::Symmetric::yes>;
using SymmetricAtomicPlannedSharedFixture = ParFixture<hsf::ExecMode::atomic_planned_shared, double, hsf::Symmetric::yes>;
using SymmetricAtomicSharedFixture = ParFixture<hsf::ExecMode::atomic_shared, double, hsf::Symmetric::yes>;
using SymmetricLocksExclusiveFixture = ParFixture<hsf::ExecMode::locks_exclusive, double, hsf::Symmetric::yes>;
using SymmetricLocksPlannedExclusiveFixture = ParFixture<hsf::ExecMode::locks_planned_exclusive, double, hsf::Symmetric::yes>;
using SymmetricLocksPlannedSharedFixture = ParFixture<hsf::ExecMode::locks_planned_shared, double, hsf::Symmetric::yes>;
using SymmetricLocksSharedFixture = ParFixture<hsf::ExecMode::locks_shared, double, hsf::Symmetric::yes>;
using SymmetricPerThreadExclusiveFixture = ParFixture<hsf::ExecMode::per_thread_exclusive, double, hsf::Symmetric::yes>;
using SymmetricPerThreadPlannedSharedFixture = ParFixture<hsf::ExecMode::per_thread_planned_shared, double, hsf::Symmetric::yes>;
using SymmetricPerThreadSharedFixture = ParFixture<hsf::ExecMode::per_thread_shared, double, hsf::Symmetric::yes>;

template<hsf::ExecMode EM, typename T, hsf::Symmetric Symm>
auto init_exec_data(hsf::Matrix<T, Symm> const& matrix, hsf::tags::OneVector) -> hsf::ExecData<EM, T, Symm>;

template<hsf::ExecMode EM, typename T, hsf::Symmetric Symm>
auto init_exec_data(hsf::Matrix<T, Symm> const& matrix, hsf::tags::TwoVectors) -> hsf::ExecData<EM, T, Symm>;

template<typename T, hsf::Symmetric Symm>
auto initialise(
        std::string_view matrix_name,
        hsf::RegionOrdering region_ordering,
        hsf::tags::OneVector,
        SeqFixture<T, Symm>& f
    ) -> void;

template<hsf::ExecMode EM, typename T, hsf::Symmetric Symm>
auto initialise(
        std::string_view matrix_name,
        hsf::RegionOrdering region_ordering,
        hsf::tags::OneVector,
        ParFixture<EM, T, Symm>& f
    ) -> void;

template<typename T, hsf::Symmetric Symm>
auto initialise(
        std::string_view matrix_name,
        hsf::RegionOrdering region_ordering,
        hsf::tags::TwoVectors,
        SeqFixture<T, Symm>& f
    ) -> void;

template<hsf::ExecMode EM, typename T, hsf::Symmetric Symm>
auto initialise(
        std::string_view matrix_name,
        hsf::RegionOrdering region_ordering,
        hsf::tags::TwoVectors,
        ParFixture<EM, T, Symm>& f
    ) -> void;

}  // namespace

BOOST_AUTO_TEST_SUITE(SpMMTV)
BOOST_AUTO_TEST_SUITE(HSF)
BOOST_AUTO_TEST_SUITE(general_one_vec)

BOOST_DATA_TEST_CASE_F(
        GeneralSequentialFixture,
        sequential,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_sequential<double>(this->matrix, this->x1, this->y1, this->y2);
    BOOST_TEST_INFO("first output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
    BOOST_TEST_INFO("second output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
}

BOOST_DATA_TEST_CASE_F(
        GeneralAtomicExclusiveFixture,
        atomic_exclusive,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        GeneralAtomicPlannedSharedFixture,
        atomic_planned_shared,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        GeneralAtomicSharedFixture,
        atomic_shared,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        GeneralLocksExclusiveFixture,
        locks_exclusive,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        GeneralLocksPlannedExclusiveFixture,
        locks_planned_exclusive,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        GeneralLocksPlannedSharedFixture,
        locks_planned_shared,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        GeneralLocksSharedFixture,
        locks_shared,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        GeneralPerThreadExclusiveFixture,
        per_thread_exclusive,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        GeneralPerThreadPlannedSharedFixture,
        per_thread_planned_shared,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        GeneralPerThreadSharedFixture,
        per_thread_shared,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(general_two_vecs)

BOOST_DATA_TEST_CASE_F(
        GeneralSequentialFixture,
        sequential,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::two_vectors, *this);
    hsf::compute::spmmtv_sequential<double>(this->matrix, this->x1, this->x2, this->y1, this->y2);
    BOOST_TEST_INFO("first output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
    BOOST_TEST_INFO("second output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
}

BOOST_DATA_TEST_CASE_F(
        GeneralLocksExclusiveFixture,
        locks_exclusive,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::two_vectors, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        GeneralLocksPlannedExclusiveFixture,
        locks_planned_exclusive,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::two_vectors, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        GeneralLocksPlannedSharedFixture,
        locks_planned_shared,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::two_vectors, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        GeneralLocksSharedFixture,
        locks_shared,
        general_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::two_vectors, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(symmetric_one_vec)

BOOST_DATA_TEST_CASE_F(
        SymmetricSequentialFixture,
        sequential,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_sequential<double>(this->matrix, this->x1, this->y1, this->y2);
    BOOST_TEST_INFO("first output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
    BOOST_TEST_INFO("second output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
}

BOOST_DATA_TEST_CASE_F(
        SymmetricAtomicExclusiveFixture,
        atomic_exclusive,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        SymmetricAtomicPlannedSharedFixture,
        atomic_planned_shared,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        SymmetricAtomicSharedFixture,
        atomic_shared,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        SymmetricLocksExclusiveFixture,
        locks_exclusive,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        SymmetricLocksPlannedExclusiveFixture,
        locks_planned_exclusive,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        SymmetricLocksPlannedSharedFixture,
        locks_planned_shared,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        SymmetricLocksSharedFixture,
        locks_shared,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        SymmetricPerThreadExclusiveFixture,
        per_thread_exclusive,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        SymmetricPerThreadPlannedSharedFixture,
        per_thread_planned_shared,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        SymmetricPerThreadSharedFixture,
        per_thread_shared,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::one_vector, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(symmetric_two_vecs)

BOOST_DATA_TEST_CASE_F(
        SymmetricSequentialFixture,
        sequential,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::two_vectors, *this);
    hsf::compute::spmmtv_sequential<double>(this->matrix, this->x1, this->x2, this->y1, this->y2);
    BOOST_TEST_INFO("first output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
    BOOST_TEST_INFO("second output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
}

BOOST_DATA_TEST_CASE_F(
        SymmetricLocksExclusiveFixture,
        locks_exclusive,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::two_vectors, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        SymmetricLocksPlannedExclusiveFixture,
        locks_planned_exclusive,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::two_vectors, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        SymmetricLocksPlannedSharedFixture,
        locks_planned_shared,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::two_vectors, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_DATA_TEST_CASE_F(
        SymmetricLocksSharedFixture,
        locks_shared,
        symmetric_matrices * make(region_orderings),
        matrix_name,
        region_ordering
    )
{
    initialise(matrix_name, region_ordering, hsf::tags::two_vectors, *this);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("first SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }

    std::fill(this->y1.begin(), this->y1.end(), 0);
    std::fill(this->y2.begin(), this->y2.end(), 0);
    hsf::compute::spmmtv_parallel<double>(this->matrix, this->x1, this->x2, this->y1, this->y2, this->exec_data);
    BOOST_TEST_CONTEXT("second SpMMᵀV operation")
    {
        BOOST_TEST_INFO("first output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
        BOOST_TEST_INFO("second output array");
        BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
    }
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()

namespace {

template<hsf::ExecMode EM, typename T, hsf::Symmetric Symm>
auto init_exec_data(hsf::Matrix<T, Symm> const& matrix, hsf::tags::OneVector) -> hsf::ExecData<EM, T, Symm>
{
    auto const thread_count = omp_get_max_threads();
    return {thread_count, matrix, hsf::tags::one_vector};
}

template<hsf::ExecMode EM, typename T, hsf::Symmetric Symm>
auto init_exec_data(hsf::Matrix<T, Symm> const& matrix, hsf::tags::TwoVectors) -> hsf::ExecData<EM, T, Symm>
{
    auto const thread_count = omp_get_max_threads();
    return {thread_count, matrix, hsf::tags::two_vectors};
}

template<typename T, hsf::Symmetric Symm>
auto initialise(
        std::string_view const matrix_name,
        hsf::RegionOrdering const region_ordering,
        hsf::tags::OneVector,
        SeqFixture<T, Symm>& f
    ) -> void
{
    f.matrix = meta::load_hsf_matrix_sequential<T, Symm>(matrix_name, region_ordering);
    f.x1 = std::vector<T>(f.matrix.column_count(), static_cast<T>(3.14));

    auto test_data = meta::TestData<T>::load_from_hdf5_one_vec(matrix_name);
    f.y1_model = std::move(test_data.y1);
    f.y2_model = std::move(test_data.y2);
    f.tolerance = test_data.mean_nonzero_element_magnitude * 0.01;

    BOOST_TEST_REQUIRE(f.matrix.column_count() == f.matrix.row_count());
    BOOST_TEST_REQUIRE(f.matrix.column_count() == f.y1_model.size());
    BOOST_TEST_REQUIRE(f.matrix.column_count() == f.y2_model.size());

    f.y1 = std::vector<T>(f.y1_model.size());
    f.y2 = std::vector<T>(f.y2_model.size());
}

template<hsf::ExecMode EM, typename T, hsf::Symmetric Symm>
auto initialise(
        std::string_view const matrix_name,
        hsf::RegionOrdering const region_ordering,
        hsf::tags::OneVector,
        ParFixture<EM, T, Symm>& f
    ) -> void
{
    f.matrix = meta::load_hsf_matrix_parallel<T, Symm>(matrix_name, region_ordering);
    f.exec_data = init_exec_data<EM, T>(f.matrix, hsf::tags::one_vector);
    f.x1 = std::vector<T>(f.matrix.column_count(), static_cast<T>(3.14));

    auto test_data = meta::TestData<T>::load_from_hdf5_one_vec(matrix_name);
    f.y1_model = std::move(test_data.y1);
    f.y2_model = std::move(test_data.y2);
    f.tolerance = test_data.mean_nonzero_element_magnitude * 0.01;

    BOOST_TEST_REQUIRE(f.matrix.column_count() == f.matrix.row_count());
    BOOST_TEST_REQUIRE(f.matrix.column_count() == f.y1_model.size());
    BOOST_TEST_REQUIRE(f.matrix.column_count() == f.y2_model.size());

    f.y1 = std::vector<T>(f.y1_model.size());
    f.y2 = std::vector<T>(f.y2_model.size());
}

template<typename T, hsf::Symmetric Symm>
auto initialise(
        std::string_view const matrix_name,
        hsf::RegionOrdering const region_ordering,
        hsf::tags::TwoVectors,
        SeqFixture<T, Symm>& f
    ) -> void
{
    f.matrix = meta::load_hsf_matrix_sequential<T, Symm>(matrix_name, region_ordering);
    f.x1 = std::vector<T>(f.matrix.column_count(), static_cast<T>(3.14));
    f.x2 = std::vector<T>(f.matrix.row_count(), static_cast<T>(-2.72));

    auto test_data = meta::TestData<T>::load_from_hdf5_two_vecs(matrix_name);
    f.y1_model = std::move(test_data.y1);
    f.y2_model = std::move(test_data.y2);
    f.tolerance = test_data.mean_nonzero_element_magnitude * 0.01;

    if constexpr (Symm == hsf::Symmetric::yes) {
        BOOST_TEST_REQUIRE(f.matrix.column_count() == f.matrix.row_count());
    }
    BOOST_TEST_REQUIRE(f.matrix.row_count() == f.y1_model.size());
    BOOST_TEST_REQUIRE(f.matrix.column_count() == f.y2_model.size());

    f.y1 = std::vector<T>(f.y1_model.size());
    f.y2 = std::vector<T>(f.y2_model.size());
}

template<hsf::ExecMode EM, typename T, hsf::Symmetric Symm>
auto initialise(
        std::string_view const matrix_name,
        hsf::RegionOrdering const region_ordering,
        hsf::tags::TwoVectors,
        ParFixture<EM, T, Symm>& f
    ) -> void
{
    f.matrix = meta::load_hsf_matrix_parallel<T, Symm>(matrix_name, region_ordering);
    f.exec_data = init_exec_data<EM, T>(f.matrix, hsf::tags::two_vectors);
    f.x1 = std::vector<T>(f.matrix.column_count(), static_cast<T>(3.14));
    f.x2 = std::vector<T>(f.matrix.row_count(), static_cast<T>(-2.72));

    auto test_data = meta::TestData<T>::load_from_hdf5_two_vecs(matrix_name);
    f.y1_model = std::move(test_data.y1);
    f.y2_model = std::move(test_data.y2);
    f.tolerance = test_data.mean_nonzero_element_magnitude * 0.01;

    if constexpr (Symm == hsf::Symmetric::yes) {
        BOOST_TEST_REQUIRE(f.matrix.column_count() == f.matrix.row_count());
    }
    BOOST_TEST_REQUIRE(f.matrix.row_count() == f.y1_model.size());
    BOOST_TEST_REQUIRE(f.matrix.column_count() == f.y2_model.size());

    f.y1 = std::vector<T>(f.y1_model.size());
    f.y2 = std::vector<T>(f.y2_model.size());
}

}  // namespace
