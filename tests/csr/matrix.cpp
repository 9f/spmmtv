#include <string_view>          // std::string_view

#include <boost/test/unit_test.hpp>  // The Unit Test Framework.
#include <boost/test/data/monomorphic.hpp>  // Dataset-related declarations.
#include <boost/test/data/test_case.hpp>  // Test-case-related declarations.

#include <csr/matrix.hpp>       // csr::Matrix
#include <csr/symmetric.hpp>    // csr::Symmetric

#include <meta/load_csr_matrix.hpp>  // meta::load_csr_matrix
#include <meta/matrix_dataset.hpp>  // meta::MatrixDataset
#include <meta/symmetric.hpp>   // meta::Symmetric

using boost::unit_test::data::make_delayed;

namespace {

auto const general_matrices = make_delayed<meta::MatrixDataset>(meta::Symmetric::no);
auto const symmetric_matrices = make_delayed<meta::MatrixDataset>(meta::Symmetric::yes);

template<typename T, csr::Symmetric Symm>
class Fixture {
public:
    csr::Matrix<T, Symm> matrix{};
};

template<typename T, csr::Symmetric Symm>
auto initialise(std::string_view matrix_name, Fixture<T, Symm>& f) -> void;

}  // namespace

BOOST_AUTO_TEST_SUITE(matrix_integrity)
BOOST_AUTO_TEST_SUITE(CSR)

using GeneralFixture = Fixture<float, csr::Symmetric::no>;
BOOST_DATA_TEST_CASE_F(GeneralFixture, general, general_matrices, matrix_name)
{
    initialise(matrix_name, *this);
    BOOST_TEST_CHECK(this->matrix.is_valid());
}

using SymmetricFixture = Fixture<float, csr::Symmetric::yes>;
BOOST_DATA_TEST_CASE_F(SymmetricFixture, symmetric, symmetric_matrices, matrix_name)
{
    initialise(matrix_name, *this);
    BOOST_TEST_CHECK(this->matrix.is_valid());
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()

namespace {

template<typename T, csr::Symmetric Symm>
auto initialise(std::string_view const matrix_name, Fixture<T, Symm>& f) -> void
{
    f.matrix = meta::load_csr_matrix<T, Symm>(matrix_name);
}

}  // namespace
