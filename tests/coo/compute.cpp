#include <string_view>          // std::string_view
#include <utility>              // std::move
#include <vector>               // std::vector

#include <boost/test/unit_test.hpp>  // The Unit Test Framework.
#include <boost/test/data/monomorphic.hpp>  // Dataset-related declarations.
#include <boost/test/data/test_case.hpp>  // Test-case-related declarations.

#include <coo/compute.hpp>      // coo::compute
#include <coo/matrix.hpp>       // coo::Matrix
#include <coo/symmetric.hpp>    // coo::Symmetric

#include <meta/are_equal.hpp>   // meta::are_equal
#include <meta/load_coo_matrix.hpp>  // meta::load_coo_matrix
#include <meta/matrix_dataset.hpp>  // meta::MatrixDataset
#include <meta/symmetric.hpp>   // meta::Symmetric
#include <meta/test_data.hpp>   // meta::TestData

using boost::unit_test::data::make_delayed;

namespace {

auto const general_matrices = make_delayed<meta::MatrixDataset>(meta::Symmetric::no);
auto const symmetric_matrices = make_delayed<meta::MatrixDataset>(meta::Symmetric::yes);

template<typename T, coo::Symmetric Symm>
class Fixture {
public:
    coo::Matrix<T, Symm> matrix{};
    std::vector<T> x1{};
    std::vector<T> x2{};
    std::vector<T> y1{};
    std::vector<T> y2{};
    std::vector<T> y1_model{};
    std::vector<T> y2_model{};
    T tolerance{};
};

using GeneralFixture = Fixture<double, coo::Symmetric::no>;
using SymmetricFixture = Fixture<double, coo::Symmetric::yes>;

template<typename T, coo::Symmetric Symm>
auto initialise_one_vec(std::string_view matrix_name, Fixture<T, Symm>& f) -> void;

template<typename T, coo::Symmetric Symm>
auto initialise_two_vecs(std::string_view matrix_name, Fixture<T, Symm>& f) -> void;

}  // namespace

BOOST_AUTO_TEST_SUITE(SpMMTV)
BOOST_AUTO_TEST_SUITE(COO)

BOOST_DATA_TEST_CASE_F(GeneralFixture, general_one_vec, general_matrices, matrix_name)
{
    initialise_one_vec(matrix_name, *this);
    coo::compute::spmmtv_sequential<double>(this->matrix, this->x1, this->y1, this->y2);
    BOOST_TEST_INFO("first output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
    BOOST_TEST_INFO("second output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
}

BOOST_DATA_TEST_CASE_F(GeneralFixture, general_two_vecs, general_matrices, matrix_name)
{
    initialise_two_vecs(matrix_name, *this);
    coo::compute::spmmtv_sequential<double>(this->matrix, this->x1, this->x2, this->y1, this->y2);
    BOOST_TEST_INFO("first output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
    BOOST_TEST_INFO("second output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
}

BOOST_DATA_TEST_CASE_F(SymmetricFixture, symmetric_one_vec, symmetric_matrices, matrix_name)
{
    initialise_one_vec(matrix_name, *this);
    coo::compute::spmmtv_sequential<double>(this->matrix, this->x1, this->y1, this->y2);
    BOOST_TEST_INFO("first output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
    BOOST_TEST_INFO("second output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
}

BOOST_DATA_TEST_CASE_F(SymmetricFixture, symmetric_two_vecs, symmetric_matrices, matrix_name)
{
    initialise_two_vecs(matrix_name, *this);
    coo::compute::spmmtv_sequential<double>(this->matrix, this->x1, this->x2, this->y1, this->y2);
    BOOST_TEST_INFO("first output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y1, this->y1_model, this->tolerance));
    BOOST_TEST_INFO("second output array");
    BOOST_TEST_CHECK(meta::are_equal<double>(this->y2, this->y2_model, this->tolerance));
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()

namespace {

template<typename T, coo::Symmetric Symm>
auto initialise_one_vec(std::string_view const matrix_name, Fixture<T, Symm>& f) -> void
{
    f.matrix = meta::load_coo_matrix<T, Symm>(matrix_name);
    f.x1 = std::vector<T>(f.matrix.column_count(), static_cast<T>(3.14));

    auto test_data = meta::TestData<T>::load_from_hdf5_one_vec(matrix_name);
    f.y1_model = std::move(test_data.y1);
    f.y2_model = std::move(test_data.y2);
    f.tolerance = test_data.mean_nonzero_element_magnitude * 0.01;

    BOOST_TEST_REQUIRE(f.matrix.column_count() == f.matrix.row_count());
    BOOST_TEST_REQUIRE(f.matrix.column_count() == f.y1_model.size());
    BOOST_TEST_REQUIRE(f.matrix.column_count() == f.y2_model.size());

    f.y1 = std::vector<T>(f.y1_model.size());
    f.y2 = std::vector<T>(f.y2_model.size());
}

template<typename T, coo::Symmetric Symm>
auto initialise_two_vecs(std::string_view const matrix_name, Fixture<T, Symm>& f) -> void
{
    f.matrix = meta::load_coo_matrix<T, Symm>(matrix_name);
    f.x1 = std::vector<T>(f.matrix.column_count(), static_cast<T>(3.14));
    f.x2 = std::vector<T>(f.matrix.row_count(), static_cast<T>(-2.72));

    auto test_data = meta::TestData<T>::load_from_hdf5_two_vecs(matrix_name);
    f.y1_model = std::move(test_data.y1);
    f.y2_model = std::move(test_data.y2);
    f.tolerance = test_data.mean_nonzero_element_magnitude * 0.01;

    if constexpr (Symm == coo::Symmetric::yes) {
        BOOST_TEST_REQUIRE(f.matrix.column_count() == f.matrix.row_count());
    }
    BOOST_TEST_REQUIRE(f.matrix.row_count() == f.y1_model.size());
    BOOST_TEST_REQUIRE(f.matrix.column_count() == f.y2_model.size());

    f.y1 = std::vector<T>(f.y1_model.size());
    f.y2 = std::vector<T>(f.y2_model.size());
}

}  // namespace
