#include <string_view>          // std::string_view

#include <boost/test/unit_test.hpp>  // The Unit Test Framework.
#include <boost/test/data/monomorphic.hpp>  // Dataset-related declarations.
#include <boost/test/data/test_case.hpp>  // Test-case-related declarations.

#include <coo/matrix.hpp>       // coo::Matrix
#include <coo/symmetric.hpp>    // coo::Symmetric

#include <meta/load_coo_matrix.hpp>  // meta::load_coo_matrix
#include <meta/matrix_dataset.hpp>  // meta::MatrixDataset
#include <meta/symmetric.hpp>   // meta::Symmetric

using boost::unit_test::data::make_delayed;

namespace {

auto const general_matrices = make_delayed<meta::MatrixDataset>(meta::Symmetric::no);
auto const symmetric_matrices = make_delayed<meta::MatrixDataset>(meta::Symmetric::yes);

template<typename T, coo::Symmetric Symm>
class Fixture {
public:
    coo::Matrix<T, Symm> matrix{};
};

template<typename T, coo::Symmetric Symm>
auto initialise(std::string_view matrix_name, Fixture<T, Symm>& f) -> void;

}  // namespace

BOOST_AUTO_TEST_SUITE(matrix_integrity)
BOOST_AUTO_TEST_SUITE(COO)
BOOST_AUTO_TEST_SUITE(general)

using GeneralIsRowMajorOrderedFixture = Fixture<float, coo::Symmetric::no>;
BOOST_DATA_TEST_CASE_F(
        GeneralIsRowMajorOrderedFixture,
        is_row_major_ordered,
        general_matrices,
        matrix_name
    )
{
    initialise(matrix_name, *this);
    BOOST_TEST_CHECK(this->matrix.is_row_major_ordered());
}

using GeneralIsValidFixture = Fixture<float, coo::Symmetric::no>;
BOOST_DATA_TEST_CASE_F(
        GeneralIsValidFixture,
        is_valid,
        general_matrices,
        matrix_name
    )
{
    initialise(matrix_name, *this);
    BOOST_TEST_CHECK(this->matrix.is_valid());
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(symmetric)

using SymmetricIsRowMajorOrderedFixture = Fixture<float, coo::Symmetric::yes>;
BOOST_DATA_TEST_CASE_F(
        SymmetricIsRowMajorOrderedFixture,
        is_row_major_ordered,
        symmetric_matrices,
        matrix_name
    )
{
    initialise(matrix_name, *this);
    BOOST_TEST_CHECK(this->matrix.is_row_major_ordered());
}

using SymmetricIsValidFixture = Fixture<float, coo::Symmetric::yes>;
BOOST_DATA_TEST_CASE_F(
        SymmetricIsValidFixture,
        is_valid,
        symmetric_matrices,
        matrix_name
    )
{
    initialise(matrix_name, *this);
    BOOST_TEST_CHECK(this->matrix.is_valid());
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()

namespace {

template<typename T, coo::Symmetric Symm>
auto initialise(std::string_view const matrix_name, Fixture<T, Symm>& f) -> void
{
    f.matrix = meta::load_coo_matrix<T, Symm>(matrix_name);
}

}  // namespace
