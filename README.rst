=================================================================
 Joint Direct and Transposed Sparse Matrix-Vector Multiplication
=================================================================

This repository includes libraries and benchmark programs for performing a
mathematical operation called *joint direct and transposed sparse matrix-vector
multiplication* (SpMMᵀV).  SpMMᵀV is a combination of sparse matrix-vector
multiplication (SpMV) and of transposed sparse matrix-vector multiplication
(SpMᵀV).  If we define SpMV as computing *y* ← *Ax* and SpMᵀV as computing *y*
← *A*\ ᵀ\ *x*\ , then SpMMᵀV corresponds to computing *p* ← *Ar* and *q* ← *A*\
ᵀ\ *s* as a single joint (or fused) operation.  Performing a single SpMMᵀV
operation instead of performing SpMV followed by SpMᵀV is more efficient
because matrix *A* is read from memory only once.  Reading matrix *A* from
memory only once is advantageous because sparse matrix-vector multiplication is
a memory-bound operation.

This repository contains a library for performing parallel SpMMᵀV with a sparse
matrix on a shared-memory computer system.  The performance of parallel SpMMᵀV
using the library can be measured using an included benchmark program, which
supports loading sparse matrices from Matrix Market files.  Other libraries and
programs included in this repository (e.g. benchmark programs for comparing the
performance of parallel SpMMᵀV with alternative approaches) are introduced
further in this document.

Building the Project
====================

Build Dependencies
------------------

Building the core libraries and programs of this project requires

* a compiler with support for C89, C++20 and OpenMP (e.g. \ Clang_ or GCC_),
* the CMake_ build system generator,
* a build system supported by CMake (e.g. \ `GNU Make`_ or Ninja_),
* Boost_ libraries and
* the HDF5_ library.

.. _Clang: https://clang.llvm.org/
.. _GCC: https://gcc.gnu.org/
.. _CMake: https://cmake.org/
.. _GNU Make: https://www.gnu.org/software/make/
.. _Ninja: https://ninja-build.org/
.. _Boost: https://www.boost.org/
.. _HDF5: https://www.hdfgroup.org/solutions/hdf5/

The project also includes programs that can be built optionally.  Those
programs require

* the Intel `oneAPI Math Kernel Library`_,
* a Message Passing Interface (MPI) implementation,
* PETSc_ and
* librsb_.

.. _oneAPI Math Kernel Library: https://software.intel.com/content/www/us/en/develop/tools/oneapi/components/onemkl.html
.. _PETSc: https://petsc.org/
.. _librsb: http://librsb.sourceforge.net/

Available Build Targets
-----------------------

The project is composed of a number of subprojects, which are exposed as build
targets.  This section describes build targets potentially relevant to end
users.  The following four targets are built by default.

* ``coo``: A library for loading COO matrices and performing sequential SpMMᵀV
  with them.
* ``csr``: A library for loading CSR matrices and performing sequential SpMMᵀV
  with them.
* ``hsf``: A library for converting CSR matrices into a hierarchical storage
  format (HSF) and for performing sequential or shared-memory parallel SpMMᵀV
  with them.
* ``tests``: A program for testing the COO, CSR and HSF libraries.

The following two targets are enabled by default but they are not built unless
explicitly requested from the build system.

* ``benchmark``: A program for benchmarking the performance of shared-memory
  parallel SpMMᵀV algorithms provided by the HSF library.  The program also
  supports benchmarking the performance of sequential SpMMᵀV algorithms
  provided by the COO, CSR and HSF libraries.
* ``hsf-matrix-info``: A program for printing information about sparse matrices
  stored using the hierarchical storage format implemented in the HSF library.

The remaining build targets are disabled by default because they require
additional dependencies.  The targets need to be explicitly enabled for CMake
to generate build system files for them.  Then the targets can then be built by
explicitly requesting them from the build system.

* ``benchmark-dhsf``: A program for benchmarking the performance of
  distributed-memory parallel SpMMᵀV.  An MPI implementation needs to be
  available to build and run this program.
* ``benchmark-mkl``: A program for benchmarking the performance of SpMMᵀV
  simulated by performing an SpMV operation followed by an SpMᵀV operation
  using the Intel oneAPI Math Kernel Library.
* ``benchmark-petsc``: A program for benchmarking the performance of SpMMᵀV
  simulated by performing an SpMV operation followed by an SpMᵀV operation
  using PETSc.  An MPI implementation needs to be available to build and run
  this program.
* ``benchmark-petsc-ksp``: A program for benchmarking the performance of Krylov
  subspace methods using PETSc.  An MPI implementation needs to be available to
  build and run this program.
* ``benchmark-rsb``: A program for benchmarking the performance of SpMMᵀV
  simulated by performing an SpMV operation followed by an SpMᵀV operation
  using librsb.
* ``dcsr``: A library for loading CSR matrices in a distributed manner.  An MPI
  implementation needs to be available to build and use this library.
* ``dhsf``: A library for converting CSR matrices into HSF matrices in a
  distributed manner and for performing distributed-memory parallel SpMMᵀV with
  them.  An MPI implementation needs to be available to build and use this
  library.

Building Core Targets
---------------------

First it is necessary to generate build system files with CMake.  The following
example will use ``build/release`` as the build directory.  Execute the
following command from the root directory of this project.

.. code-block:: shell

                cmake -B build/release -D CMAKE_BUILD_TYPE=Release

The above command automatically chooses the used build system, C compiler and
C++ compiler.  The generated build files are available in directory
``build/release``.  The above command will not succeed if it fails to detect
any of the required dependencies.

The files generated by CMake are used to build the project.  The simplest way
is to change into the build directory and let CMake invoke the appropriate
build system tool.

.. code-block:: shell

                cd build/release
                cmake --build .

It is possible to run the build tool manually instead of invoking it via CMake.
Depending on the build system chosen by CMake, the build directory can for
example contain a ``Makefile`` or a ``build.ninja`` file.  Hence running the
``cmake --build .`` command can be replaced by running ``make``, ``ninja`` or
another build tool.  In any case the the default set of build targets
(i.e. ``coo``, ``csr``, ``hsf`` and ``tests``) will be built.

It is possible to explicitly request the targets to build.  The following
command builds targets ``benchmark`` and ``hsf-matrix-info`` instead of the
default set of build targets.

.. code-block:: shell

                cmake --build . -t benchmark hsf-matrix-info

The built programs and libraries described in this section can be found at the
paths in the following list.  Please note that the resulting filenames can be
slightly different depending on the naming conventions of the used operating
system.

* ``extras/benchmark/benchmark``
* ``extras/hsf-matrix-info/hsf-matrix-info``
* ``libs/coo/libcoo.a``
* ``libs/csr/libcsr.a``
* ``libs/hsf/libhsf.a``
* ``tests/tests``

Building Optional Targets
-------------------------

Building the ``benchmark-dhsf`` target can be enabled by setting CMake variable
``SPMMTV_BUILD_BENCHMARK_DHSF`` to ``ON``.  This can be done by calling CMake
with option ``-D SPMMTV_BUILD_BENCHMARK_DHSF=ON``.  One way is to do this when
initially generating build system files.

.. code-block:: shell

                cmake -B build/release -D CMAKE_BUILD_TYPE=Release -D SPMMTV_BUILD_BENCHMARK_DHSF=ON

It is also possible to enable building the ``benchmark-dhsf`` target in a
pre-existing build directory created by CMake.  To do so, run the following
command from the build directory.

.. code-block:: shell

                cmake -D SPMMTV_BUILD_BENCHMARK_DHSF=ON .

After this the ``benchmark-dhsf`` target can be built from the build directory
like any other target.

.. code-block:: shell

                cmake --build . -t benchmark-dhsf

The ``benchmark-mkl``, ``benchmark-petsc``, ``benchmark-petsc-ksp``,
``benchmark-rsb``, ``dcsr`` and ``dhsf`` targets can be enabled analogously by
setting the ``SPMMTV_BUILD_BENCHMARK_MKL``, ``SPMMTV_BUILD_BENCHMARK_PETSC``,
``SPMMTV_BUILD_BENCHMARK_PETSC_KSP``, ``SPMMTV_BUILD_BENCHMARK_RSB``,
``SPMMTV_BUILD_DCSR`` and ``SPMMTV_BUILD_DHSF`` CMake variables to ``ON``.

The built programs described in this section can be found at the paths in the
following list.  Please note that the resulting filenames can be slightly
different depending on the naming conventions of the used operating system.

* ``extras/benchmark-dhsf/benchmark-dhsf``
* ``extras/benchmark-mkl/benchmark-mkl``
* ``extras/benchmark-petsc/benchmark-petsc``
* ``extras/benchmark-petsc-ksp/benchmark-petsc-ksp``
* ``extras/benchmark-rsb/benchmark-rsb``
* ``extras/dcsr/libdcsr.a``
* ``extras/dhsf/libdhsf.a``

Building Using a Specific Compiler
----------------------------------

The compilers used in the build system files generated by CMake are
configurable.  The used C compiler and C++ compiler can be respectively
specified using environment variables ``CC`` and ``CXX``.  The following
command generates build system files that use ``gcc`` and ``g++`` from the GNU
Compiler Collection.

.. code-block:: shell

                CC=gcc CXX=g++ cmake -B build/release -D CMAKE_BUILD_TYPE=Release

Using a Specific Build System
-----------------------------

The build system that CMake generates build files for can be specified using
option \ ``-G``.  The following command generates build system files for the
Ninja build system.

.. code-block:: shell

                cmake -B build/release -D CMAKE_BUILD_TYPE=Release -G Ninja
