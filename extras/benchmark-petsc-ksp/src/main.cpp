#include <array>                // std::array
#include <exception>            // std::exception
#include <filesystem>           // std::filesystem
#include <iostream>             // std::cerr
#include <string>               // std::string_literals

#include <mpi.h>                // MPI_*

#include <petscksp.h>           // CHKERRQ KSP* Mat* PETSC* Petsc* Vec*

#include <csr/symmetric.hpp>    // csr::Symmetric

#include <dcsr/io.hpp>          // dcsr::load_from_hdf5

#include <command_line_argument_error.hpp>  // CommandLineArgumentError

using namespace std::string_literals;

namespace {

auto init_matrix(MPI_Comm comm, std::filesystem::path const& matrix_path, Mat& petsc_matrix) -> PetscErrorCode;

auto petsc_main(MPI_Comm comm, bool& converged_correctly) -> PetscErrorCode;

auto petsc_solve(KSP ksp, std::filesystem::path const& matrix_path) -> PetscErrorCode;

}  // namespace

auto main(int argc, char** argv) -> int try
{
    auto const help =
        "Benchmark the performance of function KSPSolve. A user-specified Krylov subspace method is executed for a "
        "fixed number of iterations. Profiling results have to be explicitly requested using the run-time options of "
        "PETSc.\n"s;
    auto ierr = PetscInitialize(&argc, &argv, nullptr, help.c_str());
    if (ierr != 0) {
        std::cerr << "Error: Failed to initialise the PETSc library.\n";
        return ierr;
    }

    auto const comm = PETSC_COMM_WORLD;
    try {
        auto converged_correctly = bool{};
        ierr = petsc_main(comm, converged_correctly);
        CHKERRQ(ierr);

        if (!converged_correctly) {
            PetscFinalize();
            return 1;
        }

    } catch (CommandLineArgumentError const& e) {
        auto comm_rank = int{};
        MPI_Comm_rank(comm, &comm_rank);
        if (comm_rank == 0) {
            std::cerr << "Error: " << e.what() << '\n';
        }
        PetscFinalize();
        return 2;

    } catch (std::exception const& e) {
        std::cerr << "Error: "s + e.what() + '\n';
        PETSCABORT(comm, 1);
    }

    return PetscFinalize();

} catch (std::exception const& e) {
    std::cerr << "Error: " << e.what() << '\n';
    return 1;
}

namespace {

auto init_matrix(MPI_Comm const comm, std::filesystem::path const& matrix_path, Mat& petsc_matrix) -> PetscErrorCode
{
    auto const [csr_matrix, row_offset, global_row_count, global_element_count]
        = dcsr::load_from_hdf5<PetscReal, csr::Symmetric::no>(comm, matrix_path);

    auto ierr = MatCreateMPIAIJWithArrays(
            comm,
            static_cast<PetscInt>(csr_matrix.row_count()),
            PETSC_DECIDE,
            static_cast<PetscInt>(global_row_count),
            static_cast<PetscInt>(csr_matrix.column_count()),
            reinterpret_cast<PetscInt const*>(csr_matrix.row_pointers.data()),
            reinterpret_cast<PetscInt const*>(csr_matrix.columns.data()),
            csr_matrix.elements.data(),
            &petsc_matrix
        );
    CHKERRQ(ierr);

    return 0;
}

auto petsc_main(MPI_Comm const comm, bool& converged_correctly) -> PetscErrorCode
{
    auto ksp = KSP{};
    auto ierr = KSPCreate(comm, &ksp);
    CHKERRQ(ierr);
    ierr = KSPSetFromOptions(ksp);
    CHKERRQ(ierr);
    ierr = KSPSetConvergenceTest(ksp, KSPConvergedSkip, nullptr, nullptr);
    CHKERRQ(ierr);

    auto matrix_file_chars = std::array<char, PETSC_MAX_PATH_LEN>{};
    auto matrix_file_set = PetscBool{};

    ierr = PetscOptionsBegin(comm, nullptr, "Options for this program", nullptr);
    CHKERRQ(ierr);
    {
        ierr = PetscOptionsString(
                "-matrix_file",
                "load the input matrix from the specified file",
                nullptr,
                nullptr,
                matrix_file_chars.data(),
                matrix_file_chars.size(),
                &matrix_file_set
            );
        CHKERRQ(ierr);
    }
    ierr = PetscOptionsEnd();
    CHKERRQ(ierr);

    auto help_set = PetscBool{};
    ierr = PetscOptionsHasName(nullptr, nullptr, "-help", &help_set);
    CHKERRQ(ierr);
    if (help_set == PETSC_TRUE) {
        converged_correctly = true;
        return 0;
    }

    auto version_set = PetscBool{};
    ierr = PetscOptionsHasName(nullptr, nullptr, "-version", &version_set);
    CHKERRQ(ierr);
    if (version_set == PETSC_TRUE) {
        converged_correctly = true;
        return 0;
    }

    if (matrix_file_set != PETSC_TRUE) {
        throw CommandLineArgumentError{"Please specify a file path using option ‘-matrix_file’."};
    }

    auto matrix_file_path = std::filesystem::path{matrix_file_chars.data()};
    if (!std::filesystem::exists(matrix_file_path)) {
        throw CommandLineArgumentError{"The specified matrix file doesn’t exist."};
    }
    if (!std::filesystem::is_regular_file(matrix_file_path)) {
        throw CommandLineArgumentError{"The specified matrix file isn’t a regular file."};
    }

    ierr = petsc_solve(ksp, matrix_file_path);
    CHKERRQ(ierr);

    auto reason = KSPConvergedReason{};
    ierr = KSPGetConvergedReason(ksp, &reason);
    CHKERRQ(ierr);
    if (reason == KSP_CONVERGED_ITS) {
        converged_correctly = true;
    } else {
        converged_correctly = false;
    }

    ierr = KSPConvergedReasonView(ksp, PETSC_VIEWER_STDOUT_(comm));
    CHKERRQ(ierr);

    ierr = KSPDestroy(&ksp);
    CHKERRQ(ierr);

    return 0;
}

auto petsc_solve(KSP const ksp, std::filesystem::path const& matrix_path) -> PetscErrorCode
{
    auto comm = MPI_Comm{};
    auto ierr = PetscObjectGetComm(reinterpret_cast<PetscObject const>(ksp), &comm);
    CHKERRQ(ierr);

    auto A = Mat{};
    ierr = init_matrix(comm, matrix_path, A);
    CHKERRQ(ierr);

    auto x = Vec{};
    auto b = Vec{};
    ierr = MatCreateVecs(A, &x, &b);
    CHKERRQ(ierr);
    ierr = VecSet(x, 0);
    CHKERRQ(ierr);
    ierr = VecSet(b, 3.14);
    CHKERRQ(ierr);

    ierr = KSPSetOperators(ksp, A, A);
    CHKERRQ(ierr);
    ierr = KSPSetUp(ksp);
    CHKERRQ(ierr);

    auto stage = PetscLogStage{};
    ierr = PetscLogStageRegister("KSPSolve", &stage);
    CHKERRQ(ierr);
    ierr = PetscLogStagePush(stage);
    CHKERRQ(ierr);
    ierr = KSPSolve(ksp, b, x);
    CHKERRQ(ierr);
    ierr = PetscLogStagePop();
    CHKERRQ(ierr);

    ierr = VecDestroy(&b);
    CHKERRQ(ierr);
    ierr = VecDestroy(&x);
    CHKERRQ(ierr);
    ierr = MatDestroy(&A);
    CHKERRQ(ierr);

    return 0;
}

}  // namespace
