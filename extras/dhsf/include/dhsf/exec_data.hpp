#pragma once

#include <cstdint>              // std::uint32_t
#include <span>                 // std::span
#include <vector>               // std::vector

#include <boost/mpi/communicator.hpp>  // boost::mpi::communicator

#include <mpi.h>                // MPI_Comm

#include <hsf/exec_data.hpp>    // hsf::ExecData
#include <hsf/exec_mode.hpp>    // hsf::ExecMode
#include <hsf/symmetric.hpp>    // hsf::Symmetric

#include <dhsf/exec_mode.hpp>   // dhsf::ExecMode
#include <dhsf/priv/mpi_request_ptr.hpp>  // dhsf::MpiRequestPtr

namespace dhsf {

template<typename T> class Matrix;
template<typename T> class Vector;

// Execution data for a given distributed SpMMᵀV execution mode and a given
// distributed hierarchical matrix.  This class template can only be
// instantiated with types ‘float’ and ‘double’ in place of template argument
// ‘T’.
template<ExecMode EM, typename T> class ExecData;

template<typename T>
class ExecData<ExecMode::sequential, T> {
public:
    ExecData() = default;

    // Instantiate distributed SpMMᵀV execution data for the matrix given by
    // parameter ‘matrix’.  All processes in the communicator given by
    // parameter ‘comm’ must call this constructor collectively.
    ExecData(boost::mpi::communicator comm, Matrix<T> const& matrix);
    ExecData(MPI_Comm comm, Matrix<T> const& matrix);

    // Return the stored MPI communicator.
    auto get_communicator() const noexcept -> boost::mpi::communicator const&;

    // Return the local compressed x1 vector.
    auto get_local_compressed_x1() const noexcept -> std::span<T const>;

    // Return the local compressed y2 contribution vector.
    auto get_local_compressed_y2() noexcept -> std::span<T>;
    auto get_local_compressed_y2() const noexcept -> std::span<T const>;

    // Prepare compressed copies of local x1 vector elements according to the
    // requirements of all participating processes.  Elements are copied from
    // the vector given by parameter ‘x1’.
    auto copy_local_x1_elements(Vector<T> const& x1) -> void;

    // Transfer the compressed copies of local x1 vector elements among all
    // participating processes.
    auto transfer_copied_x1_elements() -> void;

    // Start transferring local compressed y2 vector contributions among all
    // participating processes asynchronously.
    auto start_compressed_y2_element_transfer() -> void;

    // Wait until the asynchronous transfer of local compressed y2 vector
    // contributions finishes.
    auto await_compressed_y2_element_transfer() -> void;

    // Store the final local values of vector y2 into the vector given by
    // parameter ‘y2’.
    auto store_final_y2_elements(Vector<T>& y2) -> void;

    // Prepare internal data structures for the next potential SpMMᵀV
    // iteration.
    auto reset_auxiliary_data() -> void;

private:
    // An MPI communicator specifying the processes where SpMMᵀV execution data
    // is distributed.
    boost::mpi::communicator comm_{};

    // The local compressed x1 vector.
    std::vector<T> local_x1_values_{};

    // The local compressed y2 contribution vector.
    std::vector<T> local_y2_values_{};

    // MPI data for interprocess communication.
    std::vector<int> send_counts_{};
    std::vector<int> send_displacements_{};
    std::vector<int> receive_counts_{};
    std::vector<int> receive_displacements_{};
    MpiRequestPtr y2_request_ptr_{};

    // Received x1 and y2 vector indices in compressed form.  The indices
    // correspond to compressed nonempty matrix columns received from all
    // processes.
    std::vector<std::uint32_t> received_indices_{};

    // Copies of local x1 values in compressed form.
    std::vector<T> copied_x1_values_{};

    // Received y2 contributions in compressed form.
    std::vector<T> received_y2_values_{};
};

template<typename T>
class ExecData<ExecMode::locks_exclusive, T> {
public:
    ExecData() = default;

    // Instantiate distributed SpMMᵀV execution data for a given thread count
    // and hierarchical matrix.  All processes in the communicator given by
    // parameter ‘comm’ must call this constructor collectively.
    ExecData(boost::mpi::communicator comm, int thread_count, Matrix<T> const& matrix);
    ExecData(MPI_Comm comm, int thread_count, Matrix<T> const& matrix);

    // Return the stored MPI communicator.
    auto get_communicator() const noexcept -> boost::mpi::communicator const&;

    // Return the local compressed x1 vector.
    auto get_local_compressed_x1() const noexcept -> std::span<T const>;

    // Return the local compressed y2 contribution vector.
    auto get_local_compressed_y2() noexcept -> std::span<T>;
    auto get_local_compressed_y2() const noexcept -> std::span<T const>;

    // Return a reference to local SpMMᵀV execution data of the diagonal
    // matrix.
    auto get_local_diagonal_exec_data() noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_exclusive, T, hsf::Symmetric::no>&;
    auto get_local_diagonal_exec_data() const noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_exclusive, T, hsf::Symmetric::no> const&;

    // Return a reference to local SpMMᵀV execution data of the off-diagonal
    // matrix.
    auto get_local_off_diagonal_exec_data() noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_exclusive, T, hsf::Symmetric::no>&;
    auto get_local_off_diagonal_exec_data() const noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_exclusive, T, hsf::Symmetric::no> const&;

    // Prepare compressed copies of local x1 vector elements according to the
    // requirements of all participating processes.  Elements are copied from
    // the vector given by parameter ‘x1’.
    auto copy_local_x1_elements(Vector<T> const& x1) -> void;

    // Transfer the compressed copies of local x1 vector elements among all
    // participating processes.
    auto transfer_copied_x1_elements() -> void;

    // Start transferring local compressed y2 vector contributions among all
    // participating processes asynchronously.
    auto start_compressed_y2_element_transfer() -> void;

    // Wait until the asynchronous transfer of local compressed y2 vector
    // contributions finishes.
    auto await_compressed_y2_element_transfer() -> void;

    // Store the final local values of vector y2 into the vector given by
    // parameter ‘y2’.
    auto store_final_y2_elements(Vector<T>& y2) -> void;

    // Prepare internal data structures for the next potential SpMMᵀV
    // iteration.
    auto reset_auxiliary_data() -> void;

private:
    // An MPI communicator specifying the processes where SpMMᵀV execution data
    // is distributed.
    boost::mpi::communicator comm_{};

    // The local compressed x1 vector.
    std::vector<T> local_x1_values_{};

    // The local compressed y2 contribution vector.
    std::vector<T> local_y2_values_{};

    // Local SpMMᵀV execution data of the diagonal matrix.
    hsf::ExecData<hsf::ExecMode::locks_exclusive, T, hsf::Symmetric::no> local_diagonal_exec_data_{};

    // Local SpMMᵀV execution data of the off-diagonal matrix.
    hsf::ExecData<hsf::ExecMode::locks_exclusive, T, hsf::Symmetric::no> local_off_diagonal_exec_data_{};

    // MPI data for interprocess communication.
    std::vector<int> send_counts_{};
    std::vector<int> send_displacements_{};
    std::vector<int> receive_counts_{};
    std::vector<int> receive_displacements_{};
    MpiRequestPtr y2_request_ptr_{};

    // Received x1 and y2 vector indices in compressed form.  The indices
    // correspond to compressed nonempty matrix columns received from all
    // processes.
    std::vector<std::uint32_t> received_indices_{};

    // Copies of local x1 values in compressed form.
    std::vector<T> copied_x1_values_{};

    // Received y2 contributions in compressed form.
    std::vector<T> received_y2_values_{};
};

template<typename T>
class ExecData<ExecMode::locks_planned_exclusive, T> {
public:
    ExecData() = default;

    // Instantiate distributed SpMMᵀV execution data for a given thread count
    // and hierarchical matrix.  All processes in the communicator given by
    // parameter ‘comm’ must call this constructor collectively.
    ExecData(boost::mpi::communicator comm, int thread_count, Matrix<T> const& matrix);
    ExecData(MPI_Comm comm, int thread_count, Matrix<T> const& matrix);

    // Return the stored MPI communicator.
    auto get_communicator() const noexcept -> boost::mpi::communicator const&;

    // Return the local compressed x1 vector.
    auto get_local_compressed_x1() const noexcept -> std::span<T const>;

    // Return the local compressed y2 contribution vector.
    auto get_local_compressed_y2() noexcept -> std::span<T>;
    auto get_local_compressed_y2() const noexcept -> std::span<T const>;

    // Return a reference to local SpMMᵀV execution data of the diagonal
    // matrix.
    auto get_local_diagonal_exec_data() noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_planned_exclusive, T, hsf::Symmetric::no>&;
    auto get_local_diagonal_exec_data() const noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_planned_exclusive, T, hsf::Symmetric::no> const&;

    // Return a reference to local SpMMᵀV execution data of the off-diagonal
    // matrix.
    auto get_local_off_diagonal_exec_data() noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_planned_exclusive, T, hsf::Symmetric::no>&;
    auto get_local_off_diagonal_exec_data() const noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_planned_exclusive, T, hsf::Symmetric::no> const&;

    // Prepare compressed copies of local x1 vector elements according to the
    // requirements of all participating processes.  Elements are copied from
    // the vector given by parameter ‘x1’.
    auto copy_local_x1_elements(Vector<T> const& x1) -> void;

    // Transfer the compressed copies of local x1 vector elements among all
    // participating processes.
    auto transfer_copied_x1_elements() -> void;

    // Start transferring local compressed y2 vector contributions among all
    // participating processes asynchronously.
    auto start_compressed_y2_element_transfer() -> void;

    // Wait until the asynchronous transfer of local compressed y2 vector
    // contributions finishes.
    auto await_compressed_y2_element_transfer() -> void;

    // Store the final local values of vector y2 into the vector given by
    // parameter ‘y2’.
    auto store_final_y2_elements(Vector<T>& y2) -> void;

    // Prepare internal data structures for the next potential SpMMᵀV
    // iteration.
    auto reset_auxiliary_data() -> void;

private:
    // An MPI communicator specifying the processes where SpMMᵀV execution data
    // is distributed.
    boost::mpi::communicator comm_{};

    // The local compressed x1 vector.
    std::vector<T> local_x1_values_{};

    // The local compressed y2 contribution vector.
    std::vector<T> local_y2_values_{};

    // Local SpMMᵀV execution data of the diagonal matrix.
    hsf::ExecData<hsf::ExecMode::locks_planned_exclusive, T, hsf::Symmetric::no> local_diagonal_exec_data_{};

    // Local SpMMᵀV execution data of the off-diagonal matrix.
    hsf::ExecData<hsf::ExecMode::locks_planned_exclusive, T, hsf::Symmetric::no> local_off_diagonal_exec_data_{};

    // MPI data for interprocess communication.
    std::vector<int> send_counts_{};
    std::vector<int> send_displacements_{};
    std::vector<int> receive_counts_{};
    std::vector<int> receive_displacements_{};
    MpiRequestPtr y2_request_ptr_{};

    // Received x1 and y2 vector indices in compressed form.  The indices
    // correspond to compressed nonempty matrix columns received from all
    // processes.
    std::vector<std::uint32_t> received_indices_{};

    // Copies of local x1 values in compressed form.
    std::vector<T> copied_x1_values_{};

    // Received y2 contributions in compressed form.
    std::vector<T> received_y2_values_{};
};

template<typename T>
class ExecData<ExecMode::locks_planned_shared, T> {
public:
    ExecData() = default;

    // Instantiate distributed SpMMᵀV execution data for a given thread count
    // and hierarchical matrix.  All processes in the communicator given by
    // parameter ‘comm’ must call this constructor collectively.
    ExecData(boost::mpi::communicator comm, int thread_count, Matrix<T> const& matrix);
    ExecData(MPI_Comm comm, int thread_count, Matrix<T> const& matrix);

    // Return the stored MPI communicator.
    auto get_communicator() const noexcept -> boost::mpi::communicator const&;

    // Return the local compressed x1 vector.
    auto get_local_compressed_x1() const noexcept -> std::span<T const>;

    // Return the local compressed y2 contribution vector.
    auto get_local_compressed_y2() noexcept -> std::span<T>;
    auto get_local_compressed_y2() const noexcept -> std::span<T const>;

    // Return a reference to local SpMMᵀV execution data of the diagonal
    // matrix.
    auto get_local_diagonal_exec_data() noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_planned_shared, T, hsf::Symmetric::no>&;
    auto get_local_diagonal_exec_data() const noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_planned_shared, T, hsf::Symmetric::no> const&;

    // Return a reference to local SpMMᵀV execution data of the off-diagonal
    // matrix.
    auto get_local_off_diagonal_exec_data() noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_planned_shared, T, hsf::Symmetric::no>&;
    auto get_local_off_diagonal_exec_data() const noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_planned_shared, T, hsf::Symmetric::no> const&;

    // Prepare compressed copies of local x1 vector elements according to the
    // requirements of all participating processes.  Elements are copied from
    // the vector given by parameter ‘x1’.
    auto copy_local_x1_elements(Vector<T> const& x1) -> void;

    // Transfer the compressed copies of local x1 vector elements among all
    // participating processes.
    auto transfer_copied_x1_elements() -> void;

    // Start transferring local compressed y2 vector contributions among all
    // participating processes asynchronously.
    auto start_compressed_y2_element_transfer() -> void;

    // Wait until the asynchronous transfer of local compressed y2 vector
    // contributions finishes.
    auto await_compressed_y2_element_transfer() -> void;

    // Store the final local values of vector y2 into the vector given by
    // parameter ‘y2’.
    auto store_final_y2_elements(Vector<T>& y2) -> void;

    // Prepare internal data structures for the next potential SpMMᵀV
    // iteration.
    auto reset_auxiliary_data() -> void;

private:
    // An MPI communicator specifying the processes where SpMMᵀV execution data
    // is distributed.
    boost::mpi::communicator comm_{};

    // The local compressed x1 vector.
    std::vector<T> local_x1_values_{};

    // The local compressed y2 contribution vector.
    std::vector<T> local_y2_values_{};

    // Local SpMMᵀV execution data of the diagonal matrix.
    hsf::ExecData<hsf::ExecMode::locks_planned_shared, T, hsf::Symmetric::no> local_diagonal_exec_data_{};

    // Local SpMMᵀV execution data of the off-diagonal matrix.
    hsf::ExecData<hsf::ExecMode::locks_planned_shared, T, hsf::Symmetric::no> local_off_diagonal_exec_data_{};

    // MPI data for interprocess communication.
    std::vector<int> send_counts_{};
    std::vector<int> send_displacements_{};
    std::vector<int> receive_counts_{};
    std::vector<int> receive_displacements_{};
    MpiRequestPtr y2_request_ptr_{};

    // Received x1 and y2 vector indices in compressed form.  The indices
    // correspond to compressed nonempty matrix columns received from all
    // processes.
    std::vector<std::uint32_t> received_indices_{};

    // Copies of local x1 values in compressed form.
    std::vector<T> copied_x1_values_{};

    // Received y2 contributions in compressed form.
    std::vector<T> received_y2_values_{};
};

template<typename T>
class ExecData<ExecMode::locks_shared, T> {
public:
    ExecData() = default;

    // Instantiate distributed SpMMᵀV execution data for a given thread count
    // and hierarchical matrix.  All processes in the communicator given by
    // parameter ‘comm’ must call this constructor collectively.
    ExecData(boost::mpi::communicator comm, int thread_count, Matrix<T> const& matrix);
    ExecData(MPI_Comm comm, int thread_count, Matrix<T> const& matrix);

    // Return the stored MPI communicator.
    auto get_communicator() const noexcept -> boost::mpi::communicator const&;

    // Return the local compressed x1 vector.
    auto get_local_compressed_x1() const noexcept -> std::span<T const>;

    // Return the local compressed y2 contribution vector.
    auto get_local_compressed_y2() noexcept -> std::span<T>;
    auto get_local_compressed_y2() const noexcept -> std::span<T const>;

    // Return a reference to local SpMMᵀV execution data of the diagonal
    // matrix.
    auto get_local_diagonal_exec_data() noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_shared, T, hsf::Symmetric::no>&;
    auto get_local_diagonal_exec_data() const noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_shared, T, hsf::Symmetric::no> const&;

    // Return a reference to local SpMMᵀV execution data of the off-diagonal
    // matrix.
    auto get_local_off_diagonal_exec_data() noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_shared, T, hsf::Symmetric::no>&;
    auto get_local_off_diagonal_exec_data() const noexcept
        -> hsf::ExecData<hsf::ExecMode::locks_shared, T, hsf::Symmetric::no> const&;

    // Prepare compressed copies of local x1 vector elements according to the
    // requirements of all participating processes.  Elements are copied from
    // the vector given by parameter ‘x1’.
    auto copy_local_x1_elements(Vector<T> const& x1) -> void;

    // Transfer the compressed copies of local x1 vector elements among all
    // participating processes.
    auto transfer_copied_x1_elements() -> void;

    // Start transferring local compressed y2 vector contributions among all
    // participating processes asynchronously.
    auto start_compressed_y2_element_transfer() -> void;

    // Wait until the asynchronous transfer of local compressed y2 vector
    // contributions finishes.
    auto await_compressed_y2_element_transfer() -> void;

    // Store the final local values of vector y2 into the vector given by
    // parameter ‘y2’.
    auto store_final_y2_elements(Vector<T>& y2) -> void;

    // Prepare internal data structures for the next potential SpMMᵀV
    // iteration.
    auto reset_auxiliary_data() -> void;

private:
    // An MPI communicator specifying the processes where SpMMᵀV execution data
    // is distributed.
    boost::mpi::communicator comm_{};

    // The local compressed x1 vector.
    std::vector<T> local_x1_values_{};

    // The local compressed y2 contribution vector.
    std::vector<T> local_y2_values_{};

    // Local SpMMᵀV execution data of the diagonal matrix.
    hsf::ExecData<hsf::ExecMode::locks_shared, T, hsf::Symmetric::no> local_diagonal_exec_data_{};

    // Local SpMMᵀV execution data of the off-diagonal matrix.
    hsf::ExecData<hsf::ExecMode::locks_shared, T, hsf::Symmetric::no> local_off_diagonal_exec_data_{};

    // MPI data for interprocess communication.
    std::vector<int> send_counts_{};
    std::vector<int> send_displacements_{};
    std::vector<int> receive_counts_{};
    std::vector<int> receive_displacements_{};
    MpiRequestPtr y2_request_ptr_{};

    // Received x1 and y2 vector indices in compressed form.  The indices
    // correspond to compressed nonempty matrix columns received from all
    // processes.
    std::vector<std::uint32_t> received_indices_{};

    // Copies of local x1 values in compressed form.
    std::vector<T> copied_x1_values_{};

    // Received y2 contributions in compressed form.
    std::vector<T> received_y2_values_{};
};

}  // namespace dhsf
