#pragma once

#include <chrono>               // std::chrono

namespace dhsf {

// Store the durations of various phases of a distributed SpMMᵀV operation.
// Use the setters to record the time points when each SpMMᵀV phase starts and
// stops.  Use member function ‘compute_durations’ to calculate elapsed
// durations from the recorded time points.  Use the getters to obtain the
// calculated durations.
class SpmmtvLogger {
public:
    // Calculate elapsed durations for all phases from the recorded time
    // points.  The getters will only return the correct duration after this
    // member function is invoked.
    auto compute_durations() -> void;

    // Get and set information about the total run time of distributed SpMMᵀV.
    auto get_end_to_end_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto set_end_to_end_start() noexcept -> void;
    auto set_end_to_end_stop() noexcept -> void;

    // Get and set information about the phase where processes prepare
    // compressed copies of vector x1.
    auto get_copy_x1_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto set_copy_x1_start() noexcept -> void;
    auto set_copy_x1_stop() noexcept -> void;

    // Get and set information about the phase where processes transfer their
    // compressed copies of vector x1.
    auto get_transfer_x1_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto set_transfer_x1_start() noexcept -> void;
    auto set_transfer_x1_stop() noexcept -> void;

    // Get and set information about the phase where processes perform
    // mathematical operations for computing the SpMMᵀV operation with the
    // off-diagonal matrix.
    auto get_off_diagonal_spmmtv_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto set_off_diagonal_spmmtv_start() noexcept -> void;
    auto set_off_diagonal_spmmtv_stop() noexcept -> void;

    // Get and set information about the phase where processes perform
    // mathematical operations for computing the SpMMᵀV operation with the
    // diagonal matrix.
    auto get_diagonal_spmmtv_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto set_diagonal_spmmtv_start() noexcept -> void;
    auto set_diagonal_spmmtv_stop() noexcept -> void;

    // Get and set information about the phase where processes start
    // asynchronously transferring their y2 vector contributions.
    auto get_start_y2_transfer_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto set_start_y2_transfer_start() noexcept -> void;
    auto set_start_y2_transfer_stop() noexcept -> void;

    // Get and set information about the phase where processes wait for the
    // asynchronous transfer of their y2 vector contributions to finish.
    auto get_await_y2_transfer_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto set_await_y2_transfer_start() noexcept -> void;
    auto set_await_y2_transfer_stop() noexcept -> void;

    // Get and set information about the phase where processes store the final
    // values of vector y2.
    auto get_store_y2_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto set_store_y2_start() noexcept -> void;
    auto set_store_y2_stop() noexcept -> void;

    // Get and set information about the phase where processes prepare internal
    // data structures for the next potential SpMMᵀV operation.
    auto get_clean_up_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto set_clean_up_start() noexcept -> void;
    auto set_clean_up_stop() noexcept -> void;

private:
    std::chrono::steady_clock::time_point end_to_end_start_{};
    std::chrono::steady_clock::time_point end_to_end_stop_{};
    std::chrono::steady_clock::duration end_to_end_duration_{};

    std::chrono::steady_clock::time_point copy_x1_start_{};
    std::chrono::steady_clock::time_point copy_x1_stop_{};
    std::chrono::steady_clock::duration copy_x1_duration_{};

    std::chrono::steady_clock::time_point transfer_x1_start_{};
    std::chrono::steady_clock::time_point transfer_x1_stop_{};
    std::chrono::steady_clock::duration transfer_x1_duration_{};

    std::chrono::steady_clock::time_point off_diagonal_spmmtv_start_{};
    std::chrono::steady_clock::time_point off_diagonal_spmmtv_stop_{};
    std::chrono::steady_clock::duration off_diagonal_spmmtv_duration_{};

    std::chrono::steady_clock::time_point diagonal_spmmtv_start_{};
    std::chrono::steady_clock::time_point diagonal_spmmtv_stop_{};
    std::chrono::steady_clock::duration diagonal_spmmtv_duration_{};

    std::chrono::steady_clock::time_point start_y2_transfer_start_{};
    std::chrono::steady_clock::time_point start_y2_transfer_stop_{};
    std::chrono::steady_clock::duration start_y2_transfer_duration_{};

    std::chrono::steady_clock::time_point await_y2_transfer_start_{};
    std::chrono::steady_clock::time_point await_y2_transfer_stop_{};
    std::chrono::steady_clock::duration await_y2_transfer_duration_{};

    std::chrono::steady_clock::time_point store_y2_start_{};
    std::chrono::steady_clock::time_point store_y2_stop_{};
    std::chrono::steady_clock::duration store_y2_duration_{};

    std::chrono::steady_clock::time_point clean_up_start_{};
    std::chrono::steady_clock::time_point clean_up_stop_{};
    std::chrono::steady_clock::duration clean_up_duration_{};
};

}  // namespace dhsf
