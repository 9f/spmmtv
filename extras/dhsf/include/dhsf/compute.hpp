#pragma once

#include <dhsf/spmmtv_logger.hpp>  // dhsf::SpmmtvLogger

namespace dhsf {

enum struct ExecMode;
template<ExecMode EM, typename T> class ExecData;
template<typename T> class Matrix;
template<typename T> class Vector;

}  // namespace dhsf

namespace dhsf::compute {

// Perform a distributed SpMMᵀV operation with a hierarchical matrix.
// Parameter ‘matrix’ is the input matrix.  Parameters ‘x1’ and ‘x2’ are input
// vectors.  Parameters ‘y1’ and ‘y2’ are the resulting output vectors.
// Parameter ‘exec_data’ is SpMMᵀV execution data for the used matrix.  Throw
// ‘dhsf::LogicError’ if the passed arguments don’t all use the same
// communicator.  Throw ‘dhsf::LogicError’ if the passed arguments don’t have
// dimensions appropriate for performing SpMMᵀV.  The behaviour of this
// function is undefined if it isn’t called collectively by all participating
// processes.  This function template can only be instantiated with types
// ‘float’ and ‘double’ in place of template argument ‘T’.
template<typename T, ExecMode EM>
auto spmmtv(
        Matrix<T> const& matrix,
        Vector<T> const& x1,
        Vector<T> const& x2,
        Vector<T>& y1,
        Vector<T>& y2,
        ExecData<EM, T>& exec_data
    ) -> void;

// Perform a distributed SpMMᵀV operation with a hierarchical matrix and log
// the duration of individual phases of the operation.  Parameter ‘matrix’ is
// the input matrix.  Parameters ‘x1’ and ‘x2’ are input vectors.  Parameters
// ‘y1’ and ‘y2’ are the resulting output vectors.  Parameter ‘exec_data’ is
// SpMMᵀV execution data for the used matrix.  Throw ‘dhsf::LogicError’ if the
// passed arguments don’t all use the same communicator.  Throw
// ‘dhsf::LogicError’ if the passed arguments don’t have dimensions appropriate
// for performing SpMMᵀV.  The behaviour of this function is undefined if it
// isn’t called collectively by all participating processes.  Return an object
// that contains the durations of the logged SpMMᵀV phases.  This function
// template can only be instantiated with types ‘float’ and ‘double’ in place
// of template argument ‘T’.
template<typename T, ExecMode EM>
auto spmmtv_logged(
        Matrix<T> const& matrix,
        Vector<T> const& x1,
        Vector<T> const& x2,
        Vector<T>& y1,
        Vector<T>& y2,
        ExecData<EM, T>& exec_data
    ) -> SpmmtvLogger;

}  // namespace dhsf::compute
