#pragma once

#include <cstddef>              // std::size_t
#include <span>                 // std::span
#include <vector>               // std::vector

#include <boost/mpi/communicator.hpp>  // boost::mpi::communicator

#include <mpi.h>                // MPI_Comm

namespace dhsf {

// A distributed vector.  This class template can only be instantiated with
// types ‘float’ and ‘double’.
template<typename T>
class Vector {
public:
    Vector() = default;

    // Instantiate a distributed vector with automatically calculated local
    // vector sizes.  All processes in the communicator given by parameter
    // ‘comm’ must call this constructor collectively.  Parameter
    // ‘element_count’ specifies the global element count of the distributed
    // vector.  All local vector elements are initialised to the value given by
    // parameter ‘value’.  Throw ‘dhsf::LogicError’ if the passed element count
    // is invalid.
    Vector(boost::mpi::communicator comm, std::size_t element_count, T const& value = T{});
    Vector(MPI_Comm comm, std::size_t element_count, T const& value = T{});

    // Instantiate a distributed vector with explicitly specified local vector
    // sizes.  All processes in the communicator given by parameter ‘comm’ must
    // call this constructor collectively.  Parameter ‘element_count’ specifies
    // the global element count of the distributed vector.  The element count
    // of each local vector is specified per process using parameter
    // ‘local_element_count’.  All local vector elements are initialised to the
    // value given by parameter ‘value’.  Throw ‘dhsf::LogicError’ if the
    // passed element counts are invalid.

    Vector(boost::mpi::communicator comm,
           std::size_t element_count,
           std::size_t local_element_count,
           T const& value = T{});

    Vector(MPI_Comm comm, std::size_t element_count, std::size_t local_element_count, T const& value = T{});

    // Return the stored MPI communicator.
    auto get_communicator() const noexcept -> boost::mpi::communicator const&;

    // Return the global element count of the distributed vector.
    auto get_element_count() const noexcept -> std::size_t;

    // Return the element count of the local vector.
    auto get_local_element_count() const noexcept -> std::size_t;

    // Return the index offset of the local vector.  The local index offset is
    // the global index of the first element in the local vector.
    auto get_local_index_offset() const noexcept -> std::size_t;

    // Return the local vector.
    auto get_local_vector() noexcept -> std::span<T>;
    auto get_local_vector() const noexcept -> std::span<T const>;

    // Set all elements of the local vector to the value given by parameter
    // ‘value’.
    auto set(T const& value) noexcept -> void;

private:
    // An MPI communicator specifying the processes where the vector is
    // distributed.
    boost::mpi::communicator comm_{};

    // The global element count of the distributed vector.
    std::size_t element_count_{};

    // The global index of the first element in the local vector.
    std::size_t local_index_offset_{};

    // The local part of the distributed vector.
    std::vector<T> local_vector_{};
};

}  // namespace dhsf
