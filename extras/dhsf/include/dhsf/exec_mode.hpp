#pragma once

namespace dhsf {

// Specify the SpMMᵀV execution mode for a ‘dhsf::ExecData’ object.
enum struct ExecMode {
    sequential,
    locks_exclusive,
    locks_planned_exclusive,
    locks_planned_shared,
    locks_shared,
};

}  // namespace dhsf
