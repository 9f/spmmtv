#pragma once

#include <mpi.h>                // MPI_REQUEST_NULL MPI_Request

namespace dhsf {

// Wrap an MPI request handle, that is an object of type ‘MPI_Request’.
class MpiRequestPtr {
public:
    MpiRequestPtr() = default;
    ~MpiRequestPtr() noexcept;

    MpiRequestPtr(MpiRequestPtr const&) = delete;
    MpiRequestPtr(MpiRequestPtr&& other) noexcept;

    auto operator=(MpiRequestPtr const&) -> MpiRequestPtr& = delete;
    auto operator=(MpiRequestPtr&& other) noexcept -> MpiRequestPtr&;

    // Return a pointer to the wrapped ‘MPI_Request’ object.  The returned
    // pointer is never ‘nullptr’.
    auto get() noexcept -> MPI_Request*;
    auto get() const noexcept -> MPI_Request const*;

    // Return a reference to the wrapped ‘MPI_Request’ object.
    auto get_reference() noexcept -> MPI_Request&;
    auto get_reference() const noexcept -> MPI_Request const&;

    friend auto swap(MpiRequestPtr& lhs, MpiRequestPtr& rhs) noexcept -> void;

private:
    // The wrapped MPI request handle.
    MPI_Request mpi_request_{MPI_REQUEST_NULL};
};

}  // namespace dhsf
