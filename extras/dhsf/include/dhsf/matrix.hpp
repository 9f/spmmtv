#pragma once

#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t
#include <span>                 // std::span
#include <vector>               // std::vector

#include <boost/mpi/communicator.hpp>  // boost::mpi::communicator

#include <mpi.h>                // MPI_Comm

#include <csr/symmetric.hpp>    // csr::Symmetric

#include <hsf/matrix.hpp>       // hsf::Matrix
#include <hsf/optimise_csr_regions.hpp>  // hsf::OptimiseCsrRegions
#include <hsf/symmetric.hpp>    // hsf::Symmetric

namespace csr {

template<typename T, Symmetric Symm> class Matrix;

}  // namespace csr

namespace hsf {

enum struct RegionOrdering;

}  // namespace hsf

namespace dhsf {

// A distributed hierarchical matrix.  This class template can only be
// instantiated with types ‘float’ and ‘double’.
template<typename T>
class Matrix {
public:
    Matrix() = default;

    // Instantiate a distributed hierarchical matrix from a CSR matrix.  All
    // processes in the communicator given by parameter ‘comm’ must call this
    // constructor collectively.  Parameter ‘row_count’ specifies the global
    // row count of the distributed hierarchical matrix.  Parameter
    // ‘local_row_offset’ specifies the initial global row index of the local
    // matrix for the calling process.  Parameters ‘local_matrix’,
    // ‘region_ordering’ and ‘ocr’ are forwarded to the constructor of
    // ‘hsf::Matrix’.  Throw ‘dhsf::LogicError’ if any of the passed arguments
    // are invalid.  Rethrow any exceptions thrown by the constructor of
    // ‘hsf::Matrix’.

    Matrix(boost::mpi::communicator comm,
           std::size_t row_count,
           std::size_t local_row_offset,
           csr::Matrix<T, csr::Symmetric::no> const& local_matrix,
           hsf::RegionOrdering region_ordering,
           hsf::OptimiseCsrRegions ocr = hsf::OptimiseCsrRegions::yes);

    Matrix(MPI_Comm comm,
           std::size_t row_count,
           std::size_t local_row_offset,
           csr::Matrix<T, csr::Symmetric::no> const& local_matrix,
           hsf::RegionOrdering region_ordering,
           hsf::OptimiseCsrRegions ocr = hsf::OptimiseCsrRegions::yes);

    // Instantiate a distributed hierarchical matrix with normalised regions
    // from a CSR matrix.  All processes in the communicator given by parameter
    // ‘comm’ must call this constructor collectively.  Parameter ‘row_count’
    // specifies the global row count of the distributed hierarchical matrix.
    // Parameter ‘local_row_offset’ specifies the initial global row index of
    // the local matrix for the calling process.  Parameters ‘local_matrix’,
    // ‘region_ordering’, ‘region_coefficient’, ‘thread_count’ and ‘ocr’ are
    // forwarded to the constructor of ‘hsf::Matrix’.  Throw ‘dhsf::LogicError’
    // if any of the passed arguments are invalid.  Rethrow any exceptions
    // thrown by the constructor of ‘hsf::Matrix’.

    Matrix(boost::mpi::communicator comm,
           std::size_t row_count,
           std::size_t local_row_offset,
           csr::Matrix<T, csr::Symmetric::no> const& local_matrix,
           hsf::RegionOrdering region_ordering,
           double region_coefficient,
           int thread_count,
           hsf::OptimiseCsrRegions ocr = hsf::OptimiseCsrRegions::yes);

    Matrix(MPI_Comm comm,
           std::size_t row_count,
           std::size_t local_row_offset,
           csr::Matrix<T, csr::Symmetric::no> const& local_matrix,
           hsf::RegionOrdering region_ordering,
           double region_coefficient,
           int thread_count,
           hsf::OptimiseCsrRegions ocr = hsf::OptimiseCsrRegions::yes);

    // Return the stored MPI communicator.
    auto get_communicator() const noexcept -> boost::mpi::communicator const&;

    // Return the global row count of the distributed matrix.
    auto get_row_count() const noexcept -> std::size_t;

    // Return the global column count of the distributed matrix.
    auto get_column_count() const noexcept -> std::size_t;

    // Return the row count of the local matrix.
    auto get_local_row_count() const noexcept -> std::size_t;

    // Return the column count of the local matrix.
    auto get_local_column_count() const noexcept -> std::size_t;

    // Return the row offset of the local matrix.  The local row offset is the
    // global row index of the first row in the local matrix.
    auto get_local_row_offset() const noexcept -> std::size_t;

    // Return the column offset of the local diagonal matrix.  The local
    // diagonal column offset is the global column index of the first column in
    // the local diagonal matrix.
    auto get_local_diagonal_column_offset() const noexcept -> std::size_t;

    // Return a reference to the local diagonal matrix.
    auto get_local_diagonal_matrix() noexcept -> hsf::Matrix<T, hsf::Symmetric::no>&;
    auto get_local_diagonal_matrix() const noexcept -> hsf::Matrix<T, hsf::Symmetric::no> const&;

    // Return a reference to the local off-diagonal matrix.
    auto get_local_off_diagonal_matrix() noexcept -> hsf::Matrix<T, hsf::Symmetric::no>&;
    auto get_local_off_diagonal_matrix() const noexcept -> hsf::Matrix<T, hsf::Symmetric::no> const&;

    // Return the number of nonempty columns in the local off-diagonal matrix.
    auto get_local_off_diagonal_nonempty_column_count() const noexcept -> std::size_t;

    // Return an array of local off-diagonal nonempty columns.  The array
    // contains a local off-diagonal column if and only if the local
    // off-diagonal matrix contains at least one nonzero element in that
    // column.
    auto get_local_off_diagonal_nonempty_columns() const noexcept -> std::span<std::uint32_t const>;

private:
    // An MPI communicator specifying the processes where the matrix is
    // distributed.
    boost::mpi::communicator comm_{};

    // The global row count of the distributed matrix.
    std::size_t row_count_{};

    // The global column count of the distributed matrix.
    std::size_t column_count_{};

    // The local row count of the distributed matrix.
    std::size_t local_row_count_{};

    // The global row index of the first row in the local matrix.
    std::size_t local_row_offset_{};

    // The global column index of the first column in the local diagonal
    // matrix.
    std::size_t local_diagonal_column_offset_{};

    // The local diagonal part of the distributed matrix.
    hsf::Matrix<T, hsf::Symmetric::no> local_diagonal_matrix_{};

    // The local off-diagonal part of the distributed matrix.
    hsf::Matrix<T, hsf::Symmetric::no> local_off_diagonal_matrix_{};

    // This member variable can be either viewed as a map from local
    // off-diagonal compressed columns to local off-diagonal nonempty columns
    // or simply as a list of local off-diagonal nonempty columns.
    std::vector<std::uint32_t> local_off_diagonal_compressed_columns_to_nonempty_columns_{};
};

}  // namespace dhsf
