#include <dhsf/compute_local_array_offset_size.hpp>

#include <cstddef>              // std::size_t
#include <tuple>                // std::tuple

#include <boost/mpi/communicator.hpp>  // boost::mpi::communicator

namespace dhsf {

auto compute_local_array_offset_size(boost::mpi::communicator const& comm, std::size_t const global_size)
    -> std::tuple<std::size_t, std::size_t>
{
    auto const comm_rank_uz = static_cast<std::size_t>(comm.rank());
    auto const comm_size_uz = static_cast<std::size_t>(comm.size());

    auto local_offset = std::size_t{};
    auto local_size = std::size_t{};
    auto const divided_size = global_size / comm_size_uz;
    auto const remainder_size = global_size % comm_size_uz;

    if (comm_rank_uz < remainder_size) {
        local_offset = (divided_size + 1) * comm_rank_uz;
        local_size = divided_size + 1;

    } else {
        auto const regular_rank_count = comm_rank_uz - remainder_size;
        local_offset = (divided_size + 1) * remainder_size + divided_size * regular_rank_count;
        local_size = divided_size;
    }

    return {local_offset, local_size};
}

}  // namespace dhsf
