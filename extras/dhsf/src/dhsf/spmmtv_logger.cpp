#include <dhsf/spmmtv_logger.hpp>

#include <cassert>              // assert
#include <chrono>               // std::chrono

namespace dhsf {

auto SpmmtvLogger::compute_durations() -> void
{
    assert(this->end_to_end_stop_ >= this->end_to_end_start_);
    this->end_to_end_duration_ = this->end_to_end_stop_ - this->end_to_end_start_;

    assert(this->copy_x1_stop_ >= this->copy_x1_start_);
    this->copy_x1_duration_ = this->copy_x1_stop_ - this->copy_x1_start_;

    assert(this->transfer_x1_stop_ >= this->transfer_x1_start_);
    this->transfer_x1_duration_ = this->transfer_x1_stop_ - this->transfer_x1_start_;

    assert(this->off_diagonal_spmmtv_stop_ >= this->off_diagonal_spmmtv_start_);
    this->off_diagonal_spmmtv_duration_ = this->off_diagonal_spmmtv_stop_ - this->off_diagonal_spmmtv_start_;

    assert(this->diagonal_spmmtv_stop_ >= this->diagonal_spmmtv_start_);
    this->diagonal_spmmtv_duration_ = this->diagonal_spmmtv_stop_ - this->diagonal_spmmtv_start_;

    assert(this->start_y2_transfer_stop_ >= this->start_y2_transfer_start_);
    this->start_y2_transfer_duration_ = this->start_y2_transfer_stop_ - this->start_y2_transfer_start_;

    assert(this->await_y2_transfer_stop_ >= this->await_y2_transfer_start_);
    this->await_y2_transfer_duration_ = this->await_y2_transfer_stop_ - this->await_y2_transfer_start_;

    assert(this->store_y2_stop_ >= this->store_y2_start_);
    this->store_y2_duration_ = this->store_y2_stop_ - this->store_y2_start_;

    assert(this->clean_up_stop_ >= this->clean_up_start_);
    this->clean_up_duration_ = this->clean_up_stop_ - this->clean_up_start_;
}

auto SpmmtvLogger::get_end_to_end_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->end_to_end_duration_;
}

auto SpmmtvLogger::set_end_to_end_start() noexcept -> void
{
    this->end_to_end_start_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::set_end_to_end_stop() noexcept -> void
{
    this->end_to_end_stop_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::get_copy_x1_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->copy_x1_duration_;
}

auto SpmmtvLogger::set_copy_x1_start() noexcept -> void
{
    this->copy_x1_start_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::set_copy_x1_stop() noexcept -> void
{
    this->copy_x1_stop_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::get_transfer_x1_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->transfer_x1_duration_;
}

auto SpmmtvLogger::set_transfer_x1_start() noexcept -> void
{
    this->transfer_x1_start_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::set_transfer_x1_stop() noexcept -> void
{
    this->transfer_x1_stop_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::get_off_diagonal_spmmtv_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->off_diagonal_spmmtv_duration_;
}

auto SpmmtvLogger::set_off_diagonal_spmmtv_start() noexcept -> void
{
    this->off_diagonal_spmmtv_start_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::set_off_diagonal_spmmtv_stop() noexcept -> void
{
    this->off_diagonal_spmmtv_stop_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::get_diagonal_spmmtv_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->diagonal_spmmtv_duration_;
}

auto SpmmtvLogger::set_diagonal_spmmtv_start() noexcept -> void
{
    this->diagonal_spmmtv_start_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::set_diagonal_spmmtv_stop() noexcept -> void
{
    this->diagonal_spmmtv_stop_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::get_start_y2_transfer_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->start_y2_transfer_duration_;
}

auto SpmmtvLogger::set_start_y2_transfer_start() noexcept -> void
{
    this->start_y2_transfer_start_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::set_start_y2_transfer_stop() noexcept -> void
{
    this->start_y2_transfer_stop_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::get_await_y2_transfer_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->await_y2_transfer_duration_;
}

auto SpmmtvLogger::set_await_y2_transfer_start() noexcept -> void
{
    this->await_y2_transfer_start_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::set_await_y2_transfer_stop() noexcept -> void
{
    this->await_y2_transfer_stop_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::get_store_y2_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->store_y2_duration_;
}

auto SpmmtvLogger::set_store_y2_start() noexcept -> void
{
    this->store_y2_start_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::set_store_y2_stop() noexcept -> void
{
    this->store_y2_stop_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::get_clean_up_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->clean_up_duration_;
}

auto SpmmtvLogger::set_clean_up_start() noexcept -> void
{
    this->clean_up_start_ = std::chrono::steady_clock::now();
}

auto SpmmtvLogger::set_clean_up_stop() noexcept -> void
{
    this->clean_up_stop_ = std::chrono::steady_clock::now();
}

}  // namespace dhsf
