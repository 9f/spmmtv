#include <dhsf/vector.hpp>

#include <algorithm>            // std::ranges
#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint64_t
#include <functional>           // std::plus
#include <span>                 // std::span
#include <tuple>                // std::get
#include <utility>              // std::move

#include <boost/mpi/collectives/all_reduce.hpp>  // boost::mpi::all_reduce
#include <boost/mpi/communicator.hpp>  // boost::mpi::comm_attach boost::mpi::communicator
#include <boost/mpi/inplace.hpp>  // boost::mpi::inplace

#include <mpi.h>                // MPI_*

#include <dhsf/compute_local_array_offset_size.hpp>  // dhsf::compute_local_array_offset_size
#include <dhsf/logic_error.hpp>  // dhsf::LogicError

namespace dhsf {

namespace {

// Calculate the local index offset for instantiating a distributed vector
// using the calling process.  This function must be called collectively by all
// processes in the communicator given by parameter ‘comm’.  Parameter
// ‘local_element_count’ specifies the local element count of the distributed
// vector for the calling process.  Return the calculated local index offset.
auto init_local_index_offset(boost::mpi::communicator const& comm, std::size_t local_element_count) -> std::size_t;

}  // namespace

template<typename T>
Vector<T>::Vector(boost::mpi::communicator comm, std::size_t const element_count, T const& value)
    : Vector{comm, element_count, std::get<1>(compute_local_array_offset_size(comm, element_count)), value}
{
}

template<typename T>
Vector<T>::Vector(MPI_Comm const comm, std::size_t const element_count, T const& value)
    : Vector{boost::mpi::communicator{comm, boost::mpi::comm_attach}, element_count, value}
{
}

template<typename T>
Vector<T>::Vector(
        boost::mpi::communicator comm,
        std::size_t const element_count,
        std::size_t const local_element_count,
        T const& value
    ) : comm_{std::move(comm)},
        element_count_{element_count},
        local_index_offset_{init_local_index_offset(this->get_communicator(), local_element_count)},
        local_vector_(local_element_count, value)
{
    if (this->get_local_element_count() > this->get_element_count()) {
        throw LogicError{"The local element count of dhsf::Vector cannot be greater than its global element count."};
    }

    auto local_element_count_sum = static_cast<std::uint64_t>(this->get_local_element_count());
    boost::mpi::all_reduce(
            this->get_communicator(), boost::mpi::inplace(local_element_count_sum), std::plus<std::uint64_t>{}
        );
    if (this->get_element_count() != local_element_count_sum) {
        throw LogicError{"Constructing dhsf::Vector with mismatching local and global element counts."};
    }
}

template<typename T>
Vector<T>::Vector(
        MPI_Comm const comm, std::size_t const element_count, std::size_t const local_element_count, T const& value
    ) : Vector{boost::mpi::communicator{comm, boost::mpi::comm_attach}, element_count, local_element_count, value}
{
}

template<typename T>
auto Vector<T>::get_communicator() const noexcept -> boost::mpi::communicator const&
{
    return this->comm_;
}

template<typename T>
auto Vector<T>::get_element_count() const noexcept -> std::size_t
{
    return this->element_count_;
}

template<typename T>
auto Vector<T>::get_local_element_count() const noexcept -> std::size_t
{
    return this->local_vector_.size();
}

template<typename T>
auto Vector<T>::get_local_index_offset() const noexcept -> std::size_t
{
    return this->local_index_offset_;
}

template<typename T>
auto Vector<T>::get_local_vector() noexcept -> std::span<T>
{
    return this->local_vector_;
}

template<typename T>
auto Vector<T>::get_local_vector() const noexcept -> std::span<T const>
{
    return this->local_vector_;
}

template<typename T>
auto Vector<T>::set(T const& value) noexcept -> void
{
    std::ranges::fill(this->local_vector_, value);
}

template class Vector<float>;
template class Vector<double>;

namespace {

auto init_local_index_offset(boost::mpi::communicator const& comm, std::size_t const local_element_count)
    -> std::size_t
{
    auto local_index_offset = static_cast<std::uint64_t>(local_element_count);
    MPI_Exscan(MPI_IN_PLACE, &local_index_offset, 1, MPI_UINT64_T, MPI_SUM, comm);

    if (comm.rank() == 0) {
        return 0;
    } else {
        return local_index_offset;
    }
}

}  // namespace

}  // namespace dhsf
