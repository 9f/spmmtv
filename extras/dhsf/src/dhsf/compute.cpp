#include <dhsf/compute.hpp>

#include <hsf/compute.hpp>      // hsf::compute

#include <dhsf/exec_data.hpp>   // dhsf::ExecData
#include <dhsf/exec_mode.hpp>   // dhsf::ExecMode
#include <dhsf/logic_error.hpp>  // dhsf::LogicError
#include <dhsf/matrix.hpp>      // dhsf::Matrix
#include <dhsf/spmmtv_logger.hpp>  // dhsf::SpmmtvLogger
#include <dhsf/vector.hpp>      // dhsf::Vector

namespace dhsf::compute {

namespace {

// A no-op SpMMᵀV duration logger.
struct NoOpLogger {
    constexpr auto set_end_to_end_start() const noexcept {};
    constexpr auto set_end_to_end_stop() const noexcept {};

    constexpr auto set_copy_x1_start() const noexcept {};
    constexpr auto set_copy_x1_stop() const noexcept {};

    constexpr auto set_transfer_x1_start() const noexcept {};
    constexpr auto set_transfer_x1_stop() const noexcept {};

    constexpr auto set_off_diagonal_spmmtv_start() const noexcept {};
    constexpr auto set_off_diagonal_spmmtv_stop() const noexcept {};

    constexpr auto set_diagonal_spmmtv_start() const noexcept {};
    constexpr auto set_diagonal_spmmtv_stop() const noexcept {};

    constexpr auto set_start_y2_transfer_start() const noexcept {};
    constexpr auto set_start_y2_transfer_stop() const noexcept {};

    constexpr auto set_await_y2_transfer_start() const noexcept {};
    constexpr auto set_await_y2_transfer_stop() const noexcept {};

    constexpr auto set_store_y2_start() const noexcept {};
    constexpr auto set_store_y2_stop() const noexcept {};

    constexpr auto set_clean_up_start() const noexcept {};
    constexpr auto set_clean_up_stop() const noexcept {};
};

template<typename Logger, typename T, ExecMode EM>
auto spmmtv(
        Matrix<T> const& matrix,
        Vector<T> const& x1,
        Vector<T> const& x2,
        Vector<T>& y1,
        Vector<T>& y2,
        ExecData<EM, T>& exec_data
    ) -> Logger;

}  // namespace

template<typename T, ExecMode EM>
auto spmmtv(
        Matrix<T> const& matrix,
        Vector<T> const& x1,
        Vector<T> const& x2,
        Vector<T>& y1,
        Vector<T>& y2,
        ExecData<EM, T>& exec_data
    ) -> void
{
    spmmtv<NoOpLogger>(matrix, x1, x2, y1, y2, exec_data);
}

template auto spmmtv(
        Matrix<float> const& matrix,
        Vector<float> const& x1,
        Vector<float> const& x2,
        Vector<float>& y1,
        Vector<float>& y2,
        ExecData<ExecMode::sequential, float>& exec_data
    ) -> void;

template auto spmmtv(
        Matrix<float> const& matrix,
        Vector<float> const& x1,
        Vector<float> const& x2,
        Vector<float>& y1,
        Vector<float>& y2,
        ExecData<ExecMode::locks_exclusive, float>& exec_data
    ) -> void;

template auto spmmtv(
        Matrix<float> const& matrix,
        Vector<float> const& x1,
        Vector<float> const& x2,
        Vector<float>& y1,
        Vector<float>& y2,
        ExecData<ExecMode::locks_planned_exclusive, float>& exec_data
    ) -> void;

template auto spmmtv(
        Matrix<float> const& matrix,
        Vector<float> const& x1,
        Vector<float> const& x2,
        Vector<float>& y1,
        Vector<float>& y2,
        ExecData<ExecMode::locks_planned_shared, float>& exec_data
    ) -> void;

template auto spmmtv(
        Matrix<float> const& matrix,
        Vector<float> const& x1,
        Vector<float> const& x2,
        Vector<float>& y1,
        Vector<float>& y2,
        ExecData<ExecMode::locks_shared, float>& exec_data
    ) -> void;

template auto spmmtv(
        Matrix<double> const& matrix,
        Vector<double> const& x1,
        Vector<double> const& x2,
        Vector<double>& y1,
        Vector<double>& y2,
        ExecData<ExecMode::sequential, double>& exec_data
    ) -> void;

template auto spmmtv(
        Matrix<double> const& matrix,
        Vector<double> const& x1,
        Vector<double> const& x2,
        Vector<double>& y1,
        Vector<double>& y2,
        ExecData<ExecMode::locks_exclusive, double>& exec_data
    ) -> void;

template auto spmmtv(
        Matrix<double> const& matrix,
        Vector<double> const& x1,
        Vector<double> const& x2,
        Vector<double>& y1,
        Vector<double>& y2,
        ExecData<ExecMode::locks_planned_exclusive, double>& exec_data
    ) -> void;

template auto spmmtv(
        Matrix<double> const& matrix,
        Vector<double> const& x1,
        Vector<double> const& x2,
        Vector<double>& y1,
        Vector<double>& y2,
        ExecData<ExecMode::locks_planned_shared, double>& exec_data
    ) -> void;

template auto spmmtv(
        Matrix<double> const& matrix,
        Vector<double> const& x1,
        Vector<double> const& x2,
        Vector<double>& y1,
        Vector<double>& y2,
        ExecData<ExecMode::locks_shared, double>& exec_data
    ) -> void;

template<typename T, ExecMode EM>
auto spmmtv_logged(
        Matrix<T> const& matrix,
        Vector<T> const& x1,
        Vector<T> const& x2,
        Vector<T>& y1,
        Vector<T>& y2,
        ExecData<EM, T>& exec_data
    ) -> SpmmtvLogger
{
    return spmmtv<SpmmtvLogger>(matrix, x1, x2, y1, y2, exec_data);
}

template auto spmmtv_logged(
        Matrix<float> const& matrix,
        Vector<float> const& x1,
        Vector<float> const& x2,
        Vector<float>& y1,
        Vector<float>& y2,
        ExecData<ExecMode::sequential, float>& exec_data
    ) -> SpmmtvLogger;

template auto spmmtv_logged(
        Matrix<float> const& matrix,
        Vector<float> const& x1,
        Vector<float> const& x2,
        Vector<float>& y1,
        Vector<float>& y2,
        ExecData<ExecMode::locks_exclusive, float>& exec_data
    ) -> SpmmtvLogger;

template auto spmmtv_logged(
        Matrix<float> const& matrix,
        Vector<float> const& x1,
        Vector<float> const& x2,
        Vector<float>& y1,
        Vector<float>& y2,
        ExecData<ExecMode::locks_planned_exclusive, float>& exec_data
    ) -> SpmmtvLogger;

template auto spmmtv_logged(
        Matrix<float> const& matrix,
        Vector<float> const& x1,
        Vector<float> const& x2,
        Vector<float>& y1,
        Vector<float>& y2,
        ExecData<ExecMode::locks_planned_shared, float>& exec_data
    ) -> SpmmtvLogger;

template auto spmmtv_logged(
        Matrix<float> const& matrix,
        Vector<float> const& x1,
        Vector<float> const& x2,
        Vector<float>& y1,
        Vector<float>& y2,
        ExecData<ExecMode::locks_shared, float>& exec_data
    ) -> SpmmtvLogger;

template auto spmmtv_logged(
        Matrix<double> const& matrix,
        Vector<double> const& x1,
        Vector<double> const& x2,
        Vector<double>& y1,
        Vector<double>& y2,
        ExecData<ExecMode::sequential, double>& exec_data
    ) -> SpmmtvLogger;

template auto spmmtv_logged(
        Matrix<double> const& matrix,
        Vector<double> const& x1,
        Vector<double> const& x2,
        Vector<double>& y1,
        Vector<double>& y2,
        ExecData<ExecMode::locks_exclusive, double>& exec_data
    ) -> SpmmtvLogger;

template auto spmmtv_logged(
        Matrix<double> const& matrix,
        Vector<double> const& x1,
        Vector<double> const& x2,
        Vector<double>& y1,
        Vector<double>& y2,
        ExecData<ExecMode::locks_planned_exclusive, double>& exec_data
    ) -> SpmmtvLogger;

template auto spmmtv_logged(
        Matrix<double> const& matrix,
        Vector<double> const& x1,
        Vector<double> const& x2,
        Vector<double>& y1,
        Vector<double>& y2,
        ExecData<ExecMode::locks_planned_shared, double>& exec_data
    ) -> SpmmtvLogger;

template auto spmmtv_logged(
        Matrix<double> const& matrix,
        Vector<double> const& x1,
        Vector<double> const& x2,
        Vector<double>& y1,
        Vector<double>& y2,
        ExecData<ExecMode::locks_shared, double>& exec_data
    ) -> SpmmtvLogger;

namespace {

template<typename Logger, typename T, ExecMode EM>
auto spmmtv(
        Matrix<T> const& matrix,
        Vector<T> const& x1,
        Vector<T> const& x2,
        Vector<T>& y1,
        Vector<T>& y2,
        ExecData<EM, T>& exec_data
    ) -> Logger
{
    if (matrix.get_communicator() != x1.get_communicator()
        || matrix.get_communicator() != x2.get_communicator()
        || matrix.get_communicator() != y1.get_communicator()
        || matrix.get_communicator() != y2.get_communicator()
        || matrix.get_communicator() != exec_data.get_communicator()) {
        throw LogicError{"All objects passed to dhsf::compute::spmmtv must use the same communicator."};
    }

    if (matrix.get_column_count() != x1.get_element_count()
        || matrix.get_row_count() != y1.get_element_count()
        || matrix.get_row_count() != x2.get_element_count()
        || matrix.get_column_count() != y2.get_element_count()) {
        throw LogicError{"The matrix and vectors passed to dhsf::compute::spmmtv do not have compatible dimensions."};
    }

    auto logger = Logger{};
    logger.set_end_to_end_start();

    logger.set_copy_x1_start();
    exec_data.copy_local_x1_elements(x1);
    logger.set_copy_x1_stop();

    logger.set_transfer_x1_start();
    exec_data.transfer_copied_x1_elements();
    logger.set_transfer_x1_stop();

    logger.set_off_diagonal_spmmtv_start();
    if (matrix.get_local_off_diagonal_matrix().element_count() != 0) {
        if constexpr (EM == ExecMode::sequential) {
            hsf::compute::spmmtv_sequential<T>(
                    matrix.get_local_off_diagonal_matrix(),
                    exec_data.get_local_compressed_x1(),
                    x2.get_local_vector(),
                    y1.get_local_vector(),
                    exec_data.get_local_compressed_y2()
                );

        } else {
            hsf::compute::spmmtv_parallel<T>(
                    matrix.get_local_off_diagonal_matrix(),
                    exec_data.get_local_compressed_x1(),
                    x2.get_local_vector(),
                    y1.get_local_vector(),
                    exec_data.get_local_compressed_y2(),
                    exec_data.get_local_off_diagonal_exec_data()
                );
        }
    }
    logger.set_off_diagonal_spmmtv_stop();

    logger.set_start_y2_transfer_start();
    exec_data.start_compressed_y2_element_transfer();
    logger.set_start_y2_transfer_stop();

    logger.set_diagonal_spmmtv_start();
    if constexpr (EM == ExecMode::sequential) {
        hsf::compute::spmmtv_sequential(
                matrix.get_local_diagonal_matrix(),
                x1.get_local_vector(),
                x2.get_local_vector(),
                y1.get_local_vector(),
                y2.get_local_vector()
            );

    } else {
        hsf::compute::spmmtv_parallel(
                matrix.get_local_diagonal_matrix(),
                x1.get_local_vector(),
                x2.get_local_vector(),
                y1.get_local_vector(),
                y2.get_local_vector(),
                exec_data.get_local_diagonal_exec_data()
            );
    }
    logger.set_diagonal_spmmtv_stop();

    logger.set_await_y2_transfer_start();
    exec_data.await_compressed_y2_element_transfer();
    logger.set_await_y2_transfer_stop();

    logger.set_store_y2_start();
    exec_data.store_final_y2_elements(y2);
    logger.set_store_y2_stop();

    logger.set_clean_up_start();
    exec_data.reset_auxiliary_data();
    logger.set_clean_up_stop();

    logger.set_end_to_end_stop();
    return logger;
}

}  // namespace

}  // namespace dhsf::compute
