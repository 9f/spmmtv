#include <dhsf/matrix.hpp>

#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t std::uint64_t
#include <functional>           // std::plus
#include <limits>               // std::numeric_limits
#include <span>                 // std::span
#include <tuple>                // std::tie std::tuple
#include <utility>              // std::move
#include <vector>               // std::vector

#include <boost/mpi/collectives/broadcast.hpp>  // boost::mpi::broadcast
#include <boost/mpi/collectives/all_reduce.hpp>  // boost::mpi::all_reduce
#include <boost/mpi/communicator.hpp>  // boost::mpi::comm_attach boost::mpi::communicator
#include <boost/mpi/inplace.hpp>  // boost::mpi::inplace
#include <boost/mpi/operations.hpp>  // boost::mpi::bitwise_or

#include <mpi.h>                // MPI_Comm

#include <csr/info.hpp>         // csr::info
#include <csr/matrix.hpp>       // csr::Matrix
#include <csr/symmetric.hpp>    // csr::Symmetric

#include <hsf/matrix.hpp>       // hsf::Matrix
#include <hsf/optimise_csr_regions.hpp>  // hsf::OptimiseCsrRegions
#include <hsf/region_ordering.hpp>  // hsf::RegionOrdering
#include <hsf/symmetric.hpp>    // hsf::Symmetric

#include <dhsf/compute_local_array_offset_size.hpp>  // dhsf::compute_local_array_offset_size
#include <dhsf/cstddef.hpp>     // dhsf::operator""_uz
#include <dhsf/logic_error.hpp>  // dhsf::LogicError

namespace dhsf {

namespace {

// Return ‘true’ if parameter ‘local_column_count’ isn’t identical in all
// collective calls of this function.  All processes in the communicator given
// by parameter ‘comm’ must call this function collectively.
auto are_local_column_counts_different(boost::mpi::communicator const& comm, std::size_t local_column_count) -> bool;

// Initialise arrays for mapping nonempty matrix columns to and from a
// compressed representation.  Parameter ‘column_count’ is the column count of
// the sparse matrix.  Parameter ‘columns’ is the column index array of the
// sparse matrix.  Return an array that maps nonempty columns to compressed
// columns and an array that maps compressed columns to nonempty columns.
auto init_column_compression_arrays(std::size_t column_count, std::span<std::uint32_t const> columns)
    -> std::tuple<std::vector<std::uint32_t>, std::vector<std::uint32_t>>;

// Remap the nonempty columns of a sparse matrix according to a given mapping.
// Parameter ‘columns’ is the column index array of the sparse matrix.
// Parameter ‘nonempty_columns_to_compressed_columns’ maps nonempty columns to
// compressed columns.  Return the a column index array with remapped columns.
// The returned column index array represents a sparse matrix with the same
// number of nonzero elements as the initial matrix but without any empty
// columns.
auto remap_columns(
        std::span<std::uint32_t const> columns, std::span<std::uint32_t const> nonempty_columns_to_compressed_columns
    ) -> std::vector<std::uint32_t>;

// Split the specified local matrix into a diagonal and off-diagonal local
// matrix.  Parameter ‘local_matrix’ specifies the matrix to split.  Parameter
// ‘local_diagonal_column_offset’ specifies the starting column of the
// resulting diagonal matrix.  Parameter ‘local_diagonal_column_count’
// specifies the column count of the resulting diagonal matrix.  Return the
// created local diagonal matrix and the created local off-diagonal matrix.
template<typename T>
auto split_local_matrix(
        csr::Matrix<T, csr::Symmetric::no> const& local_matrix,
        std::size_t local_diagonal_column_offset,
        std::size_t local_diagonal_column_count
    ) -> std::tuple<csr::Matrix<T, csr::Symmetric::no>, csr::Matrix<T, csr::Symmetric::no>>;

}  // namespace

template<typename T>
Matrix<T>::Matrix(
        boost::mpi::communicator comm,
        std::size_t const row_count,
        std::size_t const local_row_offset,
        csr::Matrix<T, csr::Symmetric::no> const& local_matrix,
        hsf::RegionOrdering const region_ordering,
        hsf::OptimiseCsrRegions const optimise_csr_regions
    ) : comm_{std::move(comm)},
        row_count_{row_count},
        column_count_{local_matrix.column_count()},
        local_row_count_{local_matrix.row_count()},
        local_row_offset_{local_row_offset}
{
    auto local_diagonal_column_count = std::size_t{};
    std::tie(this->local_diagonal_column_offset_, local_diagonal_column_count)
        = compute_local_array_offset_size(this->get_communicator(), local_matrix.column_count());

    auto const [local_diagonal_matrix, local_off_diagonal_matrix] = split_local_matrix(
            local_matrix, this->get_local_diagonal_column_offset(), local_diagonal_column_count
        );

    this->local_diagonal_matrix_ = hsf::Matrix<T, hsf::Symmetric::no>{
            local_diagonal_matrix.row_pointers.data(),
            local_diagonal_matrix.columns.data(),
            local_diagonal_matrix.elements.data(),
            local_diagonal_matrix.row_count(),
            local_diagonal_matrix.column_count(),
            local_diagonal_matrix.element_count(),
            region_ordering,
            optimise_csr_regions
        };

    auto local_off_diagonal_nonempty_columns_to_compressed_columns = std::vector<std::uint32_t>{};
    std::tie(local_off_diagonal_nonempty_columns_to_compressed_columns,
             this->local_off_diagonal_compressed_columns_to_nonempty_columns_)
        = init_column_compression_arrays(local_off_diagonal_matrix.column_count(), local_off_diagonal_matrix.columns);

    auto const new_off_diagonal_column_count = this->get_local_off_diagonal_nonempty_column_count();
    auto const new_off_diagonal_columns
        = remap_columns(local_off_diagonal_matrix.columns, local_off_diagonal_nonempty_columns_to_compressed_columns);
    this->local_off_diagonal_matrix_ = hsf::Matrix<T, hsf::Symmetric::no>(
            local_off_diagonal_matrix.row_pointers.data(),
            new_off_diagonal_columns.data(),
            local_off_diagonal_matrix.elements.data(),
            local_off_diagonal_matrix.row_count(),
            new_off_diagonal_column_count,
            local_off_diagonal_matrix.element_count(),
            region_ordering,
            optimise_csr_regions
        );

    auto local_row_count_sum = static_cast<std::uint64_t>(this->get_local_row_count());
    boost::mpi::all_reduce(
            this->get_communicator(), boost::mpi::inplace(local_row_count_sum), std::plus<std::uint64_t>{}
        );
    if (local_row_count_sum != this->get_row_count()) {
        throw LogicError{"Constructing dhsf::Matrix with mismatching local and global row counts."};
    }

    if (this->get_local_row_offset() + this->get_local_row_count() > this->get_row_count()) {
        throw LogicError{"Constructing dhsf::Matrix with an invalid local row offset."};
    }

    if (are_local_column_counts_different(this->get_communicator(), this->get_local_column_count())) {
        throw LogicError{"dhsf::Matrix must be constructed with all local column counts equal."};
    }
}

template<typename T>
Matrix<T>::Matrix(
        MPI_Comm const comm,
        std::size_t const row_count,
        std::size_t const local_row_offset,
        csr::Matrix<T, csr::Symmetric::no> const& local_matrix,
        hsf::RegionOrdering const region_ordering,
        hsf::OptimiseCsrRegions const optimise_csr_regions
    ) : Matrix{
            boost::mpi::communicator{comm, boost::mpi::comm_attach},
            row_count,
            local_row_offset,
            local_matrix,
            region_ordering,
            optimise_csr_regions
        }
{
}

template<typename T>
Matrix<T>::Matrix(
        boost::mpi::communicator comm,
        std::size_t const row_count,
        std::size_t const local_row_offset,
        csr::Matrix<T, csr::Symmetric::no> const& local_matrix,
        hsf::RegionOrdering const region_ordering,
        double const region_coefficient,
        int const thread_count,
        hsf::OptimiseCsrRegions const optimise_csr_regions
    ) : comm_{std::move(comm)},
        row_count_{row_count},
        column_count_{local_matrix.column_count()},
        local_row_count_{local_matrix.row_count()},
        local_row_offset_{local_row_offset}
{
    auto local_diagonal_column_count = std::size_t{};
    std::tie(this->local_diagonal_column_offset_, local_diagonal_column_count)
        = compute_local_array_offset_size(this->get_communicator(), local_matrix.column_count());

    auto const [local_diagonal_matrix, local_off_diagonal_matrix] = split_local_matrix(
            local_matrix, this->get_local_diagonal_column_offset(), local_diagonal_column_count
        );

    this->local_diagonal_matrix_ = hsf::Matrix<T, hsf::Symmetric::no>{
            local_diagonal_matrix.row_pointers.data(),
            local_diagonal_matrix.columns.data(),
            local_diagonal_matrix.elements.data(),
            local_diagonal_matrix.row_count(),
            local_diagonal_matrix.column_count(),
            local_diagonal_matrix.element_count(),
            region_ordering,
            region_coefficient,
            thread_count,
            optimise_csr_regions
        };

    auto local_off_diagonal_nonempty_columns_to_compressed_columns = std::vector<std::uint32_t>{};
    std::tie(local_off_diagonal_nonempty_columns_to_compressed_columns,
             this->local_off_diagonal_compressed_columns_to_nonempty_columns_)
        = init_column_compression_arrays(local_off_diagonal_matrix.column_count(), local_off_diagonal_matrix.columns);

    auto const new_off_diagonal_column_count = this->get_local_off_diagonal_nonempty_column_count();
    auto const new_off_diagonal_columns
        = remap_columns(local_off_diagonal_matrix.columns, local_off_diagonal_nonempty_columns_to_compressed_columns);
    this->local_off_diagonal_matrix_ = hsf::Matrix<T, hsf::Symmetric::no>(
            local_off_diagonal_matrix.row_pointers.data(),
            new_off_diagonal_columns.data(),
            local_off_diagonal_matrix.elements.data(),
            local_off_diagonal_matrix.row_count(),
            new_off_diagonal_column_count,
            local_off_diagonal_matrix.element_count(),
            region_ordering,
            region_coefficient,
            thread_count,
            optimise_csr_regions
        );

    auto local_row_count_sum = static_cast<std::uint64_t>(this->get_local_row_count());
    boost::mpi::all_reduce(
            this->get_communicator(), boost::mpi::inplace(local_row_count_sum), std::plus<std::uint64_t>{}
        );
    if (local_row_count_sum != this->get_row_count()) {
        throw LogicError{"Constructing dhsf::Matrix with mismatching local and global row counts."};
    }

    if (this->get_local_row_offset() + this->get_local_row_count() > this->get_row_count()) {
        throw LogicError{"Constructing dhsf::Matrix with an invalid local row offset."};
    }

    if (are_local_column_counts_different(this->get_communicator(), this->get_local_column_count())) {
        throw LogicError{"dhsf::Matrix must be constructed with all local column counts equal."};
    }
}

template<typename T>
Matrix<T>::Matrix(
        MPI_Comm const comm,
        std::size_t const row_count,
        std::size_t const local_row_offset,
        csr::Matrix<T, csr::Symmetric::no> const& local_matrix,
        hsf::RegionOrdering const region_ordering,
        double const region_coefficient,
        int const thread_count,
        hsf::OptimiseCsrRegions const optimise_csr_regions
    ) : Matrix{
            boost::mpi::communicator{comm, boost::mpi::comm_attach},
            row_count,
            local_row_offset,
            local_matrix,
            region_ordering,
            region_coefficient,
            thread_count,
            optimise_csr_regions
        }
{
}

template<typename T>
auto Matrix<T>::get_communicator() const noexcept -> boost::mpi::communicator const&
{
    return this->comm_;
}

template<typename T>
auto Matrix<T>::get_row_count() const noexcept -> std::size_t
{
    return this->row_count_;
}

template<typename T>
auto Matrix<T>::get_column_count() const noexcept -> std::size_t
{
    return this->column_count_;
}

template<typename T>
auto Matrix<T>::get_local_row_count() const noexcept -> std::size_t
{
    return this->local_row_count_;
}

template<typename T>
auto Matrix<T>::get_local_column_count() const noexcept -> std::size_t
{
    return this->get_column_count();
}

template<typename T>
auto Matrix<T>::get_local_row_offset() const noexcept -> std::size_t
{
    return this->local_row_offset_;
}

template<typename T>
auto Matrix<T>::get_local_diagonal_column_offset() const noexcept -> std::size_t
{
    return this->local_diagonal_column_offset_;
}

template<typename T>
auto Matrix<T>::get_local_diagonal_matrix() noexcept -> hsf::Matrix<T, hsf::Symmetric::no>&
{
    return this->local_diagonal_matrix_;
}

template<typename T>
auto Matrix<T>::get_local_diagonal_matrix() const noexcept -> hsf::Matrix<T, hsf::Symmetric::no> const&
{
    return this->local_diagonal_matrix_;
}

template<typename T>
auto Matrix<T>::get_local_off_diagonal_matrix() noexcept -> hsf::Matrix<T, hsf::Symmetric::no>&
{
    return this->local_off_diagonal_matrix_;
}

template<typename T>
auto Matrix<T>::get_local_off_diagonal_matrix() const noexcept -> hsf::Matrix<T, hsf::Symmetric::no> const&
{
    return this->local_off_diagonal_matrix_;
}

template<typename T>
auto Matrix<T>::get_local_off_diagonal_nonempty_column_count() const noexcept -> std::size_t
{
    return this->get_local_off_diagonal_nonempty_columns().size();
}

template<typename T>
auto Matrix<T>::get_local_off_diagonal_nonempty_columns() const noexcept -> std::span<std::uint32_t const>
{
    return this->local_off_diagonal_compressed_columns_to_nonempty_columns_;
}

template class Matrix<float>;
template class Matrix<double>;

namespace {

auto are_local_column_counts_different(boost::mpi::communicator const& comm, std::size_t const local_column_count)
    -> bool
{
    auto local_column_count_u64 = static_cast<std::uint64_t>(local_column_count);
    auto local_column_counts_differ = 0;

    if (comm.rank() == 0) {
        boost::mpi::broadcast(comm, local_column_count_u64, 0);

    } else {
        auto root_column_count = std::uint64_t{};
        boost::mpi::broadcast(comm, root_column_count, 0);
        if (root_column_count != local_column_count_u64) {
            local_column_counts_differ = 1;
        }
    }

    boost::mpi::all_reduce(comm, boost::mpi::inplace(local_column_counts_differ), boost::mpi::bitwise_or<int>{});
    return local_column_counts_differ;
}

auto init_column_compression_arrays(std::size_t const column_count, std::span<std::uint32_t const> const columns)
    -> std::tuple<std::vector<std::uint32_t>, std::vector<std::uint32_t>>
{
    auto nonempty_columns_to_compressed_columns = std::vector<std::uint32_t>(column_count);
    auto nonempty_column_count = 0_uz;
    for (auto const column : columns) {
        if (nonempty_columns_to_compressed_columns[column] == 0) {
            nonempty_columns_to_compressed_columns[column] = 1;
            ++nonempty_column_count;
        }
    }
    auto compressed_columns_to_nonempty_columns = std::vector<std::uint32_t>(nonempty_column_count);

    auto nonempty_column_index = 0_uz;
    for (auto column = 0_uz; column < column_count; ++column) {
        if (nonempty_columns_to_compressed_columns[column] == 1) {
            assert(column <= std::numeric_limits<std::uint32_t>::max());
            auto const column_u32 = static_cast<std::uint32_t>(column);
            compressed_columns_to_nonempty_columns[nonempty_column_index] = column_u32;
            ++nonempty_column_index;
        }
    }
    assert(nonempty_column_count == nonempty_column_index);

    for (auto i = 0_uz; i < nonempty_column_count; ++i) {
        assert(i <= std::numeric_limits<std::uint32_t>::max());
        auto const i_u32 = static_cast<std::uint32_t>(i);
        nonempty_columns_to_compressed_columns[compressed_columns_to_nonempty_columns[i_u32]] = i_u32;
    }

    return {std::move(nonempty_columns_to_compressed_columns), std::move(compressed_columns_to_nonempty_columns)};
}

auto remap_columns(
        std::span<std::uint32_t const> const columns,
        std::span<std::uint32_t const> const nonempty_columns_to_compressed_columns
    ) -> std::vector<std::uint32_t>
{
    auto remapped_columns = std::vector<std::uint32_t>(columns.size());
    for (auto i = 0_uz; i < columns.size(); ++i) {
        auto const column = columns[i];
        assert(column < nonempty_columns_to_compressed_columns.size());
        auto const remapped_column = nonempty_columns_to_compressed_columns[column];
        remapped_columns[i] = remapped_column;
    }
    return remapped_columns;
}

template<typename T>
auto split_local_matrix(
        csr::Matrix<T, csr::Symmetric::no> const& local_matrix,
        std::size_t const local_diagonal_column_offset,
        std::size_t const local_diagonal_column_count
    ) -> std::tuple<csr::Matrix<T, csr::Symmetric::no>, csr::Matrix<T, csr::Symmetric::no>>
{
    auto const diagonal_columns_begin = local_diagonal_column_offset;
    auto const diagonal_columns_end = local_diagonal_column_offset + local_diagonal_column_count;

    auto diagonal_element_count = csr::info::Index{};
    auto off_diagonal_element_count = csr::info::Index{};
    for (auto row = 0_uz; row < local_matrix.row_count(); ++row) {
        for (auto i = local_matrix.row_pointers[row]; i < local_matrix.row_pointers[row + 1]; ++i) {
            auto const column = local_matrix.columns[i];
            if (column >= diagonal_columns_begin && column < diagonal_columns_end) {
                ++diagonal_element_count;
            } else {
                ++off_diagonal_element_count;
            }
        }
    }

    if (off_diagonal_element_count == 0) {
        // This should only occur when instantiating a ‘dhsf::Matrix’ object
        // using a single process.
        return {local_matrix, csr::Matrix<T, csr::Symmetric::no>{}};
    }

    auto diagonal_row_pointers = std::vector<csr::info::Index>(local_matrix.row_count() + 1);
    auto diagonal_columns = std::vector<csr::info::Index>(diagonal_element_count);
    auto diagonal_elements = std::vector<T>(diagonal_element_count);

    auto off_diagonal_row_pointers = std::vector<csr::info::Index>(local_matrix.row_count() + 1);
    auto off_diagonal_columns = std::vector<csr::info::Index>(off_diagonal_element_count);
    auto off_diagonal_elements = std::vector<T>(off_diagonal_element_count);

    diagonal_element_count = 0;
    off_diagonal_element_count = 0;
    for (auto row = 0_uz; row < local_matrix.row_count(); ++row) {
        for (auto i = local_matrix.row_pointers[row]; i < local_matrix.row_pointers[row + 1]; ++i) {
            auto const column = local_matrix.columns[i];
            auto const element = local_matrix.elements[i];

            if (column >= diagonal_columns_begin && column < diagonal_columns_end) {
                auto const diagonal_columns_begin_u32 = static_cast<csr::info::Index>(diagonal_columns_begin);
                diagonal_columns[diagonal_element_count] = column - diagonal_columns_begin_u32;
                diagonal_elements[diagonal_element_count] = element;
                ++diagonal_element_count;

            } else {
                off_diagonal_columns[off_diagonal_element_count] = column;
                off_diagonal_elements[off_diagonal_element_count] = element;
                ++off_diagonal_element_count;
            }
        }

        diagonal_row_pointers[row + 1] = diagonal_element_count;
        off_diagonal_row_pointers[row + 1] = off_diagonal_element_count;
    }

    auto diagonal_matrix = csr::Matrix<T, csr::Symmetric::no>{
            std::move(diagonal_row_pointers),
            std::move(diagonal_columns),
            std::move(diagonal_elements),
            local_diagonal_column_count
        };
    auto off_diagonal_matrix = csr::Matrix<T, csr::Symmetric::no>{
            std::move(off_diagonal_row_pointers),
            std::move(off_diagonal_columns),
            std::move(off_diagonal_elements),
            local_matrix.column_count()
        };
    return {std::move(diagonal_matrix), std::move(off_diagonal_matrix)};
}

}  // namespace

}  // namespace dhsf
