#include <dhsf/exec_data.hpp>

#include <algorithm>            // std::fill
#include <cassert>              // assert
#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t std::uint64_t
#include <limits>               // std::numeric_limits
#include <numeric>              // std::exclusive_scan
#include <span>                 // std::span
#include <utility>              // std::move
#include <vector>               // std::vector

#include <boost/mpi/collectives/all_to_all.hpp>  // boost::mpi::all_to_all
#include <boost/mpi/communicator.hpp>  // boost::mpi::comm_attach boost::mpi::communicator
#include <boost/mpi/datatype.hpp>  // boost::mpi::get_mpi_datatype

#include <mpi.h>                // MPI_*

#include <hsf/exec_data.hpp>    // hsf::ExecData
#include <hsf/exec_mode.hpp>    // hsf::ExecMode
#include <hsf/symmetric.hpp>    // hsf::Symmetric
#include <hsf/tags.hpp>         // hsf::tags

#include <dhsf/cstddef.hpp>     // dhsf::operator""_uz
#include <dhsf/exec_mode.hpp>   // dhsf::ExecMode
#include <dhsf/matrix.hpp>      // dhsf::Matrix
#include <dhsf/priv/mpi_request_ptr.hpp>  // dhsf::MpiRequestPtr
#include <dhsf/vector.hpp>      // dhsf::Vector

namespace dhsf {

namespace {

// Invoked to initialise ‘dhsf::ExecData::send_counts_’.
template<typename T>
auto init_send_counts(boost::mpi::communicator const& comm, Matrix<T> const& matrix) -> std::vector<int>;

// Invoked to initialise ‘dhsf::ExecData::receive_counts_’.
auto init_receive_counts(boost::mpi::communicator const& comm, std::span<int const> send_counts) -> std::vector<int>;

// Return a displacements array for use with MPI functions.  The resulting
// displacements array is obtained by performing an exclusive scan operation
// over the array given by parameter ‘counts’.
auto init_mpi_displacements(std::span<int const> counts) -> std::vector<int>;

// Invoked to initialise ‘dhsf::ExecData::received_indices_’.
auto init_received_indices(
        std::span<std::uint32_t const> local_nonempty_columns,
        std::span<int const> send_counts,
        std::span<int const> send_displacements,
        std::span<int const> receive_counts,
        std::span<int const> receive_displacements,
        boost::mpi::communicator const& comm
    ) -> std::vector<std::uint32_t>;

// Invoked by ‘dhsf::ExecData::copy_local_x1_elements’.
template<typename T>
auto copy_local_x1_elements_impl(
        Vector<T> const& x1, std::span<std::uint32_t const> received_indices, std::span<T> copied_x1_values
    ) -> void;

// Invoked by ‘dhsf::ExecData::transfer_copied_x1_elements’.
template<typename T>
auto transfer_copied_x1_elements_impl(
        std::span<T const> local_values,
        std::span<int const> send_counts,
        std::span<int const> send_displacements,
        std::span<T> received_values,
        std::span<int const> receive_counts,
        std::span<int const> receive_displacements,
        boost::mpi::communicator const& comm
    ) -> void;

// Invoked by ‘dhsf::ExecData::start_compressed_y2_element_transfer’.
template<typename T>
auto start_compressed_y2_element_transfer_impl(
        std::span<T const> local_values,
        std::span<int const> send_counts,
        std::span<int const> send_displacements,
        std::span<T> received_values,
        std::span<int const> receive_counts,
        std::span<int const> receive_displacements,
        boost::mpi::communicator const& comm,
        MpiRequestPtr& y2_request_ptr
    ) -> void;

// Invoked by ‘dhsf::ExecData::await_compressed_y2_element_transfer’.
auto await_compressed_y2_element_transfer_impl(MpiRequestPtr& y2_request_ptr) -> void;

// Invoked by ‘dhsf::ExecData::store_final_y2_elements’.
template<typename T>
auto store_final_y2_elements_impl(
        std::span<std::uint32_t const> received_indices, std::span<T const> received_y2_values, Vector<T>& y2
    ) -> void;

}  // namespace

template<typename T>
ExecData<ExecMode::sequential, T>::ExecData(boost::mpi::communicator comm, Matrix<T> const& matrix)
    : comm_{std::move(comm)},
      local_x1_values_(matrix.get_local_off_diagonal_nonempty_column_count()),
      local_y2_values_(matrix.get_local_off_diagonal_nonempty_column_count()),
      send_counts_(init_send_counts(this->get_communicator(), matrix)),
      send_displacements_(init_mpi_displacements(this->send_counts_)),
      receive_counts_(init_receive_counts(this->get_communicator(), this->send_counts_)),
      receive_displacements_(init_mpi_displacements(this->receive_counts_)),
      received_indices_(init_received_indices(
                                   matrix.get_local_off_diagonal_nonempty_columns(),
                                   this->send_counts_,
                                   this->send_displacements_,
                                   this->receive_counts_,
                                   this->receive_displacements_,
                                   this->get_communicator()
                               )),
      copied_x1_values_(this->received_indices_.size()),
      received_y2_values_(this->received_indices_.size())
{
}

template<typename T>
ExecData<ExecMode::sequential, T>::ExecData(MPI_Comm const comm, Matrix<T> const& matrix)
    : ExecData{boost::mpi::communicator{comm, boost::mpi::comm_attach}, matrix}
{
}

template<typename T>
auto ExecData<ExecMode::sequential, T>::get_communicator() const noexcept -> boost::mpi::communicator const&
{
    return this->comm_;
}

template<typename T>
auto ExecData<ExecMode::sequential, T>::get_local_compressed_x1() const noexcept -> std::span<T const>
{
    return this->local_x1_values_;
}

template<typename T>
auto ExecData<ExecMode::sequential, T>::get_local_compressed_y2() noexcept -> std::span<T>
{
    return this->local_y2_values_;
}

template<typename T>
auto ExecData<ExecMode::sequential, T>::get_local_compressed_y2() const noexcept -> std::span<T const>
{
    return this->local_y2_values_;
}

template<typename T>
auto ExecData<ExecMode::sequential, T>::copy_local_x1_elements(Vector<T> const& x1) -> void
{
    copy_local_x1_elements_impl<T>(x1, this->received_indices_, this->copied_x1_values_);
}

template<typename T>
auto ExecData<ExecMode::sequential, T>::transfer_copied_x1_elements() -> void
{
    auto const& send_counts = this->receive_counts_;
    auto const& send_displacements = this->receive_displacements_;
    auto const& receive_counts = this->send_counts_;
    auto const& receive_displacements = this->send_displacements_;
    transfer_copied_x1_elements_impl<T>(
            this->copied_x1_values_,
            send_counts,
            send_displacements,
            this->local_x1_values_,
            receive_counts,
            receive_displacements,
            this->get_communicator()
        );
}

template<typename T>
auto ExecData<ExecMode::sequential, T>::start_compressed_y2_element_transfer() -> void
{
    start_compressed_y2_element_transfer_impl<T>(
            this->local_y2_values_,
            this->send_counts_,
            this->send_displacements_,
            this->received_y2_values_,
            this->receive_counts_,
            this->receive_displacements_,
            this->get_communicator(),
            this->y2_request_ptr_
        );
}

template<typename T>
auto ExecData<ExecMode::sequential, T>::await_compressed_y2_element_transfer() -> void
{
    await_compressed_y2_element_transfer_impl(this->y2_request_ptr_);
}

template<typename T>
auto ExecData<ExecMode::sequential, T>::store_final_y2_elements(Vector<T>& y2) -> void
{
    store_final_y2_elements_impl<T>(this->received_indices_, this->received_y2_values_, y2);
}

template<typename T>
auto ExecData<ExecMode::sequential, T>::reset_auxiliary_data() -> void
{
    std::fill(this->local_y2_values_.begin(), this->local_y2_values_.end(), 0);
}

template class ExecData<ExecMode::sequential, float>;
template class ExecData<ExecMode::sequential, double>;

template<typename T>
ExecData<ExecMode::locks_exclusive, T>::ExecData(
        boost::mpi::communicator comm, int const thread_count, Matrix<T> const& matrix
    ) : comm_{std::move(comm)},
        local_x1_values_(matrix.get_local_off_diagonal_nonempty_column_count()),
        local_y2_values_(matrix.get_local_off_diagonal_nonempty_column_count()),
        local_diagonal_exec_data_{thread_count, matrix.get_local_diagonal_matrix(), hsf::tags::two_vectors},
        local_off_diagonal_exec_data_{thread_count, matrix.get_local_off_diagonal_matrix(), hsf::tags::two_vectors},
        send_counts_(init_send_counts(this->get_communicator(), matrix)),
        send_displacements_(init_mpi_displacements(this->send_counts_)),
        receive_counts_(init_receive_counts(this->get_communicator(), this->send_counts_)),
        receive_displacements_(init_mpi_displacements(this->receive_counts_)),
        received_indices_(init_received_indices(
                                     matrix.get_local_off_diagonal_nonempty_columns(),
                                     this->send_counts_,
                                     this->send_displacements_,
                                     this->receive_counts_,
                                     this->receive_displacements_,
                                     this->get_communicator()
                                 )),
        copied_x1_values_(this->received_indices_.size()),
        received_y2_values_(this->received_indices_.size())
{
}

template<typename T>
ExecData<ExecMode::locks_exclusive, T>::ExecData(MPI_Comm const comm, int const thread_count, Matrix<T> const& matrix)
    : ExecData{boost::mpi::communicator{comm, boost::mpi::comm_attach}, thread_count, matrix}
{
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::get_communicator() const noexcept -> boost::mpi::communicator const&
{
    return this->comm_;
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::get_local_compressed_x1() const noexcept -> std::span<T const>
{
    return this->local_x1_values_;
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::get_local_compressed_y2() noexcept -> std::span<T>
{
    return this->local_y2_values_;
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::get_local_compressed_y2() const noexcept -> std::span<T const>
{
    return this->local_y2_values_;
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::get_local_diagonal_exec_data() noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_exclusive, T, hsf::Symmetric::no>&
{
    return this->local_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::get_local_diagonal_exec_data() const noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_exclusive, T, hsf::Symmetric::no> const&
{
    return this->local_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::get_local_off_diagonal_exec_data() noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_exclusive, T, hsf::Symmetric::no>&
{
    return this->local_off_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::get_local_off_diagonal_exec_data() const noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_exclusive, T, hsf::Symmetric::no> const&
{
    return this->local_off_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::copy_local_x1_elements(Vector<T> const& x1) -> void
{
    copy_local_x1_elements_impl<T>(x1, this->received_indices_, this->copied_x1_values_);
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::transfer_copied_x1_elements() -> void
{
    auto const& send_counts = this->receive_counts_;
    auto const& send_displacements = this->receive_displacements_;
    auto const& receive_counts = this->send_counts_;
    auto const& receive_displacements = this->send_displacements_;
    transfer_copied_x1_elements_impl<T>(
            this->copied_x1_values_,
            send_counts,
            send_displacements,
            this->local_x1_values_,
            receive_counts,
            receive_displacements,
            this->get_communicator()
        );
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::start_compressed_y2_element_transfer() -> void
{
    start_compressed_y2_element_transfer_impl<T>(
            this->local_y2_values_,
            this->send_counts_,
            this->send_displacements_,
            this->received_y2_values_,
            this->receive_counts_,
            this->receive_displacements_,
            this->get_communicator(),
            this->y2_request_ptr_
        );
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::await_compressed_y2_element_transfer() -> void
{
    await_compressed_y2_element_transfer_impl(this->y2_request_ptr_);
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::store_final_y2_elements(Vector<T>& y2) -> void
{
    store_final_y2_elements_impl<T>(this->received_indices_, this->received_y2_values_, y2);
}

template<typename T>
auto ExecData<ExecMode::locks_exclusive, T>::reset_auxiliary_data() -> void
{
    std::fill(this->local_y2_values_.begin(), this->local_y2_values_.end(), 0);
}

template class ExecData<ExecMode::locks_exclusive, float>;
template class ExecData<ExecMode::locks_exclusive, double>;

template<typename T>
ExecData<ExecMode::locks_planned_exclusive, T>::ExecData(
        boost::mpi::communicator comm, int const thread_count, Matrix<T> const& matrix
    ) : comm_{std::move(comm)},
        local_x1_values_(matrix.get_local_off_diagonal_nonempty_column_count()),
        local_y2_values_(matrix.get_local_off_diagonal_nonempty_column_count()),
        local_diagonal_exec_data_{thread_count, matrix.get_local_diagonal_matrix(), hsf::tags::two_vectors},
        local_off_diagonal_exec_data_{thread_count, matrix.get_local_off_diagonal_matrix(), hsf::tags::two_vectors},
        send_counts_(init_send_counts(this->get_communicator(), matrix)),
        send_displacements_(init_mpi_displacements(this->send_counts_)),
        receive_counts_(init_receive_counts(this->get_communicator(), this->send_counts_)),
        receive_displacements_(init_mpi_displacements(this->receive_counts_)),
        received_indices_(init_received_indices(
                                     matrix.get_local_off_diagonal_nonempty_columns(),
                                     this->send_counts_,
                                     this->send_displacements_,
                                     this->receive_counts_,
                                     this->receive_displacements_,
                                     this->get_communicator()
                                 )),
        copied_x1_values_(this->received_indices_.size()),
        received_y2_values_(this->received_indices_.size())
{
}

template<typename T>
ExecData<ExecMode::locks_planned_exclusive, T>::ExecData(
        MPI_Comm const comm, int const thread_count, Matrix<T> const& matrix
    ) : ExecData{boost::mpi::communicator{comm, boost::mpi::comm_attach}, thread_count, matrix}
{
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::get_communicator() const noexcept
    -> boost::mpi::communicator const&
{
    return this->comm_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::get_local_compressed_x1() const noexcept -> std::span<T const>
{
    return this->local_x1_values_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::get_local_compressed_y2() noexcept -> std::span<T>
{
    return this->local_y2_values_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::get_local_compressed_y2() const noexcept -> std::span<T const>
{
    return this->local_y2_values_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::get_local_diagonal_exec_data() noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_planned_exclusive, T, hsf::Symmetric::no>&
{
    return this->local_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::get_local_diagonal_exec_data() const noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_planned_exclusive, T, hsf::Symmetric::no> const&
{
    return this->local_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::get_local_off_diagonal_exec_data() noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_planned_exclusive, T, hsf::Symmetric::no>&
{
    return this->local_off_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::get_local_off_diagonal_exec_data() const noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_planned_exclusive, T, hsf::Symmetric::no> const&
{
    return this->local_off_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::copy_local_x1_elements(Vector<T> const& x1) -> void
{
    copy_local_x1_elements_impl<T>(x1, this->received_indices_, this->copied_x1_values_);
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::transfer_copied_x1_elements() -> void
{
    auto const& send_counts = this->receive_counts_;
    auto const& send_displacements = this->receive_displacements_;
    auto const& receive_counts = this->send_counts_;
    auto const& receive_displacements = this->send_displacements_;
    transfer_copied_x1_elements_impl<T>(
            this->copied_x1_values_,
            send_counts,
            send_displacements,
            this->local_x1_values_,
            receive_counts,
            receive_displacements,
            this->get_communicator()
        );
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::start_compressed_y2_element_transfer() -> void
{
    start_compressed_y2_element_transfer_impl<T>(
            this->local_y2_values_,
            this->send_counts_,
            this->send_displacements_,
            this->received_y2_values_,
            this->receive_counts_,
            this->receive_displacements_,
            this->get_communicator(),
            this->y2_request_ptr_
        );
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::await_compressed_y2_element_transfer() -> void
{
    await_compressed_y2_element_transfer_impl(this->y2_request_ptr_);
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::store_final_y2_elements(Vector<T>& y2) -> void
{
    store_final_y2_elements_impl<T>(this->received_indices_, this->received_y2_values_, y2);
}

template<typename T>
auto ExecData<ExecMode::locks_planned_exclusive, T>::reset_auxiliary_data() -> void
{
    std::fill(this->local_y2_values_.begin(), this->local_y2_values_.end(), 0);
}

template class ExecData<ExecMode::locks_planned_exclusive, float>;
template class ExecData<ExecMode::locks_planned_exclusive, double>;

template<typename T>
ExecData<ExecMode::locks_planned_shared, T>::ExecData(
        boost::mpi::communicator comm, int const thread_count, Matrix<T> const& matrix
    ) : comm_{std::move(comm)},
        local_x1_values_(matrix.get_local_off_diagonal_nonempty_column_count()),
        local_y2_values_(matrix.get_local_off_diagonal_nonempty_column_count()),
        local_diagonal_exec_data_{thread_count, matrix.get_local_diagonal_matrix(), hsf::tags::two_vectors},
        local_off_diagonal_exec_data_{thread_count, matrix.get_local_off_diagonal_matrix(), hsf::tags::two_vectors},
        send_counts_(init_send_counts(this->get_communicator(), matrix)),
        send_displacements_(init_mpi_displacements(this->send_counts_)),
        receive_counts_(init_receive_counts(this->get_communicator(), this->send_counts_)),
        receive_displacements_(init_mpi_displacements(this->receive_counts_)),
        received_indices_(init_received_indices(
                                     matrix.get_local_off_diagonal_nonempty_columns(),
                                     this->send_counts_,
                                     this->send_displacements_,
                                     this->receive_counts_,
                                     this->receive_displacements_,
                                     this->get_communicator()
                                 )),
        copied_x1_values_(this->received_indices_.size()),
        received_y2_values_(this->received_indices_.size())
{
}

template<typename T>
ExecData<ExecMode::locks_planned_shared, T>::ExecData(
        MPI_Comm const comm, int const thread_count, Matrix<T> const& matrix
    ) : ExecData{boost::mpi::communicator{comm, boost::mpi::comm_attach}, thread_count, matrix}
{
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::get_communicator() const noexcept -> boost::mpi::communicator const&
{
    return this->comm_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::get_local_compressed_x1() const noexcept -> std::span<T const>
{
    return this->local_x1_values_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::get_local_compressed_y2() noexcept -> std::span<T>
{
    return this->local_y2_values_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::get_local_compressed_y2() const noexcept -> std::span<T const>
{
    return this->local_y2_values_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::get_local_diagonal_exec_data() noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_planned_shared, T, hsf::Symmetric::no>&
{
    return this->local_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::get_local_diagonal_exec_data() const noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_planned_shared, T, hsf::Symmetric::no> const&
{
    return this->local_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::get_local_off_diagonal_exec_data() noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_planned_shared, T, hsf::Symmetric::no>&
{
    return this->local_off_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::get_local_off_diagonal_exec_data() const noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_planned_shared, T, hsf::Symmetric::no> const&
{
    return this->local_off_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::copy_local_x1_elements(Vector<T> const& x1) -> void
{
    copy_local_x1_elements_impl<T>(x1, this->received_indices_, this->copied_x1_values_);
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::transfer_copied_x1_elements() -> void
{
    auto const& send_counts = this->receive_counts_;
    auto const& send_displacements = this->receive_displacements_;
    auto const& receive_counts = this->send_counts_;
    auto const& receive_displacements = this->send_displacements_;
    transfer_copied_x1_elements_impl<T>(
            this->copied_x1_values_,
            send_counts,
            send_displacements,
            this->local_x1_values_,
            receive_counts,
            receive_displacements,
            this->get_communicator()
        );
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::start_compressed_y2_element_transfer() -> void
{
    start_compressed_y2_element_transfer_impl<T>(
            this->local_y2_values_,
            this->send_counts_,
            this->send_displacements_,
            this->received_y2_values_,
            this->receive_counts_,
            this->receive_displacements_,
            this->get_communicator(),
            this->y2_request_ptr_
        );
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::await_compressed_y2_element_transfer() -> void
{
    await_compressed_y2_element_transfer_impl(this->y2_request_ptr_);
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::store_final_y2_elements(Vector<T>& y2) -> void
{
    store_final_y2_elements_impl<T>(this->received_indices_, this->received_y2_values_, y2);
}

template<typename T>
auto ExecData<ExecMode::locks_planned_shared, T>::reset_auxiliary_data() -> void
{
    std::fill(this->local_y2_values_.begin(), this->local_y2_values_.end(), 0);
}

template class ExecData<ExecMode::locks_planned_shared, float>;
template class ExecData<ExecMode::locks_planned_shared, double>;

template<typename T>
ExecData<ExecMode::locks_shared, T>::ExecData(
        boost::mpi::communicator comm, int const thread_count, Matrix<T> const& matrix
    ) : comm_{std::move(comm)},
        local_x1_values_(matrix.get_local_off_diagonal_nonempty_column_count()),
        local_y2_values_(matrix.get_local_off_diagonal_nonempty_column_count()),
        local_diagonal_exec_data_{thread_count, matrix.get_local_diagonal_matrix(), hsf::tags::two_vectors},
        local_off_diagonal_exec_data_{thread_count, matrix.get_local_off_diagonal_matrix(), hsf::tags::two_vectors},
        send_counts_(init_send_counts(this->get_communicator(), matrix)),
        send_displacements_(init_mpi_displacements(this->send_counts_)),
        receive_counts_(init_receive_counts(this->get_communicator(), this->send_counts_)),
        receive_displacements_(init_mpi_displacements(this->receive_counts_)),
        received_indices_(init_received_indices(
                                     matrix.get_local_off_diagonal_nonempty_columns(),
                                     this->send_counts_,
                                     this->send_displacements_,
                                     this->receive_counts_,
                                     this->receive_displacements_,
                                     this->get_communicator()
                                 )),
        copied_x1_values_(this->received_indices_.size()),
        received_y2_values_(this->received_indices_.size())
{
}

template<typename T>
ExecData<ExecMode::locks_shared, T>::ExecData(MPI_Comm const comm, int const thread_count, Matrix<T> const& matrix)
    : ExecData{boost::mpi::communicator{comm, boost::mpi::comm_attach}, thread_count, matrix}
{
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::get_communicator() const noexcept -> boost::mpi::communicator const&
{
    return this->comm_;
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::get_local_compressed_x1() const noexcept -> std::span<T const>
{
    return this->local_x1_values_;
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::get_local_compressed_y2() noexcept -> std::span<T>
{
    return this->local_y2_values_;
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::get_local_compressed_y2() const noexcept -> std::span<T const>
{
    return this->local_y2_values_;
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::get_local_diagonal_exec_data() noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_shared, T, hsf::Symmetric::no>&
{
    return this->local_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::get_local_diagonal_exec_data() const noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_shared, T, hsf::Symmetric::no> const&
{
    return this->local_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::get_local_off_diagonal_exec_data() noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_shared, T, hsf::Symmetric::no>&
{
    return this->local_off_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::get_local_off_diagonal_exec_data() const noexcept
    -> hsf::ExecData<hsf::ExecMode::locks_shared, T, hsf::Symmetric::no> const&
{
    return this->local_off_diagonal_exec_data_;
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::copy_local_x1_elements(Vector<T> const& x1) -> void
{
    copy_local_x1_elements_impl<T>(x1, this->received_indices_, this->copied_x1_values_);
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::transfer_copied_x1_elements() -> void
{
    auto const& send_counts = this->receive_counts_;
    auto const& send_displacements = this->receive_displacements_;
    auto const& receive_counts = this->send_counts_;
    auto const& receive_displacements = this->send_displacements_;
    transfer_copied_x1_elements_impl<T>(
            this->copied_x1_values_,
            send_counts,
            send_displacements,
            this->local_x1_values_,
            receive_counts,
            receive_displacements,
            this->get_communicator()
        );
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::start_compressed_y2_element_transfer() -> void
{
    start_compressed_y2_element_transfer_impl<T>(
            this->local_y2_values_,
            this->send_counts_,
            this->send_displacements_,
            this->received_y2_values_,
            this->receive_counts_,
            this->receive_displacements_,
            this->get_communicator(),
            this->y2_request_ptr_
        );
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::await_compressed_y2_element_transfer() -> void
{
    await_compressed_y2_element_transfer_impl(this->y2_request_ptr_);
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::store_final_y2_elements(Vector<T>& y2) -> void
{
    store_final_y2_elements_impl<T>(this->received_indices_, this->received_y2_values_, y2);
}

template<typename T>
auto ExecData<ExecMode::locks_shared, T>::reset_auxiliary_data() -> void
{
    std::fill(this->local_y2_values_.begin(), this->local_y2_values_.end(), 0);
}

template class ExecData<ExecMode::locks_shared, float>;
template class ExecData<ExecMode::locks_shared, double>;

namespace {

template<typename T>
auto init_send_counts(boost::mpi::communicator const& comm, Matrix<T> const& matrix) -> std::vector<int>
{
    auto const comm_rank_uz = static_cast<std::size_t>(comm.rank());
    auto const comm_size_uz = static_cast<std::size_t>(comm.size());

    auto local_matrix_bounds = std::vector<std::uint64_t>(2 * comm_size_uz);
    local_matrix_bounds[2 * comm_rank_uz] = matrix.get_local_row_offset();
    local_matrix_bounds[2 * comm_rank_uz + 1] = matrix.get_local_row_offset() + matrix.get_local_row_count();
    MPI_Allgather(
            MPI_IN_PLACE,
            int{},
            MPI_DATATYPE_NULL,
            local_matrix_bounds.data(),
            2,
            MPI_UINT64_T,
            comm
        );

    auto local_nonempty_column_it = matrix.get_local_off_diagonal_nonempty_columns().begin();
    auto send_counts = std::vector<int>(comm_size_uz);
    for (auto i = 0_uz; i < comm_size_uz; ++i) {
        auto const end_it = matrix.get_local_off_diagonal_nonempty_columns().end();
        auto const local_matrix_begin = local_matrix_bounds[2 * i];
        auto const local_matrix_end = local_matrix_bounds[2 * i + 1];
        auto send_count = 0;
        while (local_nonempty_column_it != end_it
               && *local_nonempty_column_it >= local_matrix_begin
               && *local_nonempty_column_it < local_matrix_end) {
            ++local_nonempty_column_it;
            ++send_count;
        }
        send_counts[i] = send_count;
    }

    return send_counts;
}

auto init_receive_counts(boost::mpi::communicator const& comm, std::span<int const> const send_counts)
    -> std::vector<int>
{
    auto receive_counts = std::vector<int>(send_counts.size());
    boost::mpi::all_to_all(comm, send_counts.data(), receive_counts.data());
    return receive_counts;
}

auto init_mpi_displacements(std::span<int const> const counts) -> std::vector<int>
{
    auto displacements = std::vector<int>(counts.size());
    std::exclusive_scan(counts.begin(), counts.end(), displacements.begin(), 0);
    return displacements;
}

auto init_received_indices(
        std::span<std::uint32_t const> const local_nonempty_columns,
        std::span<int const> const send_counts,
        std::span<int const> const send_displacements,
        std::span<int const> const receive_counts,
        std::span<int const> const receive_displacements,
        boost::mpi::communicator const& comm
    ) -> std::vector<std::uint32_t>
{
    assert(send_counts.size() == send_displacements.size());
    assert(send_counts.size() == receive_counts.size());
    assert(send_counts.size() == receive_displacements.size());

    auto received_nonempty_columns = std::vector<std::uint32_t>{};
    if (receive_counts.empty() || receive_displacements.empty()) {
        return received_nonempty_columns;
    }

    auto const size = static_cast<std::size_t>(receive_displacements.back() + receive_counts.back());
    received_nonempty_columns.resize(size);
    MPI_Alltoallv(
            local_nonempty_columns.data(),
            send_counts.data(),
            send_displacements.data(),
            MPI_UINT32_T,
            received_nonempty_columns.data(),
            receive_counts.data(),
            receive_displacements.data(),
            MPI_UINT32_T,
            comm
        );

    return received_nonempty_columns;
}

template<typename T>
auto copy_local_x1_elements_impl(
        Vector<T> const& x1, std::span<std::uint32_t const> const received_indices, std::span<T> const copied_x1_values
    ) -> void
{
    assert(x1.get_element_count() <= std::numeric_limits<std::uint32_t>::max());
    assert(received_indices.size() == copied_x1_values.size());

    auto const local_index_offset = static_cast<std::uint32_t>(x1.get_local_index_offset());
    auto const local_x1 = x1.get_local_vector();
    for (auto i = 0_uz; i < received_indices.size(); ++i) {
        auto const local_index = received_indices[i] - local_index_offset;
        copied_x1_values[i] = local_x1[local_index];
    }
}

template<typename T>
auto transfer_copied_x1_elements_impl(
        std::span<T const> const local_values,
        std::span<int const> const send_counts,
        std::span<int const> const send_displacements,
        std::span<T> const received_values,
        std::span<int const> const receive_counts,
        std::span<int const> const receive_displacements,
        boost::mpi::communicator const& comm
    ) -> void
{
    assert(send_counts.size() == send_displacements.size());
    assert(send_counts.size() == receive_counts.size());
    assert(send_counts.size() == receive_displacements.size());

    MPI_Alltoallv(
            local_values.data(),
            send_counts.data(),
            send_displacements.data(),
            boost::mpi::get_mpi_datatype(T{}),
            received_values.data(),
            receive_counts.data(),
            receive_displacements.data(),
            boost::mpi::get_mpi_datatype(T{}),
            comm
        );
}

template<typename T>
auto start_compressed_y2_element_transfer_impl(
        std::span<T const> const local_values,
        std::span<int const> const send_counts,
        std::span<int const> const send_displacements,
        std::span<T> const received_values,
        std::span<int const> const receive_counts,
        std::span<int const> const receive_displacements,
        boost::mpi::communicator const& comm,
        MpiRequestPtr& y2_request_ptr
    ) -> void
{
    assert(send_counts.size() == send_displacements.size());
    assert(send_counts.size() == receive_counts.size());
    assert(send_counts.size() == receive_displacements.size());
    assert(y2_request_ptr.get_reference() == MPI_REQUEST_NULL);

    MPI_Ialltoallv(
            local_values.data(),
            send_counts.data(),
            send_displacements.data(),
            boost::mpi::get_mpi_datatype(T{}),
            received_values.data(),
            receive_counts.data(),
            receive_displacements.data(),
            boost::mpi::get_mpi_datatype(T{}),
            comm,
            y2_request_ptr.get()
        );
}

auto await_compressed_y2_element_transfer_impl(MpiRequestPtr& y2_request_ptr) -> void
{
    assert(y2_request_ptr.get_reference() != MPI_REQUEST_NULL);
    MPI_Wait(y2_request_ptr.get(), MPI_STATUS_IGNORE);
}

template<typename T>
auto store_final_y2_elements_impl(
        std::span<std::uint32_t const> const received_indices,
        std::span<T const> const received_y2_values,
        Vector<T>& y2
    ) -> void
{
    assert(received_indices.size() == received_y2_values.size());
    assert(y2.get_element_count() <= std::numeric_limits<std::uint32_t>::max());

    auto const local_index_offset = static_cast<std::uint32_t>(y2.get_local_index_offset());
    auto const local_y2 = y2.get_local_vector();
    for (auto i = 0_uz; i < received_indices.size(); ++i) {
        auto const local_index = received_indices[i] - local_index_offset;
        local_y2[local_index] += received_y2_values[i];
    }
}

}  // namespace

}  // namespace dhsf
