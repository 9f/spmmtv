#include <dhsf/priv/mpi_request_ptr.hpp>

#include <utility>              // std::move

#include <boost/core/swap.hpp>  // boost::swap

#include <mpi.h>                // MPI_REQUEST_NULL MPI_Request MPI_Request_free

namespace dhsf {

MpiRequestPtr::~MpiRequestPtr() noexcept
{
    if (this->get_reference() != MPI_REQUEST_NULL) {
        MPI_Request_free(this->get());
    }
}

MpiRequestPtr::MpiRequestPtr(MpiRequestPtr&& other) noexcept
{
    *this = std::move(other);
}

auto MpiRequestPtr::operator=(MpiRequestPtr&& other) noexcept -> MpiRequestPtr&
{
    boost::swap(*this, other);
    return *this;
}

auto MpiRequestPtr::get() noexcept -> MPI_Request*
{
    return &this->mpi_request_;
}

auto MpiRequestPtr::get() const noexcept -> MPI_Request const*
{
    return &this->mpi_request_;
}

auto MpiRequestPtr::get_reference() noexcept -> MPI_Request&
{
    return this->mpi_request_;
}

auto MpiRequestPtr::get_reference() const noexcept -> MPI_Request const&
{
    return this->mpi_request_;
}

auto swap(MpiRequestPtr& lhs, MpiRequestPtr& rhs) noexcept -> void
{
    boost::swap(lhs.mpi_request_, rhs.mpi_request_);
}

}  // namespace dhsf
