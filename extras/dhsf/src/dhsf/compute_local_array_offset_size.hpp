#pragma once

#include <cstddef>              // std::size_t
#include <tuple>                // std::tuple

#include <boost/mpi/communicator.hpp>  // boost::mpi::communicator

namespace dhsf {

// Calculate an offset and a size which describe the calling process’s local
// part of a distributed array.  Parameter ‘comm’ specifies the communicator
// where the array is distributed.  Parameter ‘global_size’ specifies the total
// element count of the distributed array.  The first returned value is the
// offset within the distributed array where the local part belonging to the
// calling process starts.  The second returned value is the number of elements
// of the local part of the distributed array that belong to the calling
// process.
auto compute_local_array_offset_size(boost::mpi::communicator const& comm, std::size_t global_size)
    -> std::tuple<std::size_t, std::size_t>;

}  // namespace dhsf
