import sys

import click
import h5py
import numpy
import scipy.sparse

from . import VERSION


@click.command()
@click.argument('matrix_file', type=click.Path(exists=True, dir_okay=False))
@click.argument('target_file', type=click.Path(dir_okay=False, writable=True))
@click.option(
    '-e',
    '--element-type',
    required=True,
    type=click.Choice(['float32', 'float64']),
    help='The element type used to calculate the results.'
)
@click.help_option('-h', '--help')
@click.version_option(VERSION)
def main(matrix_file, target_file, element_type):
    """Perform the same calculations that are performed in the tests of the SpMMᵀV
    project and store the results to an HDF5 file.  The resulting file can be
    used by the tests to verify the results of SpMMᵀV.  Argument MATRIX_FILE is
    expected to be a path to a general COO matrix stored in an HDF5 file.  The
    results are stored to the path specified by TARGET_FILE.
    """
    matrix = _load_matrix(matrix_file, element_type)

    with h5py.File(target_file, 'w') as f:
        y = matrix @ numpy.full(matrix.shape[0], 3.14, element_type)
        f['y1 SpMV'] = y

        matrix = matrix.transpose()
        y = matrix @ numpy.full(matrix.shape[0], 3.14, element_type)
        f['y1 SpMTV'] = y

        y = matrix @ numpy.full(matrix.shape[0], -2.72, element_type)
        f['y2 SpMTV'] = y

        mean_magnitude = _compute_absolute_value_norm(y) / matrix.nnz
        f['mean nonzero element magnitude'] = numpy.dtype(element_type).type(mean_magnitude)


def _compute_absolute_value_norm(array):
    return abs(array).sum(dtype=numpy.float64)


def _load_matrix(matrix_file, element_type):
    with h5py.File(matrix_file, 'r') as f:
        if f.attrs['matrix format'].decode('utf-8') != 'COO':
            sys.exit('Error: the specified matrix isn’t a COO matrix.')
        if f.attrs['symmetric'] != False:
            sys.exit('Error: the specified COO matrix is not general.')
        if f.attrs['element count'] != f['row indices'].size:
            sys.exit('Error: invalid element count or row index array.')
        if f.attrs['element count'] != f['column indices'].size:
            sys.exit('Error: invalid element count or column index array.')
        if f.attrs['element count'] != f['element values'].size:
            sys.exit('Error: invalid element count or element value array.')

        arrays = (f['element values'].astype(element_type)[...], (f['row indices'], f['column indices']))
        shape = (f.attrs['row count'], f.attrs['column count'])
        return scipy.sparse.coo_matrix(arrays, shape)
