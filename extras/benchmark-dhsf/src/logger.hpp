#pragma once

#include <chrono>               // std::chrono
#include <cstddef>              // std::ptrdiff_t std::size_t
#include <vector>               // std::vector

#include <dhsf/spmmtv_logger.hpp>  // dhsf::SpmmtvLogger

class Logger {
public:
    Logger() = default;
    explicit Logger(int iteration_count);

    auto compute_durations() -> void;

    auto get_iteration_count() const noexcept -> std::size_t;
    auto get_iteration_count_z() const noexcept -> std::ptrdiff_t;

    auto get_end_to_end_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto get_copy_x1_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto get_transfer_x1_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto get_off_diagonal_spmmtv_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto get_diagonal_spmmtv_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto get_start_y2_transfer_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto get_await_y2_transfer_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto get_store_y2_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto get_clean_up_duration() const noexcept -> std::chrono::steady_clock::duration const&;

    auto set_spmmtv_logger(int iteration, dhsf::SpmmtvLogger&& spmmtv_logger) -> void;

private:
    std::vector<dhsf::SpmmtvLogger> spmmtv_loggers_{};
    std::chrono::steady_clock::duration end_to_end_duration_{};
    std::chrono::steady_clock::duration copy_x1_duration_{};
    std::chrono::steady_clock::duration transfer_x1_duration_{};
    std::chrono::steady_clock::duration off_diagonal_spmmtv_duration_{};
    std::chrono::steady_clock::duration diagonal_spmmtv_duration_{};
    std::chrono::steady_clock::duration start_y2_transfer_duration_{};
    std::chrono::steady_clock::duration await_y2_transfer_duration_{};
    std::chrono::steady_clock::duration store_y2_duration_{};
    std::chrono::steady_clock::duration clean_up_duration_{};
};
