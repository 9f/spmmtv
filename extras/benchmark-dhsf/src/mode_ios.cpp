#include <mode_ios.hpp>

#include <istream>              // std::istream
#include <map>                  // std::map
#include <ostream>              // std::ostream
#include <stdexcept>            // std::logic_error std::out_of_range
#include <string>               // std::string
#include <string_view>          // std::string_view

#include <mode.hpp>             // Mode

namespace {

auto const modes = std::map<std::string_view, Mode>{
    {"sequential",              Mode::sequential},
    {"locks-exclusive",         Mode::locks_exclusive},
    {"locks-planned-exclusive", Mode::locks_planned_exclusive},
    {"locks-planned-shared",    Mode::locks_planned_shared},
    {"locks-shared",            Mode::locks_shared},
};

}  // namespace

auto operator<<(std::ostream& os, Mode const mode) -> std::ostream&
{
    switch (mode) {
    case Mode::sequential:
        return os << "sequential";
    case Mode::locks_exclusive:
        return os << "locks-exclusive";
    case Mode::locks_planned_exclusive:
        return os << "locks-planned-exclusive";
    case Mode::locks_planned_shared:
        return os << "locks-planned-shared";
    case Mode::locks_shared:
        return os << "locks-shared";
    }

    throw std::logic_error{"Unexpected enumeration value."};
}

auto operator>>(std::istream& is, Mode& mode) -> std::istream&
{
    auto token = std::string{};
    is >> token;
    try {
        mode = modes.at(token);
    } catch (std::out_of_range const& e) {
        is.setstate(std::istream::failbit);
    }
    return is;
}
