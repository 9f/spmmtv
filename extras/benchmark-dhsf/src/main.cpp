#include <array>                // std::array
#include <chrono>               // std::chrono
#include <exception>            // std::exception
#include <iostream>             // std::cerr std::cout
#include <string>               // std::string_literals

#include <boost/mpi/communicator.hpp>  // boost::mpi::communicator
#include <boost/mpi/environment.hpp>  // boost::mpi::environment boost::mpi::threading
#include <boost/program_options/errors.hpp>  // boost::program_options::error

#include <mpi.h>                // MPI_*

#include <omp.h>                // omp_set_num_threads

#include <csr/symmetric.hpp>    // csr::Symmetric

#include <dcsr/io.hpp>          // dcsr::load_from_hdf5

#include <dhsf/exec_mode.hpp>   // dhsf::ExecMode
#include <dhsf/exec_data.hpp>   // dhsf::ExecData
#include <dhsf/matrix.hpp>      // dhsf::Matrix
#include <dhsf/vector.hpp>      // dhsf::Vector

#include <hsf/region_ordering.hpp>  // hsf::RegionOrdering

#include <benchmark.hpp>        // run_spmmtv_benchmark
#include <command_line_argument_error.hpp>  // CommandLineArgumentError
#include <command_line_arguments.hpp>  // CommandLineArguments

using namespace std::string_literals;

namespace {

auto load_matrix_sequential(boost::mpi::communicator const& comm, CommandLineArguments const& command_line_arguments)
    -> dhsf::Matrix<double>;

auto load_matrix_parallel(boost::mpi::communicator const& comm, CommandLineArguments const& command_line_arguments)
    -> dhsf::Matrix<double>;

auto print_summary(boost::mpi::communicator const& comm, Logger const& logger) -> void;

auto run_benchmark(boost::mpi::communicator const& comm, CommandLineArguments const& command_line_arguments) -> void;

auto run_benchmark_sequential(boost::mpi::communicator const& comm, CommandLineArguments const& command_line_arguments)
    -> void;

template<dhsf::ExecMode EM>
auto run_benchmark_parallel(boost::mpi::communicator const& comm, CommandLineArguments const& command_line_arguments)
    -> void;

}  // namespace

auto main(int argc, char** argv) -> int try
{
    auto const thread_level = boost::mpi::threading::funneled;
    auto const env = boost::mpi::environment{argc, argv, thread_level, false};
    auto const comm = boost::mpi::communicator{};

    try {
        if (boost::mpi::environment::thread_level() < thread_level) {
            if (comm.rank() == 0) {
                std::cerr << "The used MPI library doesn’t provide the required thread support level.\n";
            }
            return 1;
        }

        auto const command_line_arguments = CommandLineArguments::parse(argc, argv);
        if (command_line_arguments.has_help()) {
            if (comm.rank() == 0) {
                std::cout << command_line_arguments.get_help_message();
            }
        } else {
            run_benchmark(comm, command_line_arguments);
        }

    } catch (boost::program_options::error const& e) {
        if (comm.rank() == 0) {
            std::cerr << "Error: " << e.what() << ".\n";
        }
        return 2;

    } catch (CommandLineArgumentError const& e) {
        if (comm.rank() == 0) {
            std::cerr << "Error: " << e.what() << '\n';
        }
        return 2;

    } catch (std::exception const& e) {
        std::cerr << "Error: "s + e.what() + '\n';
        boost::mpi::environment::abort(1);
    }

    return 0;

} catch (std::exception const& e) {
    std::cerr << "Error: " << e.what() << '\n';
    return 1;
}

namespace {

auto load_matrix_sequential(boost::mpi::communicator const& comm, CommandLineArguments const& command_line_arguments)
    -> dhsf::Matrix<double>
{
    auto const& matrix_path = command_line_arguments.get_matrix_path();
    auto const [csr_matrix, row_offset, global_row_count, global_element_count]
        = dcsr::load_from_hdf5<double, csr::Symmetric::no>(comm, matrix_path);
    return {comm, global_row_count, row_offset, csr_matrix, hsf::RegionOrdering::block_lexicographical};
}

auto load_matrix_parallel(boost::mpi::communicator const& comm, CommandLineArguments const& command_line_arguments)
    -> dhsf::Matrix<double>
{
    auto const& matrix_path = command_line_arguments.get_matrix_path();
    auto const [csr_matrix, row_offset, global_row_count, global_element_count]
        = dcsr::load_from_hdf5<double, csr::Symmetric::no>(comm, matrix_path);
    return {comm,
            global_row_count,
            row_offset,
            csr_matrix,
            hsf::RegionOrdering::block_lexicographical,
            command_line_arguments.get_region_coefficient(),
            command_line_arguments.get_thread_count()};
}

auto print_summary(boost::mpi::communicator const& comm, Logger const& logger) -> void
{
    auto duration_counts = std::array{
            std::chrono::duration<double>{logger.get_end_to_end_duration()}.count(),
            std::chrono::duration<double>{logger.get_copy_x1_duration()}.count(),
            std::chrono::duration<double>{logger.get_transfer_x1_duration()}.count(),
            std::chrono::duration<double>{logger.get_off_diagonal_spmmtv_duration()}.count(),
            std::chrono::duration<double>{logger.get_start_y2_transfer_duration()}.count(),
            std::chrono::duration<double>{logger.get_diagonal_spmmtv_duration()}.count(),
            std::chrono::duration<double>{logger.get_await_y2_transfer_duration()}.count(),
            std::chrono::duration<double>{logger.get_store_y2_duration()}.count(),
            std::chrono::duration<double>{logger.get_clean_up_duration()}.count(),
        };

    if (comm.rank() == 0) {
        MPI_Reduce(MPI_IN_PLACE, duration_counts.data(), duration_counts.size(), MPI_DOUBLE, MPI_MAX, 0, comm);
        std::cout << "the run time of a single SpMMᵀV iteration in seconds:\n"
                  << duration_counts[0] << '\n'
                  << "seconds spent preparing copies of vector x1:\n"
                  << duration_counts[1] << '\n'
                  << "seconds spent transferring copied x1 vector elements:\n"
                  << duration_counts[2] << '\n'
                  << "seconds spent performing mathematical operations with the off-diagonal matrix:\n"
                  << duration_counts[3] << '\n'
                  << "seconds spent initiating the transfer of y2 vector contributions:\n"
                  << duration_counts[4] << '\n'
                  << "seconds spent performing mathematical operations with the diagonal matrix:\n"
                  << duration_counts[5] << '\n'
                  << "seconds spent waiting for the transfer of y2 vector contributions to finish:\n"
                  << duration_counts[6] << '\n'
                  << "seconds spent storing results to vector y2:\n"
                  << duration_counts[7] << '\n'
                  << "seconds spent preparing internal data for the next potential iteration:\n"
                  << duration_counts[8] << '\n';

    } else {
        MPI_Reduce(duration_counts.data(), nullptr, duration_counts.size(), MPI_DOUBLE, MPI_MAX, 0, comm);
    }
}

auto run_benchmark(boost::mpi::communicator const& comm, CommandLineArguments const& command_line_arguments) -> void
{
    switch (command_line_arguments.get_mode()) {
    case Mode::sequential:
        return run_benchmark_sequential(comm, command_line_arguments);

    case Mode::locks_exclusive:
        return run_benchmark_parallel<dhsf::ExecMode::locks_exclusive>(comm, command_line_arguments);

    case Mode::locks_planned_exclusive:
        return run_benchmark_parallel<dhsf::ExecMode::locks_planned_exclusive>(comm, command_line_arguments);

    case Mode::locks_planned_shared:
        return run_benchmark_parallel<dhsf::ExecMode::locks_planned_shared>(comm, command_line_arguments);

    case Mode::locks_shared:
        return run_benchmark_parallel<dhsf::ExecMode::locks_shared>(comm, command_line_arguments);
    }
}

auto run_benchmark_sequential(boost::mpi::communicator const& comm, CommandLineArguments const& command_line_arguments)
    -> void
{
    auto const matrix = load_matrix_sequential(comm, command_line_arguments);
    auto exec_data = dhsf::ExecData<dhsf::ExecMode::sequential, double>{comm, matrix};
    auto const x1 = dhsf::Vector<double>{comm, matrix.get_column_count(), 3.14};
    auto const x2 = dhsf::Vector<double>{comm, matrix.get_row_count(), matrix.get_local_row_count(), -2.72};
    auto y1 = dhsf::Vector<double>{comm, matrix.get_row_count(), matrix.get_local_row_count()};
    auto y2 = dhsf::Vector<double>{comm, matrix.get_column_count()};

    auto const iteration_count = command_line_arguments.get_iteration_count();
    auto const logger = run_spmmtv_benchmark(iteration_count, matrix, x1, x2, y1, y2, exec_data);
    print_summary(comm, logger);
}

template<dhsf::ExecMode EM>
auto run_benchmark_parallel(boost::mpi::communicator const& comm, CommandLineArguments const& command_line_arguments)
    -> void
{
    auto const thread_count = command_line_arguments.get_thread_count();
    omp_set_num_threads(thread_count);

    auto const matrix = load_matrix_parallel(comm, command_line_arguments);
    auto exec_data = dhsf::ExecData<EM, double>{comm, thread_count, matrix};
    auto const x1 = dhsf::Vector<double>{comm, matrix.get_column_count(), 3.14};
    auto const x2 = dhsf::Vector<double>{comm, matrix.get_row_count(), matrix.get_local_row_count(), -2.72};
    auto y1 = dhsf::Vector<double>{comm, matrix.get_row_count(), matrix.get_local_row_count()};
    auto y2 = dhsf::Vector<double>{comm, matrix.get_column_count()};

    auto const iteration_count = command_line_arguments.get_iteration_count();
    auto const logger = run_spmmtv_benchmark(iteration_count, matrix, x1, x2, y1, y2, exec_data);
    print_summary(comm, logger);
}

}  // namespace
