#pragma once

enum struct Mode {
    sequential,
    locks_exclusive,
    locks_planned_exclusive,
    locks_planned_shared,
    locks_shared,
};
