#include <benchmark.hpp>

#include <utility>              // std::move

#include <dhsf/compute.hpp>     // dhsf::compute
#include <dhsf/exec_data.hpp>   // dhsf::ExecData
#include <dhsf/exec_mode.hpp>   // dhsf::ExecMode
#include <dhsf/matrix.hpp>      // dhsf::Matrix
#include <dhsf/vector.hpp>      // dhsf::Vector

#include <logger.hpp>           // Logger

template<dhsf::ExecMode EM>
auto run_spmmtv_benchmark(
        int const iteration_count,
        dhsf::Matrix<double> const& matrix,
        dhsf::Vector<double> const& x1,
        dhsf::Vector<double> const& x2,
        dhsf::Vector<double>& y1,
        dhsf::Vector<double>& y2,
        dhsf::ExecData<EM, double>& exec_data
    ) -> Logger
{
    auto logger = Logger{iteration_count};

    // A warm-up iteration.
    auto spmmtv_logger = dhsf::compute::spmmtv_logged(matrix, x1, x2, y1, y2, exec_data);
    logger.set_spmmtv_logger(0, std::move(spmmtv_logger));

    for (auto i = 0; i < iteration_count; ++i) {
        spmmtv_logger = dhsf::compute::spmmtv_logged(matrix, x1, x2, y1, y2, exec_data);
        logger.set_spmmtv_logger(i, std::move(spmmtv_logger));
    }

    logger.compute_durations();
    return logger;
}

template auto run_spmmtv_benchmark<dhsf::ExecMode::sequential>(
        int iteration_count,
        dhsf::Matrix<double> const& matrix,
        dhsf::Vector<double> const& x1,
        dhsf::Vector<double> const& x2,
        dhsf::Vector<double>& y1,
        dhsf::Vector<double>& y2,
        dhsf::ExecData<dhsf::ExecMode::sequential, double>& exec_data
    ) -> Logger;

template auto run_spmmtv_benchmark<dhsf::ExecMode::locks_exclusive>(
        int iteration_count,
        dhsf::Matrix<double> const& matrix,
        dhsf::Vector<double> const& x1,
        dhsf::Vector<double> const& x2,
        dhsf::Vector<double>& y1,
        dhsf::Vector<double>& y2,
        dhsf::ExecData<dhsf::ExecMode::locks_exclusive, double>& exec_data
    ) -> Logger;

template auto run_spmmtv_benchmark<dhsf::ExecMode::locks_planned_exclusive>(
        int iteration_count,
        dhsf::Matrix<double> const& matrix,
        dhsf::Vector<double> const& x1,
        dhsf::Vector<double> const& x2,
        dhsf::Vector<double>& y1,
        dhsf::Vector<double>& y2,
        dhsf::ExecData<dhsf::ExecMode::locks_planned_exclusive, double>& exec_data
    ) -> Logger;

template auto run_spmmtv_benchmark<dhsf::ExecMode::locks_planned_shared>(
        int iteration_count,
        dhsf::Matrix<double> const& matrix,
        dhsf::Vector<double> const& x1,
        dhsf::Vector<double> const& x2,
        dhsf::Vector<double>& y1,
        dhsf::Vector<double>& y2,
        dhsf::ExecData<dhsf::ExecMode::locks_planned_shared, double>& exec_data
    ) -> Logger;

template auto run_spmmtv_benchmark<dhsf::ExecMode::locks_shared>(
        int iteration_count,
        dhsf::Matrix<double> const& matrix,
        dhsf::Vector<double> const& x1,
        dhsf::Vector<double> const& x2,
        dhsf::Vector<double>& y1,
        dhsf::Vector<double>& y2,
        dhsf::ExecData<dhsf::ExecMode::locks_shared, double>& exec_data
    ) -> Logger;
