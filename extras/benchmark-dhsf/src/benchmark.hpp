#pragma once

#include <logger.hpp>           // Logger

namespace dhsf {

enum struct ExecMode;
template<ExecMode EM, typename T> class ExecData;
template<typename T> class Matrix;
template<typename T> class Vector;

}  // namespace dhsf

template<dhsf::ExecMode EM>
auto run_spmmtv_benchmark(
        int iteration_count,
        dhsf::Matrix<double> const& matrix,
        dhsf::Vector<double> const& x1,
        dhsf::Vector<double> const& x2,
        dhsf::Vector<double>& y1,
        dhsf::Vector<double>& y2,
        dhsf::ExecData<EM, double>& exec_data
    ) -> Logger;
