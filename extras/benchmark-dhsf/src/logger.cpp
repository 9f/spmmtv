#include <logger.hpp>

#include <cassert>              // assert
#include <chrono>               // std::chrono
#include <cstddef>              // std::ptrdiff_t std::size_t
#include <utility>              // std::move

#include <dhsf/spmmtv_logger.hpp>  // dhsf::SpmmtvLogger

Logger::Logger(int const iteration_count)
    : spmmtv_loggers_(static_cast<std::size_t>(iteration_count))
{
    assert(iteration_count >= 0);
}

auto Logger::compute_durations() -> void
{
    if (this->get_iteration_count() == 0) {
        return;
    }

    this->end_to_end_duration_ = std::chrono::steady_clock::duration{};
    this->copy_x1_duration_ = std::chrono::steady_clock::duration{};
    this->transfer_x1_duration_ = std::chrono::steady_clock::duration{};
    this->off_diagonal_spmmtv_duration_ = std::chrono::steady_clock::duration{};
    this->diagonal_spmmtv_duration_ = std::chrono::steady_clock::duration{};
    this->start_y2_transfer_duration_ = std::chrono::steady_clock::duration{};
    this->await_y2_transfer_duration_ = std::chrono::steady_clock::duration{};
    this->store_y2_duration_ = std::chrono::steady_clock::duration{};
    this->clean_up_duration_ = std::chrono::steady_clock::duration{};

    for (auto& spmmtv_logger : this->spmmtv_loggers_) {
        spmmtv_logger.compute_durations();
        this->end_to_end_duration_ += spmmtv_logger.get_end_to_end_duration();
        this->copy_x1_duration_ += spmmtv_logger.get_copy_x1_duration();
        this->transfer_x1_duration_ += spmmtv_logger.get_transfer_x1_duration();
        this->off_diagonal_spmmtv_duration_ += spmmtv_logger.get_off_diagonal_spmmtv_duration();
        this->diagonal_spmmtv_duration_ += spmmtv_logger.get_diagonal_spmmtv_duration();
        this->start_y2_transfer_duration_ += spmmtv_logger.get_start_y2_transfer_duration();
        this->await_y2_transfer_duration_ += spmmtv_logger.get_await_y2_transfer_duration();
        this->store_y2_duration_ += spmmtv_logger.get_store_y2_duration();
        this->clean_up_duration_ += spmmtv_logger.get_clean_up_duration();
    }

    this->end_to_end_duration_ /= this->get_iteration_count_z();
    this->copy_x1_duration_ /= this->get_iteration_count_z();
    this->transfer_x1_duration_ /= this->get_iteration_count_z();
    this->off_diagonal_spmmtv_duration_ /= this->get_iteration_count_z();
    this->diagonal_spmmtv_duration_ /= this->get_iteration_count_z();
    this->start_y2_transfer_duration_ /= this->get_iteration_count_z();
    this->await_y2_transfer_duration_ /= this->get_iteration_count_z();
    this->store_y2_duration_ /= this->get_iteration_count_z();
    this->clean_up_duration_ /= this->get_iteration_count_z();
}

auto Logger::get_iteration_count() const noexcept -> std::size_t
{
    return this->spmmtv_loggers_.size();
}

auto Logger::get_iteration_count_z() const noexcept -> std::ptrdiff_t
{
    return static_cast<std::ptrdiff_t>(this->get_iteration_count());
}

auto Logger::get_end_to_end_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->end_to_end_duration_;
}

auto Logger::get_copy_x1_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->copy_x1_duration_;
}

auto Logger::get_transfer_x1_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->transfer_x1_duration_;
}

auto Logger::get_off_diagonal_spmmtv_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->off_diagonal_spmmtv_duration_;
}

auto Logger::get_diagonal_spmmtv_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->diagonal_spmmtv_duration_;
}

auto Logger::get_start_y2_transfer_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->start_y2_transfer_duration_;
}

auto Logger::get_await_y2_transfer_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->await_y2_transfer_duration_;
}

auto Logger::get_store_y2_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->store_y2_duration_;
}

auto Logger::get_clean_up_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->clean_up_duration_;
}

auto Logger::set_spmmtv_logger(int const iteration, dhsf::SpmmtvLogger&& spmmtv_logger) -> void
{
    assert(iteration >= 0);
    auto const iteration_uz = static_cast<std::size_t>(iteration);
    assert(iteration_uz < this->spmmtv_loggers_.size());
    this->spmmtv_loggers_[iteration_uz] = std::move(spmmtv_logger);
}
