#pragma once

#include <filesystem>           // std::filesystem
#include <string>               // std::string

#include <mode.hpp>             // Mode

class CommandLineArguments {
public:
    static auto parse(int argc, char const* const* argv) -> CommandLineArguments;

    CommandLineArguments() = default;

    auto has_help() const noexcept -> bool;
    auto get_help_message() const noexcept -> std::string const&;
    auto get_iteration_count() const noexcept -> int;
    auto get_matrix_path() const noexcept -> std::filesystem::path const&;
    auto get_mode() const noexcept -> Mode;
    auto get_region_coefficient() const noexcept -> double;
    auto get_thread_count() const noexcept -> int;

private:
    // Arguments from positional command-line options.
    std::filesystem::path matrix_path_{};

    // Arguments from nonpositional command-line options.
    bool help_{};
    int iteration_count_{};
    Mode mode_{};
    double region_coefficient_{};
    int thread_count_{};

    std::string help_message_{};

    explicit CommandLineArguments(std::string&& help_message) noexcept;
    CommandLineArguments(
            std::filesystem::path const& matrix_path,
            int iteration_count,
            Mode mode,
            double region_coefficient,
            int thread_count
        );
};
