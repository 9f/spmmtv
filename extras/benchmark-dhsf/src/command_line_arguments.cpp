#include <command_line_arguments.hpp>

#include <filesystem>           // std::filesystem
#include <sstream>              // std::ostringstream
#include <string>               // std::string std::string_literals
#include <utility>              // std::move

#include <boost/program_options.hpp>  // boost::program_options

#include <omp.h>                // omp_get_max_threads omp_get_thread_limit

#include <command_line_argument_error.hpp>  // CommandLineArgumentError
#include <mode.hpp>             // Mode
#include <mode_ios.hpp>         // operator>>

using namespace std::string_literals;

namespace po = boost::program_options;

auto CommandLineArguments::parse(int const argc, char const* const* const argv) -> CommandLineArguments
{
    auto positional = po::options_description{};
    positional.add_options()
        ("matrix-file", po::value<std::filesystem::path>()->required(), "Load the specified matrix.");

    auto positional_description = po::positional_options_description{};
    positional_description.add("matrix-file", 1);

    auto const iteration_count_description =
        "Perform SpMMᵀV ‘iteration-count’ times. The run time of a single SpMMᵀV iteration is calculated by "
        "dividing the total run time by ‘iteration-count’."s;

    auto const mode_description =
        "Choose the SpMMᵀV mode. The recognised arguments are:\n"
        "    sequential,\n"
        "    locks-exclusive,\n"
        "    locks-planned-exclusive,\n"
        "    locks-planned-shared,\n"
        "    locks-shared."s;

    auto const region_coefficient_description =
        "The hierarchical matrix’s region coefficient. The specified number has to lie within "
        "interval [0, 1]."s;

    auto const thread_count_description = "Set the thread count of each MPI process."s;

    auto options = po::options_description{};
    options.add_options()
        ("help,h", po::bool_switch(), "Show this message and exit.")
        ("iteration-count,i", po::value<int>()->default_value(10), iteration_count_description.c_str())
        ("mode,m", po::value<Mode>()->required(), mode_description.c_str())
        ("region-coefficient,c", po::value<double>()->default_value(.12), region_coefficient_description.c_str())
        ("thread-count,t", po::value<int>()->default_value(omp_get_max_threads()), thread_count_description.c_str());

    auto cli = po::options_description{};
    cli.add(positional).add(options);

    auto clp = po::command_line_parser{argc, argv};
    clp.positional(positional_description).options(cli);

    auto vm = po::variables_map{};
    po::store(clp.run(), vm);

    if (vm["help"].as<bool>()) {
        auto const caption = "Usage: "s + argv[0] + " -m MODE [OPTIONS] MATRIX_FILE";
        auto cli_description = po::options_description{caption};
        cli_description.add(options);

        auto oss = std::ostringstream{};
        oss << cli_description;
        return CommandLineArguments{oss.str()};
    }

    po::notify(vm);

    auto const iteration_count = vm["iteration-count"].as<int>();
    if (iteration_count <= 0) {
        throw CommandLineArgumentError{"Invalid iteration count."};
    }

    auto const region_coefficient = vm["region-coefficient"].as<double>();
    if (region_coefficient < 0 || region_coefficient > 1) {
        throw CommandLineArgumentError{"The region coefficient doesn’t lie within interval [0, 1]."};
    }

    auto const thread_count = vm["thread-count"].as<int>();
    if (thread_count <= 0 || thread_count > omp_get_thread_limit()) {
        throw CommandLineArgumentError{"Invalid thread count."};
    }

    return {vm["matrix-file"].as<std::filesystem::path>(),
            iteration_count,
            vm["mode"].as<Mode>(),
            region_coefficient,
            thread_count};
}

auto CommandLineArguments::has_help() const noexcept -> bool
{
    return this->help_;
}

auto CommandLineArguments::get_help_message() const noexcept -> std::string const&
{
    return this->help_message_;
}

auto CommandLineArguments::get_iteration_count() const noexcept -> int
{
    return this->iteration_count_;
}

auto CommandLineArguments::get_matrix_path() const noexcept -> std::filesystem::path const&
{
    return this->matrix_path_;
}

auto CommandLineArguments::get_mode() const noexcept -> Mode
{
    return this->mode_;
}

auto CommandLineArguments::get_region_coefficient() const noexcept -> double
{
    return this->region_coefficient_;
}

auto CommandLineArguments::get_thread_count() const noexcept -> int
{
    return this->thread_count_;
}

CommandLineArguments::CommandLineArguments(std::string&& help_message) noexcept
    : help_{true},
      help_message_{std::move(help_message)}
{
}

CommandLineArguments::CommandLineArguments(
        std::filesystem::path const& matrix_path,
        int const iteration_count,
        Mode const mode,
        double const region_coefficient,
        int const thread_count
    ) : matrix_path_{matrix_path},
        iteration_count_{iteration_count},
        mode_{mode},
        region_coefficient_{region_coefficient},
        thread_count_{thread_count}
{
}
