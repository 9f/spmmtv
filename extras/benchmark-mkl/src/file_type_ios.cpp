#include <file_type_ios.hpp>

#include <istream>              // std::istream
#include <map>                  // std::map
#include <ostream>              // std::ostream
#include <stdexcept>            // std::logic_error std::out_of_range
#include <string>               // std::string
#include <string_view>          // std::string_view

#include <file_type.hpp>        // FileType

namespace {

auto const file_types = std::map<std::string_view, FileType>{
    {"HDF5", FileType::HDF5},
    {"MM",   FileType::MM},
};

}  // namespace

auto operator<<(std::ostream& os, FileType const file_type) -> std::ostream&
{
    switch (file_type) {
    case FileType::HDF5:
        return os << "HDF5";
    case FileType::MM:
        return os << "MM";
    }

    throw std::logic_error{"Unexpected enumeration value."};
}

auto operator>>(std::istream& is, FileType& file_type) -> std::istream&
{
    auto token = std::string{};
    is >> token;
    try {
        file_type = file_types.at(token);
    } catch (std::out_of_range const& e) {
        is.setstate(std::istream::failbit);
    }
    return is;
}
