#pragma once

#include <chrono>               // std::chrono
#include <span>                 // std::span

namespace csr {

enum struct Symmetric;
template<typename T, Symmetric Symm> class Matrix;

}  // namespace csr

template<typename T, csr::Symmetric Symm>
auto run_spmmtv_benchmark(csr::Matrix<T, Symm>& matrix, std::span<T const> x1, int iteration_count, int thread_count)
    -> std::chrono::duration<double>;
