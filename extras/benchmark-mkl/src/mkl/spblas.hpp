#pragma once

#include <cstdint>              // std::uint32_t

#define MKL_INT std::uint32_t   // Configure MKL to use ‘std::uint32_t’ for integers.
#include <mkl_spblas.h>         // matrix_descr sparse_index_base_t sparse_matrix_t sparse_operation_t sparse_status_t
#include <mkl_types.h>          // MKL_INT

namespace mkl {

// Wrap ‘mkl_sparse_s_create_csr’ from MKL.
auto sparse_create_csr(
        sparse_matrix_t* A,
        sparse_index_base_t indexing,
        MKL_INT rows,
        MKL_INT cols,
        MKL_INT* rows_start,
        MKL_INT* rows_end,
        MKL_INT* col_indx,
        float* values
    ) -> sparse_status_t;

// Wrap ‘mkl_sparse_d_create_csr’ from MKL.
auto sparse_create_csr(
        sparse_matrix_t* A,
        sparse_index_base_t indexing,
        MKL_INT rows,
        MKL_INT cols,
        MKL_INT* rows_start,
        MKL_INT* rows_end,
        MKL_INT* col_indx,
        double* values
    ) -> sparse_status_t;

// Wrap ‘mkl_sparse_s_mv’ from MKL.
auto sparse_mv(
        sparse_operation_t operation,
        float alpha,
        sparse_matrix_t A,
        matrix_descr descr,
        float const* x,
        float beta,
        float* y
    ) -> sparse_status_t;

// Wrap ‘mkl_sparse_d_mv’ from MKL.
auto sparse_mv(
        sparse_operation_t operation,
        double alpha,
        sparse_matrix_t A,
        matrix_descr descr,
        double const* x,
        double beta,
        double* y
    ) -> sparse_status_t;

}  // namespace mkl
