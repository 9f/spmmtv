#include <mkl/spblas.hpp>

#include <cstdint>              // std::uint32_t

#define MKL_INT std::uint32_t   // Configure MKL to use ‘std::uint32_t’ for integers.
#include <mkl_spblas.h>         // mkl_sparse_d_create_csr mkl_sparse_d_mv mkl_sparse_s_create_csr mkl_sparse_s_mv
#include <mkl_types.h>          // MKL_INT

namespace mkl {

auto sparse_create_csr(
        sparse_matrix_t* const A,
        sparse_index_base_t const indexing,
        MKL_INT const rows,
        MKL_INT const cols,
        MKL_INT* const rows_start,
        MKL_INT* const rows_end,
        MKL_INT* const col_indx,
        float* const values
    ) -> sparse_status_t
{
    return mkl_sparse_s_create_csr(A, indexing, rows, cols, rows_start, rows_end, col_indx, values);
}

auto sparse_create_csr(
        sparse_matrix_t* const A,
        sparse_index_base_t const indexing,
        MKL_INT const rows,
        MKL_INT const cols,
        MKL_INT* const rows_start,
        MKL_INT* const rows_end,
        MKL_INT* const col_indx,
        double* const values
    ) -> sparse_status_t
{
    return mkl_sparse_d_create_csr(A, indexing, rows, cols, rows_start, rows_end, col_indx, values);
}

auto sparse_mv(
        sparse_operation_t const operation,
        float const alpha,
        sparse_matrix_t const A,
        matrix_descr const descr,
        float const* const x,
        float const beta,
        float* const y
    ) -> sparse_status_t
{
    return mkl_sparse_s_mv(operation, alpha, A, descr, x, beta, y);
}

auto sparse_mv(
        sparse_operation_t const operation,
        double const alpha,
        sparse_matrix_t const A,
        matrix_descr const descr,
        double const* const x,
        double const beta,
        double* const y
    ) -> sparse_status_t
{
    return mkl_sparse_d_mv(operation, alpha, A, descr, x, beta, y);
}

}  // namespace mkl
