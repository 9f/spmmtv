#pragma once

#include <stdexcept>            // std::runtime_error

namespace mkl {

class RuntimeError : public std::runtime_error {
public:
    using std::runtime_error::runtime_error;
};

}  // namespace mkl
