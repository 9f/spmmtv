#include <mkl/sparse_matrix.hpp>

#include <cstdint>              // std::uint32_t

#define MKL_INT std::uint32_t   // Configure MKL to use ‘std::uint32_t’ for integers.
#include <mkl_spblas.h>         // mkl_sparse_destroy sparse_matrix_t

namespace mkl {

SparseMatrix::~SparseMatrix() noexcept
{
    // Function ‘mkl_sparse_destroy’ returns ‘SPARSE_STATUS_NOT_INITIALIZED’
    // when ‘this->sparse_matrix_ptr_’ is equal to ‘nullptr’.
    mkl_sparse_destroy(this->sparse_matrix_ptr_);
}

auto SparseMatrix::operator()() noexcept -> sparse_matrix_t&
{
    return this->sparse_matrix_ptr_;
}

}  // namespace mkl
