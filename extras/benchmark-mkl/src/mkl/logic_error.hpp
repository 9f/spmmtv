#pragma once

#include <stdexcept>            // std::logic_error

namespace mkl {

class LogicError : public std::logic_error {
public:
    using std::logic_error::logic_error;
};

}  // namespace mkl
