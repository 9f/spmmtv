#pragma once

#include <cstdint>              // std::uint32_t

#define MKL_INT std::uint32_t   // Configure MKL to use ‘std::uint32_t’ for integers.
#include <mkl_spblas.h>         // sparse_matrix_t

namespace mkl {

class SparseMatrix {
public:
    SparseMatrix() = default;
    ~SparseMatrix() noexcept;

    SparseMatrix(SparseMatrix const&) = delete;
    SparseMatrix(SparseMatrix&&) = delete;

    auto operator=(SparseMatrix const&) -> SparseMatrix& = delete;
    auto operator=(SparseMatrix&&) -> SparseMatrix& = delete;

    auto operator()() noexcept -> sparse_matrix_t&;

private:
    sparse_matrix_t sparse_matrix_ptr_{};
};

}  // namespace mkl
