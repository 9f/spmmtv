#pragma once

#include <mkl/logic_error.hpp>  // mkl::LogicError

namespace mkl {

class InvalidArgument : public LogicError {
public:
    using LogicError::LogicError;
};

}  // namespace mkl
