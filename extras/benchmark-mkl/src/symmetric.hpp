#pragma once

// Indicate the symmetry of a matrix.
enum struct Symmetric {
    no,
    yes,
};
