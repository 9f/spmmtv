#include <benchmark.hpp>

#include <algorithm>            // std::copy
#include <chrono>               // std::chrono
#include <span>                 // std::span
#include <stdexcept>            // std::invalid_argument std::logic_error
#include <vector>               // std::vector

#define MKL_INT std::uint32_t   // Configure MKL to use ‘std::uint32_t’ for integers.
#include <mkl_spblas.h>         // mkl_sparse_optimize mkl_sparse_set_memory_hint mkl_sparse_set_mv_hint
#include <mkl_types.h>          // MKL_INT

#include <omp.h>                // omp_set_num_threads

#include <csr/matrix.hpp>       // csr::Matrix
#include <csr/symmetric.hpp>    // csr::Symmetric

#include <mkl/invalid_argument.hpp>  // mkl::InvalidArgument
#include <mkl/logic_error.hpp>  // mkl::LogicError
#include <mkl/runtime_error.hpp>  // mkl::RuntimeError
#include <mkl/sparse_matrix.hpp>  // mkl::SparseMatrix
#include <mkl/spblas.hpp>       // mkl::sparse_create_csr mkl::sparse_mv

namespace {

auto compute_duration(
        std::chrono::steady_clock::time_point const& start,
        std::chrono::steady_clock::time_point const& end,
        int iteration_count
    ) -> std::chrono::duration<double>;

auto throw_on_error(sparse_status_t status) -> void;

}  // namespace

template<typename T, csr::Symmetric Symm>
auto run_spmmtv_benchmark(
        csr::Matrix<T, Symm>& matrix, std::span<T const> const x1, int const iteration_count, int const thread_count
    ) -> std::chrono::duration<double>
{
    omp_set_num_threads(thread_count);
    auto matrix_handle = mkl::SparseMatrix{};
    auto status = mkl::sparse_create_csr(
            &matrix_handle(),
            SPARSE_INDEX_BASE_ZERO,
            static_cast<MKL_INT>(matrix.row_count()),
            static_cast<MKL_INT>(matrix.column_count()),
            matrix.row_pointers.data(),
            matrix.row_pointers.data() + 1,
            matrix.columns.data(),
            matrix.elements.data()
        );
    throw_on_error(status);

    auto matrix_description = matrix_descr{};
    switch (Symm) {
    case csr::Symmetric::no:
        matrix_description.type = SPARSE_MATRIX_TYPE_GENERAL;
        break;
    case csr::Symmetric::yes:
        matrix_description.type = SPARSE_MATRIX_TYPE_SYMMETRIC;
        matrix_description.mode = SPARSE_FILL_MODE_UPPER;
        matrix_description.diag = SPARSE_DIAG_NON_UNIT;
        break;
    }

    // Calls to ‘mkl_sparse_set_mv_hint’ have argument ‘expected_calls’ set to
    // a high value to motivate the implementation to perform as many
    // optimisations as possible, but the resulting performance doesn’t seem to
    // differ from setting the argument to one.
    status = mkl_sparse_set_mv_hint(
            matrix_handle(),
            SPARSE_OPERATION_NON_TRANSPOSE,
            matrix_description,
            1'000'000
        );
    throw_on_error(status);
    status = mkl_sparse_set_mv_hint(
            matrix_handle(),
            SPARSE_OPERATION_TRANSPOSE,
            matrix_description,
            1'000'000
        );
    throw_on_error(status);
    status = mkl_sparse_set_memory_hint(matrix_handle(), SPARSE_MEMORY_AGGRESSIVE);
    throw_on_error(status);
    status = mkl_sparse_optimize(matrix_handle());
    throw_on_error(status);

    auto y1 = std::vector<T>(x1.size());
    auto y2 = std::vector<T>(x1.size());

    if constexpr (Symm == csr::Symmetric::no) {
        // A warm-up iteration.
        auto const mv_status1 = mkl::sparse_mv(
                SPARSE_OPERATION_NON_TRANSPOSE,
                1,
                matrix_handle(),
                matrix_description,
                x1.data(),
                0,
                y1.data()
            );
        auto const mv_status2 = mkl::sparse_mv(
                SPARSE_OPERATION_TRANSPOSE,
                1,
                matrix_handle(),
                matrix_description,
                x1.data(),
                0,
                y2.data()
        );
        throw_on_error(mv_status1);
        throw_on_error(mv_status2);

        auto const start = std::chrono::steady_clock::now();
        for (auto i = 0; i < iteration_count; ++i) {
            auto const mv_status1 = mkl::sparse_mv(
                    SPARSE_OPERATION_NON_TRANSPOSE,
                    1,
                    matrix_handle(),
                    matrix_description,
                    x1.data(),
                    0,
                    y1.data()
                );
            if (mv_status1 != SPARSE_STATUS_SUCCESS) {
                throw_on_error(mv_status1);
            }

            auto const mv_status2 = mkl::sparse_mv(
                    SPARSE_OPERATION_TRANSPOSE,
                    1,
                    matrix_handle(),
                    matrix_description,
                    x1.data(),
                    0,
                    y2.data()
            );
            if (mv_status2 != SPARSE_STATUS_SUCCESS) {
                throw_on_error(mv_status2);
            }
        }
        auto const end = std::chrono::steady_clock::now();

        return compute_duration(start, end, iteration_count);

    } else {                    // Symm == csr::Symmetric::yes
        // A warm-up iteration.
        auto const mv_status = mkl::sparse_mv(
                SPARSE_OPERATION_NON_TRANSPOSE,
                1,
                matrix_handle(),
                matrix_description,
                x1.data(),
                0,
                y1.data()
            );
        throw_on_error(mv_status);
        std::copy(y1.begin(), y1.end(), y2.begin());

        auto const start = std::chrono::steady_clock::now();
        for (auto i = 0; i < iteration_count; ++i) {
            auto const mv_status = mkl::sparse_mv(
                    SPARSE_OPERATION_NON_TRANSPOSE,
                    1,
                    matrix_handle(),
                    matrix_description,
                    x1.data(),
                    0,
                    y1.data()
                );
            if (mv_status != SPARSE_STATUS_SUCCESS) {
                throw_on_error(mv_status);
            }
            std::copy(y1.begin(), y1.end(), y2.begin());
        }
        auto const end = std::chrono::steady_clock::now();

        return compute_duration(start, end, iteration_count);
    }
}

template auto run_spmmtv_benchmark(
        csr::Matrix<float, csr::Symmetric::no>& matrix,
        std::span<float const> x1,
        int iteration_count,
        int thread_count
    ) -> std::chrono::duration<double>;

template auto run_spmmtv_benchmark(
        csr::Matrix<float, csr::Symmetric::yes>& matrix,
        std::span<float const> x1,
        int iteration_count,
        int thread_count
    ) -> std::chrono::duration<double>;

template auto run_spmmtv_benchmark(
        csr::Matrix<double, csr::Symmetric::no>& matrix,
        std::span<double const> x1,
        int iteration_count,
        int thread_count
    ) -> std::chrono::duration<double>;

template auto run_spmmtv_benchmark(
        csr::Matrix<double, csr::Symmetric::yes>& matrix,
        std::span<double const> x1,
        int iteration_count,
        int thread_count
    ) -> std::chrono::duration<double>;

namespace {

auto compute_duration(
        std::chrono::steady_clock::time_point const& start,
        std::chrono::steady_clock::time_point const& end,
        int const iteration_count
    ) -> std::chrono::duration<double>
{
    if (iteration_count <= 0) {
        throw std::invalid_argument{"The iteration count cannot be less than or equal to zero."};
    }

    auto const elapsed = std::chrono::duration<double>{end - start};
    return elapsed / iteration_count;
}

auto throw_on_error(sparse_status_t const status) -> void
{
    switch (status) {
    case SPARSE_STATUS_SUCCESS:
        return;

    case SPARSE_STATUS_NOT_INITIALIZED:
        throw mkl::InvalidArgument{"Empty handle or matrix array."};

    case SPARSE_STATUS_ALLOC_FAILED:
        throw mkl::RuntimeError{"Internal memory allocation failed."};

    case SPARSE_STATUS_INVALID_VALUE:
        throw mkl::InvalidArgument{"The input parameters contain an invalid value."};

    case SPARSE_STATUS_EXECUTION_FAILED:
        throw mkl::RuntimeError{"Execution failed."};

    case SPARSE_STATUS_INTERNAL_ERROR:
        throw mkl::RuntimeError{"Internal error."};

    case SPARSE_STATUS_NOT_SUPPORTED:
        throw mkl::LogicError{"The requested operation is not supported."};
    }

    throw std::logic_error{"Unexpected enumeration value."};
}

}  // namespace
