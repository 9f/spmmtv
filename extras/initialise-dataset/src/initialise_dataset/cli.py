import os
import pathlib
import shlex
import shutil
import subprocess
import sys

import click

from . import VERSION


@click.command()
@click.argument('target_dir', type=click.Path(file_okay=False, writable=True, resolve_path=True))
@click.help_option('-h', '--help')
@click.option(
    '-s',
    '--script-dir',
    type=click.Path(exists=True, file_okay=False, resolve_path=True),
    help='Use data initialisation scripts from the specified location.'
)
@click.version_option(VERSION)
def main(target_dir, script_dir):
    if script_dir is None:
        if not _are_dependencies_installed():
            message = (
                'Error: either install the data initialisation scripts used by this program or specify '
                'their location with ‘--script-dir’.'
            )
            sys.exit(message)

    else:
        if not _are_dependencies_present(script_dir):
            sys.exit('Error: the specified script directory doesn’t contain the required scripts.')
        os.chdir(script_dir)

    target_path = pathlib.Path(target_dir)
    target_path.mkdir(parents=True, exist_ok=True)

    hdf5_matrix_path = target_path / 'matrices-hdf5'
    mm_matrix_path = target_path / 'matrices-mm'
    test_data_path = target_path / 'test-data'

    # Download matrices, decompress them and store them to ‘mm_matrix_path’.
    _fetch_matrices(script_dir, mm_matrix_path)

    general_matrix_names = _get_general_matrix_names()
    symmetric_matrix_names = _get_symmetric_matrix_names()

    # Convert general matrices from MM to HDF5.
    for matrix_name in general_matrix_names:
        _mm2h5_general(script_dir, matrix_name, mm_matrix_path, hdf5_matrix_path)

    # Convert symmetric matrices from MM to HDF5.
    for matrix_name in symmetric_matrix_names:
        _mm2h5_symmetric(script_dir, matrix_name, mm_matrix_path, hdf5_matrix_path)

    # Calculate data for SpMMᵀV tests.
    for matrix_name in (*general_matrix_names, *symmetric_matrix_names):
        for element_type in ('float32', 'float64'):
            _calculate_test_data(script_dir, matrix_name, element_type, hdf5_matrix_path, test_data_path)


def _are_dependencies_installed():
    commands = ('calculate-test-data', 'fetch-matrices', 'mm2h5')
    if any(shutil.which(cmd) is None for cmd in commands):
        return False
    return True


def _are_dependencies_present(script_dir):
    required_paths = (
        pathlib.Path(script_dir) / 'calculate-test-data',
        pathlib.Path(script_dir) / 'fetch-matrices',
        pathlib.Path(script_dir) / 'mm2h5',
    )
    if not all(p.exists() for p in required_paths):
        return False
    return True


def _calculate_test_data(script_dir, matrix_name, element_type, matrix_dir_path, test_data_path):
    print(f'Calculating test data for matrix ‘{matrix_name}’ and element type ‘{element_type}’...')

    target_dir_path = test_data_path / element_type
    target_dir_path.mkdir(parents=True, exist_ok=True)

    if script_dir is None:
        arguments = ['calculate-test-data']
    else:
        arguments = ['python', '-m', 'calculate-test-data.calculatetestdata']
    arguments += (
        '--element-type', element_type,
        str(matrix_dir_path / 'general' / element_type / f'{matrix_name}.coo.h5'),
        str(target_dir_path / f'{matrix_name}.h5'),
    )

    completed_process = subprocess.run(arguments)
    if completed_process.returncode != 0:
        cmd = shlex.join(arguments)
        ret_code = completed_process.returncode
        sys.exit(f'Error: the exit status of command ‘{cmd}’ was {ret_code}.')


def _fetch_matrices(script_dir, target_path):
    if script_dir is None:
        arguments = ['fetch-matrices']
    else:
        arguments = ['python', '-m', 'fetch-matrices.fetchmatrices']
    arguments.append(str(target_path))

    completed_process = subprocess.run(arguments)
    if completed_process.returncode != 0:
        cmd = shlex.join(arguments)
        ret_code = completed_process.returncode
        sys.exit(f'Error: the exit status of command ‘{cmd}’ was {ret_code}.')


def _get_general_matrix_names():
    return (
        'b1_ss',
        'cage14',
        'cage15',
        'circuit5M',
        'CoupCons3D',
        'dgreen',
        'Freescale1',
        'Freescale2',
        'FullChip',
        'HV15R',
        'ML_Geer',
        'ML_Laplace',
        'nv2',
        'rajat31',
        'RM07R',
        'ss',
        'stokes',
        'Transport',
        'TSOPF_RS_b2383',
        'vas_stokes_2M',
        'vas_stokes_4M',
    )


def _get_symmetric_matrix_names():
    return (
        'bone010',
        'boneS10',
        'Bump_2911',
        'bundle_adj',
        'Cube_Coup_dt0',
        'CurlCurl_4',
        'dielFilterV2real',
        'dielFilterV3real',
        'Flan_1565',
        'Ga41As41H72',
        'gsm_106857',
        'human_gene1',
        'LFAT5',
        'Long_Coup_dt0',
        'mouse_gene',
        'nlpkkt120',
        'nlpkkt160',
        'nlpkkt200',
        'PFlow_742',
        'Queen_4147',
        'StocF-1465',
        'thread',
    )


def _mm2h5_general(script_dir, matrix_name, mm_path, hdf5_path):
    print(f'Converting general matrix ‘{matrix_name}’ from Matrix Market to HDF5...')

    f32_output_dir_path = hdf5_path / 'general' / 'float32'
    f64_output_dir_path = hdf5_path / 'general' / 'float64'
    f32_output_dir_path.mkdir(parents=True, exist_ok=True)
    f64_output_dir_path.mkdir(parents=True, exist_ok=True)

    if script_dir is None:
        arguments = ['mm2h5']
    else:
        arguments = ['python', '-m', 'mm2h5.mm2h5']
    arguments += (
        '--coo-f32', str(f32_output_dir_path / f'{matrix_name}.coo.h5'),
        '--coo-f64', str(f64_output_dir_path / f'{matrix_name}.coo.h5'),
        '--csr-f32', str(f32_output_dir_path / f'{matrix_name}.csr.h5'),
        '--csr-f64', str(f64_output_dir_path / f'{matrix_name}.csr.h5'),
        '--matrix-name', matrix_name,
        str(mm_path / f'{matrix_name}' / f'{matrix_name}.mtx'),
    )

    completed_process = subprocess.run(arguments)
    if completed_process.returncode != 0:
        cmd = shlex.join(arguments)
        ret_code = completed_process.returncode
        sys.exit(f'Error: the exit status of command ‘{cmd}’ was {ret_code}.')


def _mm2h5_symmetric(script_dir, matrix_name, mm_path, hdf5_path):
    print(f'Converting symmetric matrix ‘{matrix_name}’ from Matrix Market to HDF5...')

    general_f32_output_dir_path = hdf5_path / 'general' / 'float32'
    general_f64_output_dir_path = hdf5_path / 'general' / 'float64'
    general_f32_output_dir_path.mkdir(parents=True, exist_ok=True)
    general_f64_output_dir_path.mkdir(parents=True, exist_ok=True)

    symmetric_f32_output_dir_path = hdf5_path / 'symmetric' / 'float32'
    symmetric_f64_output_dir_path = hdf5_path / 'symmetric' / 'float64'
    symmetric_f32_output_dir_path.mkdir(parents=True, exist_ok=True)
    symmetric_f64_output_dir_path.mkdir(parents=True, exist_ok=True)

    if script_dir is None:
        arguments = ['mm2h5']
    else:
        arguments = ['python', '-m', 'mm2h5.mm2h5']
    arguments += (
        '--coo-f32', str(general_f32_output_dir_path / f'{matrix_name}.coo.h5'),
        '--coo-f64', str(general_f64_output_dir_path / f'{matrix_name}.coo.h5'),
        '--csr-f32', str(general_f32_output_dir_path / f'{matrix_name}.csr.h5'),
        '--csr-f64', str(general_f64_output_dir_path / f'{matrix_name}.csr.h5'),
        '--coo-sym-f32', str(symmetric_f32_output_dir_path / f'{matrix_name}.coo.h5'),
        '--coo-sym-f64', str(symmetric_f64_output_dir_path / f'{matrix_name}.coo.h5'),
        '--csr-sym-f32', str(symmetric_f32_output_dir_path / f'{matrix_name}.csr.h5'),
        '--csr-sym-f64', str(symmetric_f64_output_dir_path / f'{matrix_name}.csr.h5'),
        '--matrix-name', matrix_name,
        str(mm_path / f'{matrix_name}' / f'{matrix_name}.mtx'),
    )

    completed_process = subprocess.run(arguments)
    if completed_process.returncode != 0:
        cmd = shlex.join(arguments)
        ret_code = completed_process.returncode
        sys.exit(f'Error: the exit status of command ‘{cmd}’ was {ret_code}.')
