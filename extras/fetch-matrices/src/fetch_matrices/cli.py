import pathlib
import shutil
import sys
import urllib.request

import click

from . import VERSION


@click.command()
@click.argument('target_dir', type=click.Path(file_okay=False, writable=True))
@click.help_option('-h', '--help')
@click.version_option(VERSION)
def main(target_dir):
    """Download matrices from the SuiteSparse Matrix Collection, decompress them
    and store them to the target directory.
    """
    target_dir_path = pathlib.Path(target_dir)
    target_dir_path.mkdir(parents=True, exist_ok=True)

    matrix_names_groups = {
        'b1_ss': 'Grund',
        'bone010': 'Oberwolfach',
        'boneS10': 'Oberwolfach',
        'Bump_2911': 'Janna',
        'bundle_adj': 'Mazaheri',
        'cage14': 'vanHeukelum',
        'cage15': 'vanHeukelum',
        'circuit5M': 'Freescale',
        'CoupCons3D': 'Janna',
        'Cube_Coup_dt0': 'Janna',
        'CurlCurl_4': 'Bodendiek',
        'dgreen': 'VLSI',
        'dielFilterV2real': 'Dziekonski',
        'dielFilterV3real': 'Dziekonski',
        'Flan_1565': 'Janna',
        'Freescale1': 'Freescale',
        'Freescale2': 'Freescale',
        'FullChip': 'Freescale',
        'Ga41As41H72': 'PARSEC',
        'gsm_106857': 'Dziekonski',
        'human_gene1': 'Belcastro',
        'HV15R': 'Fluorem',
        'LFAT5': 'Oberwolfach',
        'Long_Coup_dt0': 'Janna',
        'ML_Geer': 'Janna',
        'ML_Laplace': 'Janna',
        'mouse_gene': 'Belcastro',
        'nlpkkt120': 'Schenk',
        'nlpkkt160': 'Schenk',
        'nlpkkt200': 'Schenk',
        'nv2': 'VLSI',
        'PFlow_742': 'Janna',
        'Queen_4147': 'Janna',
        'rajat31': 'Rajat',
        'RM07R': 'Fluorem',
        'ss': 'VLSI',
        'StocF-1465': 'Janna',
        'stokes': 'VLSI',
        'thread': 'DNVS',
        'Transport': 'Janna',
        'TSOPF_RS_b2383': 'TSOPF',
        'vas_stokes_2M': 'VLSI',
        'vas_stokes_4M': 'VLSI',
    }

    print('Downloading matrices...')
    _download_matrices(matrix_names_groups, target_dir_path)
    print('Extracting archives...')
    _extract_archives(matrix_names_groups, target_dir_path)


def _download_matrices(matrix_names_groups, target_dir):
    for matrix_name, matrix_group in matrix_names_groups.items():
        archive_name = f'{matrix_name}.tar.gz'
        archive_path = target_dir / archive_name
        result_dir = target_dir / matrix_name
        if archive_path.exists() or result_dir.exists():
            print(f'Skipped matrix ‘{matrix_name}’ because it already exists.')
            continue

        print(f'Downloading matrix ‘{matrix_name}’...')
        url_prefix = 'https://suitesparse-collection-website.herokuapp.com/MM/'
        url = f'{url_prefix}{matrix_group}/{archive_name}'
        with urllib.request.urlopen(url) as response:
            if not response.url.endswith(archive_name):
                print('Error: the response URL likely doesn’t point to the requested matrix.')
                print(f'The response URL: ⟨{response.url}⟩.')
                print(f'The request URL: ⟨{url}⟩.')
                sys.exit(1)

            with open(archive_path, 'bw') as archive_file:
                shutil.copyfileobj(response, archive_file)


def _extract_archives(matrix_names_groups, target_dir):
    for matrix_name in matrix_names_groups.keys():
        result_dir = target_dir / matrix_name
        if result_dir.exists():
            print(f'Skipped matrix ‘{matrix_name}’ because it already exists.')
            continue

        print(f'Extracting matrix ‘{matrix_name}’...')
        archive_name = f'{matrix_name}.tar.gz'
        archive_path = target_dir / archive_name
        shutil.unpack_archive(archive_path, target_dir)
        archive_path.unlink()
