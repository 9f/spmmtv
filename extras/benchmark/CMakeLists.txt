project(benchmark LANGUAGES CXX)

add_executable("${PROJECT_NAME}"
  src/command_line_arguments.cpp
  src/element_type_ios.cpp
  src/file_type_ios.cpp
  src/main.cpp
  src/mode_ios.cpp
  )


# Dependencies

find_package(Boost REQUIRED COMPONENTS program_options)
find_package(OpenMP REQUIRED COMPONENTS CXX)


# Compilation Settings

set_target_properties("${PROJECT_NAME}" PROPERTIES
  CXX_EXTENSIONS FALSE
  CXX_STANDARD_REQUIRED TRUE
  INTERPROCEDURAL_OPTIMIZATION_RELEASE TRUE
  INTERPROCEDURAL_OPTIMIZATION_RELWITHDEBINFO TRUE
  )

target_compile_features("${PROJECT_NAME}" PRIVATE cxx_std_20)
target_include_directories("${PROJECT_NAME}" PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/src")

target_link_libraries("${PROJECT_NAME}" PRIVATE
  Boost::program_options
  coo
  csr
  hsf
  OpenMP::OpenMP_CXX
  )

enable_development_options("${PROJECT_NAME}")
treat_warnings_as_errors("${PROJECT_NAME}")
