#pragma once

#include <istream>              // std::istream
#include <ostream>              // std::ostream

enum struct Mode;

auto operator<<(std::ostream& os, Mode mode) -> std::ostream&;
auto operator>>(std::istream& is, Mode& mode) -> std::istream&;
