#pragma once

#include <istream>              // std::istream
#include <ostream>              // std::ostream

enum struct FileType;

auto operator<<(std::ostream& os, FileType mode) -> std::ostream&;
auto operator>>(std::istream& is, FileType& mode) -> std::istream&;
