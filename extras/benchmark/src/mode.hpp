#pragma once

enum struct Mode {
    coo_sequential,
    csr_sequential,
    hsf_sequential,
    hsf_atomic_exclusive,
    hsf_atomic_planned_shared,
    hsf_atomic_shared,
    hsf_locks_exclusive,
    hsf_locks_planned_exclusive,
    hsf_locks_planned_shared,
    hsf_locks_shared,
    hsf_per_thread_exclusive,
    hsf_per_thread_planned_shared,
    hsf_per_thread_shared,
};
