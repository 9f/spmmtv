#include <cassert>              // assert
#include <chrono>               // std::chrono
#include <exception>            // std::exception
#include <filesystem>           // std::filesystem
#include <functional>           // std::invoke
#include <iostream>             // std::cerr std::cout
#include <stdexcept>            // std::logic_error
#include <vector>               // std::vector

#include <boost/program_options/errors.hpp>  // boost::program_options::error

#include <omp.h>                // omp_set_num_threads

#include <coo/compute.hpp>      // coo::compute
#include <coo/matrix.hpp>       // coo::Matrix
#include <coo/matrix_ios.hpp>   // coo::operator<<
#include <coo/symmetric.hpp>    // coo::Symmetric

#include <csr/compute.hpp>      // csr::compute
#include <csr/matrix.hpp>       // csr::Matrix
#include <csr/matrix_ios.hpp>   // csr::operator<<
#include <csr/symmetric.hpp>    // csr::Symmetric

#include <hsf/compute.hpp>      // hsf::compute
#include <hsf/exec_data.hpp>    // hsf::ExecData
#include <hsf/exec_mode.hpp>    // hsf::ExecMode
#include <hsf/matrix.hpp>       // hsf::Matrix
#include <hsf/matrix_ios.hpp>   // hsf::operator<<
#include <hsf/region.hpp>       // hsf::Region
// hsf::print_morton_code hsf::print_morton_code_header hsf::print_region_header hsf::operator<<
#include <hsf/region_ios.hpp>
#include <hsf/region_ordering.hpp>  // hsf::RegionOrdering
#include <hsf/symmetric.hpp>    // hsf::Symmetric
#include <hsf/tags.hpp>         // hsf::tags

#include <command_line_argument_error.hpp>  // CommandLineArgumentError
#include <command_line_arguments.hpp>  // CommandLineArguments
#include <element_type.hpp>     // ElementType
#include <file_type.hpp>        // FileType
#include <mode.hpp>             // Mode
#include <mode_ios.hpp>         // operator<<
#include <symmetric.hpp>        // Symmetric

namespace {

template<typename ToSymmetry, typename FromSymmetry>
constexpr auto convert_symmetry(FromSymmetry from_symmetry) noexcept -> ToSymmetry;

template<typename T, coo::Symmetric Symm>
auto load_coo_matrix(std::filesystem::path const& matrix_path, FileType file_type, bool verbose)
    -> coo::Matrix<T, Symm>;

template<typename T, csr::Symmetric Symm>
auto load_csr_matrix(std::filesystem::path const& matrix_path, FileType file_type, bool verbose)
    -> csr::Matrix<T, Symm>;

template<typename T, hsf::Symmetric Symm>
auto load_hsf_matrix(std::filesystem::path const& matrix_path, FileType file_type, bool verbose)
    -> hsf::Matrix<T, Symm>;

template<typename T, hsf::Symmetric Symm>
auto load_hsf_matrix(
        std::filesystem::path const& matrix_path,
        FileType file_type,
        bool verbose,
        double region_coefficient,
        int thread_count
    ) -> hsf::Matrix<T, Symm>;

template<Mode TMode>
auto print_summary(
        std::chrono::steady_clock::time_point const& start,
        std::chrono::steady_clock::time_point const& end,
        int iteration_count
    ) -> void;

template<typename T, Symmetric Symm>
auto run_benchmark(CommandLineArguments const& command_line_arguments) -> void;

template<typename T, Symmetric Symm>
auto run_sequential_coo_benchmark(
        std::filesystem::path const& matrix_path, FileType file_type, bool verbose, int iteration_count
    ) -> void;

template<typename T, Symmetric Symm>
auto run_sequential_csr_benchmark(
        std::filesystem::path const& matrix_path, FileType file_type, bool verbose, int iteration_count
    ) -> void;

template<typename T, Symmetric Symm>
auto run_sequential_hsf_benchmark(
        std::filesystem::path const& matrix_path,
        FileType file_type,
        bool verbose,
        int iteration_count,
        hsf::RegionOrdering region_ordering
    ) -> void;

template<Mode TMode, hsf::ExecMode EM, typename T, Symmetric Symm>
auto run_parallel_hsf_benchmark(
        std::filesystem::path const& matrix_path,
        FileType file_type,
        bool verbose,
        int iteration_count,
        hsf::RegionOrdering region_ordering,
        double region_coefficient,
        int thread_count
    ) -> void;

}  // namespace

auto main(int argc, char** argv) -> int try
{
    auto const command_line_arguments = CommandLineArguments::parse(argc, argv);

    if (command_line_arguments.has_help()) {
        std::cout << command_line_arguments.get_help_message();
        return 0;
    }

    switch (command_line_arguments.get_element_type()) {
    case ElementType::float32:
        if (command_line_arguments.is_symmetric()) {
            run_benchmark<float, Symmetric::yes>(command_line_arguments);
        } else {
            run_benchmark<float, Symmetric::no>(command_line_arguments);
        }
        break;

    case ElementType::float64:
        if (command_line_arguments.is_symmetric()) {
            run_benchmark<double, Symmetric::yes>(command_line_arguments);
        } else {
            run_benchmark<double, Symmetric::no>(command_line_arguments);
        }
        break;
    }

    return 0;

} catch (boost::program_options::error const& e) {
    std::cerr << "Error: " << e.what() << ".\n";
    return 2;

} catch (CommandLineArgumentError const& e) {
    std::cerr << "Error: " << e.what() << '\n';
    return 2;

} catch (std::exception const& e) {
    std::cerr << "Error: " << e.what() << '\n';
    return 1;
}

namespace {

template<typename ToSymmetry, typename FromSymmetry>
constexpr auto convert_symmetry(FromSymmetry const from_symmetry) noexcept -> ToSymmetry
{
    switch (from_symmetry) {
    case FromSymmetry::no:
        return ToSymmetry::no;
    case FromSymmetry::yes:
        return ToSymmetry::yes;
    }
}

template<typename T, coo::Symmetric Symm>
auto load_coo_matrix(std::filesystem::path const& matrix_path, FileType const file_type, bool const verbose)
    -> coo::Matrix<T, Symm>
{
    auto const matrix = std::invoke([&] {
        switch (file_type) {
        case FileType::HDF5:
            return coo::Matrix<T, Symm>::load_from_hdf5(matrix_path);
        case FileType::MM:
            return coo::Matrix<T, Symm>::load_from_mm(matrix_path);
        }
        throw std::logic_error{"Unexpected matrix file type."};
    });
    if (verbose) {
        std::cout << "the COO matrix’s properties:\n"
                  << matrix
                  << '\n';
    }
    return matrix;
}

template<typename T, csr::Symmetric Symm>
auto load_csr_matrix(std::filesystem::path const& matrix_path, FileType const file_type, bool const verbose)
    -> csr::Matrix<T, Symm>
{
    auto const matrix = std::invoke([&] {
        switch (file_type) {
        case FileType::HDF5:
            return csr::Matrix<T, Symm>::load_from_hdf5(matrix_path);
        case FileType::MM:
            return csr::Matrix<T, Symm>::load_from_mm(matrix_path);
        }
        throw std::logic_error{"Unexpected matrix file type."};
    });
    if (verbose) {
        std::cout << "the CSR matrix’s properties:\n"
                  << matrix
                  << '\n';
    }
    return matrix;
}

template<typename T, hsf::Symmetric Symm>
auto load_hsf_matrix(
        std::filesystem::path const& matrix_path,
        FileType const file_type,
        bool const verbose,
        hsf::RegionOrdering const region_ordering
    ) -> hsf::Matrix<T, Symm>
{
    constexpr auto csr_symmetry = convert_symmetry<csr::Symmetric>(Symm);
    auto const csr_matrix = load_csr_matrix<T, csr_symmetry>(matrix_path, file_type, verbose);

    auto const start = std::chrono::steady_clock::now();
    auto const hsf_matrix = hsf::Matrix<T, Symm>{
        csr_matrix.row_pointers.data(),
        csr_matrix.columns.data(),
        csr_matrix.elements.data(),
        csr_matrix.row_count(),
        csr_matrix.column_count(),
        csr_matrix.element_count(),
        region_ordering
    };
    auto const end = std::chrono::steady_clock::now();

    auto const elapsed = std::chrono::duration<double>(end - start);
    std::cout << "the duration of converting a CSR matrix to an HSF matrix in seconds:\n"
              << elapsed.count() << '\n'
              << '\n';

    if (verbose) {
        std::cout << "the hierarchical matrix’s properties:\n"
                  << hsf_matrix
                  << '\n';

        hsf::print_region_header(std::cout) << '\n';
        for (auto const& region : hsf_matrix.regions()) {
            std::cout << region << '\n';
        }
        hsf::print_region_header(std::cout) << '\n'
                                            << '\n';

        if (hsf_matrix.region_ordering() == hsf::RegionOrdering::morton) {
            hsf::print_morton_code_header(std::cout) << '\n';
            for (auto const& region : hsf_matrix.regions()) {
                hsf::print_morton_code(region, std::cout) << '\n';
            }
            hsf::print_morton_code_header(std::cout) << '\n'
                                                     << '\n';
        }
    }

    return hsf_matrix;
}

template<typename T, hsf::Symmetric Symm>
auto load_hsf_matrix(
        std::filesystem::path const& matrix_path,
        FileType const file_type,
        bool const verbose,
        hsf::RegionOrdering const region_ordering,
        double const region_coefficient,
        int const thread_count
    ) -> hsf::Matrix<T, Symm>
{
    constexpr auto csr_symmetry = convert_symmetry<csr::Symmetric>(Symm);
    auto const csr_matrix = load_csr_matrix<T, csr_symmetry>(matrix_path, file_type, verbose);

    auto const start = std::chrono::steady_clock::now();
    auto const hsf_matrix = hsf::Matrix<T, Symm>{
        csr_matrix.row_pointers.data(),
        csr_matrix.columns.data(),
        csr_matrix.elements.data(),
        csr_matrix.row_count(),
        csr_matrix.column_count(),
        csr_matrix.element_count(),
        region_ordering,
        region_coefficient,
        thread_count
    };
    auto const end = std::chrono::steady_clock::now();

    auto const elapsed = std::chrono::duration<double>(end - start);
    std::cout << "the duration of converting a CSR matrix to an HSF matrix in seconds:\n"
              << elapsed.count() << '\n'
              << '\n';

    if (verbose) {
        std::cout << "the hierarchical matrix’s properties:\n"
                  << hsf_matrix
                  << '\n';

        hsf::print_region_header(std::cout) << '\n';
        for (auto const& region : hsf_matrix.regions()) {
            std::cout << region << '\n';
        }
        hsf::print_region_header(std::cout) << '\n'
                                            << '\n';

        if (hsf_matrix.region_ordering() == hsf::RegionOrdering::morton) {
            hsf::print_morton_code_header(std::cout) << '\n';
            for (auto const& region : hsf_matrix.regions()) {
                hsf::print_morton_code(region, std::cout) << '\n';
            }
            hsf::print_morton_code_header(std::cout) << '\n'
                                                     << '\n';
        }
    }

    return hsf_matrix;
}

template<Mode TMode>
auto print_summary(
        std::chrono::steady_clock::time_point const& start,
        std::chrono::steady_clock::time_point const& end,
        int const iteration_count
    ) -> void
{
    assert(iteration_count > 0);
    auto const elapsed = std::chrono::duration<double>(end - start);
    auto const one_iteration = elapsed / iteration_count;
    std::cout << "the run time of a single SpMMᵀV iteration in seconds (mode " << TMode << "):\n"
              << one_iteration.count() << '\n';
}

template<typename T, Symmetric Symm>
auto run_benchmark(CommandLineArguments const& command_line_arguments) -> void
{
    switch (command_line_arguments.get_mode()) {
    case Mode::coo_sequential:
        run_sequential_coo_benchmark<T, Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count()
            );
        break;

    case Mode::csr_sequential:
        run_sequential_csr_benchmark<T, Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count()
            );
        break;

    case Mode::hsf_sequential:
        run_sequential_hsf_benchmark<T, Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count(),
                command_line_arguments.get_region_ordering()
            );
        break;

    case Mode::hsf_atomic_exclusive:
        run_parallel_hsf_benchmark<Mode::hsf_atomic_exclusive,
                                   hsf::ExecMode::atomic_exclusive,
                                   T,
                                   Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count(),
                command_line_arguments.get_region_ordering(),
                command_line_arguments.get_region_coefficient(),
                command_line_arguments.get_thread_count()
            );
        break;

    case Mode::hsf_atomic_planned_shared:
        run_parallel_hsf_benchmark<Mode::hsf_atomic_planned_shared,
                                   hsf::ExecMode::atomic_planned_shared,
                                   T,
                                   Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count(),
                command_line_arguments.get_region_ordering(),
                command_line_arguments.get_region_coefficient(),
                command_line_arguments.get_thread_count()
            );
        break;

    case Mode::hsf_atomic_shared:
        run_parallel_hsf_benchmark<Mode::hsf_atomic_shared,
                                   hsf::ExecMode::atomic_shared,
                                   T,
                                   Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count(),
                command_line_arguments.get_region_ordering(),
                command_line_arguments.get_region_coefficient(),
                command_line_arguments.get_thread_count()
            );
        break;

    case Mode::hsf_locks_exclusive:
        run_parallel_hsf_benchmark<Mode::hsf_locks_exclusive,
                                   hsf::ExecMode::locks_exclusive,
                                   T,
                                   Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count(),
                command_line_arguments.get_region_ordering(),
                command_line_arguments.get_region_coefficient(),
                command_line_arguments.get_thread_count()
            );
        break;

    case Mode::hsf_locks_planned_exclusive:
        run_parallel_hsf_benchmark<Mode::hsf_locks_planned_exclusive,
                                   hsf::ExecMode::locks_planned_exclusive,
                                   T,
                                   Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count(),
                command_line_arguments.get_region_ordering(),
                command_line_arguments.get_region_coefficient(),
                command_line_arguments.get_thread_count()
            );
        break;

    case Mode::hsf_locks_planned_shared:
        run_parallel_hsf_benchmark<Mode::hsf_locks_planned_shared,
                                   hsf::ExecMode::locks_planned_shared,
                                   T,
                                   Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count(),
                command_line_arguments.get_region_ordering(),
                command_line_arguments.get_region_coefficient(),
                command_line_arguments.get_thread_count()
            );
        break;

    case Mode::hsf_locks_shared:
        run_parallel_hsf_benchmark<Mode::hsf_locks_shared,
                                   hsf::ExecMode::locks_shared,
                                   T,
                                   Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count(),
                command_line_arguments.get_region_ordering(),
                command_line_arguments.get_region_coefficient(),
                command_line_arguments.get_thread_count()
            );
        break;

    case Mode::hsf_per_thread_exclusive:
        run_parallel_hsf_benchmark<Mode::hsf_per_thread_exclusive,
                                   hsf::ExecMode::per_thread_exclusive,
                                   T,
                                   Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count(),
                command_line_arguments.get_region_ordering(),
                command_line_arguments.get_region_coefficient(),
                command_line_arguments.get_thread_count()
            );
        break;

    case Mode::hsf_per_thread_planned_shared:
        run_parallel_hsf_benchmark<Mode::hsf_per_thread_planned_shared,
                                   hsf::ExecMode::per_thread_planned_shared,
                                   T,
                                   Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count(),
                command_line_arguments.get_region_ordering(),
                command_line_arguments.get_region_coefficient(),
                command_line_arguments.get_thread_count()
            );
        break;

    case Mode::hsf_per_thread_shared:
        run_parallel_hsf_benchmark<Mode::hsf_per_thread_shared,
                                   hsf::ExecMode::per_thread_shared,
                                   T,
                                   Symm>(
                command_line_arguments.get_matrix_path(),
                command_line_arguments.get_file_type(),
                command_line_arguments.be_verbose(),
                command_line_arguments.get_iteration_count(),
                command_line_arguments.get_region_ordering(),
                command_line_arguments.get_region_coefficient(),
                command_line_arguments.get_thread_count()
            );
        break;
    }
}

template<typename T, Symmetric Symm>
auto run_sequential_coo_benchmark(
        std::filesystem::path const& matrix_path,
        FileType const file_type,
        bool const verbose,
        int const iteration_count
    ) -> void
{
    constexpr auto coo_symmetry = convert_symmetry<coo::Symmetric>(Symm);
    auto const matrix = load_coo_matrix<T, coo_symmetry>(matrix_path, file_type, verbose);

    auto const x1 = std::vector<T>(matrix.column_count(), static_cast<T>(3.14));
    auto y1 = std::vector<T>(x1.size());
    auto y2 = std::vector<T>(x1.size());

    // A warm-up iteration.
    coo::compute::spmmtv_sequential<T>(matrix, x1, y1, y2);

    auto const start = std::chrono::steady_clock::now();
    for (auto i = 0; i < iteration_count; ++i) {
        coo::compute::spmmtv_sequential<T>(matrix, x1, y1, y2);
    }
    auto const end = std::chrono::steady_clock::now();

    print_summary<Mode::coo_sequential>(start, end, iteration_count);
}

template<typename T, Symmetric Symm>
auto run_sequential_csr_benchmark(
        std::filesystem::path const& matrix_path,
        FileType const file_type,
        bool const verbose,
        int const iteration_count
    ) -> void
{
    constexpr auto csr_symmetry = convert_symmetry<csr::Symmetric>(Symm);
    auto const matrix = load_csr_matrix<T, csr_symmetry>(matrix_path, file_type, verbose);

    auto const x1 = std::vector<T>(matrix.column_count(), static_cast<T>(3.14));
    auto y1 = std::vector<T>(x1.size());
    auto y2 = std::vector<T>(x1.size());

    // A warm-up iteration.
    csr::compute::spmmtv_sequential<T>(matrix, x1, y1, y2);

    auto const start = std::chrono::steady_clock::now();
    for (auto i = 0; i < iteration_count; ++i) {
        csr::compute::spmmtv_sequential<T>(matrix, x1, y1, y2);
    }
    auto const end = std::chrono::steady_clock::now();

    print_summary<Mode::csr_sequential>(start, end, iteration_count);
}

template<typename T, Symmetric Symm>
auto run_sequential_hsf_benchmark(
        std::filesystem::path const& matrix_path,
        FileType const file_type,
        bool const verbose,
        int const iteration_count,
        hsf::RegionOrdering const region_ordering
    ) -> void
{
    constexpr auto hsf_symmetry = convert_symmetry<hsf::Symmetric>(Symm);
    auto const matrix = load_hsf_matrix<T, hsf_symmetry>(matrix_path, file_type, verbose, region_ordering);

    auto const x1 = std::vector<T>(matrix.column_count(), static_cast<T>(3.14));
    auto y1 = std::vector<T>(x1.size());
    auto y2 = std::vector<T>(x1.size());

    // A warm-up iteration.
    hsf::compute::spmmtv_sequential<T>(matrix, x1, y1, y2);

    auto const start = std::chrono::steady_clock::now();
    for (auto i = 0; i < iteration_count; ++i) {
        hsf::compute::spmmtv_sequential<T>(matrix, x1, y1, y2);
    }
    auto const end = std::chrono::steady_clock::now();

    print_summary<Mode::hsf_sequential>(start, end, iteration_count);
}

template<Mode TMode, hsf::ExecMode EM, typename T, Symmetric Symm>
auto run_parallel_hsf_benchmark(
        std::filesystem::path const& matrix_path,
        FileType const file_type,
        bool const verbose,
        int const iteration_count,
        hsf::RegionOrdering const region_ordering,
        double const region_coefficient,
        int const thread_count
    ) -> void
{
    omp_set_num_threads(thread_count);
    constexpr auto hsf_symmetry = convert_symmetry<hsf::Symmetric>(Symm);
    auto const matrix = load_hsf_matrix<T, hsf_symmetry>(
            matrix_path, file_type, verbose, region_ordering, region_coefficient, thread_count
        );

    auto exec_data = hsf::ExecData<EM, T, hsf_symmetry>{thread_count, matrix, hsf::tags::one_vector};
    auto const x1 = std::vector<T>(matrix.column_count(), static_cast<T>(3.14));
    auto y1 = std::vector<T>(x1.size());
    auto y2 = std::vector<T>(x1.size());

    // A warm-up iteration.
    hsf::compute::spmmtv_parallel<T>(matrix, x1, y1, y2, exec_data);

    auto const start = std::chrono::steady_clock::now();
    for (auto i = 0; i < iteration_count; ++i) {
        hsf::compute::spmmtv_parallel<T>(matrix, x1, y1, y2, exec_data);
    }
    auto const end = std::chrono::steady_clock::now();

    print_summary<TMode>(start, end, iteration_count);
}

}  // namespace
