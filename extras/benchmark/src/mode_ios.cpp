#include <mode_ios.hpp>

#include <istream>              // std::istream
#include <map>                  // std::map
#include <ostream>              // std::ostream
#include <stdexcept>            // std::logic_error std::out_of_range
#include <string>               // std::string
#include <string_view>          // std::string_view

#include <mode.hpp>             // Mode

namespace {

auto const modes = std::map<std::string_view, Mode>{
    {"coo-sequential",                Mode::coo_sequential},
    {"csr-sequential",                Mode::csr_sequential},
    {"hsf-sequential",                Mode::hsf_sequential},
    {"hsf-atomic-exclusive",          Mode::hsf_atomic_exclusive},
    {"hsf-atomic-planned-shared",     Mode::hsf_atomic_planned_shared},
    {"hsf-atomic-shared",             Mode::hsf_atomic_shared},
    {"hsf-locks-exclusive",           Mode::hsf_locks_exclusive},
    {"hsf-locks-planned-exclusive",   Mode::hsf_locks_planned_exclusive},
    {"hsf-locks-planned-shared",      Mode::hsf_locks_planned_shared},
    {"hsf-locks-shared",              Mode::hsf_locks_shared},
    {"hsf-per-thread-exclusive",      Mode::hsf_per_thread_exclusive},
    {"hsf-per-thread-planned-shared", Mode::hsf_per_thread_planned_shared},
    {"hsf-per-thread-shared",         Mode::hsf_per_thread_shared},
};

}  // namespace

auto operator<<(std::ostream& os, Mode const mode) -> std::ostream&
{
    switch (mode) {
    case Mode::coo_sequential:
        return os << "coo-sequential";
    case Mode::csr_sequential:
        return os << "csr-sequential";
    case Mode::hsf_sequential:
        return os << "hsf-sequential";
    case Mode::hsf_atomic_exclusive:
        return os << "hsf-atomic-exclusive";
    case Mode::hsf_atomic_planned_shared:
        return os << "hsf-atomic-planned-shared";
    case Mode::hsf_atomic_shared:
        return os << "hsf-atomic-shared";
    case Mode::hsf_locks_exclusive:
        return os << "hsf-locks-exclusive";
    case Mode::hsf_locks_planned_exclusive:
        return os << "hsf-locks-planned-exclusive";
    case Mode::hsf_locks_planned_shared:
        return os << "hsf-locks-planned-shared";
    case Mode::hsf_locks_shared:
        return os << "hsf-locks-shared";
    case Mode::hsf_per_thread_exclusive:
        return os << "hsf-per-thread-exclusive";
    case Mode::hsf_per_thread_planned_shared:
        return os << "hsf-per-thread-planned-shared";
    case Mode::hsf_per_thread_shared:
        return os << "hsf-per-thread-shared";
    }

    throw std::logic_error{"Unexpected enumeration value."};
}

auto operator>>(std::istream& is, Mode& mode) -> std::istream&
{
    auto token = std::string{};
    is >> token;
    try {
        mode = modes.at(token);
    } catch (std::out_of_range const& e) {
        is.setstate(std::istream::failbit);
    }
    return is;
}
