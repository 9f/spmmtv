#include <benchmark.hpp>

#include <algorithm>            // std::copy
#include <chrono>               // std::chrono
#include <functional>           // std::invoke
#include <span>                 // std::span
#include <stdexcept>            // std::invalid_argument std::runtime_error
#include <vector>               // std::vector

#include <omp.h>                // omp_set_num_threads

#include <rsb.h>                // rsb_mtx_alloc_from_coo_const rsb_spmv

#include <coo/matrix.hpp>       // coo::Matrix
#include <coo/symmetric.hpp>    // coo::Symmetric

#include <rsb/library.hpp>      // rsb::Library
#include <rsb/matrix.hpp>       // rsb::Matrix

namespace {

auto compute_duration(
        std::chrono::steady_clock::time_point const& start,
        std::chrono::steady_clock::time_point const& end,
        int iteration_count
    ) -> std::chrono::duration<double>;

template<typename T>
auto get_type_code() noexcept -> rsb_type_t;

template<typename T, coo::Symmetric Symm>
auto init_rsb_matrix(coo::Matrix<T, Symm> const& matrix) -> rsb::Matrix;

template<typename T>
auto run_spmmtv_benchmark_general(
        rsb::Matrix const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        int iteration_count
    ) -> std::chrono::duration<double>;

template<typename T>
auto run_spmmtv_benchmark_symmetric(
        rsb::Matrix const& matrix,
        std::span<T const> x1,
        std::span<T> y1,
        std::span<T> y2,
        int iteration_count
    ) -> std::chrono::duration<double>;

}  // namespace

template<typename T, coo::Symmetric Symm>
auto run_spmmtv_benchmark(
        coo::Matrix<T, Symm>& matrix, std::span<T const> const x1, int const iteration_count, int const thread_count
    ) -> std::chrono::duration<double>
{
    omp_set_num_threads(thread_count);

    auto library = rsb::Library{RSB_NULL_INIT_OPTIONS};
    auto matrix_rsb = init_rsb_matrix(matrix);

    auto y1 = std::vector<T>(matrix.row_count());
    auto y2 = std::vector<T>(matrix.column_count());

    auto const duration = std::invoke([&] {
        switch (Symm) {
        case coo::Symmetric::no:
            return run_spmmtv_benchmark_general<T>(matrix_rsb, x1, y1, y2, iteration_count);
        case coo::Symmetric::yes:
            return run_spmmtv_benchmark_symmetric<T>(matrix_rsb, x1, y1, y2, iteration_count);
        }
    });

    library.exit(RSB_NULL_EXIT_OPTIONS);
    return duration;
}

template auto run_spmmtv_benchmark(
        coo::Matrix<float, coo::Symmetric::no>& matrix,
        std::span<float const> x1,
        int iteration_count,
        int thread_count
    ) -> std::chrono::duration<double>;

template auto run_spmmtv_benchmark(
        coo::Matrix<float, coo::Symmetric::yes>& matrix,
        std::span<float const> x1,
        int iteration_count,
        int thread_count
    ) -> std::chrono::duration<double>;

template auto run_spmmtv_benchmark(
        coo::Matrix<double, coo::Symmetric::no>& matrix,
        std::span<double const> x1,
        int iteration_count,
        int thread_count
    ) -> std::chrono::duration<double>;

template auto run_spmmtv_benchmark(
        coo::Matrix<double, coo::Symmetric::yes>& matrix,
        std::span<double const> x1,
        int iteration_count,
        int thread_count
    ) -> std::chrono::duration<double>;

namespace {

auto compute_duration(
        std::chrono::steady_clock::time_point const& start,
        std::chrono::steady_clock::time_point const& end,
        int const iteration_count
    ) -> std::chrono::duration<double>
{
    if (iteration_count <= 0) {
        throw std::invalid_argument{"The iteration count cannot be less than or equal to zero."};
    }

    auto const elapsed = std::chrono::duration<double>{end - start};
    return elapsed / iteration_count;
}

template<>
auto get_type_code<float>() noexcept -> rsb_type_t
{
    return RSB_NUMERICAL_TYPE_FLOAT;
}

template<>
auto get_type_code<double>() noexcept -> rsb_type_t
{
    return RSB_NUMERICAL_TYPE_DOUBLE;
}

template<typename T, coo::Symmetric Symm>
auto init_rsb_matrix(coo::Matrix<T, Symm> const& matrix) -> rsb::Matrix
{
    auto const matrix_flags = std::invoke([&] {
        switch (Symm) {
        case coo::Symmetric::no:
            return RSB_FLAG_DEFAULT_RSB_MATRIX_FLAGS;
        case coo::Symmetric::yes:
            return RSB_FLAG_DEFAULT_RSB_MATRIX_FLAGS | RSB_FLAG_SYMMETRIC | RSB_FLAG_UPPER;
        }
    });

    auto error = rsb_err_t{};
    auto* const matrix_rsb = rsb_mtx_alloc_from_coo_const(
            matrix.elements.data(),
            reinterpret_cast<std::int32_t const*>(matrix.rows.data()),
            reinterpret_cast<std::int32_t const*>(matrix.columns.data()),
            static_cast<rsb_nnz_idx_t>(matrix.element_count()),
            get_type_code<T>(),
            static_cast<rsb_coo_idx_t>(matrix.row_count()),
            static_cast<rsb_coo_idx_t>(matrix.column_count()),
            RSB_DEFAULT_ROW_BLOCKING,
            RSB_DEFAULT_ROW_BLOCKING,
            matrix_flags,
            &error
        );

    if (matrix_rsb == nullptr) {
        throw std::runtime_error{"Function ‘rsb_mtx_alloc_from_coo_const’ has returned ‘nullptr’."};
    }

    auto matrix_rsb_obj = rsb::Matrix{matrix_rsb};

    if (error != RSB_ERR_NO_ERROR) {
        throw std::runtime_error{"Function ‘rsb_mtx_alloc_from_coo_const’ has failed."};
    }

    return matrix_rsb_obj;
}

template<typename T>
auto run_spmmtv_benchmark_general(
        rsb::Matrix const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        int const iteration_count
    ) -> std::chrono::duration<double>
{
    // Warm up.
    auto const error1 = rsb_spmv(
            RSB_TRANSPOSITION_N,
            nullptr,
            matrix(),
            x1.data(),
            1,
            nullptr,
            y1.data(),
            1
        );
    if (error1 != RSB_ERR_NO_ERROR) {
        throw std::runtime_error{"The first call to function ‘rsb_spmv’ has failed."};
    }

    // Warm up.
    auto const error2 = rsb_spmv(
            RSB_TRANSPOSITION_T,
            nullptr,
            matrix(),
            x1.data(),
            1,
            nullptr,
            y2.data(),
            1
        );
    if (error2 != RSB_ERR_NO_ERROR) {
        throw std::runtime_error{"The second call to function ‘rsb_spmv’ has failed."};
    }

    auto const start = std::chrono::steady_clock::now();
    for (auto i = 0; i < iteration_count; ++i) {
        auto const error1 = rsb_spmv(
                RSB_TRANSPOSITION_N,
                nullptr,
                matrix(),
                x1.data(),
                1,
                nullptr,
                y1.data(),
                1
            );
        if (error1 != RSB_ERR_NO_ERROR) {
            throw std::runtime_error{"The first call to function ‘rsb_spmv’ has failed."};
        }

        auto const error2 = rsb_spmv(
                RSB_TRANSPOSITION_T,
                nullptr,
                matrix(),
                x1.data(),
                1,
                nullptr,
                y2.data(),
                1
            );
        if (error2 != RSB_ERR_NO_ERROR) {
            throw std::runtime_error{"The second call to function ‘rsb_spmv’ has failed."};
        }
    }
    auto const end = std::chrono::steady_clock::now();

    return compute_duration(start, end, iteration_count);
}

template<typename T>
auto run_spmmtv_benchmark_symmetric(
        rsb::Matrix const& matrix,
        std::span<T const> const x1,
        std::span<T> const y1,
        std::span<T> const y2,
        int const iteration_count
    ) -> std::chrono::duration<double>
{
    // Warm up.
    auto const error = rsb_spmv(
            RSB_TRANSPOSITION_N,
            nullptr,
            matrix(),
            x1.data(),
            1,
            nullptr,
            y1.data(),
            1
        );
    if (error != RSB_ERR_NO_ERROR) {
        throw std::runtime_error{"Function ‘rsb_spmv’ has failed."};
    }
    std::copy(y1.begin(), y1.end(), y2.begin());

    auto const start = std::chrono::steady_clock::now();
    for (auto i = 0; i < iteration_count; ++i) {
        auto const error = rsb_spmv(
                RSB_TRANSPOSITION_N,
                nullptr,
                matrix(),
                x1.data(),
                1,
                nullptr,
                y1.data(),
                1
            );
        if (error != RSB_ERR_NO_ERROR) {
            throw std::runtime_error{"Function ‘rsb_spmv’ has failed."};
        }
        std::copy(y1.begin(), y1.end(), y2.begin());
    }
    auto const end = std::chrono::steady_clock::now();

    return compute_duration(start, end, iteration_count);
}

}  // namespace
