#pragma once

#include <chrono>               // std::chrono
#include <span>                 // std::span

namespace coo {

enum struct Symmetric;
template<typename T, Symmetric Symm> class Matrix;

}  // namespace coo

template<typename T, coo::Symmetric Symm>
auto run_spmmtv_benchmark(coo::Matrix<T, Symm>& matrix, std::span<T const> x1, int iteration_count, int thread_count)
    -> std::chrono::duration<double>;
