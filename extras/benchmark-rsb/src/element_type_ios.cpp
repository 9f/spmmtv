#include <element_type_ios.hpp>

#include <istream>              // std::istream
#include <map>                  // std::map
#include <ostream>              // std::ostream
#include <stdexcept>            // std::logic_error std::out_of_range
#include <string>               // std::string
#include <string_view>          // std::string_view

#include <element_type.hpp>     // ElementType

namespace {

auto const element_types = std::map<std::string_view, ElementType>{
    {"float32", ElementType::float32},
    {"float64", ElementType::float64},
};

}  // namespace

auto operator<<(std::ostream& os, ElementType element_type) -> std::ostream&
{
    switch (element_type) {
    case ElementType::float32:
        return os << "float32";
    case ElementType::float64:
        return os << "float64";
    }

    throw std::logic_error{"Unexpected enumeration value."};
}

auto operator>>(std::istream& is, ElementType& element_type) -> std::istream&
{
    auto token = std::string{};
    is >> token;
    try {
        element_type = element_types.at(token);
    } catch (std::out_of_range const& e) {
        is.setstate(std::istream::failbit);
    }
    return is;
}
