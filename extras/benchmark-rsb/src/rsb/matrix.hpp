#pragma once

#include <rsb.h>                // rsb_mtx_t

namespace rsb {

class Matrix {
public:
    Matrix() = default;
    explicit Matrix(rsb_mtx_t* handle) noexcept;

    ~Matrix() noexcept;

    Matrix(Matrix const&) = delete;
    Matrix(Matrix&&) = default;

    auto operator=(Matrix const&) -> Matrix& = delete;
    auto operator=(Matrix&&) -> Matrix& = default;

    auto operator()() noexcept -> rsb_mtx_t*;
    auto operator()() const noexcept -> rsb_mtx_t const*;

private:
    rsb_mtx_t* handle_{};
};

}  // namespace rsb
