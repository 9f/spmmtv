#pragma once

#include <rsb.h>                // rsb_initopts

namespace rsb {

class Library {
public:
    Library() = default;
    explicit Library(rsb_initopts* initopts);

    ~Library() noexcept;

    Library(Library const&) = delete;
    Library(Library&&) = default;

    auto operator=(Library const&) -> Library& = delete;
    auto operator=(Library&&) -> Library& = default;

    auto exit(rsb_initopts* initopts) -> void;

private:
    bool initialised_{};
};

}  // namespace rsb
