#include <rsb/matrix.hpp>

#include <rsb.h>                // rsb_mtx_free rsb_mtx_t

namespace rsb {

Matrix::Matrix(rsb_mtx_t* const handle) noexcept
    : handle_{handle}
{
}

Matrix::~Matrix() noexcept
{
    if (this->handle_ != nullptr) {
        rsb_mtx_free(this->handle_);
    }
}

auto Matrix::operator()() noexcept -> rsb_mtx_t*
{
    return this->handle_;
}

auto Matrix::operator()() const noexcept -> rsb_mtx_t const*
{
    return this->handle_;
}

}  // namespace rsb
