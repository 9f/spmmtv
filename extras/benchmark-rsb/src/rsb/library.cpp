#include <rsb/library.hpp>

#include <stdexcept>            // std::runtime_error

#include <rsb.h>                // rsb_lib_exit rsb_lib_init

namespace rsb {

Library::Library(rsb_initopts* const initopts)
{
    if (rsb_lib_init(initopts) != RSB_ERR_NO_ERROR) {
        throw std::runtime_error{"Function ‘rsb_lib_init’ has failed."};
    }
    this->initialised_ = true;
}

Library::~Library() noexcept
{
    if (this->initialised_) {
        rsb_lib_exit(RSB_NULL_EXIT_OPTIONS);
    }
}

auto Library::exit(rsb_initopts* const initopts) -> void
{
    this->initialised_ = false;
    if (rsb_lib_exit(initopts) != RSB_ERR_NO_ERROR) {
        throw std::runtime_error{"Function ‘rsb_lib_exit’ has failed."};
    }
}

}  // namespace rsb
