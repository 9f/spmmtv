#include <exception>            // std::exception
#include <filesystem>           // std::filesystem
#include <functional>           // std::invoke
#include <iostream>             // std::cout std::cerr
#include <stdexcept>            // std::logic_error
#include <vector>               // std::vector

#include <boost/program_options/errors.hpp>  // boost::program_options::error

#include <coo/matrix.hpp>       // coo::Matrix
#include <coo/matrix_ios.hpp>   // coo::operator<<
#include <coo/symmetric.hpp>    // coo::Symmetric

#include <benchmark.hpp>        // run_spmmtv_benchmark
#include <command_line_argument_error.hpp>  // CommandLineArgumentError
#include <command_line_arguments.hpp>  // CommandLineArguments
#include <element_type.hpp>     // ElementType
#include <file_type.hpp>        // FileType
#include <symmetric.hpp>        // Symmetric

namespace {

template<typename ToSymmetry, typename FromSymmetry>
constexpr auto convert_symmetry(FromSymmetry from_symmetry) noexcept -> ToSymmetry;

template<typename T, coo::Symmetric Symm>
auto load_coo_matrix(std::filesystem::path const& matrix_path, FileType file_type, bool verbose)
    -> coo::Matrix<T, Symm>;

template<typename T, Symmetric Symm>
auto run_benchmark(CommandLineArguments const& command_line_arguments) -> void;

}  // namespace

auto main(int argc, char** argv) -> int try
{
    auto const command_line_arguments = CommandLineArguments::parse(argc, argv);

    if (command_line_arguments.has_help()) {
        std::cout << command_line_arguments.get_help_message();
        return 0;
    }

    switch (command_line_arguments.get_element_type()) {
    case ElementType::float32:
        if (command_line_arguments.is_symmetric()) {
            run_benchmark<float, Symmetric::yes>(command_line_arguments);
        } else {
            run_benchmark<float, Symmetric::no>(command_line_arguments);
        }
        break;

    case ElementType::float64:
        if (command_line_arguments.is_symmetric()) {
            run_benchmark<double, Symmetric::yes>(command_line_arguments);
        } else {
            run_benchmark<double, Symmetric::no>(command_line_arguments);
        }
        break;
    }

    return 0;

} catch (boost::program_options::error const& e) {
    std::cerr << "Error: " << e.what() << ".\n";
    return 2;

} catch (CommandLineArgumentError const& e) {
    std::cerr << "Error: " << e.what() << '\n';
    return 2;

} catch (std::exception const& e) {
    std::cerr << "Error: " << e.what() << '\n';
    return 1;
}

namespace {

template<typename ToSymmetry, typename FromSymmetry>
constexpr auto convert_symmetry(FromSymmetry const from_symmetry) noexcept -> ToSymmetry
{
    switch (from_symmetry) {
    case FromSymmetry::no:
        return ToSymmetry::no;
    case FromSymmetry::yes:
        return ToSymmetry::yes;
    }
}

template<typename T, coo::Symmetric Symm>
auto load_coo_matrix(std::filesystem::path const& matrix_path, FileType const file_type, bool const verbose)
    -> coo::Matrix<T, Symm>
{
    auto const matrix = std::invoke([&] {
        switch (file_type) {
        case FileType::HDF5:
            return coo::Matrix<T, Symm>::load_from_hdf5(matrix_path);
        case FileType::MM:
            return coo::Matrix<T, Symm>::load_from_mm(matrix_path);
        }
        throw std::logic_error{"Unexpected matrix file type."};
    });
    if (verbose) {
        std::cout << "COO matrix properties:\n"
                  << matrix
                  << '\n';
    }
    return matrix;
}

template<typename T, Symmetric Symm>
auto run_benchmark(CommandLineArguments const& command_line_arguments) -> void
{
    constexpr auto coo_symmetry = convert_symmetry<coo::Symmetric>(Symm);
    auto matrix = load_coo_matrix<T, coo_symmetry>(
            command_line_arguments.get_matrix_path(),
            command_line_arguments.get_file_type(),
            command_line_arguments.be_verbose()
        );
    auto const x1 = std::vector<T>(matrix.column_count(), static_cast<T>(3.14));

    auto const duration = run_spmmtv_benchmark<T>(
            matrix,
            x1,
            command_line_arguments.get_iteration_count(),
            command_line_arguments.get_thread_count()
        );

    std::cout << "the run time of a single SpMMᵀV iteration in seconds:\n"
              << duration.count() << '\n';
}

}  // namespace
