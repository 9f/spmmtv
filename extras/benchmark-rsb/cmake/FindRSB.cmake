# FindRSB
# -------
#
# Find the Recursive Sparse Blocks library (librsb).
#
# Imported Targets
# ^^^^^^^^^^^^^^^^
#
# RSB::RSB
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# RSB_FOUND: True if librsb has been found.
#
# RSB_INCLUDE_DIRS: The directories to include when using librsb.
#
# RSB_LIBRARIES: The libraries to link when using librsb.

find_path(RSB_INCLUDE_DIR NAMES rsb.h)
mark_as_advanced(RSB_INCLUDE_DIR)

find_library(RSB_LIBRARY NAMES librsb rsb)
mark_as_advanced(RSB_LIBRARY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(RSB REQUIRED_VARS RSB_INCLUDE_DIR RSB_LIBRARY)

if(RSB_FOUND)
  set(RSB_INCLUDE_DIRS "${RSB_INCLUDE_DIR}")
  set(RSB_LIBRARIES "${RSB_LIBRARY}")
endif()

if(RSB_FOUND AND NOT TARGET RSB::RSB)
  add_library(RSB::RSB UNKNOWN IMPORTED)
  set_target_properties(RSB::RSB PROPERTIES
    IMPORTED_LOCATION "${RSB_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${RSB_INCLUDE_DIRS}"
    )
endif()
