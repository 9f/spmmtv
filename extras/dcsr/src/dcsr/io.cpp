#include <dcsr/io.hpp>

#include <array>                // std::to_array
#include <cstddef>              // std::size_t
#include <cstdint>              // std::uint32_t std::uint64_t
#include <filesystem>           // std::filesystem
#include <string>               // std::string
#include <tuple>                // std::tuple
#include <utility>              // std::move
#include <vector>               // std::vector

#include <boost/mpi/communicator.hpp>  // boost::mpi::comm_attach boost::mpi::communicator

#include <H5Cpp.h>              // H5

#include <mpi.h>                // MPI_*

#include <csr/matrix.hpp>       // csr::Matrix
#include <csr/symmetric.hpp>    // csr::Symmetric

#include <dcsr/runtime_error.hpp>  // dcsr::RuntimeError

namespace dcsr {

namespace {

// Return ‘H5::PredType::NATIVE_FLOAT’.
auto get_h5_pred_type(float) noexcept -> H5::PredType const&;

// Return ‘H5::PredType::NATIVE_DOUBLE’.
auto get_h5_pred_type(double) noexcept -> H5::PredType const&;

}  // namespace

template<typename T, csr::Symmetric Symm>
auto load_from_hdf5(boost::mpi::communicator const& comm, std::filesystem::path const& file_path)
    -> std::tuple<csr::Matrix<T, Symm>, std::size_t, std::size_t, std::size_t> try
{
    auto const file_acc_prop_list = H5::FileAccPropList{};
    H5Pset_fapl_mpio(file_acc_prop_list.getId(), comm, MPI_INFO_NULL);
    auto const file = H5::H5File{file_path, H5F_ACC_RDONLY, H5::FileCreatPropList::DEFAULT, file_acc_prop_list};

    auto const matrix_format_attribute = file.openAttribute("matrix format");
    if (matrix_format_attribute.getSpace().getSelectNpoints() != 1) {
        throw RuntimeError{"Failed to read the matrix format from the specified HDF5 file."};
    }
    auto const matrix_format_str_size = matrix_format_attribute.getDataType().getSize();
    auto matrix_format_type = H5::StrType{H5::PredType::C_S1};
    matrix_format_type.setCset(H5T_CSET_UTF8);
    matrix_format_type.setSize(matrix_format_str_size + 1);
    auto matrix_format = std::string{};
    matrix_format_attribute.read(matrix_format_type, matrix_format);
    if (matrix_format != "CSR") {
        throw RuntimeError{"The specified HDF5 file doesn’t contain a CSR matrix."};
    }

    auto const symmetric_attribute = file.openAttribute("symmetric");
    if (symmetric_attribute.getSpace().getSelectNpoints() != 1) {
        throw RuntimeError{"Failed to read the symmetry of the CSR matrix from the specified HDF5 file."};
    }
    auto const symmetric_type = H5::EnumType{H5::PredType::NATIVE_INT};
    auto symmetric = static_cast<int>(csr::Symmetric::no);
    symmetric_type.insert("FALSE", &symmetric);
    symmetric = static_cast<int>(csr::Symmetric::yes);
    symmetric_type.insert("TRUE", &symmetric);
    symmetric_attribute.read(symmetric_type, &symmetric);
    switch (Symm) {
    case csr::Symmetric::no:
        if (symmetric != static_cast<int>(csr::Symmetric::no)) {
            throw RuntimeError{"The CSR matrix in the specified HDF5 file is symmetric instead of general."};
        }
        break;
    case csr::Symmetric::yes:
        if (symmetric != static_cast<int>(csr::Symmetric::yes)) {
            throw RuntimeError{"The CSR matrix in the specified HDF5 file is general instead of symmetric."};
        }
        break;
    }

    auto const row_count_attribute = file.openAttribute("row count");
    if (row_count_attribute.getSpace().getSelectNpoints() != 1) {
        throw RuntimeError{"Failed to read the row count of the CSR matrix from the specified HDF5 file."};
    }
    auto global_row_count = std::uint64_t{};
    row_count_attribute.read(H5::PredType::NATIVE_UINT64, &global_row_count);

    auto const column_count_attribute = file.openAttribute("column count");
    if (column_count_attribute.getSpace().getSelectNpoints() != 1) {
        throw RuntimeError{"Failed to read the column count of the CSR matrix from the specified HDF5 file."};
    }
    auto global_column_count = std::uint64_t{};
    column_count_attribute.read(H5::PredType::NATIVE_UINT64, &global_column_count);

    auto const element_count_attribute = file.openAttribute("element count");
    if (element_count_attribute.getSpace().getSelectNpoints() != 1) {
        throw RuntimeError{"Failed to read the element count of the CSR matrix from the specified HDF5 file."};
    }
    auto global_element_count = std::uint64_t{};
    element_count_attribute.read(H5::PredType::NATIVE_UINT64, &global_element_count);

    auto const comm_rank_u64 = static_cast<std::uint64_t>(comm.rank());
    auto const comm_size_u64 = static_cast<std::uint64_t>(comm.size());

    auto row_begin = std::uint64_t{};
    auto row_end = std::uint64_t{};
    auto const divided_row_count = global_row_count / comm_size_u64;
    auto const remainder_row_count = global_row_count % comm_size_u64;

    if (comm_rank_u64 < remainder_row_count) {
        row_begin = (divided_row_count + 1) * comm_rank_u64;
        row_end = row_begin + (divided_row_count + 1);

    } else {
        auto const regular_rank_count = comm_rank_u64 - remainder_row_count;
        row_begin = (divided_row_count + 1) * remainder_row_count + divided_row_count * regular_rank_count;
        row_end = row_begin + divided_row_count;
    }

    auto const xfer_prop_list = H5::DSetMemXferPropList{};
    H5Pset_dxpl_mpio(xfer_prop_list.getId(), H5FD_MPIO_COLLECTIVE);

    auto row_pointers = std::vector<std::uint32_t>((row_end + 1) - row_begin);
    auto const row_ptrs_mem_dims = std::to_array<hsize_t>({row_pointers.size()});
    auto const row_ptrs_mem_space = H5::DataSpace{1, row_ptrs_mem_dims.data()};
    auto const row_ptrs_dataset = file.openDataSet("row pointers");
    auto const row_ptrs_file_space = row_ptrs_dataset.getSpace();
    auto const row_ptrs_file_count = std::to_array<hsize_t>({row_pointers.size()});
    auto const row_ptrs_file_start = std::to_array<hsize_t>({row_begin});
    row_ptrs_file_space.selectHyperslab(H5S_SELECT_SET, row_ptrs_file_count.data(), row_ptrs_file_start.data());
    row_ptrs_dataset.read(
            row_pointers.data(), H5::PredType::NATIVE_UINT32, row_ptrs_mem_space, row_ptrs_file_space, xfer_prop_list
        );

    auto const element_count = static_cast<std::uint64_t>(row_pointers.back() - row_pointers.front());
    auto columns = std::vector<std::uint32_t>(element_count);
    auto const columns_mem_dims = std::to_array<hsize_t>({columns.size()});
    auto const columns_mem_space = H5::DataSpace{1, columns_mem_dims.data()};
    auto const columns_dataset = file.openDataSet("column indices");
    auto const columns_file_space = columns_dataset.getSpace();
    auto const columns_file_count = std::to_array<hsize_t>({columns.size()});
    auto const columns_file_start = std::to_array<hsize_t>({row_pointers.front()});
    columns_file_space.selectHyperslab(H5S_SELECT_SET, columns_file_count.data(), columns_file_start.data());
    columns_dataset.read(
            columns.data(), H5::PredType::NATIVE_UINT32, columns_mem_space, columns_file_space, xfer_prop_list
        );

    auto elements = std::vector<T>(element_count);
    auto const elements_mem_dims = std::to_array<hsize_t>({elements.size()});
    auto const elements_mem_space = H5::DataSpace{1, elements_mem_dims.data()};
    auto const elements_dataset = file.openDataSet("element values");
    auto const elements_file_space = elements_dataset.getSpace();
    auto const elements_file_count = std::to_array<hsize_t>({elements.size()});
    auto const elements_file_start = std::to_array<hsize_t>({row_pointers.front()});
    elements_file_space.selectHyperslab(H5S_SELECT_SET, elements_file_count.data(), elements_file_start.data());
    elements_dataset.read(
            elements.data(), get_h5_pred_type(T{}), elements_mem_space, elements_file_space, xfer_prop_list
        );

    if (!row_pointers.empty() && row_pointers.front() != 0) {
        auto const offset = row_pointers.front();
        for (auto& row_pointer : row_pointers) {
            row_pointer -= offset;
        }
    }

    auto matrix = csr::Matrix<T, Symm>{
            std::move(row_pointers), std::move(columns), std::move(elements), global_column_count
        };
    return {std::move(matrix), row_begin, global_row_count, global_element_count};

} catch (H5::Exception const&) {
    throw RuntimeError{"Reading the CSR matrix from the specified HDF5 file has failed."};
}

template auto load_from_hdf5(boost::mpi::communicator const& comm, std::filesystem::path const& file_path)
    -> std::tuple<csr::Matrix<float, csr::Symmetric::no>, std::size_t, std::size_t, std::size_t>;

template auto load_from_hdf5(boost::mpi::communicator const& comm, std::filesystem::path const& file_path)
    -> std::tuple<csr::Matrix<float, csr::Symmetric::yes>, std::size_t, std::size_t, std::size_t>;

template auto load_from_hdf5(boost::mpi::communicator const& comm, std::filesystem::path const& file_path)
    -> std::tuple<csr::Matrix<double, csr::Symmetric::no>, std::size_t, std::size_t, std::size_t>;

template auto load_from_hdf5(boost::mpi::communicator const& comm, std::filesystem::path const& file_path)
    -> std::tuple<csr::Matrix<double, csr::Symmetric::yes>, std::size_t, std::size_t, std::size_t>;

template<typename T, csr::Symmetric Symm>
auto load_from_hdf5(MPI_Comm const comm, std::filesystem::path const& file_path)
    -> std::tuple<csr::Matrix<T, Symm>, std::size_t, std::size_t, std::size_t>
{
    auto const boost_comm = boost::mpi::communicator{comm, boost::mpi::comm_attach};
    return load_from_hdf5<T, Symm>(boost_comm, file_path);
}

template auto load_from_hdf5(MPI_Comm comm, std::filesystem::path const& file_path)
    -> std::tuple<csr::Matrix<float, csr::Symmetric::no>, std::size_t, std::size_t, std::size_t>;

template auto load_from_hdf5(MPI_Comm comm, std::filesystem::path const& file_path)
    -> std::tuple<csr::Matrix<float, csr::Symmetric::yes>, std::size_t, std::size_t, std::size_t>;

template auto load_from_hdf5(MPI_Comm comm, std::filesystem::path const& file_path)
    -> std::tuple<csr::Matrix<double, csr::Symmetric::no>, std::size_t, std::size_t, std::size_t>;

template auto load_from_hdf5(MPI_Comm comm, std::filesystem::path const& file_path)
    -> std::tuple<csr::Matrix<double, csr::Symmetric::yes>, std::size_t, std::size_t, std::size_t>;

namespace {

auto get_h5_pred_type(float) noexcept -> H5::PredType const&
{
    return H5::PredType::NATIVE_FLOAT;
}

auto get_h5_pred_type(double) noexcept -> H5::PredType const&
{
    return H5::PredType::NATIVE_DOUBLE;
}

}  // namespace

}  // namespace dcsr
