#pragma once

#include <cstddef>              // std::size_t
#include <filesystem>           // std::filesystem
#include <tuple>                // std::tuple

#include <boost/mpi/communicator.hpp>  // boost::mpi::communicator

#include <mpi.h>                // MPI_Comm

#include <csr/matrix.hpp>       // csr::Matrix
#include <csr/symmetric.hpp>    // csr::Symmetric

namespace dcsr {

// Load a CSR matrix from the HDF5 file specified by ‘file_path’.  The rows of
// the loaded CSR matrix are evenly distributed between all processes of the
// MPI communicator specified by ‘comm’.  Throw ‘dcsr::RuntimeError’ if loading
// the matrix fails.  Throw ‘csr::LogicError’ if constructing the CSR matrix
// from the loaded data fails.  The behaviour of this function is undefined if
// it isn’t called collectively by all processes in ‘comm’.  Return the loaded
// CSR matrix, the initial row index, the global row count and the global
// nonzero element count.  The global column count is equal to the local column
// count of each returned CSR matrix.

template<typename T, csr::Symmetric Symm>
auto load_from_hdf5(boost::mpi::communicator const& comm, std::filesystem::path const& file_path)
    -> std::tuple<csr::Matrix<T, Symm>, std::size_t, std::size_t, std::size_t>;

template<typename T, csr::Symmetric Symm>
auto load_from_hdf5(MPI_Comm comm, std::filesystem::path const& file_path)
    -> std::tuple<csr::Matrix<T, Symm>, std::size_t, std::size_t, std::size_t>;

}  // namespace dcsr
