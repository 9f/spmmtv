project(dcsr LANGUAGES CXX)

add_library("${PROJECT_NAME}" src/dcsr/io.cpp)


# Dependencies

find_package(Boost REQUIRED COMPONENTS mpi)
find_package(HDF5 REQUIRED COMPONENTS CXX)
find_package(MPI REQUIRED COMPONENTS CXX)

if(NOT HDF5_IS_PARALLEL)
    message(FATAL_ERROR "The found HDF5 library lacks parallel I/O support.")
endif()


# Compilation Settings

set_target_properties("${PROJECT_NAME}" PROPERTIES
  CXX_EXTENSIONS FALSE
  CXX_STANDARD_REQUIRED TRUE
  INTERPROCEDURAL_OPTIMIZATION_RELEASE TRUE
  INTERPROCEDURAL_OPTIMIZATION_RELWITHDEBINFO TRUE
  )

target_compile_features("${PROJECT_NAME}" PUBLIC cxx_std_20)
target_compile_options("${PROJECT_NAME}" PRIVATE "${HDF5_CXX_DEFINITIONS}")
target_include_directories("${PROJECT_NAME}" PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
target_include_directories("${PROJECT_NAME}" SYSTEM PRIVATE "${HDF5_CXX_INCLUDE_DIRS}")

target_link_libraries("${PROJECT_NAME}"
  PRIVATE
  "${HDF5_CXX_LIBRARIES}"
  PUBLIC
  Boost::mpi
  csr
  MPI::MPI_CXX
  )

enable_development_options("${PROJECT_NAME}")
treat_warnings_as_errors("${PROJECT_NAME}")
