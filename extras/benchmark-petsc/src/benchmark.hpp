#pragma once

#include <petscmat.h>           // Mat PetscErrorCode PetscInt Vec

#include <logger.hpp>           // Logger

auto run_benchmark(Mat A, Vec x1, Vec x2, Vec y1, Vec y2, PetscInt iteration_count, Logger& logger)
    -> PetscErrorCode;
