#include <benchmark.hpp>

#include <petscmat.h>           // CHKERRQ Mat* Petsc* Vec

#include <logger.hpp>           // Logger

auto run_benchmark(
        Mat const A,
        Vec const x1,
        Vec const x2,
        Vec const y1,
        Vec const y2,
        PetscInt const iteration_count,
        Logger& logger
    ) -> PetscErrorCode
{
    logger = Logger{iteration_count};
    if (iteration_count == 0) {
        return 0;
    }

    // A warm-up iteration.
    logger.set_end_to_end_start(0);

    logger.set_spmv_start(0);
    auto ierr = MatMult(A, x1, y1);
    CHKERRQ(ierr);
    logger.set_spmv_stop(0);

    logger.set_spmtv_start(0);
    ierr = MatMultTranspose(A, x2, y2);
    CHKERRQ(ierr);
    logger.set_spmtv_stop(0);

    logger.set_end_to_end_stop(0);

    auto stage = PetscLogStage{};
    ierr = PetscLogStageRegister("SpMMTV", &stage);
    CHKERRQ(ierr);
    ierr = PetscLogStagePush(stage);
    CHKERRQ(ierr);

    for (auto i = PetscInt{}; i < iteration_count; ++i) {
        logger.set_end_to_end_start(i);

        logger.set_spmv_start(i);
        ierr = MatMult(A, x1, y1);
        CHKERRQ(ierr);
        logger.set_spmv_stop(i);

        logger.set_spmtv_start(i);
        ierr = MatMultTranspose(A, x2, y2);
        CHKERRQ(ierr);
        logger.set_spmtv_stop(i);

        logger.set_end_to_end_stop(i);
    }

    ierr = PetscLogStagePop();
    CHKERRQ(ierr);

    logger.compute_durations();
    return 0;
}
