#pragma once

#include <chrono>               // std::chrono
#include <cstddef>              // std::ptrdiff_t std::size_t
#include <vector>               // std::vector

#include <petscsystypes.h>      // PetscInt

class Logger {
public:
    Logger() = default;
    explicit Logger(PetscInt iteration_count);

    auto compute_durations() -> void;

    auto get_iteration_count() const noexcept -> std::size_t;
    auto get_iteration_count_z() const noexcept -> std::ptrdiff_t;

    auto get_end_to_end_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto set_end_to_end_start(PetscInt iteration) -> void;
    auto set_end_to_end_stop(PetscInt iteration) -> void;

    auto get_spmv_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto set_spmv_start(PetscInt iteration) -> void;
    auto set_spmv_stop(PetscInt iteration) -> void;

    auto get_spmtv_duration() const noexcept -> std::chrono::steady_clock::duration const&;
    auto set_spmtv_start(PetscInt iteration) -> void;
    auto set_spmtv_stop(PetscInt iteration) -> void;

private:
    struct IterationTimePoints {
        std::chrono::steady_clock::time_point end_to_end_start{};
        std::chrono::steady_clock::time_point end_to_end_stop{};
        std::chrono::steady_clock::time_point spmv_start{};
        std::chrono::steady_clock::time_point spmv_stop{};
        std::chrono::steady_clock::time_point spmtv_start{};
        std::chrono::steady_clock::time_point spmtv_stop{};
    };

    std::vector<IterationTimePoints> iteration_time_points_{};
    std::chrono::steady_clock::duration end_to_end_duration_{};
    std::chrono::steady_clock::duration spmv_duration_{};
    std::chrono::steady_clock::duration spmtv_duration_{};
};
