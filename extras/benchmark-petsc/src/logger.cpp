#include <logger.hpp>

#include <cassert>              // assert
#include <chrono>               // std::chrono
#include <cstddef>              // std::ptrdiff_t std::size_t

#include <petscsystypes.h>      // PetscInt

Logger::Logger(PetscInt const iteration_count)
    : iteration_time_points_(static_cast<std::size_t>(iteration_count))
{
    assert(iteration_count >= 0);
}

auto Logger::compute_durations() -> void
{
    if (this->get_iteration_count() == 0) {
        return;
    }

    this->end_to_end_duration_ = std::chrono::steady_clock::duration{};
    this->spmv_duration_ = std::chrono::steady_clock::duration{};
    this->spmtv_duration_ = std::chrono::steady_clock::duration{};

    for (auto const& time_points : this->iteration_time_points_) {
        assert(time_points.end_to_end_stop >= time_points.end_to_end_start);
        this->end_to_end_duration_ += time_points.end_to_end_stop - time_points.end_to_end_start;

        assert(time_points.spmv_stop >= time_points.spmv_start);
        this->spmv_duration_ += time_points.spmv_stop - time_points.spmv_start;

        assert(time_points.spmtv_stop >= time_points.spmtv_start);
        this->spmtv_duration_ += time_points.spmtv_stop - time_points.spmtv_start;
    }

    this->end_to_end_duration_ /= this->get_iteration_count_z();
    this->spmv_duration_ /= this->get_iteration_count_z();
    this->spmtv_duration_ /= this->get_iteration_count_z();
}

auto Logger::get_iteration_count() const noexcept -> std::size_t
{
    return this->iteration_time_points_.size();
}

auto Logger::get_iteration_count_z() const noexcept -> std::ptrdiff_t
{
    return static_cast<std::ptrdiff_t>(this->get_iteration_count());
}

auto Logger::get_end_to_end_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->end_to_end_duration_;
}

auto Logger::set_end_to_end_start(PetscInt const iteration) -> void
{
    assert(iteration >= 0);
    auto const iteration_uz = static_cast<std::size_t>(iteration);
    assert(iteration_uz < this->iteration_time_points_.size());
    this->iteration_time_points_[iteration_uz].end_to_end_start = std::chrono::steady_clock::now();
}

auto Logger::set_end_to_end_stop(PetscInt const iteration) -> void
{
    assert(iteration >= 0);
    auto const iteration_uz = static_cast<std::size_t>(iteration);
    assert(iteration_uz < this->iteration_time_points_.size());
    this->iteration_time_points_[iteration_uz].end_to_end_stop = std::chrono::steady_clock::now();
}

auto Logger::get_spmv_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->spmv_duration_;
}

auto Logger::set_spmv_start(PetscInt const iteration) -> void
{
    assert(iteration >= 0);
    auto const iteration_uz = static_cast<std::size_t>(iteration);
    assert(iteration_uz < this->iteration_time_points_.size());
    this->iteration_time_points_[iteration_uz].spmv_start = std::chrono::steady_clock::now();
}

auto Logger::set_spmv_stop(PetscInt const iteration) -> void
{
    assert(iteration >= 0);
    auto const iteration_uz = static_cast<std::size_t>(iteration);
    assert(iteration_uz < this->iteration_time_points_.size());
    this->iteration_time_points_[iteration_uz].spmv_stop = std::chrono::steady_clock::now();
}

auto Logger::get_spmtv_duration() const noexcept -> std::chrono::steady_clock::duration const&
{
    return this->spmtv_duration_;
}

auto Logger::set_spmtv_start(PetscInt const iteration) -> void
{
    assert(iteration >= 0);
    auto const iteration_uz = static_cast<std::size_t>(iteration);
    assert(iteration_uz < this->iteration_time_points_.size());
    this->iteration_time_points_[iteration_uz].spmtv_start = std::chrono::steady_clock::now();
}

auto Logger::set_spmtv_stop(PetscInt const iteration) -> void
{
    assert(iteration >= 0);
    auto const iteration_uz = static_cast<std::size_t>(iteration);
    assert(iteration_uz < this->iteration_time_points_.size());
    this->iteration_time_points_[iteration_uz].spmtv_stop = std::chrono::steady_clock::now();
}
