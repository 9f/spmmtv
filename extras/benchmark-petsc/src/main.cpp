#include <array>                // std::array
#include <chrono>               // std::chrono
#include <exception>            // std::exception
#include <filesystem>           // std::filesystem
#include <iostream>             // std::cerr std::cout
#include <string>               // std::string_literals

#include <mpi.h>                // MPI_*

#include <petscmat.h>           // CHKERRMPI CHKERRQ Mat* PETSC* Petsc* Vec*

#include <csr/symmetric.hpp>    // csr::Symmetric

#include <dcsr/io.hpp>          // dcsr::load_from_hdf5

#include <benchmark.hpp>        // run_benchmark
#include <command_line_argument_error.hpp>  // CommandLineArgumentError
#include <logger.hpp>           // Logger

using namespace std::string_literals;

namespace {

auto init_matrix(MPI_Comm comm, std::filesystem::path const& matrix_path, Mat& matrix) -> PetscErrorCode;

auto measure_duration(
        MPI_Comm comm, PetscInt iteration_count, std::filesystem::path const& matrix_path, Logger& duration
    ) -> PetscErrorCode;

auto petsc_main(MPI_Comm comm) -> PetscErrorCode;

auto print_duration(MPI_Comm comm, PetscInt iteration_count, std::filesystem::path const& matrix_path)
    -> PetscErrorCode;

}  // namespace

auto main(int argc, char** argv) -> int try
{
    auto const help =
        "Measure the run time of distributed SpMMᵀV implemented using PETSc. Additional profiling results can by "
        "requested using the run-time options of PETSc.\n"s;
    auto ierr = PetscInitialize(&argc, &argv, nullptr, help.c_str());
    if (ierr != 0) {
        std::cerr << "Error: Failed to initialise the PETSc library.\n";
        return ierr;
    }

    auto const comm = PETSC_COMM_WORLD;
    try {
        ierr = petsc_main(comm);
        CHKERRQ(ierr);

    } catch (CommandLineArgumentError const& e) {
        auto comm_rank = int{};
        MPI_Comm_rank(comm, &comm_rank);
        if (comm_rank == 0) {
            std::cerr << "Error: " << e.what() << '\n';
        }
        PetscFinalize();
        return 2;

    } catch (std::exception const& e) {
        std::cerr << "Error: "s + e.what() + '\n';
        PETSCABORT(comm, 1);
    }

    return PetscFinalize();

} catch (std::exception const& e) {
    std::cerr << "Error: " << e.what() << '\n';
    return 1;
}

namespace {

auto init_matrix(MPI_Comm const comm, std::filesystem::path const& matrix_path, Mat& petsc_matrix) -> PetscErrorCode
{
    auto const [csr_matrix, row_offset, global_row_count, global_element_count]
        = dcsr::load_from_hdf5<PetscReal, csr::Symmetric::no>(comm, matrix_path);

    auto ierr = MatCreateMPIAIJWithArrays(
            comm,
            static_cast<PetscInt>(csr_matrix.row_count()),
            PETSC_DECIDE,
            static_cast<PetscInt>(global_row_count),
            static_cast<PetscInt>(csr_matrix.column_count()),
            reinterpret_cast<PetscInt const*>(csr_matrix.row_pointers.data()),
            reinterpret_cast<PetscInt const*>(csr_matrix.columns.data()),
            csr_matrix.elements.data(),
            &petsc_matrix
        );
    CHKERRQ(ierr);

    return 0;
}

auto measure_duration(
        MPI_Comm const comm, PetscInt const iteration_count, std::filesystem::path const& matrix_path, Logger& duration
    ) -> PetscErrorCode
{
    auto A = Mat{};
    auto ierr = init_matrix(comm, matrix_path, A);
    CHKERRQ(ierr);

    auto x1 = Vec{};
    auto y1 = Vec{};
    ierr = MatCreateVecs(A, &x1, &y1);
    CHKERRQ(ierr);
    ierr = VecSet(x1, 3.14);
    CHKERRQ(ierr);
    ierr = VecSet(y1, 0);
    CHKERRQ(ierr);

    auto x2 = Vec{};
    auto y2 = Vec{};
    ierr = MatCreateVecs(A, &y2, &x2);
    CHKERRQ(ierr);
    ierr = VecSet(x2, -2.72);
    CHKERRQ(ierr);
    ierr = VecSet(y2, 0);
    CHKERRQ(ierr);

    ierr = run_benchmark(A, x1, x2, y1, y2, iteration_count, duration);
    CHKERRQ(ierr);

    ierr = VecDestroy(&y2);
    CHKERRQ(ierr);
    ierr = VecDestroy(&x2);
    CHKERRQ(ierr);
    ierr = VecDestroy(&y1);
    CHKERRQ(ierr);
    ierr = VecDestroy(&x1);
    CHKERRQ(ierr);
    ierr = MatDestroy(&A);
    CHKERRQ(ierr);

    return 0;
}

auto petsc_main(MPI_Comm const comm) -> PetscErrorCode
{
    auto iteration_count = PetscInt{10};
    auto matrix_file_chars = std::array<char, PETSC_MAX_PATH_LEN>{};
    auto matrix_file_set = PetscBool{};

    auto ierr = PetscOptionsBegin(comm, nullptr, "Options for this program", nullptr);
    CHKERRQ(ierr);
    {
        ierr = PetscOptionsInt(
                "-iteration_count",
                "perform SpMMᵀV ‘iteration_count’ times",
                nullptr,
                iteration_count,
                &iteration_count,
                nullptr
            );
        CHKERRQ(ierr);

        ierr = PetscOptionsString(
                "-matrix_file",
                "load the input matrix from the specified file",
                nullptr,
                nullptr,
                matrix_file_chars.data(),
                matrix_file_chars.size(),
                &matrix_file_set
            );
        CHKERRQ(ierr);
    }
    ierr = PetscOptionsEnd();
    CHKERRQ(ierr);

    auto help_set = PetscBool{};
    ierr = PetscOptionsHasName(nullptr, nullptr, "-help", &help_set);
    CHKERRQ(ierr);
    if (help_set == PETSC_TRUE) {
        return 0;
    }

    auto version_set = PetscBool{};
    ierr = PetscOptionsHasName(nullptr, nullptr, "-version", &version_set);
    CHKERRQ(ierr);
    if (version_set == PETSC_TRUE) {
        return 0;
    }

    if (iteration_count <= 0) {
        throw CommandLineArgumentError{"The specified iteration count has to be greater than zero."};
    }

    if (matrix_file_set != PETSC_TRUE) {
        throw CommandLineArgumentError{"Please specify a file path using option ‘-matrix_file’."};
    }

    auto matrix_file_path = std::filesystem::path{matrix_file_chars.data()};
    if (!std::filesystem::exists(matrix_file_path)) {
        throw CommandLineArgumentError{"The specified matrix file doesn’t exist."};
    }
    if (!std::filesystem::is_regular_file(matrix_file_path)) {
        throw CommandLineArgumentError{"The specified matrix file isn’t a regular file."};
    }

    ierr = print_duration(comm, iteration_count, matrix_file_path);
    CHKERRQ(ierr);

    return 0;
}

auto print_duration(MPI_Comm const comm, PetscInt const iteration_count, std::filesystem::path const& matrix_path)
    -> PetscErrorCode
{
    auto logger = Logger{};
    auto ierr = measure_duration(comm, iteration_count, matrix_path, logger);
    CHKERRQ(ierr);

    auto duration_counts = std::array{
            std::chrono::duration<double>{logger.get_end_to_end_duration()}.count(),
            std::chrono::duration<double>{logger.get_spmv_duration()}.count(),
            std::chrono::duration<double>{logger.get_spmtv_duration()}.count(),
        };

    auto comm_rank = int{};
    ierr = MPI_Comm_rank(comm, &comm_rank);
    CHKERRMPI(ierr);

    if (comm_rank == 0) {
        ierr = MPI_Reduce(MPI_IN_PLACE, duration_counts.data(), duration_counts.size(), MPI_DOUBLE, MPI_MAX, 0, comm);
        CHKERRMPI(ierr);
        std::cout << "the run time of a single SpMMᵀV iteration in seconds:\n"
                  << duration_counts[0] << '\n'
                  << "seconds spent performing SpMV:\n"
                  << duration_counts[1] << '\n'
                  << "seconds spent performing SpMᵀV:\n"
                  << duration_counts[2] << '\n';

    } else {
        ierr = MPI_Reduce(duration_counts.data(), nullptr, duration_counts.size(), MPI_DOUBLE, MPI_MAX, 0, comm);
        CHKERRMPI(ierr);
    }

    return 0;
}

}  // namespace
