#include <exception>            // std::exception
#include <filesystem>           // std::filesystem
#include <functional>           // std::invoke
#include <iostream>             // std::cerr std::cout
#include <stdexcept>            // std::logic_error

#include <boost/program_options/errors.hpp>  // boost::program_options::error

#include <csr/matrix.hpp>       // csr::Matrix
#include <csr/symmetric.hpp>    // csr::Symmetric

#include <hsf/matrix.hpp>       // hsf::Matrix
#include <hsf/matrix_ios.hpp>   // hsf::operator<<
#include <hsf/region.hpp>       // hsf::Region
// hsf::print_morton_code hsf::print_morton_code_header hsf::print_region_header hsf::operator<<
#include <hsf/region_ios.hpp>
#include <hsf/region_ordering.hpp>  // hsf::RegionOrdering
#include <hsf/symmetric.hpp>    // hsf::Symmetric

#include <command_line_argument_error.hpp>  // CommandLineArgumentError
#include <command_line_arguments.hpp>  // CommandLineArguments
#include <element_type.hpp>     // ElementType
#include <file_type.hpp>        // FileType
#include <symmetric.hpp>        // Symmetric

namespace {

template<typename ToSymmetry, typename FromSymmetry>
constexpr auto convert_symmetry(FromSymmetry from_symmetry) noexcept -> ToSymmetry;

template<typename T, hsf::Symmetric Symm>
auto load_hsf_matrix(
        std::filesystem::path const& matrix_path,
        FileType file_type,
        double region_coefficient,
        hsf::RegionOrdering region_ordering,
        int thread_count
    ) -> hsf::Matrix<T, Symm>;

template<typename T, Symmetric Symm>
auto print_matrix_info(CommandLineArguments const& command_line_arguments) -> void;

}  // namespace

auto main(int argc, char** argv) -> int try
{
    auto const command_line_arguments = CommandLineArguments::parse(argc, argv);

    if (command_line_arguments.has_help()) {
        std::cout << command_line_arguments.get_help_message();
        return 0;
    }

    switch (command_line_arguments.get_element_type()) {
    case ElementType::float32:
        if (command_line_arguments.is_symmetric()) {
            print_matrix_info<float, Symmetric::yes>(command_line_arguments);
        } else {
            print_matrix_info<float, Symmetric::no>(command_line_arguments);
        }
        break;

    case ElementType::float64:
        if (command_line_arguments.is_symmetric()) {
            print_matrix_info<double, Symmetric::yes>(command_line_arguments);
        } else {
            print_matrix_info<double, Symmetric::no>(command_line_arguments);
        }
        break;
    }

    return 0;

} catch (boost::program_options::error const& e) {
    std::cerr << "Error: " << e.what() << ".\n";
    return 2;

} catch (CommandLineArgumentError const& e) {
    std::cerr << "Error: " << e.what() << '\n';
    return 2;

} catch (std::exception const& e) {
    std::cerr << "Error: " << e.what() << '\n';
    return 1;
}

namespace {

template<typename ToSymmetry, typename FromSymmetry>
constexpr auto convert_symmetry(FromSymmetry const from_symmetry) noexcept -> ToSymmetry
{
    switch (from_symmetry) {
    case FromSymmetry::no:
        return ToSymmetry::no;
    case FromSymmetry::yes:
        return ToSymmetry::yes;
    }
}

template<typename T, hsf::Symmetric Symm>
auto load_hsf_matrix(
        std::filesystem::path const& matrix_path,
        FileType const file_type,
        double const region_coefficient,
        hsf::RegionOrdering const region_ordering,
        int const thread_count
    ) -> hsf::Matrix<T, Symm>
{
    constexpr auto csr_symmetry = convert_symmetry<csr::Symmetric>(Symm);
    auto const csr_matrix = std::invoke([&] {
        switch (file_type) {
        case FileType::HDF5:
            return csr::Matrix<T, csr_symmetry>::load_from_hdf5(matrix_path);
        case FileType::MM:
            return csr::Matrix<T, csr_symmetry>::load_from_mm(matrix_path);
        }
        throw std::logic_error{"Unexpected matrix file type."};
    });
    return {csr_matrix.row_pointers.data(),
            csr_matrix.columns.data(),
            csr_matrix.elements.data(),
            csr_matrix.row_count(),
            csr_matrix.column_count(),
            csr_matrix.element_count(),
            region_ordering,
            region_coefficient,
            thread_count};
}

template<typename T, Symmetric Symm>
auto print_matrix_info(CommandLineArguments const& command_line_arguments) -> void
{
    constexpr auto hsf_symmetry = convert_symmetry<hsf::Symmetric>(Symm);
    auto const matrix = load_hsf_matrix<T, hsf_symmetry>(
            command_line_arguments.get_matrix_path(),
            command_line_arguments.get_file_type(),
            command_line_arguments.get_region_coefficient(),
            command_line_arguments.get_region_ordering(),
            command_line_arguments.get_thread_count()
        );

    std::cout << "matrix properties:\n"
              << matrix
              << "size in bytes: " << matrix.size_bytes() << '\n';

    if (!command_line_arguments.be_verbose()) {
        return;
    }

    std::cout << "\nregion properties:\n";
    hsf::print_region_header(std::cout) << '\n';
    for (auto const& region : matrix.regions()) {
        std::cout << region << '\n';
    }
    hsf::print_region_header(std::cout) << '\n';

    if (matrix.region_ordering() == hsf::RegionOrdering::morton) {
        std::cout << "\nMorton codes:\n";
        hsf::print_morton_code_header(std::cout) << '\n';
        for (auto const& region : matrix.regions()) {
            hsf::print_morton_code(region, std::cout) << '\n';
        }
        hsf::print_morton_code_header(std::cout) << '\n';
    }
}

}  // namespace
