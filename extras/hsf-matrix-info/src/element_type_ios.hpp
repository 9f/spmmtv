#pragma once

#include <istream>              // std::istream
#include <ostream>              // std::ostream

enum struct ElementType;

auto operator<<(std::ostream& os, ElementType element_type) -> std::ostream&;
auto operator>>(std::istream& is, ElementType& element_type) -> std::istream&;
