#include <command_line_arguments.hpp>

#include <filesystem>           // std::filesystem
#include <sstream>              // std::ostringstream
#include <string>               // std::string std::string_literals
#include <utility>              // std::move

#include <boost/program_options.hpp>  // boost::program_options

#include <omp.h>                // omp_get_max_threads omp_get_thread_limit

#include <hsf/region_ordering.hpp>  // hsf::RegionOrdering
#include <hsf/region_ordering_ios.hpp>  // hsf::operator<< hsf::operator>>

#include <command_line_argument_error.hpp>  // CommandLineArgumentError
#include <element_type.hpp>     // ElementType
#include <element_type_ios.hpp>  // operator<< operator>>
#include <file_type.hpp>        // FileType
#include <file_type_ios.hpp>    // operator<< operator>>

using namespace std::string_literals;

namespace po = boost::program_options;

auto CommandLineArguments::parse(int const argc, char const* const* const argv) -> CommandLineArguments
{
    auto positional = po::options_description{};
    positional.add_options()
        ("matrix-file", po::value<std::filesystem::path>()->required(), "Load the specified matrix.");

    auto positional_description = po::positional_options_description{};
    positional_description.add("matrix-file", 1);

    auto const element_type_description =
        "The hierarchical matrix’s element type. The recognised arguments are ‘float32’ and ‘float64’."s;
    auto const element_type_value = po::value<ElementType>()->default_value(ElementType::float64);

    auto const file_type_description =
        "The file type of the specified matrix. The recognised arguments are ‘HDF5’ and ‘MM’ (Matrix Market)."s;

    auto const region_coefficient_description =
        "The hierarchical matrix’s region coefficient. The specified number has to lie within "
        "interval [0, 1]."s;

    auto const region_ordering_description =
        "Specify the hierarchical matrix’s region ordering. The recognised arguments are:\n"
        "    block-lexicographical,\n"
        "    lexicographical,\n"
        "    morton."s;
    auto const region_ordering_value
        = po::value<hsf::RegionOrdering>()->default_value(hsf::RegionOrdering::block_lexicographical);

    auto options = po::options_description{};
    options.add_options()
        ("element-type,e", element_type_value, element_type_description.c_str())
        ("file-type,f", po::value<FileType>()->default_value(FileType::MM), file_type_description.c_str())
        ("help,h", po::bool_switch(), "Show this message and exit.")
        ("region-coefficient,c", po::value<double>()->default_value(.12), region_coefficient_description.c_str())
        ("region-ordering,o", region_ordering_value, region_ordering_description.c_str())
        ("symmetric,s", po::bool_switch(), "The passed matrix is symmetric.")
        ("thread-count,t", po::value<int>()->default_value(omp_get_max_threads()), "Set the thread count.")
        ("verbose,v", po::bool_switch(), "Be verbose.");

    auto cli = po::options_description{};
    cli.add(positional).add(options);

    auto clp = po::command_line_parser{argc, argv};
    clp.positional(positional_description).options(cli);

    auto vm = po::variables_map{};
    po::store(clp.run(), vm);

    if (vm["help"].as<bool>()) {
        auto const caption = "Usage: "s + argv[0] + " [OPTIONS] MATRIX_FILE";
        auto cli_description = po::options_description{caption};
        cli_description.add(options);

        auto oss = std::ostringstream{};
        oss << cli_description;
        return CommandLineArguments{oss.str()};
    }

    po::notify(vm);

    auto const region_coefficient = vm["region-coefficient"].as<double>();
    if (region_coefficient < 0 || region_coefficient > 1) {
        throw CommandLineArgumentError{"The region coefficient doesn’t lie within interval [0, 1]."};
    }

    auto const thread_count = vm["thread-count"].as<int>();
    if (thread_count <= 0 || thread_count > omp_get_thread_limit()) {
        throw CommandLineArgumentError{"Invalid thread count."};
    }

    return {vm["matrix-file"].as<std::filesystem::path>(),
            vm["element-type"].as<ElementType>(),
            vm["file-type"].as<FileType>(),
            region_coefficient,
            vm["region-ordering"].as<hsf::RegionOrdering>(),
            vm["symmetric"].as<bool>(),
            thread_count,
            vm["verbose"].as<bool>()};
}

auto CommandLineArguments::be_verbose() const noexcept -> bool
{
    return this->verbose_;
}

auto CommandLineArguments::has_help() const noexcept -> bool
{
    return this->help_;
}

auto CommandLineArguments::is_symmetric() const noexcept -> bool
{
    return this->symmetric_;
}

auto CommandLineArguments::get_element_type() const noexcept -> ElementType
{
    return this->element_type_;
}

auto CommandLineArguments::get_file_type() const noexcept -> FileType
{
    return this->file_type_;
}

auto CommandLineArguments::get_help_message() const noexcept -> std::string const&
{
    return this->help_message_;
}

auto CommandLineArguments::get_matrix_path() const noexcept -> std::filesystem::path const&
{
    return this->matrix_path_;
}

auto CommandLineArguments::get_region_coefficient() const noexcept -> double
{
    return this->region_coefficient_;
}

auto CommandLineArguments::get_region_ordering() const noexcept -> hsf::RegionOrdering
{
    return this->region_ordering_;
}

auto CommandLineArguments::get_thread_count() const noexcept -> int
{
    return this->thread_count_;
}

CommandLineArguments::CommandLineArguments(std::string&& help_message) noexcept
    : help_{true},
      help_message_{std::move(help_message)}
{
}

CommandLineArguments::CommandLineArguments(
        std::filesystem::path const& matrix_path,
        ElementType const element_type,
        FileType const file_type,
        double const region_coefficient,
        hsf::RegionOrdering const region_ordering,
        bool const symmetric,
        int const thread_count,
        bool const verbose
    ) : matrix_path_{matrix_path},
        element_type_{element_type},
        file_type_{file_type},
        region_coefficient_{region_coefficient},
        region_ordering_{region_ordering},
        symmetric_{symmetric},
        thread_count_{thread_count},
        verbose_{verbose}
{
}
