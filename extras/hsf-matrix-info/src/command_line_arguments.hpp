#pragma once

#include <filesystem>           // std::filesystem
#include <string>               // std::string

#include <hsf/region_ordering.hpp>  // hsf::RegionOrdering

#include <element_type.hpp>     // ElementType
#include <file_type.hpp>        // FileType

class CommandLineArguments {
public:
    static auto parse(int argc, char const* const* argv) -> CommandLineArguments;

    CommandLineArguments() = default;

    auto be_verbose() const noexcept -> bool;
    auto has_help() const noexcept -> bool;
    auto is_symmetric() const noexcept -> bool;
    auto get_element_type() const noexcept -> ElementType;
    auto get_file_type() const noexcept -> FileType;
    auto get_help_message() const noexcept -> std::string const&;
    auto get_matrix_path() const noexcept -> std::filesystem::path const&;
    auto get_region_coefficient() const noexcept -> double;
    auto get_region_ordering() const noexcept -> hsf::RegionOrdering;
    auto get_thread_count() const noexcept -> int;

private:
    // Arguments from positional command-line options.
    std::filesystem::path matrix_path_{};

    // Arguments from nonpositional command-line options.
    ElementType element_type_{};
    FileType file_type_{};
    bool help_{};
    double region_coefficient_{};
    hsf::RegionOrdering region_ordering_{};
    bool symmetric_{};
    int thread_count_{};
    bool verbose_{};

    std::string help_message_{};

    explicit CommandLineArguments(std::string&& help_message) noexcept;

    CommandLineArguments(
            std::filesystem::path const& matrix_path,
            ElementType element_type,
            FileType file_type,
            double region_coefficient,
            hsf::RegionOrdering region_ordering,
            bool symmetric,
            int thread_count,
            bool verbose
        );
};
