import sys

import click
import h5py
import numpy
import scipy.io
import scipy.sparse

from . import VERSION


@click.command()
@click.argument('input_file', type=click.Path(exists=True, dir_okay=False))
@click.option(
    '--coo-f32',
    'coo_f32_file',
    type=click.Path(dir_okay=False, writable=True),
    help='Write a matrix with float32 elements in the COO format to the specified file.'
)
@click.option(
    '--coo-f64',
    'coo_f64_file',
    type=click.Path(dir_okay=False, writable=True),
    help='Write a matrix with float64 elements in the COO format to the specified file.'
)
@click.option(
    '--coo-sym-f32',
    'coo_sym_f32_file',
    type=click.Path(dir_okay=False, writable=True),
    help='Write a symmetric matrix with float32 elements in the COO format to the specified file.'
)
@click.option(
    '--coo-sym-f64',
    'coo_sym_f64_file',
    type=click.Path(dir_okay=False, writable=True),
    help='Write a symmetric matrix with float64 elements in the COO format to the specified file.'
)
@click.option(
    '--csr-f32',
    'csr_f32_file',
    type=click.Path(dir_okay=False, writable=True),
    help='Write a matrix with float32 elements in the CSR format to the specified file.'
)
@click.option(
    '--csr-f64',
    'csr_f64_file',
    type=click.Path(dir_okay=False, writable=True),
    help='Write a matrix with float64 elements in the CSR format to the specified file.'
)
@click.option(
    '--csr-sym-f32',
    'csr_sym_f32_file',
    type=click.Path(dir_okay=False, writable=True),
    help='Write a symmetric matrix with float32 elements in the CSR format to the specified file.'
)
@click.option(
    '--csr-sym-f64',
    'csr_sym_f64_file',
    type=click.Path(dir_okay=False, writable=True),
    help='Write a symmetric matrix with float64 elements in the CSR format to the specified file.'
)
@click.help_option('-h', '--help')
@click.option('-n', '--matrix-name', help='The matrix name to store in the created files.')
@click.version_option(VERSION)
def main(input_file, coo_f32_file, coo_f64_file, coo_sym_f32_file,
         coo_sym_f64_file, csr_f32_file, csr_f64_file, csr_sym_f32_file,
         csr_sym_f64_file, matrix_name):
    """Convert a matrix stored using the Matrix Market format to a custom binary
    representation, which uses version 5 of the Hierarchical Data Format.
    """
    files = (
        coo_f32_file, coo_f64_file, coo_sym_f32_file, coo_sym_f64_file,
        csr_f32_file, csr_f64_file, csr_sym_f32_file, csr_sym_f64_file,
    )
    if all(f is None for f in files):
        click.echo('At least one output filename needs be specified.', err=True)
        sys.exit(1)

    coo_matrix = scipy.io.mmread(input_file)
    _to_row_major_order(coo_matrix)

    if coo_f32_file is not None:
        _write_coo(coo_matrix, matrix_name, numpy.float32, False, coo_f32_file)

    if coo_f64_file is not None:
        _write_coo(coo_matrix, matrix_name, numpy.float64, False, coo_f64_file)

    if csr_f32_file is not None or csr_f64_file is not None:
        csr_matrix = coo_matrix.tocsr()
    else:
        csr_matrix = None
    if all(f is None for f in (coo_sym_f32_file, coo_sym_f64_file, csr_sym_f32_file, csr_sym_f64_file)):
        del coo_matrix

    if csr_f32_file is not None:
        _write_csr(csr_matrix, matrix_name, numpy.float32, False, csr_f32_file)

    if csr_f64_file is not None:
        _write_csr(csr_matrix, matrix_name, numpy.float64, False, csr_f64_file)

    if all(f is None for f in (coo_sym_f32_file, coo_sym_f64_file, csr_sym_f32_file, csr_sym_f64_file)):
        return
    del csr_matrix
    coo_matrix = scipy.sparse.triu(coo_matrix)

    if coo_sym_f32_file is not None:
        _write_coo(coo_matrix, matrix_name, numpy.float32, True, coo_sym_f32_file)

    if coo_sym_f64_file is not None:
        _write_coo(coo_matrix, matrix_name, numpy.float64, True, coo_sym_f64_file)

    if csr_sym_f32_file is None and csr_sym_f64_file is None:
        return
    csr_matrix = coo_matrix.tocsr()
    del coo_matrix

    if csr_sym_f32_file is not None:
        _write_csr(csr_matrix, matrix_name, numpy.float32, True, csr_sym_f32_file)

    if csr_sym_f64_file is not None:
        _write_csr(csr_matrix, matrix_name, numpy.float64, True, csr_sym_f64_file)


def _to_row_major_order(coo_matrix):
    """Sort ‘coo_matrix’ by rows, then by columns."""
    indices = numpy.lexsort((coo_matrix.col, coo_matrix.row))
    coo_matrix.row = coo_matrix.row[indices]
    coo_matrix.col = coo_matrix.col[indices]
    coo_matrix.data = coo_matrix.data[indices]


def _write_coo(coo_matrix, matrix_name, element_type, symmetric, output_path):
    with h5py.File(output_path, 'w') as file:
        if matrix_name is not None:
            matrix_name_bytes = matrix_name.encode('utf-8')
            matrix_name_type = h5py.string_dtype(length=len(matrix_name_bytes))
            file.attrs['matrix name'] = numpy.array(matrix_name_bytes, dtype=matrix_name_type)

        matrix_format_bytes = 'COO'.encode('utf-8')
        matrix_format_type = h5py.string_dtype(length=len(matrix_format_bytes))
        file.attrs['matrix format'] = numpy.array(matrix_format_bytes, dtype=matrix_format_type)

        file.attrs['symmetric'] = symmetric
        file.attrs['row count'] = numpy.uint64(coo_matrix.shape[0])
        file.attrs['column count'] = numpy.uint64(coo_matrix.shape[1])
        file.attrs['element count'] = numpy.uint64(coo_matrix.nnz)

        file.create_dataset('row indices', data=coo_matrix.row, dtype=numpy.uint32)
        file.create_dataset('column indices', data=coo_matrix.col, dtype=numpy.uint32)
        file.create_dataset('element values', data=coo_matrix.data, dtype=element_type)


def _write_csr(csr_matrix, matrix_name, element_type, symmetric, output_path):
    with h5py.File(output_path, 'w') as file:
        if matrix_name is not None:
            matrix_name_bytes = matrix_name.encode('utf-8')
            matrix_name_type = h5py.string_dtype(length=len(matrix_name_bytes))
            file.attrs['matrix name'] = numpy.array(matrix_name_bytes, dtype=matrix_name_type)

        matrix_format_bytes = 'CSR'.encode('utf-8')
        matrix_format_type = h5py.string_dtype(length=len(matrix_format_bytes))
        file.attrs['matrix format'] = numpy.array(matrix_format_bytes, dtype=matrix_format_type)

        file.attrs['symmetric'] = symmetric
        file.attrs['row count'] = numpy.uint64(csr_matrix.shape[0])
        file.attrs['column count'] = numpy.uint64(csr_matrix.shape[1])
        file.attrs['element count'] = numpy.uint64(csr_matrix.nnz)

        file.create_dataset('row pointers', data=csr_matrix.indptr, dtype=numpy.uint32)
        file.create_dataset('column indices', data=csr_matrix.indices, dtype=numpy.uint32)
        file.create_dataset('element values', data=csr_matrix.data, dtype=element_type)
